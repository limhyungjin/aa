\name{derpo}
\alias{derpo}
\title{derive position table po(date, bui)}
\usage{
  derpo(driver = c("si", "me"),
    method = c("neutral", "unit", "asis"),
    te = "industry_subgroup", motab = c("pa", "ce"),
    mo = "mo", form = c("linear", "asis", "quadratic"),
    me2band = getsione(tab = "co", par = "leverband", def = 0.15) * extract("me2", "AVG(tgt)", "WHERE tgt !=0"),
    me2alphamin = getsione(tab = "co", par = "alphamin", def = 0.85),
    me2neutralband = getsione(tab = "co", par = "neutralband", def = 0.002) * extract("me2", "AVG(tgt)", "WHERE tgt !=0"),
    extendtrade = 0.1,
    lodirtyp = c("loadings1", "betaevp", "betamcp", "betamvp"),
    poshorttyp = c("evp", "mcp", "fmp1"), invert = TRUE,
    jcon = 1, poco = FALSE, dollar = FALSE, pomaxmult = -1,
    ngroups = -1, wmax = -1, wmin = wmax, r1 = -1, r2 = 2,
    noshortco = "none", region = FALSE, dirallowed = -1,
    tapoextend = TRUE, ttyp = c("gross", "vol"), minte = 1,
    maxte = 10000, ijo = NA, relever = 1, maxnet = NA,
    evolve = TRUE, liquid = NA, ...)
}
\arguments{
  \item{dirallowed}{direction of position allowed for
  noshortco countries}

  \item{extendtrade}{additional textension of trade beyond
  the 'acceptance zone' for leverage, transfer coefficient,
  neutrality; expressed as fraction of euclidian distance
  from current to model tilt}

  \item{form}{transformation from raw alpha metric: linear
  = rank, asis = no transform, quadratic = (rank deviation
  from median)^2}

  \item{ijo}{index of jobs to combine, special action where
  existing portfolios are combined}

  \item{invert}{logical flag for quadratic optimisation}

  \item{maxte}{maximum number of constituents allowed in
  te, otherwise constrained to zero}

  \item{method}{neutral = market neutral, unit = all-long}

  \item{minte}{minimum number of constituents in te,
  otherwise constrained to zero}

  \item{noshortco}{ISO codes of countries to not short}

  \item{region}{flag to constrain regions neutral}

  \item{te}{pa.te hold the categorical variable within
  which neutrality constraint applies}

  \item{ttyp}{target type, gross or vol}
}
\value{
  logical : success status - called for sideffect of
  generating SQL table
}
\description{
  derive position table po(date, bui)
}
\section{Details}{
  Position (po) has components ti=model tilt, xo=opening,
  xc=closing, xt=trade; this function (1) derives the model
  po.ti and (2) updates positions po.xo, po.xc, po.xt when
  trades are triggered
}
\author{
  Giles Heywood
}
\seealso{
  dergu

  Other deriveMethods: \code{\link{deraabworld}},
  \code{\link{deraauniv}}, \code{\link{derca}},
  \code{\link{derce}}, \code{\link{derco}},
  \code{\link{dercu}}, \code{\link{derdu}},
  \code{\link{derfi}}, \code{\link{derga}},
  \code{\link{derla}}, \code{\link{derma}},
  \code{\link{derpa}}, \code{\link{derpe}},
  \code{\link{derre}}, \code{\link{derro}},
  \code{\link{dersu}}, \code{\link{derta}},
  \code{\link{derte}}, \code{\link{derty}},
  \code{\link{derva}}, \code{\link{dervi}},
  \code{\link{newli}}
}

