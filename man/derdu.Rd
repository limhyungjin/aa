\name{derdu}
\alias{derdu}
\title{derive indexUniverse table (du)}
\usage{
  derdu(du = rbind(t(c("FR", "2008-12-31", "2009-12-31", "1")), t(c("BE", "2008-12-31", "2009-12-31", "1"))))
}
\arguments{
  \item{du}{character matrix with columns country ISO code,
  start date, end date}
}
\value{
  logical : success status - called for sideffect of
  generating SQL table
}
\description{
  derive indexUniverse table (du)
}
\section{Details}{
  Index universe (du) defines countries included in the
  universe over time
}
\author{
  Giles Heywood
}
\seealso{
  dergu

  Other deriveMethods: \code{\link{deraabworld}},
  \code{\link{deraauniv}}, \code{\link{derca}},
  \code{\link{derce}}, \code{\link{derco}},
  \code{\link{dercu}}, \code{\link{derfi}},
  \code{\link{derga}}, \code{\link{derla}},
  \code{\link{derma}}, \code{\link{derpa}},
  \code{\link{derpe}}, \code{\link{derpo}},
  \code{\link{derre}}, \code{\link{derro}},
  \code{\link{dersu}}, \code{\link{derta}},
  \code{\link{derte}}, \code{\link{derty}},
  \code{\link{derva}}, \code{\link{dervi}},
  \code{\link{newli}}
}

