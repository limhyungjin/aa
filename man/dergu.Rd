\name{dergu}
\alias{dergu}
\title{launch gui}
\usage{
  dergu(run = runall(), restart = FALSE, label = "",
    parent = c(100, 250), size = 700, visible = TRUE, ...)
}
\arguments{
  \item{run}{character vector of pagenames, default
  runall()}

  \item{label}{character added to the title bar of the gui
  window}

  \item{parent}{numeric default 100,250: window location,
  passed to gwindow}

  \item{size}{numeric default 700 passed to
  gwindow(height)}

  \item{visible}{logical default TRUE}
}
\value{
  logical : success status, also has sideffect of creating
  an object gnb in globalenv()
}
\description{
  launch gui
}
\section{Details}{
  Generates a structure to address gui objects
}

