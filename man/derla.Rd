\name{derla}
\alias{derla}
\title{derive lag tables laxx}
\usage{
  derla(wi = 230, laca = -1:1,
    lapo = c(-20, -10, -1, 0, 1, 10), lawo = -1,
    lace = -1:3, lava = lace, lacexy = 0, lami = -3:3)
}
\arguments{
  \item{lace}{the covariance lag to be used in ma, pe}

  \item{lapo}{the positiom lag to be used in va, pe}

  \item{restart}{}
}
\value{
  logical : success status - called for sideffect of
  generating SQL table
}
\description{
  derive lag tables laxx
}
\section{Details}{
  Lag (la) tables are simply persistent global variables
}
\author{
  Giles Heywood
}
\seealso{
  dergu

  Other deriveMethods: \code{\link{deraabworld}},
  \code{\link{deraauniv}}, \code{\link{derca}},
  \code{\link{derce}}, \code{\link{derco}},
  \code{\link{dercu}}, \code{\link{derdu}},
  \code{\link{derfi}}, \code{\link{derga}},
  \code{\link{derma}}, \code{\link{derpa}},
  \code{\link{derpe}}, \code{\link{derpo}},
  \code{\link{derre}}, \code{\link{derro}},
  \code{\link{dersu}}, \code{\link{derta}},
  \code{\link{derte}}, \code{\link{derty}},
  \code{\link{derva}}, \code{\link{dervi}},
  \code{\link{newli}}
}

