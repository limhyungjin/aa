\name{deraabworld}
\alias{deraabworld}
\title{derive bworld table aabworld}
\usage{
  deraabworld(y1 = 2003, y2 = 2012, month = c(2, 8),
    mytab = "aabworld",
    bti = c("bworldus", "bworldpr", "bworldeu"))
}
\arguments{
  \item{y1}{start year}

  \item{y2}{end year}

  \item{month}{month numbers in year}

  \item{mytab}{output table name}

  \item{bti}{ticker}
}
\value{
  none : called for sideffect
}
\description{
  derive bworld table aabworld
}
\section{Details}{
  option to use identifiers from aabworld
}
\author{
  Giles Heywood
}
\seealso{
  Other deriveMethods: \code{\link{deraauniv}},
  \code{\link{derca}}, \code{\link{derce}},
  \code{\link{derco}}, \code{\link{dercu}},
  \code{\link{derdu}}, \code{\link{derfi}},
  \code{\link{derga}}, \code{\link{derla}},
  \code{\link{derma}}, \code{\link{derpa}},
  \code{\link{derpe}}, \code{\link{derpo}},
  \code{\link{derre}}, \code{\link{derro}},
  \code{\link{dersu}}, \code{\link{derta}},
  \code{\link{derte}}, \code{\link{derty}},
  \code{\link{derva}}, \code{\link{dervi}},
  \code{\link{newli}}
}

