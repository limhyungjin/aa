\name{derre}
\alias{derre}
\title{derive return table re(date, bui)}
\usage{
  derre(mnem = c("re", "yi"), from = c("prem", "yi"),
    rece = FALSE, qupanel = "mcapl1", daily = FALSE,
    para = TRUE, fresh = TRUE, updatefinal = TRUE)
}
\arguments{
  \item{from}{a two-length vector with elements
  [1]return-like panel mnemonic [2] (optional) the value
  'yi', flagging pa.yi for decomposition also}

  \item{qupanel}{mnemonics of panels to be used for
  quantile attribution}

  \item{rece}{logical flag for additional decompositions:
  variance and return by factor index (table rece), and
  score total/systematic variance (table fa)}
}
\value{
  logical: status
}
\description{
  derive return table re(date, bui)
}
\section{Details}{
  Return (re) and yield are decomposed into market,
  systematic, and residual components
}
\author{
  Giles Heywood
}
\seealso{
  dergu

  Other deriveMethods: \code{\link{deraabworld}},
  \code{\link{deraauniv}}, \code{\link{derca}},
  \code{\link{derce}}, \code{\link{derco}},
  \code{\link{dercu}}, \code{\link{derdu}},
  \code{\link{derfi}}, \code{\link{derga}},
  \code{\link{derla}}, \code{\link{derma}},
  \code{\link{derpa}}, \code{\link{derpe}},
  \code{\link{derpo}}, \code{\link{derro}},
  \code{\link{dersu}}, \code{\link{derta}},
  \code{\link{derte}}, \code{\link{derty}},
  \code{\link{derva}}, \code{\link{dervi}},
  \code{\link{newli}}
}

