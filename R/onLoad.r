#.onLoad
.onLoad <- function(libname,pkgname) {
  options(warn=1)
  options("stringsAsFactors"=FALSE)
  require(RODBC)
  bloomberg <<- DBcon <<- odbcConnect("bloomberg")
  aa <- aadb()
  for(i in seq_along(aa)) {
    assign(aa[i],value=odbcConnect(aa[i]),env=globalenv())
  }
}
