dergu <-
function(run=runall(),restart=FALSE,label="",parent=c(100,250),size=700,visible=TRUE,...) 
{
  options("guiToolkit"="RGtk2")
  #notebooks
  gnb <<- list(universe=NULL,lib=NULL,parameter=NULL,run=NULL,report=NULL,archive=NULL,edit=NULL,nb=gnotebook())
  #addHandlerDestroy(obj=gnb$nb,handler=function(h,...){rm(gnb,envir=.GlobalEnv)})
  gnb$universe <<- list(index=NULL,constituent=NULL,security=NULL,nb=gnotebook())
      gnb$universe$constituent <<- list(tree=NULL,editor=NULL,table=NULL,nb=gnotebook())
      gnb$universe$index <<- listof2(use.scroll=TRUE)  #
          gnb$universe$constituent$tree <<- listof2()
          gnb$universe$constituent$editor <<- listof2()
          gnb$universe$constituent$table <<- listof2() ##
      gnb$universe$security <<- listof2()
          gnb$universe$security$table <<- listof2()
          gnb$universe$security$graphics <<- listof2()
  gnb$lib <<- listof2(horizontal=TRUE)
  gnb$parameter <<- listof2()
  gnb$run <<- list(job=NULL,log=NULL,job=NULL,queue=NULL,nb=gnotebook())
      gnb$run$current <<- listof2(use.scroll=TRUE)
      gnb$run$log <<- listof2(use.scroll=TRUE)
      gnb$run$job <<- listof2(use.scroll=TRUE)
      gnb$run$job$tab <<- ""
      gnb$run$queue <<- listof2()
      #gnb$run$meta <<- listof2()
  gnb$archive <<- listof2(horizontal=TRUE)
  gnb$report <<- list(rank=NULL,aggregate=NULL,summary=NULL,parameter=NULL,nb=gnotebook())
      gnb$report$rank <<- listof2(horiz=TRUE,use.scroll=TRUE)
          gnb$report$rank$bottom <<- ggroup(horizontal=TRUE)
          gnb$report$rank$graphics <<- listof2()
      gnb$report$aggregate <<- listof2(horiz=TRUE,use.scroll=TRUE)
          gnb$report$aggregate$bottom <<- ggroup(horizontal=TRUE)
          gnb$report$aggregate$graphics <<- listof2()
      gnb$report$summary <<- listof2(horiz=TRUE,use.scroll=TRUE)
          gnb$report$summary$bottom <<- ggroup(horizontal=TRUE)
          gnb$report$summary$graphics <<- listof2()
      #gnb$report$meta <<- listof2(horiz=TRUE,use.scroll=TRUE)
      #    gnb$report$meta$bottom <<- ggroup(horizontal=TRUE)
      #    gnb$report$meta$graphics <<- listof2()
      #gnb$report$beta <<- listof2(horiz=TRUE,use.scroll=TRUE)
      #    gnb$report$beta$bottom <<- ggroup(horizontal=TRUE)
      #    gnb$report$beta$graphics <<- listof2()
      gnb$report$panel <<- listof2(horiz=TRUE,use.scroll=TRUE)
          gnb$report$panel$bottomleft <<- listof2()
          gnb$report$panel$bottomright <<- listof2()
          gnb$report$panel$graphics <<- listof2()
      #gnb$report$performance <<- listof2(horiz=TRUE,use.scroll=FALSE)
      #    gnb$report$performance$upperleft <<- listof2()
      #    gnb$report$performance$upperright <<- listof2()
      #    gnb$report$performance$upperleft$perm <<- gframe()
      #    gnb$report$performance$upperright$perm <<- gframe()
      #gnb$report$fundamental <<- listof2()
      #gnb$report$parameter <<- listof2(horizontal=TRUE)  #commented out repa because slow - 1 of 4
  gnb$edit <<- listof2(horizontal=TRUE)
  gnb$present <<-list(perm=ggroup(horizontal=FALSE),temp=ggroup(horizontal=TRUE),nb=gnotebook())
      gnb$present$universe <<-listof2(horizontal=TRUE)
      gnb$present$covariance <<-listof2(horizontal=TRUE)
      gnb$present$attribution <<-listof2(horizontal=TRUE)
      gnb$present$alpha <<-listof2(horizontal=TRUE)
      gnb$present$positions <<-listof2(horizontal=TRUE)
      gnb$present$performance <<-listof2(horizontal=TRUE)
      gnb$present$implementation <<-listof2(horizontal=TRUE)
  #compound pages (reports) : toolbars only
  uncota2(cont=gnb$universe$constituent$table$perm)
  unse2(cont=gnb$universe$security$perm)
  reag2(cont=gnb$report$aggregate$perm)
  resu2(cont=gnb$report$summary$perm)
  #rera2(cont=gnb$report$rank$perm)
  #reme2(cont=gnb$report$meta$perm)
  #rebe2(cont=gnb$report$beta$perm)
  rene2(cont=gnb$report$panel$perm)
  #repa2(cont=gnb$report$parameter$perm) #commented out repa because slow - 2 of 4
  dita2(cont=gnb$edit$perm)
  #rere2(cont=gnb$present$perm)
  #repe2(cont=gnb$report$performance$perm)
   #adds to notebooks
  add(gnb$nb,gnb$universe$nb,label="universe")
      add(gnb$universe$nb,gnb$universe$index$perm,label="index")  
      add(gnb$universe$nb,gnb$universe$constituent$nb,label="constituent")
      add(gnb$universe$nb,gnb$universe$security$perm,label="security")
          add(gnb$universe$constituent$nb,gnb$universe$constituent$tree$perm,label="tree")
          add(gnb$universe$constituent$nb,gnb$universe$constituent$editor$perm,label="editor")
          add(gnb$universe$constituent$nb,gnb$universe$constituent$table$perm,label="table")  
  add(gnb$nb,gnb$lib$perm,label="library")
  add(gnb$nb,gnb$parameter$perm,label="parameter")
  add(gnb$nb,gnb$run$nb,label="run")
      add(gnb$run$nb,gnb$run$current$perm,label="current")
      add(gnb$run$nb,gnb$run$log$perm,label="log")
      add(gnb$run$nb,gnb$run$job$perm,label="job")
      add(gnb$run$nb,gnb$run$queue$perm,label="queue")
      #add(gnb$run$nb,gnb$run$meta$perm,label="meta")
  add(gnb$nb,gnb$archive$perm,label="archive")
  #add(gnb$report$nb,gnb$report$parameter$perm,label="parameter")  #3 of 4
  add(gnb$nb,gnb$report$nb,expand=TRUE,label="report")
      #add(gnb$report$nb,gnb$report$rank$perm,label="rank",expand=TRUE)
      add(gnb$report$nb,gnb$report$aggregate$perm,label="aggregate",expand=TRUE)
      add(gnb$report$nb,gnb$report$summary$perm,label="summary")
      #add(gnb$report$nb,gnb$report$meta$perm,label="meta")
      #add(gnb$report$nb,gnb$report$beta$perm,label="beta")
      add(gnb$report$nb,gnb$report$panel$perm,label="panel")
      #add(gnb$report$nb,gnb$report$performance$perm,label="performance")
      #add(gnb$report$nb,gnb$report$fundamental$perm,label="fundamental")
  add(gnb$nb,gnb$edit$perm,label="edit")
  #add(gnb$nb,gnb$present$perm,label="present")        #this is a slight departure: the nested notebook is in a permanent container
  #add(gnb$present$perm,gnb$present$nb,expand=TRUE)
  #    add(gnb$present$nb,gnb$present$universe$perm,label="universe")
  #    add(gnb$present$nb,gnb$present$covariance$perm,label="covariance")
  #    add(gnb$present$nb,gnb$present$attribution$perm,label="attribution")
  #    add(gnb$present$nb,gnb$present$alpha$perm,label="alpha")
  #    add(gnb$present$nb,gnb$present$positions$perm,label="positions")
  #    add(gnb$present$nb,gnb$present$performance$perm,label="performance")
  #    add(gnb$present$nb,gnb$present$implementation$perm,label="implementation")
  gnb$win <<- gwindow(title=psz("Aa ",label),parent=parent,width=size*1.618,height=size,visible=FALSE)
  add(gnb$win,gnb$nb)
  #svalue(gnb$nb)<-1
  #svalue(gnb$universe$nb)<-1
  #svalue(gnb$universe$constituent$nb)<-1
  #svalue(gnb$run$nb)<-1
  svalue(gnb$nb)<<-4
  svalue(gnb$run$nb)<<-4
  on.exit({
    enablegu(TRUE)
    if(restart) {tryautorestart()}
  })
  enablegu(FALSE)
  for (i in seq_along(run)) {
    print(run[i])
    print(system.time(do.call(run[i],list())))
    if(i==1) {
      visible(gnb$win) <<- visible
    }
  }
  visible(gnb$win) <<- visible
  return(TRUE)
}
runall <-
function() 
{
#c("unin1","uncoed1","uncotr1","papa1","rera1","reag1","resu1","reme1","rebe1","rulo1","arar1","rucu1","rujo1","ruqu1","rume1","dita1","lili1")
c(
  "unin1","uncoed1","uncotr1",#universe
  "papa1",#parameter
  "reag1","resu1",#"repe1","refu1","rera1","reme1","rebe1",#report
  "rulo1",#run
  "arar1",#archive
  "rucu1","rujo1","ruqu1",#"rume1",#run
  "dita1",#edit
  "lili1")#library
#c("unin","uncota","uncoed","uncotr","unse","papa","rera","reag","resu","repa","rulo","arar","rucu","rujo","ruqu","dita","lili") #4 of 4
}
listof2 <-
function(horizontal=FALSE,...) 
{
  list(perm=ggroup(horizontal=FALSE),temp=ggroup(horizontal=horizontal,...))
}
gudepend <-
function(xx=allvexx()) 
{
    #xx <- match.arg(xx)
  unseupdate(clear=TRUE,disable=FALSE)
  uncotaupdate(clear=TRUE,disable=FALSE)
  uncotrupdate(clear=TRUE,disable=FALSE)
  uncoedupdate(clear=TRUE,disable=FALSE)
  uninupdate(clear=TRUE,disable=FALSE)
  papaupdate(clear=TRUE,disable=FALSE)
  liliupdate(clear=TRUE,disable=FALSE)
  reagupdate(clear=TRUE,disable=FALSE)
  resuupdate(clear=TRUE,disable=FALSE)
  ruloupdate(clear=TRUE,disable=FALSE)
  if("su"%in%xx) unseupdate(clear=FALSE,disable=FALSE)
  if("si"%in%xx) papaupdate(clear=FALSE,disable=FALSE)
  if("li"%in%xx) liliupdate(clear=FALSE,disable=FALSE)
  #if("ra"%in%xx) reraupdate(clear=FALSE,disable=FALSE)
  if("ga"%in%xx) reagupdate(clear=FALSE,disable=FALSE)
  if("ma"%in%xx) resuupdate(clear=FALSE,disable=FALSE)
  if("lo"%in%xx) ruloupdate(clear=FALSE,disable=FALSE)
}
existsgu <-
function() 
{
  exists("gnb")
}
enablegu <-
function(
                enable=TRUE
                ) 
{
  rapply(object=gnb,f=get("enabled<-"),value=enable,class="gGroup")
  enabled(gnb$nb) <- TRUE
  enabled(gnb$universe$nb) <- TRUE
  enabled(gnb$universe$constituent$nb) <- TRUE
  enabled(gnb$run$nb) <- TRUE
  enabled(gnb$report$nb) <- TRUE
  if(existsresu())  resudisable()
}
confirmDialog <-
function(
                    message="do X?",
                    todo=print("X")
                    ) 
{
  delayedAssign('promise',todo)
  mywin = gwindow("Confirm",width=230,height=80,visible=FALSE)
  group = ggroup(container = mywin)
  gimage("info", dirname="stock", size="dialog", container=group)
  inner.group = ggroup(horizontal=FALSE, container = group)
  glabel(message, container=inner.group, expand=TRUE)
  button.group = ggroup(container = inner.group)
  addSpring(button.group)
  gbutton("ok", handler=function(h,...){dispose(mywin);done<-promise}, container=button.group)
  gbutton("cancel", handler = function(h,...){dispose(mywin)},container=button.group)
  visible(mywin) <- TRUE
}
updateinnerrightgroup <-
function(...) 
{
    delete(obj=gnb$archive$gararinnerrightgroup,widg=gnb$archive$garartext)
        gnb$archive$garartext <<- gedit(switch(garar(),'li'=lisummarystring(),'si'=sisummarystring(),'su'=susummarystring()),'')
    add(obj=gnb$archive$gararinnerrightgroup,value=gnb$archive$garartext)
    delete(obj=gnb$archive$gararinnerrightgroup,widg=gnb$archive$garartab)
        gnb$archive$garartab <<- gtable(garartab(),handler=doubleClickTableF)
    add(obj=gnb$archive$gararinnerrightgroup,value=gnb$archive$garartab,expand=TRUE)
    svalue(gnb$archive$garartab) <<- 1#nrow(gnb$archive$garartab)
}
larartab <-
function(channel=chve()) {gettab(larardomain(),ret="data",qual="ORDER BY ixx DESC",channel=chve())}
larardomain <-
function(...) {psz("ve",allvexx()[svalue(gnb$archive$gararradio,index=TRUE)],"di")}
ghararrestore <-
function(h,...) 
{
    on.exit({enablegu(TRUE);pt("...archive restored")})
    enablegu(FALSE)
    pt("restoring archive...")
    resve(
        xx=garar(),
        ixx=as.numeric(svalue(gnb$archive$garartab)),
        updategu=FALSE
        )
}
ghararpromise <-
function()
    { 
        on.exit({enablegu(TRUE)})
        enablegu(FALSE)
        delve(
            xx=garar(),
            ixx=as.numeric(svalue(gnb$archive$garartab))
            )
        updateinnerrightgroup()
        rujoupdate(disable=FALSE)
        rumeupdate(disable=FALSE)
    }
gharardelete <-
function(h,...) 
{
    if(gnb$archive$garartab[as.numeric(svalue(gnb$archive$garartab,index=TRUE)),]$status=="lock") { #request confirm
        confirmDialog(
                    message="locked archive : delete?",
                    todo=ghararpromise()
                    )
    } else {    #just do it
        ghararpromise()
    }
}
ghararcleanup <-
function(h,...) 
{
    cleanup <- function()
    {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
        cleanve(xx=garar())
        updateinnerrightgroup()
        rujoupdate()
    }
    confirmDialog(message="cleanup PERMANENTLY DELETES unassociated output archives - continue?",todo=cleanup()) 
}
gharararchive <-
function(h,...) 
{
    on.exit({enablegu(enable=TRUE)})   
    enablegu(enable=FALSE)
    arcve(
        xx=garar(),
        mycomment=svalue(gnb$archive$garartext)
        )
    updateinnerrightgroup()
    rujoupdate(disable=FALSE)
    rumeupdate(disable=FALSE)
}
garartab <-
function(channel=chve()) {gettab(garardi(),ret="data",qual="ORDER BY ixx DESC",channel=chve())}
garardi <-
function() 
{
    psz("ve",garar(),"di")
}
garar <-
function() 
{
    switch(svalue(gnb$archive$gararradio),security="su",lib="li",parameter="si",control="co",covariance="ce",return="re",portfolio="po",rank="ra",aggregate="ga",summary="ma",log="lo",factor="fa",meta="me",beta="be",timeseries="ti",constituent="cu",number="nu",folio="fo",regression="ge")
}
doubleClickTableF <-
function(h,...) 
{
    xx <- garar()
    if(xx=="su") {
        tab <- sumsu(getve(xx=xx,ixx=svalue(h$obj)))
        gdf(tab,cont=gwindow())
    } else {
        tab <- getve(xx=xx,ixx=svalue(h$obj),limit="LIMIT 2000")
        gdf(tab,cont=gwindow("preview: limit 2000"))
    }
}
ararupdate <-
function(...,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$archive$perm,widget=gnb$archive$temp)
        gnb$archive$temp <<- ggroup(use.scroll=TRUE,horiz=TRUE)
    add(gnb$archive$perm,arar3(gnb$archive$temp),expand=TRUE)
}
arar3 <-
function(container) 
{
        innerleft <- ggroup(horiz=FALSE)
            gnb$archive$gararradio <<- gradio(items=c("security","lib","parameter","control","covariance","return","portfolio","rank","aggregate","summary","log","factor","meta","beta","timeseries","constituent","number","folio","regression"),selected=1) #must be in same order as 
                addhandlerchanged(obj=gnb$archive$gararradio,handler=function(...) {updateinnerrightgroup()}) #lazy evaluation makes this work
        add(innerleft,gnb$archive$gararradio,expand=FALSE)
    add(container,innerleft,expand=FALSE)
        gnb$archive$gararinnerrightgroup <<- ggroup(horiz=FALSE)
            gnb$archive$garartext <<- gedit(susummarystring())
        add(obj=gnb$archive$gararinnerrightgroup,value=gnb$archive$garartext)
            gnb$archive$garartab <<- gtable(larartab(),handler=doubleClickTableF)
        add(obj=gnb$archive$gararinnerrightgroup,value=gnb$archive$garartab,expand=TRUE)
    add(container,gnb$archive$gararinnerrightgroup,expand=TRUE)
    container
}
arar2 <-
function(container) 
{
    tbl <- list(
        restore=gaction(    icon="open",     label="restore",   handler = ghararrestore),
        delete=gaction(     icon="delete",      label="delete",    handler = gharardelete),
        archive=gaction(       icon="floppy",        label="archive",      handler = gharararchive),
        cleanup=gaction(       icon="cancel",        label="cleanup",      handler = ghararcleanup)
        )
    add(container,gtoolbar(tbl))
    container
}
arar1 <-
function(...) 
{
    arar2(cont=gnb$archive$perm)                                #perm <- toolbar
    add(gnb$archive$perm,arar3(gnb$archive$temp),expand=TRUE)   #perm <- temp <- content
}
refresheditfields <-
function()   
{
    gfpanel <- gframe("panel options",horiz=TRUE)       #panel secton - gnb$edit$panelfields
    g1 <- ggroup(horiz=TRUE)
    g1f <- gframe("panel")              #panel yes/no
    g2 <- ggroup(,horiz=FALSE)
    add(g2,g1f,expand=FALSE)
    add(gfpanel,g2,expand=FALSE)
    add(g1f,gnb$edit$w5,expand=FALSE)
    g1f <- gframe("row field")          #row
    g2 <- ggroup(,horiz=FALSE)
    add(g2,g1f,expand=FALSE)
    add(gfpanel,g2,expand=FALSE)
    add(g1f,gnb$edit$w6,expand=FALSE)
    g1f <- gframe("col field")          #col
    g2 <- ggroup(,horiz=FALSE)
    add(g2,g1f,expand=FALSE)
    add(gfpanel,g2,expand=FALSE)
    add(g1f,gnb$edit$w7,expand=FALSE)
    g1f <- gframe("value field")        #value
    g2 <- ggroup(,horiz=FALSE)
    add(g2,g1f,expand=FALSE)
    add(gfpanel,g2,expand=FALSE)
    add(g1f,gnb$edit$w9,expand=FALSE)
    g1f <- gframe("mode")               #mode
    g2 <- ggroup(,horiz=FALSE)
    add(g2,g1f,expand=FALSE)
    add(gfpanel,g2,expand=FALSE)
    add(g1f,gnb$edit$w8,expand=FALSE)
    gnb$edit$panelfields <<- ggroup(horiz=TRUE)
    add(gnb$edit$panelfields,gfpanel,expand=FALSE)   #end of panel section
    gnb$edit$fields <<- ggroup(horiz=TRUE)          #edit section
    g1f <- gframe("fields")             #fields
    g2 <- ggroup(,horiz=FALSE)
    add(g2,g1f,expand=FALSE)
    add(gnb$edit$fields,g2,expand=FALSE)
    add(g1f,gnb$edit$w2,expand=FALSE)
    g1f <- gframe("key")                #key
    g2 <- ggroup(,horiz=FALSE)
    add(g2,g1f,expand=FALSE)
    add(gnb$edit$fields,g2,expand=FALSE)
    add(g1f,gnb$edit$w3,expand=FALSE)

}
lhzooplot <-
function(...) 
{
    ggraphics(container=gwindow(svalue(gnb$edit$w1)))
    mat <- t(as.matrix(gnb$edit$rhs$rawtable[svalue(gnb$edit$rhs$table,index=TRUE),,drop=F]))
    mode(mat) <- "numeric"
    if(svalue(gnb$edit$w10)) mat[!is.na(mat)] <- cumsum(mat[!is.na(mat)]) #cumulative
    plot.zoo(mz(x),xlab="",ylab="")
}
lhqqplot <-
function(...) 
{
    ggraphics(container=gwindow(svalue(gnb$edit$w1)))
    if(svalue(gnb$edit$w11)) 
        {mat <- t(as.matrix(gnb$edit$rhs$rawtable[]))} else    #pooled
        {mat <- t(as.matrix(gnb$edit$rhs$rawtable[svalue(gnb$edit$rhs$table,index=TRUE),,drop=F]))}
    mode(mat) <- "numeric"
    qqnorm(mat)
}
lhhistplot <-
function(...) 
{
    ggraphics(container=gwindow(svalue(gnb$edit$w1)))
    if(svalue(gnb$edit$w11)) 
        {mat <- t(as.matrix(gnb$edit$rhs$rawtable[]))} else    #pooled
        {mat <- t(as.matrix(gnb$edit$rhs$rawtable[svalue(gnb$edit$rhs$table,index=TRUE),,drop=F]))}
    mode(mat) <- "numeric"
    hist(mat,xlab="",ylab="",main="")
}
lhdita1 <-
function(...)  
{  
    delete(gnb$edit$lhs$temp,gnb$edit$panelfields)
    delete(gnb$edit$lhs$temp,gnb$edit$fields)
    dowidgets()
    refresheditfields()
    add(gnb$edit$lhs$temp,gnb$edit$panelfields)
    add(gnb$edit$lhs$temp,gnb$edit$fields)
    ghdita1()
}
lhbarplot <-
function(...) 
{
    ggraphics(container=gwindow(svalue(gnb$edit$w1)))
    mat <- as.matrix(gnb$edit$rhs$rawtable[svalue(gnb$edit$rhs$table,index=TRUE),,drop=F])
    mode(mat) <- "numeric"
    barplot(mat,xlab="",ylab="")
}
ghdita1 <-
function(h,...) 
{
    gditatabUpdate()
    delete(obj=gnb$edit$rhs$perm,widget=gnb$edit$rhs$temp)
    gnb$edit$rhs$temp <<- ggroup(use.scroll=TRUE,horiz=FALSE)
    add(gnb$edit$rhs$temp,gnb$edit$rhs$table,expand=TRUE)
    add(obj=gnb$edit$rhs$perm,value=gnb$edit$rhs$temp,expand=TRUE)
}
gditatabUpdate <-
function(...) {  #updates tab
    if(svalue(gnb$edit$w5)){            #panel
        gnb$edit$rhs$rawtable <<- as.data.frame(
                    getpa(field=svalue(gnb$edit$w9),
                        tab=svalue(gnb$edit$w1),
                        ifield=svalue(gnb$edit$w6),
                        jfield=svalue(gnb$edit$w7),
                        modeout=svalue(gnb$edit$w8),
                        classout="matrix",
                        qual=svalue(gnb$edit$w4)
                        )
                    )
    } else {                            #table
        tmp <<- extract(          #nb the rawtable is a dataframe/matrix and not a gwidget object, it is needed for popup plots
                    tabname=svalue(gnb$edit$w1),
                    field=union(svalue(gnb$edit$w2),svalue(gnb$edit$w3)),
                    qualifier=svalue(gnb$edit$w4),
                    result=as.data.frame)
        if(svalue(gnb$edit$w12)) gnb$edit$rhs$rawtable <<- t(tmp) else gnb$edit$rhs$rawtable <<- tmp
    }
    slim <- gnb$edit$rhs$rawtable[,1:min(100,ncol(gnb$edit$rhs$rawtable))]    #performance suffers badly for wider tables, but need all of it for popup graphics
    gnb$edit$rhs$table <<- gdf(slim,do.subset=FALSE) 
    lst <- list()
    lst$barplot$handler <- lhbarplot
    lst$zooplot$handler <- lhzooplot
    lst$histogram$handler <- lhhistplot
    lst$qqplot$handler <- lhqqplot
    add3rdmousepopupmenu(gnb$edit$rhs$table, lst)
}
dowidgets <-
function()   #
{
    gnb$edit$w2 <<- gcheckboxgroup(dirfld(svalue(gnb$edit$w1)),checked=TRUE)    #fields
    key <- keyfields(svalue(gnb$edit$w1))       #key
    gnb$edit$w3 <<- gcheckboxgroup(dirfld(svalue(gnb$edit$w1)),checked=(dirfld(svalue(gnb$edit$w1))%in%key))
    gnb$edit$w5 <<- gcheckbox("panel")          #is.panel
    gnb$edit$w6 <<- gcombobox(dirfld(svalue(gnb$edit$w1))) #rowfield
    gnb$edit$w7 <<- gcombobox(dirfld(svalue(gnb$edit$w1))) #colfield
    gnb$edit$w9 <<- gcombobox(dirfld(svalue(gnb$edit$w1))) #valuefield
    gnb$edit$w8 <<- gradio(c("numeric","character"))       #mode
    nfields <- length(dirfld(svalue(gnb$edit$w1)))
    svalue(gnb$edit$w6) <<- c(TRUE,rep(FALSE,nfields-1))
    svalue(gnb$edit$w7) <<- c(FALSE,TRUE,rep(FALSE,max(0,nfields-2)))
    svalue(gnb$edit$w8) <<- "numeric"
    svalue(gnb$edit$w9) <<- dirfld(svalue(gnb$edit$w1))[1]
}
dita3 <-
function(container) 
{
    gnb$edit$w1 <<- gcombobox(dirtab())     #table
    gnb$edit$w4 <<- gedit("")               #qualifier
    svalue(gnb$edit$w1) <<- smalltab()
    gnb$edit$w10 <<- gcheckbox("cumul") 
    gnb$edit$w11 <<- gcheckbox("pool")
    gnb$edit$w12 <<- gcheckbox("t(x)")
    dowidgets()                             #the refreshable widgets
    g1 <- ggroup(horiz=TRUE)
        g1f <- gframe("table")                  #table
        g2 <- ggroup(,horiz=FALSE)
        add(g2,g1f,expand=FALSE)
        add(g1,g2,expand=FALSE)
        add(g1f,gnb$edit$w1,expand=FALSE)
        g1f <- gframe("qualifier")              #qualifier
        g2 <- ggroup(,horiz=FALSE)
        add(g2,g1f,expand=FALSE)
        add(g1,g2,expand=FALSE)
        add(g1f,gnb$edit$w4,expand=FALSE)
        add(container,g1,expand=FALSE)
        g1f <- gframe("plot",horiz=TRUE)        #plot
        g2 <- ggroup(,horiz=FALSE)
        add(g2,g1f,expand=FALSE)
        add(g1,g2,expand=FALSE)
        add(g1f,gnb$edit$w10,expand=FALSE)
        add(g1f,gnb$edit$w11,expand=FALSE)
        g1f <- gframe("transpose")                #transpose
        g2 <- ggroup(,horiz=FALSE)
        add(g2,g1f,expand=FALSE)
        add(g1,g2,expand=FALSE)
        add(g1f,gnb$edit$w12,expand=FALSE)
    add(container,g1,expand=FALSE)
    refresheditfields()                     #updates the two containers
    add(container,gnb$edit$panelfields,expand=FALSE)
    add(container,gnb$edit$fields,expand=FALSE)
    addhandlerchanged(obj=gnb$edit$w1,handler=lhdita1)  #table change triggers refresh
    addhandlerchanged(obj=gnb$edit$w4,handler=ghdita1)  #qualifier change triggers refresh
    gditatabUpdate()
    add(gnb$edit$rhs$temp,gnb$edit$rhs$table,expand=TRUE)
    container
}
dita2 <-
function(container) 
{
    tbl <- list(
        read=gaction(    icon="open",        label="read",   handler = function(...) {ghdita1()} ),
        refresh=gaction(    icon="refresh",     label="refresh",   handler = dita1)
        )
    add(container,gtoolbar(tbl))
    container
}
dita1 <-
function(...,clear=FALSE) 
{
    delete(gnb$edit$perm,gnb$edit$temp)
    gnb$edit$temp <<- ggroup(horiz=TRUE,use.scroll=TRUE)
    gnb$edit$lhs <<- listof2(use.scroll=TRUE)
    gnb$edit$rhs <<- listof2(use.scroll=TRUE)
    add(gnb$edit$perm,gnb$edit$temp,expand=TRUE)
    add(gnb$edit$rhs$perm,gnb$edit$rhs$temp,expand=TRUE)
    gpg <- gpanedgroup(widget2=gnb$edit$rhs$perm,widget1=gnb$edit$lhs$perm)
    gpg@widget@widget["position"] <- getsione("panecurtainpx")*.5
    add(gnb$edit$temp,gpg,expand=TRUE)
    if(clear) {
        add(gnb$edit$lhs$perm,gnb$edit$lhs$temp,expand=TRUE)
    } else {
        add(gnb$edit$lhs$perm,dita3(gnb$edit$lhs$temp),expand=TRUE) #perm <- temp <- content
    }
}
ruquupdate <-
function(...,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$run$queue$perm,widget=gnb$run$queue$temp)
    gnb$run$queue$temp <<- ggroup(use.scroll=TRUE,horiz=FALSE)
    add(gnb$run$queue$perm,ruqu3(gnb$run$queue$temp),expand=TRUE)
    ruquselect()
}
ruquselect <-
function() 
{
    tab <- extract("qu","ijo","ORDER BY ijo DESC LIMIT 1")
    if(length(tab)>0) {
        ijo=max(tab)
        svalue(gnb$run$queue$tab) <<- ijo
    }
}
ruqu3 <-
function(container) 
{
    #derqu()
    tab <- gettab("qu",qual="ORDER BY ijo")
    gnb$run$queue$tab <<- gtable(tab,chosencol=which(colnames(tab)=="ijo"))
    add(container,gnb$run$queue$tab,expand=TRUE)
    ruquselect()
    container
}
ruqu2 <-
function(container) 
{
    tbl <- list(
        derive=gaction(    icon="symbol_plus",     label="derive",   handler = ghruquupdate),
        derivelocal=gaction(    icon="symbol_minus",     label="derivelocal",   handler = ghruquupdatecurrentijo),
        refresh=gaction(    icon="refresh",     label="refresh",   handler = ghruqurefresh),
        delete=gaction(     icon="delete",      label="delete",    handler = ghruqudelete),
        start=gaction(       icon="go-forward",        label="start",      handler = ghruqustart)
        )
    add(container,gtoolbar(tbl,style="both"))
    container
}
ruqu1 <-
function(...) 
{
    ruqu2(cont=gnb$run$queue$perm)                         #perm <- toolbar
    if(sqlnotempty("qu")) add(gnb$run$queue$perm,ruqu3(gnb$run$queue$temp),expand=TRUE) #perm <- temp <- content
}
ghruquupdatecurrentijo <-
function(h,...) 
{
    derqu(ijoselect=as.numeric(extract("qu","DISTINCT ijo","WHERE status IN('start','todo')")))
    ruquupdate()
}
ghruquupdate <-
function(h,...) 
{
    derqu()
    ruquupdate()
}
ghruqustart <-
function(h,...) 
{
    rucuupdate()
    startqu(irestart=TRUE,disable=TRUE)
}
ghruqurefresh <-
function(h,...) 
{
    ruquupdate()
}
ghruqudelete <-
function(h,...) 
{
    delqu(ijo=svalue(gnb$run$queue$tab))
    ruquupdate()
}
rujoupdate <-
function(...,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$run$job$perm,widget=gnb$run$job$temp)
    gnb$run$job$temp <<- ggroup(use.scroll=TRUE,horiz=FALSE)
    add(gnb$run$job$perm,rujo3(gnb$run$job$temp),expand=TRUE)
    rujoselect(getijo()-1)
    rujoselect(getijo())
}
rujoselect <-
function(ijo=getijo())
{
    svalue(gnb$run$job$gsb[[1]]) <<- ijo
}
rujo3handler <-
function(h,...) {rujoselect(svalue(h$obj))}
rujo3 <-
function(container) 
{
    if(!chkjo()) return({add(container,gbutton("job table integrity check failed"));container})
    gnb$run$job$layout <<- glayout()
#    gnb$run$job$mol <<- moldeclare(6,6)    
    gnb$run$job$mol <<- moldeclare(12,6)    
    xx <- c("jo",inpxx(),intxx())
#    desc <- c("job","security","lib","parameter","aggregate","summary","log")
    desc <- c("job","security","lib","parameter","covariance","return","control","portfolio","aggregate","timeseries")
    gnb$run$job$gsb <<- vector("list",length(xx))
    for(j in 3:6) gnb$run$job$layout[1,j] <<- c("","table","index","status","comment","datestamp")[j]
    ijo <- getijo(what="all")
    gnb$run$job$gsb[[1]] <<- gspinbutton(from=min(ijo),to=max(ijo))
    spin <- gnb$run$job$gsb[[1]]
    svalue(spin) <- max(ijo) 
    gnb$run$job$layout[3,1] <<- "jo"
    gnb$run$job$layout[3,2] <<- "job"
    gnb$run$job$layout[3,3] <<- spin
    detail <- as.character(extract("jo",
                        c("status","comment","datestamp"),
                        psz("WHERE ijo = ",max(ijo)),channel=chve()))
    gnb$run$job$mol <<- molassign(mol=gnb$run$job$mol,i=3,j=4,x=glabel(detail[1]))
    gnb$run$job$mol <<- molassign(mol=gnb$run$job$mol,i=3,j=5,x=glabel(detail[2]))
    gnb$run$job$mol <<- molassign(mol=gnb$run$job$mol,i=3,j=6,x=glabel(detail[3]))
    gnb$run$job$layout[3,4] <<- gnb$run$job$mol[3,4][[1]]
    gnb$run$job$layout[3,5] <<- gnb$run$job$mol[3,5][[1]]
    gnb$run$job$layout[3,6] <<- gnb$run$job$mol[3,6][[1]]
    addhandlerchanged(gnb$run$job$gsb[[1]],handler=lhrujojo)
    for(i in 2:length(xx)){
        ixx <- getive(xx=xx[i],what="ixx",howmany="all")
        if(xx[i]%in%intxx()) ixx <- c(0,ixx)
        #if(xx[i]=="li") ixx <- ixx[ixx!=1]#library 1 reserved for 'beta' jobs
        spin <- gnb$run$job$gsb[[i]] <<- gspinbutton(from=min(ixx),to=max(ixx))
        svalue(spin) <- max(ixx)
        gnb$run$job$layout[i+2,1] <<- xx[i]
        gnb$run$job$layout[i+2,2] <<- desc[i]
        gnb$run$job$layout[i+2,3] <<- spin
        detail <- as.character(extract(psz("ve",xx[i],"di"),
                            c("status","comment","datestamp"),
                            psz("WHERE ixx = ",max(ixx)),channel=chve()))
        gnb$run$job$mol <<- molassign(gnb$run$job$mol,i=i+2,j=4,x=glabel(detail[1]))
        gnb$run$job$mol <<- molassign(gnb$run$job$mol,i=i+2,j=5,x=glabel(detail[2]))
        gnb$run$job$mol <<- molassign(gnb$run$job$mol,i=i+2,j=6,x=glabel(detail[3]))
        gnb$run$job$layout[i+2,4] <<- gnb$run$job$mol[i+2,4][[1]]
        gnb$run$job$layout[i+2,5] <<- gnb$run$job$mol[i+2,5][[1]]
        gnb$run$job$layout[i+2,6] <<- gnb$run$job$mol[i+2,6][[1]]
        addhandlerchanged(gnb$run$job$gsb[[i]],handler=lhrujo,action=list(xx=xx[i],i=i))
#        if(xx[i]=="su") {gnb$run$job$layout[2,5] <<- gnb$run$job$mycomment <<- gedit(detail[2])} #no idea why I cannot use gnb to store the gedit
        if(xx[i]=="su") {gnb$run$job$layout[2,5] <<- gnbtest <<- gedit(detail[2])} #default job comment comes from su
    }
    gnb$run$job$layout[2,2] <<- "new job"
    gnb$run$job$layout[2,3] <<- as.character(getijo()+1)
    gnb$run$job$layout[2,4] <<- gnb$run$job$status <<- gedit("todo") 
    gnb$run$job$grp <<- ggroup(horiz=TRUE)
    tab <- gettab("jo",qual="ORDER BY ijo DESC",channel=chve()) 
    gnb$run$job$tab <<- gtable(tab,chosencol=which(colnames(tab)=="ijo"))
    svalue(gnb$run$job$tab,index=TRUE) <<- 1
    addhandlerclicked(gnb$run$job$tab,handler=rujo3handler)
    add(gnb$run$job$grp,gnb$run$job$tab,expand=TRUE)
    add(container,gpanedgroup(widget1=gnb$run$job$layout,widget2=gnb$run$job$grp,horiz=FALSE),expand=TRUE)
    container
}
rujo2 <-
function(container) 
{
    tbl <- list(
        refresh=gaction(    icon="refresh",     label="refresh",    handler = rujoupdate),
        delete=gaction(     icon="delete",      label="delete",     handler = ghrujodelete),
        save=gaction(       icon="save",        label="save",       handler = ghrujosave),
        restore=gaction(    icon="open",        label="restore",    handler = ghrujorestore)
        )
    add(container,gtoolbar(tbl))
    container
}
rujo1 <-
function(...)
{
    rujo2(cont=gnb$run$job$perm)                         #perm <- toolbar
    if(sqlnotempty("jo",channel=chve())) add(gnb$run$job$perm,rujo3(gnb$run$job$temp),expand=TRUE) #perm <- temp <- content
}
lhrujojo <-
function(h,...) 
{   
    ijo <- svalue(gnb$run$job$gsb[[1]])
    xx <- c("jo",inpxx(),intxx()) 
    idxx <- psz("i",xx)
    for(i in 2:length(idxx)) {  #update all other rows for changes to job
        ixx <- as.numeric(extract("jo",
                            idxx[i],
                            psz("WHERE ijo = ",ijo),channel=chve()))
        if(length(ixx)==0 || is.na(ixx)) ixx<-0
        svalue(gnb$run$job$gsb[[i]]) <<- ixx    #this triggers the change handler for the spinner, but gnbrunjobcomment not changed
    }      
    detail <- as.character(extract("jo",
                        c("status","comment","datestamp"),
                        psz("WHERE ijo = ",ijo),channel=chve()))
    svalue(gnb$run$job$mol[3,4][[1]]) <<- detail[1]
    svalue(gnb$run$job$mol[3,5][[1]]) <<- detail[2]
    svalue(gnb$run$job$mol[3,6][[1]]) <<- detail[3]
    svalue(gnb$run$job$tab,index=TRUE) <<- which(as.numeric(gnb$run$job$tab[,"ijo"])==as.numeric(svalue(gnb$run$job$gsb[[1]])))
}
lhrujo <-
function(h,...)
{ 
    xx <- h$action$xx
    i <- h$action$i
    detail <- as.character(extract(psz("ve",xx,"di"),
                        c("status","comment","datestamp"),
                        psz("WHERE ixx = ",svalue(gnb$run$job$gsb[[i]])),channel=chve()))
    svalue(gnb$run$job$mol[i+2,4][[1]]) <<- detail[1]
    svalue(gnb$run$job$mol[i+2,5][[1]]) <<- detail[2]
    svalue(gnb$run$job$mol[i+2,6][[1]]) <<- detail[3]
    if(xx=="li") {
#        gnb$run$job$mycomment <<- gedit(detail[2]) #no idea why I cannot use gnb to store the gedit - even though this is a global assign, the value gets lost
#        gnb$run$job$layout[2,5] <<- gnb$run$job$mycomment
#        print(svalue(gnb$run$job$mycomment))
        gnbtest <<- gedit(detail[2])
        gnb$run$job$layout[2,5] <<- gnbtest
        }
}
ghrujosave <-
function(h,...)    #saves the current selections, including status and comment but not job index, as a new job
{
    on.exit({enablegu(TRUE);pt("...job saved")})
    pt("saving job...")
    enablegu(FALSE)
    addjo(
        isu=svalue(gnb$run$job$gsb[[2]]),
        ili=svalue(gnb$run$job$gsb[[3]]),
        isi=svalue(gnb$run$job$gsb[[4]]),
        ice=ifelse(svalue(gnb$run$job$gsb[[5]])==0,NA,svalue(gnb$run$job$gsb[[5]])),
        ire=ifelse(svalue(gnb$run$job$gsb[[6]])==0,NA,svalue(gnb$run$job$gsb[[6]])),
        ico=ifelse(svalue(gnb$run$job$gsb[[7]])==0,NA,svalue(gnb$run$job$gsb[[7]])),
        ipo=ifelse(svalue(gnb$run$job$gsb[[8]])==0,NA,svalue(gnb$run$job$gsb[[8]])),
        status=svalue(gnb$run$job$status),
#        comment=svalue(gnb$run$job$mycomment)
        comment=svalue(gnbtest)
        )
    rujoupdate(disable=FALSE)
}
ghrujorestore <-
function(h,...) 
{
    ijo <- svalue(gnb$run$job$gsb[[1]])
    resjo(ijo,disablegu=TRUE)
    rujoselect(ijo)
    unseupdate(disable=FALSE)
    liliupdate(disable=FALSE)
    papaupdate(disable=FALSE)
    ruloupdate(disable=FALSE)
}
ghrujopromise <-
function()
{ 
    deljo(ijo=svalue(gnb$run$job$gsb[[1]]))
    rujoupdate()
    rujoselect(max(extract("jo","ijo",channel=chve())))
}
ghrujodelete <-
function(h,...) 
{
    stopifnot(nrow(gettab("jo"))>1)
    confirmDialog(
                message=psz("deleting a job deletes all output : delete ijo = ",svalue(gnb$run$job$gsb[[1]])," ?"),
                todo=ghrujopromise()
                )
}
rucuupdate <-
function(...,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$run$current$perm,widget=gnb$run$current$temp)
    gnb$run$current$temp <<- ggroup(use.scroll=TRUE)
    add(gnb$run$current$perm,rucu3(gnb$run$current$temp),expand=TRUE)
}
rucu3 <-
function(container) 
{
    alltabs <- allxx()%in%extract("si","mytable","WHERE parameter='run' AND value IN ('TRUE', 'T')")
    gnb$run$current$grucu3 <<- gdf(data.frame(seq=1:length(alltabs),table=allxx(),run=ifelse(alltabs,"Y",""),start="",end="",row.names=allxxlong()))    #gets updated by putlo()
    gg <- ggroup()
    add(gg,gnb$run$current$grucu3,expand=TRUE)
    add(container,gg,expand=TRUE)
    container
}
rucu2 <-
function(container) 
{
    tbl <- list()
    tbl <- list(
        run=gaction(    icon="go-forward",        label="run",   handler = ghrucu1)
        )
    add(container,gtoolbar(tbl,style="both"))
    container
}
rucu1 <-
function(...) 
{
    rucu2(cont=gnb$run$current$perm)                                    #perm <- toolbar
    add(gnb$run$current$perm,rucu3(gnb$run$current$temp),expand=TRUE)   #perm <- temp <- content
}
ghrucu1 <-
function(h,...) 
{
    rucuupdate()
    runjo()
}
ruloupdate <-
function(...,clear=FALSE,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$run$log$perm,widget=gnb$run$log$temp)
    gnb$run$log$temp <<- ggroup()
    if(clear) {
        add(gnb$run$log$perm,gnb$run$log$temp,expand=TRUE)
    } else {
        add(gnb$run$log$perm,rulo3(gnb$run$log$temp),expand=TRUE)
    }
}
rulo3 <-
function(container) 
{
    if(!chklo()) return(container)
    lo <- gettab("lo",ret="da")
    mode(lo[["mysize"]]) <- "character"
    rownames(lo) <- lo[,1]
    ddf <- cbind(1:nrow(lo),lo)
    colnames(ddf)[1] <- "seq"
    gnb$run$log$grulo <<- gtable(ddf)    #gets updated by putlo()
    add(container,gnb$run$log$grulo,expand=TRUE)
    container
}
rulo2 <-
function(container) 
{
    tbl <- list(
        restore=gaction(    icon="refresh",        label="refresh",   handler = ruloupdate)
        )
    add(container,gtoolbar(tbl))
    container
}
rulo1 <-
function(...) 
{
    rulo2(cont=gnb$run$log$perm)                         #perm <- toolbar
    if(sqlnotempty("lo")) add(gnb$run$log$perm,rulo3(gnb$run$log$temp),expand=TRUE) #perm <- temp <- content
}
reneupdate <-
function(...,clear=FALSE) 
{
    on.exit({enablegu(enable=TRUE)})   
    enablegu(enable=FALSE)
    delete(gnb$report$panel$perm,gnb$report$panel$temp)
        gnb$report$panel$temp <<- ggroup(horiz=TRUE,use.scroll=TRUE)
    gnb$report$panel$graphics <<- listof2()  
        add(gnb$report$panel$graphics$temp,gtext(''),expand=TRUE)   #required to anchor the window
    gnb$report$panel$bottomleft <<- listof2() 
    gnb$report$panel$bottomright <<- listof2()
    add(gnb$report$panel$graphics$perm,gnb$report$panel$graphics$temp,expand=TRUE)
    add(gnb$report$panel$bottomleft$perm,gnb$report$panel$bottomleft$temp,expand=TRUE)
    add(gnb$report$panel$bottomright$perm,gnb$report$panel$bottomright$temp,expand=TRUE)
    if(clear) {
        add(gnb$report$panel$perm,gnb$report$panel$temp,expand=TRUE)
    } else {
        add(gnb$report$panel$perm,rene3(gnb$report$panel$temp),expand=TRUE)
    }
}
rene3 <-
function(container) 
{
    #if(!chkne()) return(container)
    gtopleft <- gframe(horiz=FALSE)
        buttonframe <- ggroup(horiz=TRUE)
        add(buttonframe,gbutton("add row",handler = ghrenestratdeftab),expand=FALSE)
        add(buttonframe,gbutton("update",handler = ghreneupdate),expand=FALSE)
        add(buttonframe,gbutton("plot",handler = ghreneplotUpdate),expand=FALSE)
    add(gtopleft,buttonframe,expand=FALSE)
        editstrat <- gframe("row spec",horiz=TRUE)
            f1 <- gframe("strategy name")
                gnb$report$panel$w1 <<- gedit()
                add(f1,gnb$report$panel$w1,expand=FALSE)
        add(editstrat,f1,expand=FALSE)
            f1 <- gframe("iga")
                gnb$report$panel$w2 <<- gdroplist(as.integer(getive("ga",how="all")))
                add(f1,gnb$report$panel$w2,expand=FALSE)
        add(editstrat,f1,expand=FALSE)
            f1 <- gframe("type")
                gnb$report$panel$w3 <<- gradio(c("raw","cor","mkt","all"),horiz=TRUE)
                add(f1,gnb$report$panel$w3,expand=TRUE)
        add(editstrat,f1,expand=FALSE)
    add(gtopleft,editstrat,expand=FALSE)
        gnb$report$panel$stratcontainer <<- gframe("selection",horiz=FALSE)
                gnb$report$panel$strat <<- gtable(initialstrats())
                add(gnb$report$panel$stratcontainer,gnb$report$panel$strat,expand=TRUE)
    add(gtopleft,gnb$report$panel$stratcontainer,expand=TRUE)
    gpgtop <- gpanedgroup(widget1=gtopleft,widget2=gnb$report$panel$graphics$perm,horiz=TRUE)
    gpgbottom <- gpanedgroup(widget1=gnb$report$panel$bottomleft$perm,widget2=gnb$report$panel$bottomright$perm,horiz=TRUE)
    gpgvert <- gpanedgroup(widget1=gpgtop,widget2=gpgbottom,horiz=FALSE)
    gpgtop@widget@widget["position"] <- 440 
    gpgbottom@widget@widget["position"] <- 440 
    gpgvert@widget@widget["position"] <- 400 
    addHandlerDoubleclick(obj=gnb$report$panel$strat,handler=ghrenedelrow)
    add(container,gpgvert,expand=TRUE)
    container
}
rene2 <-
function(container) 
{
    tbl <- list(
            refresh=gaction(    icon="refresh",     label="refresh",   handler = reneupdate),
            snap=gaction(    icon="plot",     label="snap",   handler = ghrenesnap)
            )
    add(container,gtoolbar(tbl))
    container
}
initialstrats <-
function(nstrats=5) { #returns a table from me1 to populate the gui with defaults
        stopifnot(nstrats>0)
        if(TRUE) {#!"me1jo" %in% dirtab() || !nrow(gettab("me1jo"))>0 || any(is.na(gettab("me1jo")[,c("iga","ili")]))) { 
            blanks <- ""
            res <- data.frame(stratname=blanks,iga=blanks,type=blanks)
        } else {
            me1jo <- gettab("me1jo",qual="WHERE method!='unit'")
            res <- rbind(
                        cbind(stratname=me1jo[,"ili"],iga=me1jo[,"iga"],type="raw"),
                        cbind(stratname=me1jo[,"ili"],iga=me1jo[,"iga"],type="mkt"),
                        cbind(stratname=me1jo[,"ili"],iga=me1jo[,"iga"],type="cor")
                        )
        }
        istrat <- unique(res[,"stratname"])[1:nstrats]
        res[res[,"stratname"]%in%istrat,]
    }
ghreneupdate <-
function(...) 
{
    on.exit({enablegu(enable=TRUE)})   
    enablegu(enable=FALSE)
    stratmat <- gnb$report$panel$strat[,,drop=FALSE]
    stratnames <- unique(as.character(stratmat[,"stratname"]))
    mylist <- vector("list",length(stratnames))
    names(mylist) <- stratnames
    for(i in 1:length(mylist)) mylist[[i]] <- as.numeric(stratmat[stratmat[,"stratname"]==stratnames[i],"iga"])
    derne(strats=mylist)
    ghreneplotUpdate(h=list(action='plot'))#(ghreneargs())
    ghrenesumUpdate()
    ghrenecorUpdate()
}
ghrenetype <-
function()
{
    if(svalue(gnb$report$panel$w3)=="all") rawtype <- c("peraw","mkt","pecor") else
    if(svalue(gnb$report$panel$w3)=="raw") rawtype <- "peraw" else
    if(svalue(gnb$report$panel$w3)=="cor") rawtype <- "pecor" else
    if(svalue(gnb$report$panel$w3)=="mkt") rawtype <- "mkt"
    rep(rawtype,length(unique(extract("ne","strategy"))))
}
ghrenesumUpdate <-
function(...) { 
    delete(gnb$report$panel$bottomleft$perm,gnb$report$panel$bottomleft$temp)
    gnb$report$panel$bottomleft$temp <<- gframe("performance")
    add(gnb$report$panel$bottomleft$temp,ggtable(sumne(),fun=signif,digits=getsione("digits")),expand=TRUE)
    add(gnb$report$panel$bottomleft$perm,gnb$report$panel$bottomleft$temp,expand=TRUE)
}
ghrenestratdeftab <-
function(...)
{
    on.exit({enablegu(enable=TRUE)})   
    enablegu(enable=FALSE)
    stopifnot(nchar(svalue(gnb$report$panel$w1))>0)
    startmat <- gnb$report$panel$strat[,,drop=FALSE]
    if(nrow(startmat)==1 && (is.na(startmat[1,1]) || startmat[1,1]=="")) startmat <- startmat[-1,,drop=FALSE]
    if(svalue(gnb$report$panel$w3)!="all") {
        addmat <- as.matrix(data.frame(
            stratname=svalue(gnb$report$panel$w1),
            iga=svalue(gnb$report$panel$w2),
            type=svalue(gnb$report$panel$w3)
            ))
    } else {
        addmat <- as.matrix(data.frame(
            stratname=rep(svalue(gnb$report$panel$w1),3),
            iga=rep(svalue(gnb$report$panel$w2),3),
            type=c("raw","mkt","cor")
            ))
    }
    mat <- rbind(startmat,addmat)
    ikeep <- !duplicated(apply(mat,1,paste,collapse=""))
    delete(gnb$report$panel$stratcontainer,gnb$report$panel$strat)
    gnb$report$panel$strat <<- gtable(mat[ikeep,,drop=FALSE])
    add(gnb$report$panel$stratcontainer,gnb$report$panel$strat,expand=TRUE)
    addHandlerDoubleclick(obj=gnb$report$panel$strat,handler=ghrenedelrow)
}
ghrenesnap <-
function(...) {snap(ghreneplotUpdate)}
ghreneplotUpdate <-
function(h,...) {
    delete(gnb$report$panel$graphics$perm,gnb$report$panel$graphics$temp)
    gnb$report$panel$graphics$temp <<- ggroup()
    if(!any(h$action=="file")) ggraphics(container=gnb$report$panel$graphics$temp)          
    add(gnb$report$panel$graphics$perm,gnb$report$panel$graphics$temp,expand=TRUE)
    plotne(getne(type=ghrenetype()))
}
ghrenedelrow <-
function(...) 
{
    i <- which(gnb$report$panel$strat[,1]!=svalue(gnb$report$panel$strat,index=FALSE))
    #mat <- gnb$report$panel$strat[-svalue(gnb$report$panel$strat,index=TRUE),,drop=FALSE]
    mat <- gnb$report$panel$strat[i,,drop=FALSE]
    delete(gnb$report$panel$stratcontainer,gnb$report$panel$strat)
    gnb$report$panel$strat <<- gtable(mat)
    add(gnb$report$panel$stratcontainer,gnb$report$panel$strat,expand=TRUE)
    addHandlerDoubleclick(obj=gnb$report$panel$strat,handler=ghrenedelrow)
}
ghrenecorUpdate <-
function(...) { 
    delete(gnb$report$panel$bottomright$perm,gnb$report$panel$bottomright$temp)
    gnb$report$panel$bottomright$temp <<- gframe("correlation")
    rets <- getne()
    vols <- sqrt(apply(rets,2,var,na.rm=TRUE)*getsione("annfactor"))*ifelse(getsione("percent"),100,1)
    mat <- cor(rets,use="p")
    dfr <- data.frame(mat)
    dfr1 <- cbind(vol=vols," "=rownames(dfr),dfr)
    rownames(dfr1) <- NULL
    if(as.logical(extract("si","value","WHERE parameter='sigfig'"))) numberfun <- signif else numberfun <- round
    digits <- as.numeric(extract("si","value","WHERE parameter='digits'"))
    tab <- ggtable(dfr1,digits=digits,fun=numberfun) 
    add(gnb$report$panel$bottomright$temp,tab,expand=TRUE)
    add(gnb$report$panel$bottomright$perm,gnb$report$panel$bottomright$temp,expand=TRUE)
}
typeset <-
function() 
{
    type <- as.character(unique(extract("ma","type")))
    if(!getsione("qmom")) type <- setdiff(type,unique(extract("ma","type","WHERE type LIKE 'q%' OR type LIKE 'm%'")))
    type
}
typedisable <-
function(disable=TRUE) {#disables the radiobuttons for type
    if(disable) {
        enabled(gnb$report$summary$text$type) <<- FALSE
    } else {
        enabled(gnb$report$summary$text$type) <<- TRUE
    }
}
te2disable <-
function(disable=TRUE) {#disables the radiobuttons for category2
    if(disable) {
            enabled(gnb$report$summary$text$category2) <<- FALSE
        } else {
            enabled(gnb$report$summary$text$category2) <<- TRUE
        }
}
te1disable <-
function(disable=TRUE) {#disables the radiobuttons for category1 
    if(disable) {
        enabled(gnb$report$summary$text$category1) <<- FALSE
    } else {
        enabled(gnb$report$summary$text$category1) <<- TRUE
    }
}
rowset <-
function() {colset()}
resuupdate <-
function(...,clear=FALSE,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(gnb$report$summary$perm,gnb$report$summary$temp)
        gnb$report$summary$temp <<- ggroup(horiz=TRUE,use.scroll=TRUE)
        add(gnb$report$summary$graphics$temp,gtext(''),expand=TRUE)   #required to anchor the window
    add(gnb$report$summary$graphics$perm,gnb$report$summary$graphics$temp,expand=TRUE)
    gnb$report$summary$graphics <<- listof2() 
    gnb$report$summary$bottom <<- gframe(horizontal=TRUE)
    if(clear) {
        add(gnb$report$summary$perm,gnb$report$summary$temp,expand=TRUE)
    } else {
        add(gnb$report$summary$perm,resu3(gnb$report$summary$temp),expand=TRUE)
    }
}
resudisable <-
function()
{
    if("category1" %in% c(svalue(gnb$report$summary$text$w3),svalue(gnb$report$summary$text$w2))) te1disable(TRUE) else te1disable(FALSE)
    if("category2" %in% c(svalue(gnb$report$summary$text$w3),svalue(gnb$report$summary$text$w2))) te2disable(TRUE) else te2disable(FALSE)
    if("lag" %in% c(svalue(gnb$report$summary$text$w3),svalue(gnb$report$summary$text$w2))) lagdisable(TRUE) else lagdisable(FALSE)
    if("type" %in% c(svalue(gnb$report$summary$text$w3),svalue(gnb$report$summary$text$w2))) typedisable(TRUE) else typedisable(FALSE)
    if("component" %in% c(svalue(gnb$report$summary$text$w3),svalue(gnb$report$summary$text$w2))) compdisable(TRUE) else compdisable(FALSE)
}
resucompset <-
function()
{
    comp <- as.character(unique(extract("ma","comp")))
    if(!getsione("intercept")) comp <- setdiff(comp,unique(extract("ma","comp","WHERE comp LIKE '%int%'")))
    if(!getsione("factors")) comp <- setdiff(comp,unique(extract("ma","comp","WHERE equation=0")))
    if(!getsione("reproxy")) comp <- setdiff(comp,unique(extract("ma","comp","WHERE equation IN (1,2,3)")))
    if(!getsione("yiproxy")) comp <- setdiff(comp,unique(extract("ma","comp","WHERE equation IN (4,5,6)")))
    droptab("mavi")
    mysqlQuery(psz("CREATE VIEW mavi AS SELECT * FROM ma WHERE comp IN (",fldlst(comp,"'","'"),") AND type IN (",fldlst(typeset(),"'","'"),")"))
    compset <- as.list(comp)
    names(compset) <- comp
    compset
}
resuarglst <-
function()  {
    list(
        comp=as.character(resucompset()[svalue(gnb$report$summary$text$w1,index=TRUE)]),
        rows=as.character(rowset()[svalue(gnb$report$summary$text$w2,index=TRUE)]),
        cols=as.character(colset()[svalue(gnb$report$summary$text$w3,index=TRUE)]),
        te1=svalue(gnb$report$summary$text$w4),
        te2=svalue(gnb$report$summary$text$w5),
        lag=ifelse(is.null(gnb$report$summary$text$w6),0,svalue(gnb$report$summary$text$w6)),
        type=svalue(gnb$report$summary$text$w7),
        tab="ma"
        )
}
resu3 <-
function(container) {
    if(!chkma()) return(container)
    te1set <- unique(extract("ma","te1"))
    te2set <- unique(extract("ma","te2"))
    lagset <- unique(extract("ma","lag","WHERE equation<7"))
    gnb$report$summary$text$w1 <<- gcombobox(names(resucompset()))
    gnb$report$summary$text$w2 <<- gcombobox(names(rowset()))
    gnb$report$summary$text$w3 <<- gcombobox(names(colset()))
    if(length(te1set)<14) gnb$report$summary$text$w4 <<- gcombobox(te1set) else  gnb$report$summary$text$w4 <<- gdroplist(te1set)
    if(length(te2set)<14) gnb$report$summary$text$w5 <<- gcombobox(te2set) else  gnb$report$summary$text$w5 <<- gdroplist(te2set)
    if(length(lagset)>0) gnb$report$summary$text$w6 <<- gcombobox(lagset) else gnb$report$summary$text$w6 <<- NULL
    gnb$report$summary$text$w7 <<- gcombobox(typeset())
    gnb$report$summary$text$w8 <<- gcheckboxgroup(c("sort","horizontal"))
    gnb$report$summary$text$w9 <<- gcheckbox()
            gtopleft <- gframe(horiz=TRUE)                             #paned groups
                g1left <- gframe("plot options",horiz=TRUE)
                    g2 <- ggroup(horiz=FALSE)
                        g1f <- ggroup() 
                        add(g1f,gnb$report$summary$text$w8,expand=FALSE)
                    add(g2,g1f,expand=FALSE)
                add(g1left,g2,expand=FALSE)
            add(gtopleft,g1left,expand=FALSE)
                g1outer <- gframe("table",horiz=FALSE)          #table frame
                    g2 <- ggroup(,horiz=FALSE)
                        g1f <- gframe("rows")                           #rows dropbox
                            add(g1f,gnb$report$summary$text$w2,expand=FALSE)    
                    add(g2,g1f,expand=FALSE)
                add(g1outer,g2,expand=FALSE)     
                    g2 <- ggroup(,horiz=FALSE)
                        g1f <- gframe("cols")                           #cols dropbox
                            add(g1f,gnb$report$summary$text$w3,expand=FALSE)
                    add(g2,g1f,expand=FALSE)
                add(g1outer,g2,expand=FALSE)     
                    g2 <- ggroup(,horiz=FALSE)
                        g1f <- gframe("transpose")                           #transpose checkbox
                            add(g1f,gnb$report$summary$text$w9,expand=FALSE)
                    add(g2,g1f,expand=FALSE)
                add(g1outer,g2,expand=FALSE)
            add(gtopleft,g1outer,expand=FALSE)
                g1outer <- gframe("key",horiz=FALSE)        #'defaults' frame
                    gnb$report$summary$text$category1 <<- gframe("category1",horiz=FALSE)       #category1 frame
                        add(gnb$report$summary$text$category1,gnb$report$summary$text$w4,expand=FALSE) 
                add(g1outer,gnb$report$summary$text$category1,expand=FALSE)     
                    gnb$report$summary$text$category2 <<- gframe("category2",horiz=FALSE)       #category2 frame
                        add(gnb$report$summary$text$category2,gnb$report$summary$text$w5,expand=FALSE) 
                add(g1outer,gnb$report$summary$text$category2,expand=FALSE)     
            if(!is.null(gnb$report$summary$text$w6)) {
                    gnb$report$summary$text$lag <<- gframe("lag",horiz=FALSE)             #lag frame
                        add(gnb$report$summary$text$lag,gnb$report$summary$text$w6,expand=FALSE)
                add(g1outer,gnb$report$summary$text$lag,expand=FALSE)     
                } 
                    gnb$report$summary$text$type <<- gframe("type",horiz=FALSE)            #type frame
                        add(gnb$report$summary$text$type,gnb$report$summary$text$w7,expand=FALSE) 
                add(g1outer,gnb$report$summary$text$type,expand=FALSE)     
                    gnb$report$summary$text$comp <<- gframe("component",horiz=FALSE)       #component frame
                        add(gnb$report$summary$text$comp,gnb$report$summary$text$w1,expand=FALSE) 
                add(g1outer,gnb$report$summary$text$comp,expand=FALSE)     
            add(gtopleft,g1outer,expand=FALSE)
        gpghoriz <- gpanedgroup(widget1=gtopleft,widget2=gnb$report$summary$graphics$perm,horiz=TRUE)
    gpgvert <- gpanedgroup(widget1=gpghoriz,widget2=gnb$report$summary$bottom,horiz=FALSE)
    gpghoriz@widget@widget["position"] <- 440 
    gpgvert@widget@widget["position"] <- 400 
    add(container,gpgvert,expand=TRUE)                          #place all into container
    svalue(gnb$report$summary$text$w3) <<- "component"    #this line has to be after the assignments to gnb$report$summary, not sure why
    svalue(gnb$report$summary$text$w1) <<- "rem" #names(resucompset())[1] #[length(resucompset())-1]    #this line has to be after the assignments to gnb$report$summary, not sure why
    svalue(gnb$report$summary$text$w2) <<- "type" #
    if("ma" %in% dirtab()) {
        gresutabUpdate()
        add(gnb$report$summary$bottom,gnb$report$summary$text$wtab,expand=TRUE)
    }
    addhandlerchanged(gnb$report$summary$text$w1,handler=ghresu1,action="retainrow")
    addhandlerchanged(gnb$report$summary$text$w2,handler=ghresu1)
    addhandlerchanged(gnb$report$summary$text$w3,handler=ghresu1,action="retainrow")
    addhandlerchanged(gnb$report$summary$text$w4,handler=ghresu1,action="retainrow")
    addhandlerchanged(gnb$report$summary$text$w5,handler=ghresu1,action="retainrow")
    addhandlerchanged(gnb$report$summary$text$w6,handler=ghresu1,action="retainrow")
    addhandlerchanged(gnb$report$summary$text$w7,handler=ghresu1,action="retainrow")
    addhandlerchanged(gnb$report$summary$text$w8,handler=ghresu2)
    addhandlerchanged(gnb$report$summary$text$w9,handler=ghresu1,action="retainrow")
    resudisable()
    container
}
resu2 <-
function(container) 
{
    tbl <- list(
            refresh=gaction(    icon="refresh",     label="refresh",   handler = resuupdate),
            snap=gaction(    icon="plot",     label="snap",   handler = ghresusnap)
            )
    add(container,gtoolbar(tbl))
    container
}
resu1 <-
function(...) 
{
    add(gnb$report$summary$graphics$perm,gnb$report$summary$graphics$temp,expand=TRUE)
    add(gnb$report$summary$perm,resu3(gnb$report$summary$temp),expand=TRUE)   #perm <- temp <- content
}
lagdisable <-
function(disable=TRUE) {#disables the radiobuttons for lag
    if(disable) {
        enabled(gnb$report$summary$text$lag) <<- FALSE
    } else {
        enabled(gnb$report$summary$text$lag) <<- TRUE
    }
}
gresutabUpdate <-
function() { 
    mat <- do.call(what="getma",args=resuarglst())
    dfr <- data.frame(mat)
    dfr <- cbind(rownames(dfr),dfr)
    colnames(dfr) <- c("",colnames(mat))
    if(getsione('sigfig')) numberfun <- signif else numberfun <- round
    if(svalue(gnb$report$summary$text$w9)) dfr <- data.frame(x=colnames(dfr[,-1]),t(dfr[,-1]))
    gnb$report$summary$text$wtab <<- ggtable(dfr,digits=getsione('digits'),fun=numberfun) 
    svalue(gnb$report$summary$text$wtab,index=TRUE) <<- 1
    addhandlerdoubleclick(gnb$report$summary$text$wtab,handler=ghresu2)
}
ghresusnap <-
function(...) {snap(ghresu2)}
ghresu2 <-
function(h,...) {#graphics on doubleclick
    delete(gnb$report$summary$graphics$perm,gnb$report$summary$graphics$temp)
    gnb$report$summary$graphics$temp <<- ggroup()
    if(!any(h$action=="file")) ggraphics(container=gnb$report$summary$graphics$temp)             #graphics device
    add(gnb$report$summary$graphics$perm,gnb$report$summary$graphics$temp,expand=TRUE)
    tab <- gnb$report$summary$text$wtab[,,drop=FALSE]
    irow <- svalue(gnb$report$summary$text$wtab,index=TRUE)
    if("sort"%in%svalue(gnb$report$summary$text$w8)) jorder <- order(as.numeric(tab[irow,-1,drop=TRUE])) else jorder <- seq(1:(ncol(tab)-1))
    is.horiz <- ("horizontal"%in%svalue(gnb$report$summary$text$w8))
    rows <- svalue(gnb$report$summary$text$w2)
    cols <- svalue(gnb$report$summary$text$w3)
    comp <- svalue(gnb$report$summary$text$w1)
    type <- svalue(gnb$report$summary$text$w7)
    abcissa <- cols
    ordinate <- paste(sep=":",comp,type)
    if(cols=="type") ordinate <- "" 
    if(rows=="type") ordinate <- irow
    suppressWarnings(barplot( #the warning on hadj is spurious/bug
        height=as.numeric(tab[,-1,drop=FALSE][irow,jorder,drop=TRUE]),
        names.arg=names(tab[,-1,drop=FALSE])[jorder],
        horiz=is.horiz,
        hadj=ifelse(("horizontal"%in%svalue(gnb$report$summary$text$w8)),0,1),
        col=c(rep("dark orange",6),rep("purple",7)),#rainbow(1,getsione("baralpha")*.7,start=.6),
        border=NA,
        las=1,
        ylab="",#ifelse(is.horiz,abcissa,ordinate),
        xlab="weeks out of sample",#ifelse(is.horiz,ordinate,abcissa)
        ))
#    suppressWarnings(barplot( #the warning on hadj is spurious/bug
#        height=as.numeric(tab[,-1,drop=FALSE][irow,jorder,drop=TRUE]),
#        names.arg=names(tab[,-1,drop=FALSE])[jorder],
#        horiz=is.horiz,
#        hadj=ifelse(("horizontal"%in%svalue(gnb$report$summary$text$w8)),0,1),
#        col=rainbow(1,getsione("baralpha")*.7,start=.6),
#        border=NA,
#        las=1,
#        ylab=ifelse(is.horiz,abcissa,ordinate),
#        xlab=ifelse(is.horiz,ordinate,abcissa)
#        ))
}
ghresu1 <-
function(h,...) {
    on.exit({enablegu(enable=TRUE)})   
    enablegu(enable=FALSE)          #disable entire page temporarily
    irow <- svalue(gnb$report$summary$text$wtab,index=TRUE)
    delete(obj=gnb$report$summary$bottom,widget=gnb$report$summary$text$wtab)
    gresutabUpdate()
    add(obj=gnb$report$summary$bottom,value=gnb$report$summary$text$wtab,expand=TRUE)
    enablegu(enable=TRUE)           #need page enabled to disable widgets
    svalue(gnb$report$summary$text$wtab) <<- ifelse(h$action=="retainrow",irow,1)
    ghresu2(h,...)
}
existsresu <-
function()
{
    length(gnb$report$summary$text$w3)>0
}
compdisable <-
function(disable=TRUE) {#disables the radiobuttons for comp
    if(disable) {
        enabled(gnb$report$summary$text$comp) <<- FALSE
    } else {
        enabled(gnb$report$summary$text$comp) <<- TRUE
    }
}
colset <-
function() {list(category1="te1",category2="te2",lag="lag",type="type",component="comp",job='ixx')}
viewlist <-
function()
{
list(
    'plot-default ' =list(plotsel=NULL),
    'plot-horizontal' =list(plotsel=c('horizontal','sort')),
    'plot-line-acc' =list(plotsel=c('line','cumulative')),
    'pe-te1-co'     =list(domain='performance', rowsel='category1', colsel='component', timeseries='sum',  vartype='v', plotsel=c('horizontal','sort')),
    'pe-te1-te2'    =list(domain='performance', rowsel='category1', colsel='category2', timeseries='sum',  vartype='v', plotsel=c('horizontal','sort')),
    'pe-te1-tP'     =list(domain='performance', rowsel='category1', colsel='lag-perf',  timeseries='sum',  vartype='v', plotsel=NULL),
    'po-co-te1'     =list(domain='position',    rowsel='component', colsel='category1', timeseries='sum',  vartype='v', plotsel=c('horizontal','sort')),
    'po-co-te2'     =list(domain='position',    rowsel='component', colsel='category2', timeseries='sum',  vartype='v', plotsel=c('horizontal','sort')),
    'va-co-te1'     =list(domain='variance',    rowsel='component', colsel='category1', timeseries='sum',  vartype='v', plotsel=c('horizontal','sort')),
    'va-co-te2'     =list(domain='variance',    rowsel='component', colsel='category2', timeseries='sum',  vartype='v', plotsel=c('horizontal','sort')),
    'va-te1-te2'    =list(domain='variance',    rowsel='category1', colsel='category2', timeseries='sum',  vartype='v', plotsel=c('horizontal','sort')),
    'f1-te1-te2'    =list(domain='factor1',     rowsel='category1', colsel='category2', timeseries='sum',  vartype='v', plotsel=c('horizontal','sort')),
    'pe-te1-da'      =list(domain='performance',rowsel='category1', colsel='date',      timeseries='sum',  vartype='v', plotsel=c('line','cumulative')),
    'po-te1-da'      =list(domain='position',   rowsel='category1', colsel='date',      timeseries='sum',  vartype='v', plotsel=c('line')),
    'va-te1-da'      =list(domain='variance',   rowsel='category1', colsel='date',      timeseries='sum',  vartype='v', plotsel=c('line')),
    'va-co-da'       =list(domain='variance',   rowsel='component', colsel='date',      timeseries='sum',  vartype='v', plotsel=c('line'))
    )
}
vartypedisable <-
function(disable=TRUE) #disables the vartype radiobuttons for vcv domains
    {
        if(disable) {
            svalue(gnb$report$aggregate$text$variance) <<- "v"
            enabled(gnb$report$aggregate$text$vartype) <<- FALSE
        } else {
            enabled(gnb$report$aggregate$text$vartype) <<- TRUE
        }
    }
setwidgets <-
function(
                    domain=svalue(gnb$report$aggregate$text$domain),
                    rowsel=svalue(gnb$report$aggregate$text$rows),
                    colsel=svalue(gnb$report$aggregate$text$cols),
                    timeseries=svalue(gnb$report$aggregate$text$timeseries),
                    vartype=svalue(gnb$report$aggregate$text$variance),
                    plotsel=svalue(gnb$report$aggregate$text$plotopt)
                    )
{
    plotoptions <- c('line','cumulative','horizontal','sort','multiple','stack','grey','legend','label','none')
    stopifnot(all(plotsel%in%plotoptions))
    updatetable <- FALSE
    updateplot <- FALSE
    if(svalue(gnb$report$aggregate$text$domain)!=domain) {
        removehandler(obj=gnb$report$aggregate$text$domain,ID=gnb$report$aggregate$handler$domain)
        svalue(gnb$report$aggregate$text$domain) <<- domain
        gnb$report$aggregate$handler$domain <<- addhandlerchanged(gnb$report$aggregate$text$domain,handler=ghreagrownames)
        updatetable <- TRUE
    }
    if(svalue(gnb$report$aggregate$text$rows)!=rowsel) {
        removehandler(obj=gnb$report$aggregate$text$rows,ID=gnb$report$aggregate$handler$rows)
        svalue(gnb$report$aggregate$text$rows) <<- rowsel
        gnb$report$aggregate$handler$rows <<- addhandlerchanged(gnb$report$aggregate$text$rows,handler=ghreagrownames)
        updatetable <- TRUE
    }
    if(svalue(gnb$report$aggregate$text$cols)!=colsel) {
        removehandler(obj=gnb$report$aggregate$text$cols,ID=gnb$report$aggregate$handler$cols)
        svalue(gnb$report$aggregate$text$cols) <<- colsel
        gnb$report$aggregate$handler$cols <<- addhandlerchanged(gnb$report$aggregate$text$cols,handler=ghreagdrop)
        updatetable <- TRUE
    }
    if(svalue(gnb$report$aggregate$text$timeseries)!=timeseries) {
        removehandler(obj=gnb$report$aggregate$text$timeseries,ID=gnb$report$aggregate$handler$timeseries)
        svalue(gnb$report$aggregate$text$timeseries) <<- timeseries
        gnb$report$aggregate$handler$timeseries <<- addhandlerchanged(gnb$report$aggregate$text$timeseries,handler=ghreagdrop)
        updatetable <- TRUE
    }
    if(svalue(gnb$report$aggregate$text$variance)!=vartype) {
        removehandler(obj=gnb$report$aggregate$text$variance,ID=gnb$report$aggregate$handler$variance)
        svalue(gnb$report$aggregate$text$variance) <<- vartype
        gnb$report$aggregate$handler$variance <<- addhandlerchanged(gnb$report$aggregate$text$variance,handler=ghreagdrop)
        updatetable <- TRUE
    }
    if(!identical(svalue(gnb$report$aggregate$text$plotopt),plotsel)) {
        #removehandler(obj=gnb$report$aggregate$text$plotopt,ID=gnb$report$aggregate$handler$plotopt)   #not required - no handlers
        svalue(gnb$report$aggregate$text$plotopt) <<- plotoptions%in%plotsel
        #gnb$report$aggregate$handler$plotopt <<- addhandlerchanged(gnb$report$aggregate$text$plotopt,handler=ghreagplot)
        updateplot <- TRUE
    }
    if(updatetable) {
        ghreagrownames(h=list(action='none'))
        ghreagtable(h=list(action='none'))
    }
    if(updatetable || updateplot) {
        ghreagplot(h=list(action='none'))
    }
}
selectview <-
function(view=names(viewlist())[4])
{
    stopifnot(view%in%(names(viewlist())) && length(view)==1)
    settings <- viewlist()[view][[1]]
    do.call(what="setwidgets",args=settings)
}
reagupdate <-
function(...,clear=FALSE,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(gnb$report$aggregate$perm,gnb$report$aggregate$temp)
    gnb$report$aggregate$temp <<- ggroup(horiz=TRUE,use.scroll=TRUE)
    gnb$report$aggregate$graphics <<- listof2() 
    add(gnb$report$aggregate$graphics$perm,gnb$report$aggregate$graphics$temp,expand=TRUE)
    add(gnb$report$aggregate$graphics$temp,gtext(''),expand=TRUE)   #required to anchor the window
    if(clear) {
        add(gnb$report$aggregate$perm,gnb$report$aggregate$temp,expand=TRUE)
    } else {
        add(gnb$report$aggregate$perm,reag3(gnb$report$aggregate$temp),expand=TRUE)
    }
}
reagtypedisable <-
function(disable=TRUE,type="mean") #disables the 'timeseries' radiobuttons for vcv domains
    {
        if(disable) {
            svalue(gnb$report$aggregate$text$timeseries) <<- type
            enabled(gnb$report$aggregate$text$type) <<- FALSE
        } else {
            enabled(gnb$report$aggregate$text$type) <<- TRUE
        }
    }
reagrows2 <-
function()
{
    list(comp="component",te1="category1",te2="category2",lag="lag-cov",lapo="lag-perf")
}
reagrows <-
function()
{
    list(comp="component",te1="category1",te2="category2",lace="lag-cov",lapo="lag-perf")
}
reagdisable <-
function() 
{
    if(svalue(gnb$report$aggregate$text$cols)=="date") {
        reagtypedisable(TRUE,type="sum")
    } else {            
        if(svalue(gnb$report$aggregate$text$domain)%in%c("variance","factor1","position")) {
            reagtypedisable(TRUE,type="mean")
        } else {
            reagtypedisable(FALSE)
        }
    }
    if( !svalue(gnb$report$aggregate$text$domain)=="variance"                   #not variance domain
        #|| !(svalue(gnb$report$aggregate$text$rows) %in% c('category1','category2','component'))         #rows cannot be summed
        #|| !(svalue(gnb$report$aggregate$text$cols) %in% c('category1','category2','component'))         #columns cannot be summed
        ) {     
        vartypedisable(TRUE) 
    } else { 
        vartypedisable(FALSE)
    }
    #if( svalue(gnb$report$aggregate$text$cols)=="job" ) {
    #    enabled(gnb$report$aggregate$text$transpose) <- TRUE
    #} else {
    #    svalue(gnb$report$aggregate$text$transpose) <- FALSE
    #    enabled(gnb$report$aggregate$text$transpose) <- FALSE
    #}

}
reagargmap3 <-
function(){c("SUM","AVG","STD","IR")}
reagargmap2row <-
function(){names(reagrows2()[match(gnb$report$aggregate$text$rows[],reagrows2())])}
reagargmap2col <-
function(){c("comp","te1","te2","lag","lapo","date","job",qufigaplus())}
reagargmap1 <-
function(){c("pe","po","va","f1","ic")}
reagarglst <-
function()
{
    if(svalue(gnb$report$aggregate$text$cols) %in% qufigaplus()) {
        cols <- "te2"
        colstart <- svalue(gnb$report$aggregate$text$cols)
        } else {
        cols <- reagargmap2col()[svalue(gnb$report$aggregate$text$cols,index=TRUE)]
        colstart <- NULL
        }
    list(
        domain=reagargmap1()[svalue(gnb$report$aggregate$text$domain,index=TRUE)],
        rows=reagargmap2row()[svalue(gnb$report$aggregate$text$rows,index=TRUE)],
        cols=cols,
        colstart=colstart,
        type=reagargmap3()[svalue(gnb$report$aggregate$text$timeseries,index=TRUE)],
        variance=svalue(gnb$report$aggregate$text$variance),
        sums=TRUE,
        avgs=TRUE,
        iinc=svalue(gnb$report$aggregate$text$rownames),
        percent=getsione("percent"),
        annfactor=getsione("annfactor")
        )
}
reag3 <-
function(container) 
{
    if(!chkga()) return(container)
    if(!is.null(qufiga())) {qu <- qufigaplus()} else {qu <- 'year'}
    gnb$report$aggregate$text$view <<- gcombobox(names(viewlist()))
    gnb$report$aggregate$text$domain <<- gcombobox(c("performance","position","variance","factor1","IC"))
    gnb$report$aggregate$text$rows <<- gcombobox(sort(setdiff(as.character(reagrows2()[as.character(extract("ga","DISTINCT field1"))]),"NULL"))) #c("component","category1","category2","lag-cov","lag-perf")
    gnb$report$aggregate$text$cols <<- gcombobox(c("component","category1","category2","lag-cov","lag-perf","date","job",qu))
    gnb$report$aggregate$text$timeseries <<- gcombobox(c("sum","mean","sdev","IR"))
    gnb$report$aggregate$text$variance <<- gcombobox(c('v','v/(V^.5)','v/V'))
    gnb$report$aggregate$text$rownames <<- gcheckboxgroup(initialrownames())
    gnb$report$aggregate$text$transpose <<- gcheckbox("transpose")
    gnb$report$aggregate$text$region <<- gcheckbox("reg")
    gnb$report$aggregate$text$development <<- gcheckbox("dev")
    gnb$report$aggregate$text$component <<- gcheckbox("comp")
    gnb$report$aggregate$text$sign <<- gcheckbox("sign")
    gnb$report$aggregate$text$plotopt <<- gcheckboxgroup(c("line","cumulative","horizontal","sort","multiple","stack","grey","legend","label","none","table"))
    ggtopleft <- gframe(horiz=TRUE,use.scroll=FALSE)
        ggplotopt <-  gframe("plot options",horiz=TRUE)                            #ggplotopt - container for plot options
            g2 <- ggroup(horiz=FALSE)
                g2b <- ggroup(horiz=TRUE)
                add(g2b,gbutton("plot",handler = ghreagplot,action="refresh"),expand=FALSE)
                add(g2b,gbutton("boxplot",handler = ghreagboxplot,action="refresh"),expand=FALSE)
            add(g2,g2b,expand=FALSE) 
                g1f <- ggroup(horiz=FALSE)                               #plot options
                add(g1f,gnb$report$aggregate$text$plotopt,expand=FALSE)
            add(g2,g1f,expand=TRUE)
        add(ggplotopt,g2,expand=FALSE) 
    add(ggtopleft,ggplotopt,expand=TRUE)
        ggcombo <- gframe("table",horiz=FALSE)                              #ggcombo - container for comboboxes
            #g2 <- ggroup(horiz=TRUE)
            #    g1f <- gframe("view")                                       #comboboxes
            #    add(g1f,gnb$report$aggregate$text$view,expand=TRUE)
            #add(g2,g1f,expand=FALSE)
        #add(ggcombo,g2,expand=FALSE)
            ggcombob <- ggroup(horiz=TRUE)
            add(ggcombob,gbutton("update",handler = ghreagtableplot),expand=FALSE)
        add(ggcombo,ggcombob,expand=FALSE)
                g2 <- ggroup(horiz=TRUE)
                g1f <- gframe("domain")         
                add(g1f,gnb$report$aggregate$text$domain,expand=TRUE)
            add(g2,g1f,expand=FALSE)
        add(ggcombo,g2,expand=FALSE)
            g2 <- ggroup(horiz=TRUE) 
                g1f <- gframe("rows")          
                add(g1f,gnb$report$aggregate$text$rows,expand=TRUE)
            add(g2,g1f,expand=FALSE)
        add(ggcombo,g2,expand=FALSE)
            g2 <- ggroup(horiz=TRUE)
                g1f <- gframe("cols")          
                add(g1f,gnb$report$aggregate$text$cols,expand=TRUE)
            add(g2,g1f,expand=FALSE)
        add(ggcombo,g2,expand=FALSE)
            gnb$report$aggregate$text$type <<- ggroup(horiz=TRUE)
                g1f <- gframe("timeseries")          
                add(g1f,gnb$report$aggregate$text$timeseries,expand=TRUE)
            add(gnb$report$aggregate$text$type,g1f,expand=FALSE)
        add(ggcombo,gnb$report$aggregate$text$type,expand=FALSE)
            gnb$report$aggregate$text$vartype <<- ggroup(horiz=TRUE)
                g1f <- gframe("variance")        
                add(g1f,gnb$report$aggregate$text$variance,expand=TRUE)
            add(gnb$report$aggregate$text$vartype,g1f,expand=FALSE)
        add(ggcombo,gnb$report$aggregate$text$vartype,expand=FALSE)
        add(ggcombo,gnb$report$aggregate$text$transpose,expand=FALSE)
            gnb$report$aggregate$text$fag <<- ggroup(horiz=TRUE)
                g1f <- gframe("further aggregate")        
                add(g1f,gnb$report$aggregate$text$region,expand=TRUE)
                add(g1f,gnb$report$aggregate$text$development,expand=TRUE)
                add(g1f,gnb$report$aggregate$text$component,expand=TRUE)
                add(g1f,gnb$report$aggregate$text$sign,expand=TRUE)
            add(gnb$report$aggregate$text$fag,g1f,expand=FALSE)
        add(ggcombo,gnb$report$aggregate$text$fag,expand=FALSE)
        add(ggcombo,gnb$report$aggregate$text$transpose,expand=FALSE)
    add(ggtopleft,ggcombo,expand=TRUE)


    gnb$report$aggregate$bottom <<- ggroup(horiz=TRUE,use.scroll=TRUE)          #group with row selections, table
        g2 <- ggroup(horiz=TRUE,use.scroll=FALSE)
            gnb$report$aggregate$text$gfrrownames <<- ggroup(use.scroll=FALSE,horiz=FALSE)#row selection checkboxes  gframe("rows",horiz=FALSE) 
            add(gnb$report$aggregate$text$gfrrownames,gnb$report$aggregate$text$rownames,expand=TRUE)
        add(g2,gnb$report$aggregate$text$gfrrownames,expand=FALSE)##
    add(gnb$report$aggregate$bottom,g2,expand=FALSE)##
    gnb$report$aggregate$gpghoriz <<- gpanedgroup(widget1=ggtopleft,widget2=gnb$report$aggregate$graphics$perm,horiz=TRUE)
    gnb$report$aggregate$gpgvert <<- gpanedgroup(widget1=gnb$report$aggregate$gpghoriz,widget2=gnb$report$aggregate$bottom,horiz=FALSE)
    gnb$report$aggregate$gpghoriz@widget@widget["position"] <<- 320 
    gnb$report$aggregate$gpgvert@widget@widget["position"] <<- 690 
    add(container,gnb$report$aggregate$gpgvert,expand=TRUE)                          #place all into container
    svalue(gnb$report$aggregate$text$domain) <<- "performance"    
    if('lag-cov'%in%gnb$report$aggregate$text$rows[]) svalue(gnb$report$aggregate$text$rows) <<- "lag-cov"    
    svalue(gnb$report$aggregate$text$cols) <<- "date"    
    svalue(gnb$report$aggregate$text$timeseries) <<- "sum" 
    svalue(gnb$report$aggregate$text$rownames) <<- gnb$report$aggregate$text$rownames[1]#"lag"   
    svalue(gnb$report$aggregate$text$plotopt) <<- c("line","cumulative","multiple")
    if("ga"%in%dirtab()) {
        greagtabUpdate()
        if(!is(gnb$report$aggregate$text$wtab,"data.frame")) add(obj=gnb$report$aggregate$bottom,value=gnb$report$aggregate$text$wtab,expand=TRUE)
    }
    reagdisable()
    #gnb$report$aggregate$handler$view <<- addhandlerchanged(gnb$report$aggregate$text$view,handler=ghreagview)   
    gnb$report$aggregate$handler$domain <<- addhandlerchanged(gnb$report$aggregate$text$domain,handler=ghreagrownames)  
    gnb$report$aggregate$handler$rows <<- addhandlerchanged(gnb$report$aggregate$text$rows,handler=ghreagrownames) 
    gnb$report$aggregate$handler$cols <<- addhandlerchanged(gnb$report$aggregate$text$cols,handler=ghreagrownames) 
    gnb$report$aggregate$handler$timeseries <<- addhandlerchanged(gnb$report$aggregate$text$timeseries,handler=ghreagdrop) 
    gnb$report$aggregate$handler$variance <<- addhandlerchanged(gnb$report$aggregate$text$variance,handler=ghreagdrop)
    gnb$report$aggregate$handler$region <<- addhandlerchanged(gnb$report$aggregate$text$region,handler=ghreagag,action="region")
    gnb$report$aggregate$handler$development <<- addhandlerchanged(gnb$report$aggregate$text$development,handler=ghreagag,action="development")
    gnb$report$aggregate$handler$component <<- addhandlerchanged(gnb$report$aggregate$text$component,handler=ghreagag,action="component")
    gnb$report$aggregate$handler$component <<- addhandlerchanged(gnb$report$aggregate$text$sign,handler=ghreagag,action="sign")
    #gnb$report$aggregate$handler$plotopt <<- addhandlerchanged(gnb$report$aggregate$text$plotopt,handler=ghreagplot)     #w7: plot options; re-plot
    container
}
reag2 <-
function(container) 
{
    tbl <- list(
            refresh=gaction(    icon="refresh",     label="refresh",   handler = reagupdate),
            snap=gaction(    icon="plot",     label="snap",   handler = ghreagsnap)
            )
    add(container,gtoolbar(tbl))
    container
}
reag1 <-
function(...) 
{
    #reag2(cont=gnb$report$aggregate$perm)                                  #perm <- toolbar
    #stopifnot(nrow(gettab("ga"))>0)
    add(gnb$report$aggregate$graphics$perm,gnb$report$aggregate$graphics$temp,expand=TRUE)
    add(gnb$report$aggregate$perm,reag3(gnb$report$aggregate$temp),expand=TRUE)   #perm <- temp <- content
}
initialrownames <-
function() 
{
#    x <- unique(extract("ga","value1",psz("WHERE domain='pe' AND field1='",reagargmap2row()[1],"'")))
#    if(length(x)==0) x <- 'none'
#    setdiff(x," ")
    0
}
greagtabUpdate <-
function() 
{
    mat <- do.call(what="getga",args=reagarglst())
    if(reagarglst()$cols=="date") mat[is.na(mat)] <- 0
    if(svalue(gnb$report$aggregate$text$transpose)) mat <- t(mat)    
    dfr <- data.frame(mat)
    dfr1 <- cbind(rownames(dfr),dfr)
    rownames(dfr1) <- NULL
    colnames(dfr1) <- c("",colnames(mat))
    if(as.logical(getsione(par="sigfig",tab="gu"))) numberfun <- signif else numberfun <- round
    if(svalue(gnb$report$aggregate$text$cols)=="date" && !'table'%in%svalue(gnb$report$aggregate$text$plotopt)) {
        gnb$report$aggregate$text$wtab <<- dfr1
    } else {
        digits <- as.numeric(extract("si","value","WHERE parameter='digits'"))
        gnb$report$aggregate$text$wtab <<- ggtable(dfr1,digits=digits,fun=numberfun) 
        svalue(gnb$report$aggregate$text$wtab,index=TRUE) <<- 1
        addhandlerdoubleclick(gnb$report$aggregate$text$wtab,handler=ghreagplot)
    }
}
ghreagview <-
function(h,...)    
{
    selectview(svalue(gnb$report$aggregate$text$view))
}
ghreagtableplot <-
function(h,...)
{
    #ghreagrownames(h,...)
    ghreagtable(h,...)
    ghreagplot(h,...)
}
ghreagtable <-
function(h,...)    
{
    on.exit({enabled(gnb$report$aggregate$perm) <<- TRUE})
    reagdisable()                                                                   #set and disable settings as required
    enabled(gnb$report$aggregate$perm) <<- FALSE                                    #disable entire page temporarily
    if(is(gnb$report$aggregate$text$wtab,"guiComponent")) {
        irow <- svalue(gnb$report$aggregate$text$wtab,index=TRUE)                       #then the table
        delete(obj=gnb$report$aggregate$bottom,widget=gnb$report$aggregate$text$wtab)
    }
    if(svalue(gnb$report$aggregate$text$cols)!="date" || 'table'%in%svalue(gnb$report$aggregate$text$plotopt)) {
        greagtabUpdate()
        add(obj=gnb$report$aggregate$bottom,value=gnb$report$aggregate$text$wtab,expand=TRUE)
        if(!is(h,"GtkAction")) svalue(gnb$report$aggregate$text$wtab,index=TRUE) <<- ifelse(isTRUE(!h$action%in%c("w1","w2")),irow,1) #retain row selection unless (i) button action (ii)change is to domain or row
    } else {
        greagtabUpdate()
    }
    #ghreagplot(h,...)
}
ghreagsnap <-
function(...) {snap(ghreagplot)}
ghreagrownames <-
function(h,...)
{
    reagdisable()
    delete(obj=gnb$report$aggregate$text$gfrrownames,widget=gnb$report$aggregate$text$rownames)   
    gnb$report$aggregate$text$rownames <<- gcheckboxgroup(currentrownames())
    svalue(gnb$report$aggregate$text$rownames) <<- setdiff(currentrownames(),"")#c("mktlong","mktshort"))
    add(gnb$report$aggregate$text$gfrrownames,gnb$report$aggregate$text$rownames)
    ghreagdrop(h,...)
}
ghreagplot <-
function(
                    h,
                    #ppt=getsione(parameter="ppt",tab="gu",default=TRUE),
                    ...) #graphics 
{
    ppt <- getsione(parameter="ppt",tab="gu",default=FALSE) # would be better to pass as argument
    delete(gnb$report$aggregate$graphics$perm,gnb$report$aggregate$graphics$temp)
    gnb$report$aggregate$graphics$temp <<- ggroup()
    if(!any(h$action=="file")) ggraphics(container=gnb$report$aggregate$graphics$temp)             #graphics device
    add(gnb$report$aggregate$graphics$perm,gnb$report$aggregate$graphics$temp,expand=TRUE)
    if('none'%in%svalue(gnb$report$aggregate$text$plotopt)) return()
    tab <- gnb$report$aggregate$text$wtab[,,drop=FALSE]
    if(is(gnb$report$aggregate$text$wtab,"guiComponent")) irow <- svalue(gnb$report$aggregate$text$wtab,index=TRUE) else irow<-1
    if(!'multiple'%in%svalue(gnb$report$aggregate$text$plotopt)) {
        tab <- tab[irow,,drop=FALSE]
        jorder <- order(as.numeric(tab[,-1,drop=TRUE])) #for a single row plot, column order derived from that row
    } else {
        iplot <- !tab[,1]%in%c('MEAN','SUM')
        tab <- tab[iplot,,drop=FALSE]
        jorder <- order(as.numeric(tab[irow,-1,drop=TRUE]))  #for multiple, column order derived from selected row
    }
    if(!"sort"%in%svalue(gnb$report$aggregate$text$plotopt)) {
        jorder <- seq(1:(ncol(tab)-1))      #no sort so use columns in sequence
    }
    is.horiz <- ("horizontal"%in%svalue(gnb$report$aggregate$text$plotopt))
    is.cumulative <- ("cumulative"%in%svalue(gnb$report$aggregate$text$plotopt))
    is.timeseries <- svalue(gnb$report$aggregate$text$cols)=="date"
    x <- names(tab[,-1,drop=FALSE])[jorder]
    is.line <- ("line"%in%svalue(gnb$report$aggregate$text$plotopt) && 
                    (suppressWarnings(all(!is.na(as.numeric(x)))) && all(as.character(as.numeric(x))==x) ||
                    svalue(gnb$report$aggregate$text$cols)=="date")
                )
    if(is.timeseries && valda(x)) {
        x <- as.Date(x)
    } else if(is.line) {
        x <- as.numeric(x)
    }
    y <- t(tab[,-1,drop=FALSE][,jorder,drop=FALSE])
    dimnames(y) <- list(colnames(tab)[-1][jorder],tab[,1])
    mode(y) <- "numeric"
    if(is.cumulative) {
        y[] <- apply(y,2,cumsum)
    } 
    abcissa <- svalue(gnb$report$aggregate$text$cols)
    ordinate <- svalue(gnb$report$aggregate$text$domain)
    if(ordinate=="variance") ordinate <- svalue(gnb$report$aggregate$text$variance)
    if(is.line) {
        if(abcissa=="date") abcissa <- ""
        zobj <- zoo(x=y,order.by=x)
        arg <- list(
            x=zobj,
            ylab=ordinate,
            xlab=abcissa,
            plot.type='single',
            lwd=1
            )
        if(!'grey'%in%svalue(gnb$report$aggregate$text$plotopt)) {
            if(ppt) {
                arg[[length(arg)+1]] <- mycol <- c("purple")#POWERPOINT1#
            } else {
                arg[[length(arg)+1]] <- mycol <- rainbow(ncol(zobj),alpha=getsione("linealpha")) #NORMAL#grey((1:ncol(zobj))/ncol(zobj)*.7)#STET#
            }
            names(arg)[length(arg)] <- 'col'
        } else {
            if(ppt) {
                arg[[length(arg)+1]] <- mycol <- c("orange","pink","purple")#POWERPOINT2# 
            } else {
                arg[[length(arg)+1]] <- mycol <- grey((1:ncol(zobj))/ncol(zobj)*.7)#NORMAL#arg[[length(arg)+1]] <- mycol <- STET# 
            }
            names(arg)[length(arg)] <- 'col'
        }
        if(ppt) {
            arg[[length(arg)+1]] <- 2   #POWERPOINT3#
            names(arg)[length(arg)] <- 'lwd'    #POWERPOINT#
        }
        do.call(what="plot",args=arg)
        if("label"%in%svalue(gnb$report$aggregate$text$plotopt)) {
            curves <- vector("list",ncol(zobj))
            for(i in 1:ncol(zobj)) {
                curves[[i]] <- list(x=as.Date(index(zobj)),y=as.numeric(zobj[,i]))
            }
            labcurve(
                curves=curves,
                labels=colnames(zobj),
                col.=mycol
                )
        }
        if("legend"%in%svalue(gnb$report$aggregate$text$plotopt)) {
            legend(
                x='topleft',
                legend=colnames(zobj),
                col=mycol,
                lty=1,
                bty='n')
        }
    } else {
        if(abcissa=='date') { yarg <- t(y) } else {yarg <- t(y)} #[,,drop=TRUE]}
        arg <- list(
            height=yarg,
            names.arg=x,
            horiz=is.horiz,
            #density=30,    #POWERPOINT4#
            border=NA,
            las=1,
            legend="legend"%in%svalue(gnb$report$aggregate$text$plotopt),
            ylab=ifelse(isTRUE(is.horiz),abcissa,ordinate),
            xlab=ifelse(isTRUE(abcissa=="date"),"",ifelse(isTRUE(is.horiz),ordinate,abcissa))
            )
        if(ppt) { arg <- c(arg,list(density=30)) }
        if(!'grey'%in%svalue(gnb$report$aggregate$text$plotopt)) {
            if(ppt) {
                if( !is.null(y) && length(y)>0 && ncol(y)==1) {nicecol <- "orange"} else {nicecol <- rainbow(ncol(y),alpha=getsione("baralpha"),start=0)}#POWERPOINT5##rainbow(ncol(y),alpha=getsione("baralpha")*.7,start=.6)}
            } else {
                if( !is.null(y) && length(y)>0 && ncol(y)==1) {nicecol <- rainbow(ncol(y),alpha=getsione("baralpha")*.7,start=.6)} else {nicecol <- rainbow(ncol(y),alpha=getsione("baralpha"),start=0)} #NORMAL#
            }
            arg[[length(arg)+1]] <- nicecol
            names(arg)[length(arg)] <- 'col'
        }
        if(!'stack'%in%svalue(gnb$report$aggregate$text$plotopt)) {
            arg[[length(arg)+1]] <- TRUE
            names(arg)[length(arg)] <- 'beside'
        }
        if('horizontal'%in%svalue(gnb$report$aggregate$text$plotopt)) {
            arg[[length(arg)+1]] <- 0
            names(arg)[length(arg)] <- 'hadj'
        }
        #arg$ylab<-"weekly variance/%%"
        do.call(what="barplot",args=arg)
    }
    grid(col='darkgray')
}
ghreagdrop <-
function(h,...)
{
    delete(obj=gnb$report$aggregate$graphics$perm,widget=gnb$report$aggregate$graphics$temp)
    if(is(gnb$report$aggregate$text$wtab,"guiComponent")) delete(obj=gnb$report$aggregate$bottom,widget=gnb$report$aggregate$text$wtab)
}
ghreagboxplot <-
function(...) {
    x <- as.matrix(gnb$report$aggregate$text$wtab[,,drop=FALSE])
    mode(x)<-"numeric"
    boxplot(x,ylab=svalue(gnb$report$aggregate$text$domain))#,outline=FALSE)
    grid()
}
ghreagag <-
function(h,...)
{
    on.exit({enabled(gnb$report$aggregate$perm) <<- TRUE})
    enabled(gnb$report$aggregate$perm) <<- FALSE                                    #disable entire page temporarily
    if(h$action%in%c("development","region")) {
        enabled(gnb$report$aggregate$text$region) <<- FALSE
        enabled(gnb$report$aggregate$text$development) <<- FALSE        
        garegion(h$action)
    } else {
        enabled(gnb$report$aggregate$text$component) <<- FALSE
        enabled(gnb$report$aggregate$text$sign) <<- FALSE
        gacomponent(h$action)
    }
}
currentrownames <-
function()
{
    domain <- svalue(gnb$report$aggregate$text$domain,index=TRUE)
    value <- "value1"
    field <- "field1"
    comp <- svalue(gnb$report$aggregate$text$rows,index=TRUE)
    x <- unique(extract("ga",value,psz("WHERE domain='",reagargmap1()[domain],"' AND ",field,"='",reagargmap2row()[comp],"'")))
    x <- setdiff(x," ")
    if(suppressWarnings(all(!is.na(as.numeric(x)))) && all(as.character(as.numeric(x))==x)) x <- x[order(as.numeric(x))]
    x
}
reraupdate <-
function(...,clear=FALSE,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(gnb$report$rank$perm,gnb$report$rank$temp)
    gnb$report$rank$temp <<- ggroup(horiz=TRUE,use.scroll=TRUE)
    gnb$report$rank$graphics <<- listof2() 
    add(gnb$report$rank$graphics$perm,gnb$report$rank$graphics$temp,expand=TRUE)
    add(gnb$report$rank$graphics$temp,gtext(''),expand=TRUE)   #required to anchor the window
    if(clear) {
        add(gnb$report$rank$perm,gnb$report$rank$temp,expand=TRUE)
    } else {
        add(gnb$report$rank$perm,rera3(gnb$report$rank$temp),expand=TRUE)
    }
}
reraarglst <-
function()
{
    list(
        rows=raargmap1()[svalue(gnb$report$rank$text$row,index=TRUE)],
        cols=raargmap1()[svalue(gnb$report$rank$text$col,index=TRUE)],
        domain=svalue(gnb$report$rank$text$domain),
        minv=   list(
                    date= svalue(gnb$report$rank$text$mindate),
                    lag= svalue(gnb$report$rank$text$minlag),
                    comp= svalue(gnb$report$rank$text$mincomponent),
                    qu= svalue(gnb$report$rank$text$minquantile)
                    ),
        maxv=   list(
                    date= svalue(gnb$report$rank$text$maxdate),
                    lag= svalue(gnb$report$rank$text$maxlag),
                    comp= svalue(gnb$report$rank$text$maxcomponent),
                    qu= svalue(gnb$report$rank$text$maxquantile)
                    ),
        variance=svalue(gnb$report$rank$text$variance),
        percent=getsione("percent"),
        annfactor=getsione("annfactor")
        )
}
rera3 <-
function(container) 
{
    if(!chkra()) return(container)
    gnb$report$rank$text$plotopt <<- gcheckboxgroup(c("line","cumulative","horizontal","sort","multiple","stack","grey","legend","label","none"))
    rav <- raval("sort")    #list of all values
    tbl <- glayout()
    gnb$report$rank$text$updateb <<- gbutton("update",handler = ghreratableplot)
    gnb$report$rank$text$mincomponent <<- gcombobox(rav$comp)
    gnb$report$rank$text$minquantile <<-  gcombobox(rav$qu)
    gnb$report$rank$text$minlag <<- gcombobox(rav$lag)
    gnb$report$rank$text$mindate <<- gcombobox(rav$date)
    gnb$report$rank$text$maxcomponent <<- gcombobox(rav$comp)
    gnb$report$rank$text$maxquantile <<-  gcombobox(rav$qu)
    gnb$report$rank$text$maxlag <<- gcombobox(rav$lag)
    gnb$report$rank$text$maxdate <<- gcombobox(rav$date)
    tbl[1,2,anchor=c(-1,0)] <- "min"
    tbl[1,3,anchor=c(-1,0)] <- "max"
    tbl[2,1,anchor=c(-1,0)] <- "component"
    tbl[3,1,anchor=c(-1,0)] <- "quantile"
    tbl[4,1,anchor=c(-1,0)] <- "lag"
    tbl[5,1,anchor=c(-1,0)] <- "date"
    tbl[2,2,anchor=c(-1,0)] <- gnb$report$rank$text$mincomponent
    tbl[3,2,anchor=c(-1,0)] <- gnb$report$rank$text$minquantile
    tbl[4,2,anchor=c(-1,0)] <- gnb$report$rank$text$minlag
    tbl[5,2,anchor=c(-1,0)] <- gnb$report$rank$text$mindate
    tbl[2,3,anchor=c(-1,0)] <- gnb$report$rank$text$maxcomponent
    tbl[3,3,anchor=c(-1,0)] <- gnb$report$rank$text$maxquantile
    tbl[4,3,anchor=c(-1,0)] <- gnb$report$rank$text$maxlag
    tbl[5,3,anchor=c(-1,0)] <- gnb$report$rank$text$maxdate
    dims <- c("component","quantile","lag","date")
    gnb$report$rank$text$row <<- gcombobox(dims)
    gnb$report$rank$text$col <<- gcombobox(dims)
    gnb$report$rank$text$domain <<- gcombobox(c("re","va"))
    gnb$report$rank$text$variance <<- gcombobox(c("variance","volatility"))
    ggtopleft <- gframe(horiz=TRUE)                             #containers
    ggplotopt <-  gframe("plot",horiz=TRUE)                            #ggplotopt - container for plot options
    add(ggtopleft,ggplotopt,expand=FALSE)
    ggtblspin <- gframe("table",horiz=FALSE)
    g2b <- ggroup(horiz=TRUE)
    add(g2b,gnb$report$rank$text$updateb,expand=FALSE)
    add(ggtblspin,g2b,expand=FALSE)
    g2 <- gframe("rows",horiz=FALSE)
    add(ggtblspin,g2,expand=FALSE)                              #spinners
    add(g2,gnb$report$rank$text$row,expand=FALSE)
    g2 <- gframe("cols",horiz=FALSE)
    add(ggtblspin,g2,expand=FALSE)
    add(g2,gnb$report$rank$text$col,expand=FALSE)
    g2 <- gframe("domain",horiz=FALSE)
    add(ggtblspin,g2,expand=FALSE)
    add(g2,gnb$report$rank$text$domain,expand=FALSE)
    g2 <- gframe("variance",horiz=FALSE)
    add(ggtblspin,g2,expand=FALSE)
    add(g2,gnb$report$rank$text$variance,expand=FALSE)
    add(ggtopleft,ggtblspin,expand=FALSE)
    g2 <- gframe("average over",horiz=FALSE)
    add(ggtopleft,g2,expand=FALSE)
    add(g2,tbl,expand=TRUE)
    g1f <- gframe("plot options")                               #plot options
    g2 <- ggroup(horiz=FALSE)
    g2b <- ggroup()
    add(g2,g2b,expand=FALSE)
    add(g2b,gbutton("plot",handler = ghreraplot,action="refresh"),expand=FALSE)
    add(g2,g1f,expand=FALSE)
    add(ggplotopt,g2,expand=FALSE)
    add(g1f,gnb$report$rank$text$plotopt,expand=FALSE)
    gnb$report$rank$bottom <<- ggroup(horiz=TRUE)          
    gpghoriz <- gpanedgroup(widget1=ggtopleft,widget2=gnb$report$rank$graphics$perm,horiz=TRUE)
    gpgvert <- gpanedgroup(widget1=gpghoriz,widget2=gnb$report$rank$bottom,horiz=FALSE)
    gpghoriz@widget@widget["position"] <- 570 
    gpgvert@widget@widget["position"] <- 400 
    add(container,gpgvert,expand=TRUE)                         #place all into container
    svalue(gnb$report$rank$text$col) <<- "quantile"              #svalue() initial assignments
    svalue(gnb$report$rank$text$minlag) <<- "0"
    svalue(gnb$report$rank$text$maxlag) <<- "1"
    svalue(gnb$report$rank$text$mindate) <<- min(rav$date)
    svalue(gnb$report$rank$text$maxdate) <<- max(rav$date)
    svalue(gnb$report$rank$text$minquantile) <<- "1"
    svalue(gnb$report$rank$text$maxquantile) <<- max(rav$qu)
    if("ra"%in%dirtab()) {
        greratabUpdate()
        add(obj=gnb$report$rank$bottom,value=gnb$report$rank$text$wtab,expand=TRUE)
    }
    addhandlerchanged(gnb$report$rank$text$maxcomponent,handler=ghrera1)
    addhandlerchanged(gnb$report$rank$text$maxquantile,handler=ghrera1)
    addhandlerchanged(gnb$report$rank$text$maxlag,handler=ghrera1)
    addhandlerchanged(gnb$report$rank$text$maxdate,handler=ghrera1)
    addhandlerchanged(gnb$report$rank$text$mincomponent,handler=ghrera2)
    addhandlerchanged(gnb$report$rank$text$minquantile,handler=ghrera2)
    addhandlerchanged(gnb$report$rank$text$minlag,handler=ghrera2)
    addhandlerchanged(gnb$report$rank$text$mindate,handler=ghrera2)
    container
}
rera2 <-
function(container) 
{
    tbl <- list(refresh=gaction(    icon="refresh",     label="refresh",   handler = reraupdate))
    add(container,gtoolbar(tbl))
    container
}
rera1 <-
function(...) 
{
    #rera2(cont=gnb$report$rank$perm)                                  #perm <- toolbar
    add(gnb$report$rank$graphics$perm,gnb$report$rank$graphics$temp,expand=TRUE)
    add(gnb$report$rank$perm,rera3(gnb$report$rank$temp),expand=TRUE)   #perm <- temp <- content
}
raargmap1 <-
function(){ c("comp","qu","lag","date")}
greratabUpdate <-
function() 
{
    mat <- do.call(what="getra",args=reraarglst())
    dfr <- data.frame(mat)
    dfr1 <- cbind(rownames(dfr),dfr)
    rownames(dfr1) <- NULL
    colnames(dfr1) <- c("",colnames(mat))
    if(as.logical(extract("si","value","WHERE parameter='sigfig'"))) numberfun <- signif else numberfun <- round
    digits <- as.numeric(extract("si","value","WHERE parameter='digits'"))
    gnb$report$rank$text$wtab <<- ggtable(dfr1,digits=digits,fun=numberfun) 
    svalue(gnb$report$rank$text$wtab) <<- 1
    addhandlerdoubleclick(gnb$report$rank$text$wtab,handler=ghreraplot)
}
ghreratableplot <-
function(h,...)
{
    on.exit({enablegu(enable=TRUE)})   
    enablegu(enable=FALSE)
    ghreratable(h,...)
    ghreraplot(h,...)
}
ghreratable <-
function(h,...)    
{
    on.exit({enablegu(enable=TRUE)})   
    enablegu(enable=FALSE)
    irow <- svalue(gnb$report$rank$text$wtab,index=TRUE) 
    delete(obj=gnb$report$rank$bottom,widget=gnb$report$rank$text$wtab)
    greratabUpdate()
    add(obj=gnb$report$rank$bottom,value=gnb$report$rank$text$wtab,expand=TRUE)
}
ghreraplot <-
function(h,...) #graphics 
{    
    delete(gnb$report$rank$graphics$perm,gnb$report$rank$graphics$temp)
    gnb$report$rank$graphics$temp <<- ggroup()
    ggraphics(container=gnb$report$rank$graphics$temp)             #graphics device
    add(gnb$report$rank$graphics$perm,gnb$report$rank$graphics$temp,expand=TRUE)
    if('none'%in%svalue(gnb$report$rank$text$plotopt)) return()
    tab <- gnb$report$rank$text$wtab[,,drop=FALSE]
    irow <- svalue(gnb$report$rank$text$wtab,index=TRUE)
    if(!'multiple'%in%svalue(gnb$report$rank$text$plotopt)) {
        tab <- tab[irow,,drop=FALSE]
        jorder <- order(as.numeric(tab[,-1,drop=TRUE])) #for a single row plot, column order derived from that row
    } else {
        jorder <- order(as.numeric(tab[irow,-1,drop=TRUE]))  #for multiple, column order derived from selected row
    }
    if(!"sort"%in%svalue(gnb$report$rank$text$plotopt)) {
        jorder <- seq(1:(ncol(tab)-1))      #no sort so use columns in sequence
    }
    is.horiz <- ("horizontal"%in%svalue(gnb$report$rank$text$plotopt))
    is.cumulative <- ("cumulative"%in%svalue(gnb$report$rank$text$plotopt))
    is.timeseries <- svalue(gnb$report$rank$text$col)=="date"
    is.lagseries <- svalue(gnb$report$rank$text$col)=="lag"
    is.line <- ("line"%in%svalue(gnb$report$rank$text$plotopt) && (is.timeseries || is.lagseries))
    if(is.timeseries) {
        x <- as.Date(names(tab[,-1,drop=FALSE])[jorder])
        orderBy <- x
    } else if(is.lagseries) {
        x <- as.numeric(names(tab[,-1,drop=FALSE])[jorder])
        orderBy <- x
    } else {
        x <- names(tab[,-1,drop=FALSE])[jorder]
    }
    y <- t(tab[,-1,drop=FALSE][,jorder,drop=FALSE])
    dimnames(y) <- list(colnames(tab)[-1][jorder],tab[,1])
    mode(y) <- "numeric"
    if(is.cumulative) {
        y <- apply(y,2,cumsum)
    } 
    abcissa <- svalue(gnb$report$rank$text$col)
    ordinate <- svalue(gnb$report$rank$text$domain)
    if(ordinate=="va") ordinate <- svalue(gnb$report$rank$text$variance)
    if(is.line) {
        if(abcissa=="date") abcissa <- ""
        zobj <- zoo(x=y,order.by=orderBy)
        arg <- list(
            x=zobj,
            ylab=ordinate,
            xlab=abcissa,
            plot.type='single'
            )
        if(!'grey'%in%svalue(gnb$report$rank$text$plotopt)) {
            arg[[length(arg)+1]] <- mycol <- rainbow(ncol(zobj),alpha=getsione("linealpha"))
            names(arg)[length(arg)] <- 'col'
        } else {
            arg[[length(arg)+1]] <- mycol <- grey((1:ncol(zobj))/ncol(zobj)*.7)
            names(arg)[length(arg)] <- 'col'
        }
        do.call(what="plot",args=arg)
        if("label"%in%svalue(gnb$report$rank$text$plotopt)) {
            curves <- vector("list",ncol(zobj))
            for(i in 1:ncol(zobj)) {
                curves[[i]] <- list(x=as.Date(index(zobj)),y=as.numeric(zobj[,i]))
            }
            labcurve(
                curves=curves,
                labels=colnames(zobj),
                col.=mycol
                )
        }
        if("legend"%in%svalue(gnb$report$rank$text$plotopt)) {
            legend(
                x='topleft',
                legend=colnames(zobj),
                col=mycol,
                lty=1,
                bty='n')
        }
    } else {
        if(abcissa=='date') { yarg <- t(y) } else {yarg <- t(y)[,,drop=TRUE]}
        arg <- list(
            height=yarg,
            names.arg=x,
            horiz=is.horiz,
            border=NA,
            las=1,
            legend="legend"%in%svalue(gnb$report$rank$text$plotopt),
            ylab=ifelse(is.horiz,abcissa,ordinate),
            xlab=ifelse(is.horiz,ordinate,abcissa)
            )
        if(!'grey'%in%svalue(gnb$report$rank$text$plotopt)) {
            if(length(y)>0 && ncol(y)==1) {nicecol <- rainbow(ncol(y),alpha=getsione("baralpha")*.7,start=.6)} else {nicecol <- rainbow(ncol(y),alpha=getsione("baralpha"),start=0)}
            arg[[length(arg)+1]] <- nicecol
            names(arg)[length(arg)] <- 'col'
        }
        if(!'stack'%in%svalue(gnb$report$rank$text$plotopt)) {
            arg[[length(arg)+1]] <- TRUE
            names(arg)[length(arg)] <- 'beside'
        }
        if('horizontal'%in%svalue(gnb$report$rank$text$plotopt)) {
            arg[[length(arg)+1]] <- 0
            names(arg)[length(arg)] <- 'hadj'
        }
        do.call(what="barplot",args=arg)
    }
}
ghrera2 <-
function(h,...)
{
    if(svalue(gnb$report$rank$text$maxcomponent)<svalue(gnb$report$rank$text$mincomponent)) svalue(gnb$report$rank$text$maxcomponent) <<- svalue(gnb$report$rank$text$mincomponent)
    if(svalue(gnb$report$rank$text$maxquantile)<svalue(gnb$report$rank$text$minquantile)) svalue(gnb$report$rank$text$maxquantile) <<- svalue(gnb$report$rank$text$minquantile)
    if(svalue(gnb$report$rank$text$maxlag)<svalue(gnb$report$rank$text$minlag)) svalue(gnb$report$rank$text$maxlag) <<- svalue(gnb$report$rank$text$minlag)
    if(svalue(gnb$report$rank$text$maxdate)<svalue(gnb$report$rank$text$mindate)) svalue(gnb$report$rank$text$maxdate) <<- svalue(gnb$report$rank$text$mindate)
}
ghrera1 <-
function(h,...)
{
    if(svalue(gnb$report$rank$text$mincomponent)>svalue(gnb$report$rank$text$maxcomponent)) svalue(gnb$report$rank$text$mincomponent) <<- svalue(gnb$report$rank$text$maxcomponent)
    if(svalue(gnb$report$rank$text$minquantile)>svalue(gnb$report$rank$text$maxquantile)) svalue(gnb$report$rank$text$minquantile) <<- svalue(gnb$report$rank$text$maxquantile)
    if(svalue(gnb$report$rank$text$minlag)>svalue(gnb$report$rank$text$maxlag)) svalue(gnb$report$rank$text$minlag) <<- svalue(gnb$report$rank$text$maxlag)
    if(svalue(gnb$report$rank$text$mindate)>svalue(gnb$report$rank$text$maxdate)) svalue(gnb$report$rank$text$mindate) <<- svalue(gnb$report$rank$text$maxdate)
}
papaupdate <-
function(...,clear=FALSE,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$parameter$perm,widget=gnb$parameter$temp)
    gnb$parameter$temp <<- ggroup(use.scroll=TRUE)
    if(clear) {
        add(gnb$parameter$perm,gnb$parameter$temp,expand=TRUE)
    } else {
        add(gnb$parameter$perm,papa3(gnb$parameter$temp),expand=TRUE)
    }
}
papa3 <-
function(container) 
{
    gnb$parameter$gpapa <<- gdf(gettab("si",qual="ORDER BY mytable, parameter, ipa"),do.subset=FALSE)  #global because editable, hence accessed by sisave
    add(container,gnb$parameter$gpapa,expand=TRUE)
    container
}
papa2 <-
function(container) 
{
    tbl <- list(
        save=gaction(       icon="save",        label="save",      handler = ghpapa1),
        read=gaction(       icon="open",        label="read",   handler = ghpapa3),
        refresh=gaction(    icon="refresh",     label="refresh",   handler = ghpapa2)
        )
    add(container,gtoolbar(tbl))
    container
}
papa1 <-
function(...) 
{
    papa2(cont=gnb$parameter$perm)                                  #perm <- toolbar
    add(gnb$parameter$perm,papa3(gnb$parameter$temp),expand=TRUE)   #perm <- temp <- content
}
ghpapa3 <-
function(h,...) {
    newsi()
    addsi()
    papaupdate()
}
ghpapa2 <-
function(h,...) {
    papaupdate()
}
ghpapa1 <-
function(h,...) 
{
    tab <- gnb$parameter$gpapa[,,drop=FALSE]
    si <- tab[!(apply(is.na(tab),1,any) | apply(tab=="NA",1,any)),]
    declare("si",decsi())
    sqlSave(channel=DBcon,      
            dat=data.frame(si),
            tablename="si",
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
    rucuupdate()
}
lliliclass <-
function(...) {val <- svalue(gnb$lib$gliliradio);if(val=="ALL") val <- "__";val}
liliupdateinnerrightgroup <-
function(...) 
{
    delete(obj=gnb$lib$gliliinnerrightgroup,widg=gnb$lib$gli)
        code <- gettab("li",qual=psz("WHERE beforeclass LIKE '",lliliclass(),"'"))[,"code"]
        if(length(code)==0) code <- ""
        gnb$lib$gli <<- gtext(code)
    add(obj=gnb$lib$gliliinnerrightgroup,value=gnb$lib$gli,expand=TRUE)
}
liliupdate <-
function(...,clear=FALSE,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$lib$perm,widget=gnb$lib$temp)
    gnb$lib$temp <<- ggroup(horiz=TRUE,use.scroll=TRUE)
    if(clear) {
        add(gnb$lib$perm,gnb$lib$temp,expand=TRUE)
    } else {
        add(gnb$lib$perm,lili3(gnb$lib$temp),expand=TRUE)
    }
}
lili3 <-
function(container) 
{
        innerleft <- gframe(horiz=FALSE)
            gnb$lib$gliliradio <<- gradio(c("ALL",extract("li","DISTINCT beforeclass")),selected=1) 
                addhandlerchanged(gnb$lib$gliliradio,handler=function(...) {liliupdateinnerrightgroup()}) #lazy evaluation makes this work
        add(innerleft,gnb$lib$gliliradio,expand=FALSE)
    add(container,innerleft,expand=FALSE)
        gnb$lib$gliliinnerrightgroup <<- ggroup(horiz=FALSE)
            gnb$lib$gli <<- gtext(gettab("li",qual=psz("WHERE beforeclass LIKE '",lliliclass(),"'"))[,"code"])
        add(obj=gnb$lib$gliliinnerrightgroup,value=gnb$lib$gli,expand=TRUE)
    add(container,gnb$lib$gliliinnerrightgroup,expand=TRUE)
    container
}
lili2 <-
function(container) 
{
    tbl <- list(
        read=gaction(    icon="open",     label="read",   handler = ghlili2),
        archive=gaction(       icon="floppy",        label="batchread",      handler = ghlili2, action="archive"),
        refresh=gaction(    icon="refresh",     label="refresh",   handler = liliupdate)
        )
    add(container,gtoolbar(tbl))
    container
}
lili1 <-
function(...) 
{
    lili2(cont=gnb$lib$perm)                            #perm <- toolbar
    add(gnb$lib$perm,lili3(gnb$lib$temp),expand=TRUE)   #perm <- temp <- content   
}
ghlili2 <-
function(h,...,archive=FALSE) 
{
    #mess <- gfile(type="selectdir")    #this works but because cannot set initial dir, it is laborious
    #filepath <- gsub(
    #    pattern='\\\\',
    #    replacement='/',
    #    x=mess
    #    )
    filepath <- psz(Aaroot,"lib/")
    if(nchar(filepath)>0) {
        newli(filepath,archive=(!is.null(h$action) && h$action=="archive"))
        liliupdate()
    }
    ararupdate(disable=FALSE)
}
unseupdate <-
function(...,clear=FALSE,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$universe$security$perm,widget=gnb$universe$security$temp)
    gnb$universe$security$temp <<- ggroup(use.scroll=TRUE)
    gnb$universe$security$graphics <<- listof2() 
    add(gnb$universe$security$graphics$perm,gnb$universe$security$graphics$temp,expand=TRUE)
    add(gnb$universe$security$graphics$temp,gtext(''),expand=TRUE) 
    if(clear) {
        add(gnb$universe$security$perm,gnb$universe$security$temp,expand=TRUE)
    } else {
        add(gnb$universe$security$perm,unse3(gnb$universe$security$temp),expand=TRUE)
    }
}
unseplot <-
function(h,...) #graphics 
{
    delete(gnb$universe$security$graphics$perm,gnb$universe$security$graphics$temp)
    gnb$universe$security$graphics$temp <<- ggroup()
    ggraphics(container=gnb$universe$security$graphics$temp)             #graphics device
    add(gnb$universe$security$graphics$perm,gnb$universe$security$graphics$temp,expand=TRUE)
    xx<-rnorm(1e7)
    imgzoo(suzoo(),col="purple")
}
unse3 <-
function(container) 
{
    lg <- ggroup()
    rg <- ggroup()
    pg <- gpanedgroup(widget1=lg,widget2=gnb$universe$security$graphics$perm,horiz=TRUE)
    pg@widget@widget["position"] <- 400 
    tab <- gtable(extract("su INNER JOIN aaxs ON su.bui=aaxs.bui","su.bui, su.date, aaxs.name"))
    addHandlerDoubleclick(
        obj=tab,
        handler=ghdisplaystock)
    add(lg,tab,expand=TRUE)
    add(container,pg,expand=TRUE)
    container
}
unse2 <-
function(container) 
{
    tbl <- list(
        refresh=gaction(    icon="refresh",     label="refresh",   handler = unseupdate),
        image=gaction(    icon="plot",     label="image",   handler = unseplot)
        )
    add(container,gtoolbar(tbl))
    container
}
uncotrupdate <-
function(...,clear=FALSE,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$universe$constituent$tree$perm,widget=gnb$universe$constituent$tree$temp)
    gnb$universe$constituent$tree$temp <<- ggroup(use.scroll=TRUE)
    if(clear) {
        add(gnb$universe$constituent$tree$perm,gnb$universe$constituent$tree$temp,expand=TRUE)
    } else {
        add(gnb$universe$constituent$tree$perm,uncotr3(gnb$universe$constituent$tree$temp),expand=TRUE)
    }
}
uncotr4 <-
function(classification="b")
{
    `offspring` <- function(
                    path,
                    user.data=NULL,
                    ...
                    )
    {
        if(user.data=="g") {
            sector <- "gics_industry_group_name"
            industry <- "gics_industry_name"
            subindustry <- "gics_sub_industry_name"
            indtab1 <- "aagindtree1"
            indtab2 <- "aagindtree2"
        } else {
            sector <- "industry_sector"
            industry <- "industry_group"
            subindustry <- "industry_subgroup"
            indtab1 <- "aaindtree1"
            indtab2 <- "aaindtree2"
        }
        uniquelevel1 <- extract(indtab1,paste("DISTINCT",sector))
        uniquelevel2 <- extract(indtab1,paste("DISTINCT",industry))
        uniquelevel3 <- extract(indtab2,paste("DISTINCT",subindustry))
        if(is.null(path)) {
            tmp1 <- uniquelevel1
            tmp2 <- data.frame(tmp1,subclass=TRUE) 
        } else if (length(path)==1) { 
            tmp1 <- extract(indtab1,psz("DISTINCT ",industry),psz(" WHERE ",sector," = '",path[length(path)],"'"))
            tmp2 <- data.frame(tmp1,subclass=TRUE) 
        } else if (length(path)==2) { 
            tmp1 <- extract(indtab2,psz("DISTINCT ",subindustry),psz(" WHERE ",industry," = '",path[length(path)],"'"))
            hasmembers <- NULL
            for(i in tmp1) hasmembers <- c(hasmembers,nrow(extract("cu INNER JOIN aaxs ON cu.bui=aaxs.bui ","DISTINCT cu.bui, aaxs.name",psz(" WHERE ",subindustry," = '",i,"'")))>0)
            tmp2 <- data.frame(tmp1,subclass=hasmembers) 
        }  else if (length(path)==3) {
            tmp1 <- extract("cu INNER JOIN aaxs ON cu.bui=aaxs.bui ","DISTINCT cu.bui, aaxs.name",psz(" WHERE ",subindustry," = '",path[length(path)],"'"))
            tmp2 <- data.frame(id=apply(as.matrix(tmp1),1,paste,collapse="    "),subclass=FALSE) 
        }
        tmp2
    }
    `hasOffspring` <- function(
                    children,
                    user.data=NULL, 
                    ...) 
    {
        return(children$subclass)
    }
    `icon.FUN` <- function(
                children,
                user.data=NULL, 
                ...) 
    {
        x <- rep("file",length= nrow(children))
        x[children$subclass] <- "rarrow"
        return(x)
    }
    if(classification=="g") windowname <- "GICS" else windowname <- "Bloomberg"
    x <- gtree(
        offspring = offspring, 
        #hasOffspring=hasOffspring,
        offspring.data = classification,
        col.types = NULL, 
        icon.FUN = icon.FUN, 
        chosencol = 1, 
        multiple = FALSE,
        handler = NULL, 
        action = NULL
        )
    x
}
uncotr3 <-
function(container) 
{
    tr1 <- uncotr4(classification="b")
    tr2 <- uncotr4(classification="g")
    addHandlerDoubleclick(
        obj=tr1,
        handler=ghdisplaystock)
    addHandlerDoubleclick(
        obj=tr2,
        handler=ghdisplaystock)
    pg <- ggroup(horiz=TRUE)
    add(pg,tr1,expand=TRUE)
    add(pg,tr2,expand=TRUE)
    add(container,pg,expand=TRUE)
    container
}
uncotr2 <-
function(container) 
{
    tbl <- list(
        refresh=gaction(    icon="refresh",     label="refresh",   handler = uncotrupdate)
        )
    add(container,gtoolbar(tbl))
    container
}
uncotr1 <-
function(...) 
{
    uncotr2(cont=gnb$universe$constituent$tree$perm)                         #perm <- toolbar
    add(gnb$universe$constituent$tree$perm,uncotr3(gnb$universe$constituent$tree$temp),expand=TRUE) #perm <- temp <- content
}
uncoedupdate <-
function(...,clear=FALSE,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$universe$constituent$editor$perm,widget=gnb$universe$constituent$editor$temp)
    gnb$universe$constituent$editor$temp <<- ggroup(use.scroll=TRUE)
    if(clear) {
        add(gnb$universe$constituent$editor$perm,gnb$universe$constituent$editor$temp,expand=TRUE)
    } else {
        add(gnb$universe$constituent$editor$perm,uncoed3(gnb$universe$constituent$editor$temp),expand=TRUE)
    }
}
uncoedsave <-
function(h,...) 
{ 
    on.exit({enablegu(enable=TRUE);pt("...updated")})
    enablegu(enable=FALSE)
    bui <- substr(gnb$universe$constituent$editor$gtab[which(visible(gnb$universe$constituent$editor$gtab)),1,drop=TRUE],1,18)
    mysqlQuery(psz("DELETE FROM cu WHERE bui NOT IN (",fldlst(bui,"'","'"),")"))
    dersu()
    uncoedupdate(disable=FALSE)
    uncotaupdate(disable=FALSE)
    uncotrupdate(disable=FALSE)
    unseupdate(disable=FALSE)
    ararupdate(disable=FALSE)
}
uncoed3 <-
function(container) 
{
    if(!("cu"%in%dirtab())) {return(container)}
    mytab <- cusumbybui(sizefilter="n")
    gnb$universe$constituent$editor$gtab <<- gdf(mytab)#,do.subset=TRUE)  #global because editable, hence accessed by cusave
    addHandlerDoubleclick(
        obj=gnb$universe$constituent$editor$gtab,
        handler=ghdisplaystock)
    add(container,gnb$universe$constituent$editor$gtab,expand=TRUE)
    container
}
uncoed2 <-
function(container) 
{
    tbl <- list(
        save=gaction(       icon="save",        label="save",      handler = uncoedsave),
        refresh=gaction(    icon="refresh",     label="refresh",   handler = uncoedupdate)
        )
    add(container,gtoolbar(tbl))
    container
}
uncoed1 <-
function(...) 
{
    uncoed2(cont=gnb$universe$constituent$editor$perm)                         #perm <- toolbar
    add(gnb$universe$constituent$editor$perm,uncoed3(gnb$universe$constituent$editor$temp),expand=TRUE) #perm <- temp <- content
}
cusumbybui <-
function(sizefilter=c("none","big"))
{
    sizefilter <- match.arg(sizefilter)
    sizefilter <- switch(sizefilter,none="",big=" WHERE cu.big OR cu.bigish ")
    xsfields <- fldlst(c('countryfull','industry_sector','weightmax','name','countryissue','gics_sub_industry_name','eqy_prim_exch','industry_group','industry_subgroup','crncy','eqy_fiscal_yr_end'),pre="aaxs.",postf="")
    cufields <- fldlst(c("cu.bui","MIN(cu.date) AS t1","MAX(cu.date) AS t2","cu.bti","cu.btk"))
    extract("cu INNER JOIN aaxs on cu.bui=aaxs.bui",psz(cufields,",",xsfields),psz(sizefilter," GROUP BY bui ORDER BY countryfull ASC, industry_sector ASC, weightmax DESC"))
}
uncotaupdate <-
function(...,clear=FALSE,disable=TRUE) {
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$universe$constituent$table$perm,widget=gnb$universe$constituent$table$temp)
    gnb$universe$constituent$table$temp <<- ggroup(use.scroll=TRUE)
    if(clear) {
        add(gnb$universe$constituent$table$perm,gnb$universe$constituent$table$temp,expand=TRUE)
    } else {
        add(gnb$universe$constituent$table$perm,uncota3(gnb$universe$constituent$table$temp),expand=TRUE)
    }
}
uncota3 <-
function(container) {
    gt <- gtable(cusumbybui(sizefilter="b"))
    addHandlerDoubleclick(
        obj=gt,
        handler=ghdisplaystock)
    add(container,gt,expand=TRUE,horiz=FALSE)
    container
}
uncota2 <-
function(container) {
    tbl <- list(
        refresh=gaction(    icon="refresh",     label="refresh",   handler = uncotaupdate)
        )
    add(container,gtoolbar(tbl))
    container
}
ghdisplaystock <-
function(h,...) {
    bui <- substr(svalue(h$obj),1,18)
    win <- ggroup(cont=gwindow("Stock detail"),expand=TRUE,horizontal=FALSE)
    dfr <- extract("aaxs","*",psz("WHERE bui = '",bui,"'"))
    tab <- t(dfr)
    rownames(tab) <- colnames(dfr)
    add(win,gdf(tab),expand=TRUE,horizontal=FALSE,use.scroll=FALSE) 
}
uninupdate <-
function(...,clear=FALSE,disable=TRUE) 
{
    if(disable) {
        on.exit({enablegu(enable=TRUE)})   
        enablegu(enable=FALSE)
    }
    delete(obj=gnb$universe$index$perm,widget=gnb$universe$index$temp)
    gnb$universe$index$temp <<- ggroup(use.scroll=TRUE,horiz=FALSE)
    if(clear) {
        add(gnb$universe$index$perm,gnb$universe$index$temp,expand=TRUE)
    } else {
        add(gnb$universe$index$perm,unin3(gnb$universe$index$temp),expand=TRUE)
    }
}
unindusa <-
function(h,...    #'save' handler
        ) 
{
    on.exit({enablegu(enable=TRUE)})   
    enablegu(enable=FALSE)
    checkboxgroup <- gnb$universe$index$gcbg
    spinbutton1 <- gnb$universe$index$gsb1
    spinbutton2 <- gnb$universe$index$gsb2
    icheck <- !is.na(as.numeric(lapply(lapply(checkboxgroup,svalue),nchar)))
    btiselected <- as.character(gnb$universe$index$gtab[icheck,c("ticker")])
    startyear <- unlist(lapply(spinbutton1,svalue))
    endyear <- unlist(lapply(spinbutton2,svalue))
    startmonthday <- endmonthday <- endyear*NA
    for(i in seq_along(startyear)) {
        startmonthday[i] <- substr(extract("aauniv","DISTINCT date",psz("WHERE YEAR(date) = '",startyear[i],"' ORDER BY date ASC LIMIT 1")),5,10)
        endmonthday[i] <- substr(extract("aats","DISTINCT date",psz("WHERE YEAR(date) = '",endyear[i],"' ORDER BY date DESC LIMIT 1")),5,10)
    }
    gnb$universe$index$gtab[,"start"] <<- psz(unlist(lapply(spinbutton1,svalue)),startmonthday)
    gnb$universe$index$gtab[,"end"] <- psz(unlist(lapply(spinbutton2,svalue)),endmonthday)
    du <- gnb$universe$index$gtab[icheck,c("start","end","ticker","include"),drop=FALSE]
    colnames(du) <- c("t1","t2","bti","include")
    mincol <- apply(du[,1:2],1,min)
    maxcol <- apply(du[,1:2],1,max)
    du[,"t1"] <- mincol
    du[,"t2"] <- maxcol
    du[,"include"] <- 1
    newdu()
    sqlSave(channel=DBcon,
            dat=data.frame(du),    
            tablename="du",
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
#    derfi(myfixa=c("name","countryfull","industry_sector","industry_subgroup","gics_sub_industry_name"))
    derfi(myfixa=c("name","countryfull","industry_sector","industry_subgroup"))
    dercu()
    dersu()
    uncotaupdate(disable=FALSE)
    uncoedupdate(disable=FALSE)
#    uncotrupdate(disable=FALSE)
    unseupdate(disable=FALSE,clear=TRUE)
    ararupdate(disable=FALSE)
}
unin3 <-
function(container)
{
    gt <- edtdu(alldu())
        g2b <- ggroup(horiz=TRUE)
            add(g2b,gbutton("all",handler=ghunin1,action="all"),expand=FALSE)
            add(g2b,gbutton("MSCIemer",handler=ghunin1,action="MSCIemer"),expand=FALSE)
            add(g2b,gbutton("Tradebook",handler=ghunin1,action="Tradebook"),expand=FALSE)
            add(g2b,gbutton("SFMplus4",handler=ghunin1,action="SFMplus4"),expand=FALSE)
            add(g2b,gbutton("local",handler=ghunin1,action="local"),expand=FALSE)
            add(g2b,gbutton("none",handler=ghunin1,action="none"),expand=FALSE)
        add(container,g2b,expand=FALSE) 
    add(container,gt,expand=TRUE)
    container
}
unin2 <-
function(container) 
{
    tbl <- list(
        refresh=gaction(    icon="refresh",     label="refresh",    handler = uninupdate),
        save=gaction(       icon="save",        label="save",       handler = unindusa)
        )
    add(container,gtoolbar(tbl))
}
unin1 <-
function(...) 
{
    unin2(cont=gnb$universe$index$perm)                                     #perm <- toolbar
    add(gnb$universe$index$perm,unin3(gnb$universe$index$temp),expand=TRUE) #perm <- temp <- content
}
uncanned <-
function(sel=c("all","none","MSCIemer","Tradebook","local","SFMplus4"))
{
    sel <- match.arg(sel)
    tkr <- switch(sel,
        all=c(gnb$universe$index$gtab[,"ticker"]),
        none=NULL,
        MSCIemer=c("TW","PH","MY","KR","ID","TH","MX","PE","BR","CL","CO","IL","ZA","MA","RU","EG","TR","ZA","PL","HU","CZ"),
        Tradebook=bbtrbk(),
        SFMplus4=c("US","JP","GB","HK","FR","DE","CH","SE","IT","ES","NL","FI","NO","DK","BE","AT","PT","IE","AU","KR","SG","CA"),
        local=as.character(extract("aaindices","bti","WHERE 2<CHAR_LENGTH(bti)"))
    )
    tab <- gnb$universe$index$gtab
    iselect <- match(tkr,tab[,"ticker"])
    for(i in iselect) {
        svalue(gnb$universe$index$gcbg[[i]]) <<- TRUE
        svalue(gnb$universe$index$gsb1[[i]]) <<- substr(tab[i,"start"],1,4)
        svalue(gnb$universe$index$gsb2[[i]]) <<- substr(tab[i,"end"],1,4)
    }
    for(i in setdiff(1:nrow(tab),iselect)) {
        svalue(gnb$universe$index$gcbg[[i]]) <<- FALSE
    }
}
regionflags <-
function(co=bbtrbk(),simple=TRUE) {
    index <- gettab("aaindices")
    region <- unique(index[,"region"])
    memb <- NULL
    for(i in seq_along(region)) {
        ireg <- which(index[,"region"]==region[i])
        memb <- cbind(memb,as.numeric(co%in%index[ireg,"bti"]))
    }
    stopifnot(all(apply(memb,1,sum)==1))    #country must have 1:1 with region
    colnames(memb) <- region
    rownames(memb) <- co
    if(simple) {
        res <- memb%*%region3()
    } else {
        res <- memb
    }
    res
}
region3 <-
function() {
    matrix(c(c(0,1,0,1,0),c(1,0,0,0,0),c(0,0,1,0,1)),5,3,dimnames=list(c("Europe","Far East","Latin America","MEA","North America"),c("Other","Europe","Americas")))
}
ghunin1 <-
function(h,...){
    uncanned(sel=h$action)
}
euroiso <-
function() 
{
c('AT','BE','CY','CZ','DK','EE','FI','FR','DE','GR','IE','IT','NL','PL','PT','ES')
}
edtdu <-
function(
            tab,
            spin1="start",
            spin2="end",
            check="name",
            status="include",
            sort1="country",
            sort2="region",
            desc1="country",
            desc2="ticker"
            ) 
{
    width <- c(20,60,15,15,15)
    stopifnot(all(c(spin1,spin2,check,status,sort1,sort2,desc1,desc2)%in%colnames(tab)))
    groups <- unique(tab[,sort2])
    groupcontainer <- ggroup(horiz=FALSE)
    ii <- 0
    gnb$universe$index$gcbg <<- vector("list",nrow(tab))
    gnb$universe$index$gsb1 <<- vector("list",nrow(tab))
    gnb$universe$index$gsb2 <<- vector("list",nrow(tab))
    gnb$universe$index$gtab <<- tab
    du <- getdu() #initialise selections using du
    for(frame in groups) {
        tbl <- glayout(container=gframe(text=toupper(frame),cont=groupcontainer))
        for(j in 1:5) {tbl[1,j,anchor=c(-1,0)] <- substr(paste(c("","",spin1,spin2,desc2),paste(rep(" ",100),collapse="")),1,width[j])[j]}
        igroup <- which(tab[,sort2]==frame)
        for(i in seq(along=igroup)) {   #column order is 1desc1, 2check, 3spin1, 4spin2, 5desc2
            tbl[i+1,1,anchor=c(-1,0)] <- as.character(tab[igroup[i],desc1])
            ii <- ii+1
 
            idxchk  <- gnb$universe$index$gcbg[[ii]] <<- gcheckboxgroup(tab[igroup[i],check])
            spinmin <- gnb$universe$index$gsb1[[ii]] <<- gspinbutton(tab[igroup[i],spin1],tab[igroup[i],spin2])
            spinmax <- gnb$universe$index$gsb2[[ii]] <<- gspinbutton(tab[igroup[i],spin1],tab[igroup[i],spin2])
            if(tab[igroup[i],"ticker"]%in%du[,"bti"]) {
                jj <- match(tab[igroup[i],"ticker"],du[,"bti"])
                svalue(idxchk) <- 1
                svalue(spinmin) <- as.numeric(substr(du[jj,"t1"],1,4))
                svalue(spinmax) <- as.numeric(substr(du[jj,"t2"],1,4))
                } else {
                svalue(spinmax) <- tab[igroup[i],spin2]
            }
            tbl[i+1,2,anchor=c(-1,0)] <- idxchk
            tbl[i+1,3,anchor=c(-1,0)] <- spinmin
            tbl[i+1,4,anchor=c(-1,0)] <- spinmax
            tbl[i+1,5,anchor=c(-1,0)] <- as.character(tab[igroup[i],desc2])
        }
    }
    groupcontainer
 }
bbtrbk <-
function() 
{
    c('AU','HK','JP','MY','NZ','SG','KR','TW','AT','BE','CY','CZ','DK','EE','FI','FR','DE','GR','HU','IE','IT','NL','NO','PL','PT','ZA','ES','SE','CH','TH','TR','GB','BR','CA','MX','US')
}
alldu <-
function(directory=Aaroot) 
{
    droptab("tmp")
    droptab("tmp0")
    mysqlQuery("
            CREATE TABLE tmp0
            SELECT bti, 0 AS include, MIN(date) AS t1, MAX(date) AS t2
            FROM aauniv 
            GROUP BY bti
            ORDER BY bti
            ")
    mysqlQuery("
            CREATE TABLE tmp
            SELECT  i, tmp0.bti AS ticker, aaindices.region, aaindices.country, aaindices.name, tmp0.include AS include, tmp0.t1 AS start, tmp0.t2 AS end
            FROM tmp0 INNER JOIN aaindices
            ON tmp0.bti = aaindices.bti 
            ORDER BY region, include, i, country
            "
            )
    tab <- gettab("tmp",return="dataframe")[,-1]
    tab[,"start"] <- as.numeric(format(as.POSIXlt(tab[,"start"]),"%Y")) #do this because spinners want numeric
    tab[,"end"] <- as.numeric(format(as.POSIXlt(tab[,"end"]),"%Y"))
    tab[,"country"] <- capwords(tolower(tab[,"country"]))
    tab
}
derxx <-
function(tabs=setdiff(allxx(),"su")) 
{
    for(i in seq(along=tabs[-1])) {
        if(do.call(psz("der",tabs[i]),list())) print(tabs[i])
        }
}
chkxx <-
function(tabs=allxx()) 
{
    for(i in seq(along=tabs[-1])) {
        if(do.call(psz("chk",tabs[i]),list())) print(tabs[i])
        }
}
deraauniv <-
function(use=c("bworld","sfm","all"))
{
    use <- match.arg(use)
    mysqlQuery("DELETE FROM aauniv")    
    if(use%in%c("sfm","all")) {
       mysqlQuery("
            INSERT INTO aauniv (date, btk, weight, bui, countryfull, countryissue, bti)
            SELECT date, TRIM(TRAILING ' equity' FROM btk), NULL AS weight, bui, countryfull, countryissue, 'SFM' AS bti
            FROM sabretk
            ")
    }
    if(use%in%c("bworld","all")) {
        mysqlQuery(psz("
            INSERT INTO aauniv (date, btk, weight, bui, countryfull, countryissue, bti)
            SELECT date, btk, weight, bui, countryfull, countryissue, countryissue AS bti 
            FROM aabworld
            WHERE weight>=",minmaxweight(big="bigish")
            ))
    }
    mysqlQuery("DELETE FROM aauniv WHERE bui IN (SELECT bui FROM bbexclude)")
    addidx("aauniv","bui")
    addidx("aauniv","countryissue")
    addidx("aauniv","bti")
    addidx("aauniv","weight")
}
deraalocalnew <-
function(
                    y1=2003,
                    y2=2012,
                    month=c(2,8),
                    mytab=btifn()
                    )
{
    newaabworld(mytab="aalocal")
    `stripyellow` <- function(x) {
        stopifnot(is.character(x) && tolower(substr(x,nchar(x)-5,nchar(x)))=="equity")
        substr(x,1,nchar(x)-7)
    }
    x <- bbicon(bti=mytab,y1=y1,y2=y2,month=month)
    xx <- cbind(x[,-1],bui=NA,countryfull=NA,countryissue=NA)
    mode(xx[,"bui"]) <- "character"
    sqlSave(channel = DBcon, dat = xx, tablename = mytab, append = TRUE, rownames = FALSE, safer = TRUE)
    btk <- as.character(extract(mytab,"DISTINCT btk","ORDER BY btk"))
    x <- bbxsect(bbcode=paste(btk,"equity"),bbitem="ID_BB_UNIQUE")
    colnames(x) <- "bui"
    sqlUpdate(channel = DBcon, dat = data.frame(cbind(bui=x,btk=stripyellow(rownames(x)))), tablename = mytab, index = "btk")
    mysqlQuery(psz("DELETE FROM ",mytab," WHERE bui LIKE '%Sec'"))    #GEKTERNAGA 2005-02-28 etc, SCOTIAC1PE, HITSTELEKK - these would work with a space in the ticker, but others work without a space, so just zap these
    bui <- as.character(extract(mytab,"DISTINCT bui","ORDER BY bui"))
    x <- bbxsect(bbcode=paste(bui,"equity"),bbitem=c("COUNTRY_FULL_NAME","CNTRY_ISSUE_ISO"))
    colnames(x) <- c("countryfull","countryissue")
    sqlUpdate(channel = DBcon, dat = data.frame(cbind(country=x,bui=stripyellow(rownames(x)))), tablename = mytab, index = "bui")
    mysqlQuery(psz("DELETE FROM ",mytab," WHERE countryissue IS NULL"))    #SNTS BC, Senegal

}
deraalocal <-
function()
{
    #mysqlQuery("DROP TABLE backupaalocal ")
    #mysqlQuery("CREATE TABLE backupaalocal AS SELECT * FROM aalocal")
    mysqlQuery("DELETE FROM aalocal")
    `stripyellow` <- function(x) {
        stopifnot(is.character(x) && tolower(substr(x,nchar(x)-5,nchar(x)))=="equity")
        substr(x,1,nchar(x)-7)
    }
    x <- NULL
    x[[1]] <- bbicon(bti="as51",y1=2003,y2=2010,month=c(2,8))       #Australia
    x[[2]] <- bbicon(bti="bel20",y1=2003,y2=2010,month=c(2,8))       #Belgium
    x[[3]] <- bbicon(bti="ibov",y1=2003,y2=2010,month=c(2,8))       #Brazil
    x[[4]] <- bbicon(bti="sptsx",y1=2003,y2=2010,month=c(2,8))       #Canada
    x[[5]] <- bbicon(bti="sbf250",y1=2003,y2=2010,month=c(2,8))       #France
    x[[6]] <- bbicon(bti="dax",y1=2003,y2=2010,month=c(2,8))       #Germany
    x[[7]] <- bbicon(bti="mdax",y1=2003,y2=2010,month=c(2,8))       #Germany
    x[[8]] <- bbicon(bti="ase",y1=2003,y2=2010,month=c(2,8))       #Greece
    x[[9]] <- bbicon(bti="hsci",y1=2003,y2=2010,month=c(2,8))       #Hong Kong
    x[[10]] <- bbicon(bti="bse200",y1=2003,y2=2010,month=c(2,8))       #India
    save(x,file="temp")
    x[[11]] <- bbicon(bti="iseq",y1=2003,y2=2010,month=c(2,8))       #Ireland
    x[[12]] <- bbicon(bti="tpx500",y1=2003,y2=2010,month=c(2,8))       #Japan
    x[[13]] <- bbicon(bti="kospi50",y1=2003,y2=2010,month=c(2,8))       #Korea
    x[[14]] <- bbicon(bti="aex",y1=2003,y2=2010,month=c(2,8))       #Netherlands
    x[[15]] <- bbicon(bti="sesall",y1=2003,y2=2007,month=c(2,8))       #Singapore
    x[[16]] <- bbicon(bti="fssti",y1=2008,y2=2010,month=c(2,8))       #Singapore
    x[[17]] <- bbicon(bti="madx",y1=2003,y2=2010,month=c(2,8))       #Spain
    x[[18]] <- bbicon(bti="sbx",y1=2003,y2=2010,month=c(2,8))       #Sweden
    x[[19]] <- bbicon(bti="spi",y1=2003,y2=2010,month=c(2,8))       #Switzerland
    x[[20]] <- bbicon(bti="tw50",y1=2003,y2=2010,month=c(2,8))       #Taiwan
    x[[21]] <- bbicon(bti="asx",y1=2003,y2=2010,month=c(2,8))       #UK
    x[[22]] <- bbicon(bti="spx",y1=2003,y2=2010,month=c(2,8))       #USA
    x[[23]] <- bbicon(bti="mid",y1=2003,y2=2010,month=c(2,8))       #USA
    x[[24]] <- bbicon(bti="irt",y1=2003,y2=2010,month=c(2,8))       #Mexico
    save(x,file="temp")
    for(i in seq_along(x)) {
        xx <- cbind(x[[i]][!duplicated(apply(x[[i]][,c("date","bti","btk")],1,paste,collapse=" ")),],bui=NA)
        mode(xx[,"bui"]) <- "character"
        sqlSave(channel = DBcon, dat = xx, tablename = "aalocal", append = TRUE, rownames = FALSE, safer = TRUE)
    }
    addidx("aalocal","btk")

    mysqlQuery("UPDATE aalocal, aaindex SET aalocal.bui=aaindex.bui WHERE aalocal.btk=aaindex.btk")

    btk <- as.character(extract("aalocal","DISTINCT btk","WHERE bui IS NULL ORDER BY btk"))
    x <- bbxsect(bbcode=paste(btk,"equity"),bbitem="ID_BB_UNIQUE")  #are these needed?
    colnames(x) <- "bui"
    sqlUpdate(channel = DBcon, dat = data.frame(cbind(bui=x,btk=stripyellow(rownames(x)))), tablename = "aalocal", index = "btk")
    mysqlQuery("DELETE FROM aalocal WHERE bui='#N/A Sec'") #only 2 which have no space in btk : GEKETERNAGA and MTROSDBBSS
}
deraaindtree <-
function()
{
    mysqlQuery("
            CREATE TABLE aaindtree1 AS
            SELECT DISTINCT industry_sector, industry_group 
            FROM aaxs
            ORDER BY  industry_sector, industry_group
            ")
    mysqlQuery("
            CREATE TABLE aaindtree2 AS
            SELECT DISTINCT industry_group, industry_subgroup 
            FROM aaxs
            ORDER BY  industry_group, industry_subgroup
            ")
    mysqlQuery("
            CREATE TABLE aagindtree1 AS
            SELECT DISTINCT gics_industry_group_name, gics_industry_name 
            FROM aaxs
            ORDER BY  gics_industry_group_name, gics_industry_name
            ")
    mysqlQuery("
            CREATE TABLE aagindtree2 AS
            SELECT DISTINCT gics_industry_name, gics_sub_industry_name 
            FROM aaxs
            ORDER BY  gics_industry_name, gics_sub_industry_name
            ")
    mysqlQuery("DROP VIEW IF EXISTS aagicstree")
    mysqlQuery("
            CREATE VIEW aagicstree AS
            SELECT gics_industry_group_name, aagindtree1.gics_industry_name, gics_sub_industry_name 
            FROM aagindtree1, aagindtree2 
            WHERE aagindtree1.gics_industry_name = aagindtree2.gics_industry_name
            ORDER BY gics_industry_group_name, aagindtree1.gics_industry_name, gics_sub_industry_name 
            ")
    mysqlQuery("DROP VIEW IF EXISTS aaindtree")
    mysqlQuery("
            CREATE VIEW aaindtree AS
            SELECT aaindtree1.industry_sector, aaindtree1.industry_group, aaindtree2.industry_subgroup
            FROM aaindtree1, aaindtree2 
            WHERE aaindtree1.industry_group = aaindtree2.industry_group
            ORDER BY aaindtree1.industry_sector, aaindtree1.industry_group, aaindtree2.industry_subgroup
            ")
}
deraabworld <-
function(
                    y1=2003,
                    y2=2012,
                    month=c(2,8),
                    mytab="aabworld",
                    bti=c("bworldus","bworldpr","bworldeu")
                    )
{
    newaabworld(mytab=mytab)
    `stripyellow` <- function(x) {
        stopifnot(is.character(x) && tolower(substr(x,nchar(x)-5,nchar(x)))=="equity")
        substr(x,1,nchar(x)-7)
    }
    x <- bbicon(bti=bti,y1=y1,y2=y2,month=month)
    xx <- cbind(x[,-1],bui=NA,countryfull=NA,countryissue=NA)
    mode(xx[,"bui"]) <- "character"
    sqlSave(channel = DBcon, dat = xx, tablename = mytab, append = TRUE, rownames = FALSE, safer = TRUE)
    btk <- as.character(extract(mytab,"DISTINCT btk","ORDER BY btk"))
    x <- bbxsect(bbcode=paste(btk,"equity"),bbitem="ID_BB_UNIQUE")
    colnames(x) <- "bui"
    sqlUpdate(channel = DBcon, dat = data.frame(cbind(bui=x,btk=stripyellow(rownames(x)))), tablename = mytab, index = "btk")
    mysqlQuery(psz("DELETE FROM ",mytab," WHERE bui LIKE '%Sec'"))    #GEKTERNAGA 2005-02-28 etc, SCOTIAC1PE, HITSTELEKK - these would work with a space in the ticker, but others work without a space, so just zap these
    bui <- as.character(extract(mytab,"DISTINCT bui","ORDER BY bui"))
    x <- bbxsect(bbcode=paste(bui,"equity"),bbitem=c("COUNTRY_FULL_NAME","CNTRY_ISSUE_ISO"))
    colnames(x) <- c("countryfull","countryissue")
    sqlUpdate(channel = DBcon, dat = data.frame(cbind(country=x,bui=stripyellow(rownames(x)))), tablename = mytab, index = "bui")
    mysqlQuery(psz("DELETE FROM ",mytab," WHERE countryissue IS NULL"))    #SNTS BC, Senegal
    droptab("tmp")
    if(y1<2004 && y2>=2004) {
        mysqlQuery(psz("CREATE TABLE tmp AS SELECT * FROM ",mytab," WHERE date='2003-08-31' AND countryfull='SAUDI ARABIA'"))
        mysqlQuery("UPDATE tmp SET date='2004-02-29'")
        mysqlQuery(psz("INSERT INTO ",mytab," SELECT * FROM tmp")) #these were dropped in error (by BB) for one rebalance
        mysqlQuery(psz("DELETE FROM ",mytab," WHERE weight>5"))    #TPHL ZH in 2003-08 has the wrong weight by a factor of 1e4, has weight of 9(%)
    }
    mysqlQuery(psz("DELETE FROM ",mytab," WHERE weight IS NULL"))    #193 oddball instances
}
newaapeerdaily <-
function() {
    mysqlQuery("DROP TABLE IF EXISTS aapeerdaily")
    mysqlQuery("
        CREATE TABLE `aapeerdaily` (
            `date` DATE NOT NULL DEFAULT '0000-00-00',
            `bui` CHAR(18) NOT NULL DEFAULT '',
            `px_last` DOUBLE NULL DEFAULT NULL,
            PRIMARY KEY (`date`, `bui`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
deraaxs <-
function (bui = setdiff(latestsu(), extract("aaxs", "distinct bui"))) 
{
    bui <- setdiff(bui, extract("aaxs", "distinct bui"))
    if (length(bui) == 0) {
        return()
    }
    x1 <- extract("aauniv", "btk as btkmax, max(weight) as weightmax, bui, countryfull, countryissue, max(bti) as btimax, NULL AS btklocalmax", 
        psz("where bui in (", fldlst(bui, "'", "'"), ") GROUP BY bui ORDER BY bui ASC"))
        myalias <- c(bui = "id_bb_unique", btkmax = "ticker_and_exch_code", 
            countryfull = "COUNTRY_FULL_NAME", countryissue = "CNTRY_ISSUE_ISO", 
            btimax = "REL_INDEX", btklocalmax = "TICKER_AND_EXCH CODE")
        bbi <- dirfld("aaxs")
        bbi[match(names(myalias), bbi)] <- myalias
        bbi <- bbi[bbi != "weightmax"]
        bbxs <- bbxsect(bbc = psz(sort(bui), " equity"), bbi = bbi)
        colnames(bbxs)[match(myalias, bbi)] <- names(myalias)
        bbxs <- cbind(bbxs, as.matrix(data.frame(weightmax = rep(1,nrow(bbxs)))))
        all.equal(sort(dirfld("aaxs")), sort(colnames(bbxs)))
        sqlSave(channel = DBcon, dat = data.frame(bbxs), tablename = "aaxs", 
        append = TRUE, rownames = FALSE, safer = TRUE)
}
deraats <-
function(
            AAloc=AA,
            cash=getstir(), #getpa(field="value",tab="aamacro",ifield="date",jfield="btk",qual="WHERE btk='us0001m'")/100,
            dayofweek=3,    #3 => wednesday
            dayslag1=1,       #2 days => monday close used to design wed po
            dayslag2=2       #2 days => tue close used to design wed po
            )
{
    eps <- 1.e-6 #for preventing singularity
    stopifnot(is.list(AAloc) && c("AA1","AA2","AA3")%in%names(AAloc))
    AAloc <- aazeroprice(AAloc)
    stir <- crncytostir(buicrncy(bui=rownames(AA[[2]])))
    carry <- accruexts(extractxts(cash,period="week",select=dayofweek))
    da <- as.Date(intersect(index(extractxts(AAloc[[3]],period="week",select=dayofweek)),index(carry)))
    resnams <- aatsfields()
    res <- data.frame(matrix(NA,nrow=length(da),ncol=length(resnams)))
    colnames(res) <- resnams
    ffield <- c("px_last","eqy_weighted_avg_px","px_last_local")
    pfield <- c("px_last","best_target_price","px_last_noincome","cur_mkt_cap","eqy_dvd_yld_ind","pe_ratio","px_to_cash_flow","px_to_book_ratio")
    AA3 <- AAloc[[3]]
    AA3[is.na(AA3[,"px_volume"]),"px_volume"] <- 0
    ivwaptest <- which(abs(AA3[,"eqy_weighted_avg_px"]/AA3[,"px_last"]-1)>0.1)
    if(length(ivwaptest)>0 && !any(is.na(ivwaptest))) AA3[ivwaptest,"eqy_weighted_avg_px"] <- AA3[ivwaptest,"px_last"]
    AA3f <- focb.local(AA3[,ffield],maxper=4)    #future
    AA3fx <- extractxts(AA3f,period="week",select=dayofweek)[da,]   #future extract
    AA3p1 <- lag.xts(x=locf.local(AA3[,pfield],maxper=4),k=dayslag1) #for class xts positive lag k is feasible (moves past->present)
    AA3p1x <- extractxts(AA3p1,period="week",select=dayofweek)[da,]    #past extract
    AA3p2 <- lag.xts(x=locf.local(AA3[,pfield],maxper=4),k=dayslag2)
    AA3p2x <- extractxts(AA3p2,period="week",select=dayofweek)[da,]    
    res[,"date"] <- da
    res[,"bui"] <- rownames(AAloc[[2]])
    res[,"prdo"] <- AA3fx[,"px_last"]                                    
    res[,"prdovw"] <- AA3fx[,"eqy_weighted_avg_px"]                                    
    res[,"prlo"] <- AA3fx[,"px_last_local"]
    res[,"reloto"] <- retxts(res[,"prlo"])
    res[,"rrloto"] <- retxts(res[,"prlo"])-carry[da,stir,drop=FALSE]
    res[,"redoto"] <- retxts(res[,"prdo"])
    res[,"rrdoto"] <- retxts(res[,"prdo"])-carry[da,"us0001m",drop=FALSE]
    res[,"rrdotovw"] <- retxts(res[,"prdovw"])-carry[da,"us0001m",drop=FALSE]
    res[,"turn"] <-  extractxts(rollxts(locf.local(AA3[,"px_last"])*AA3[,"px_volume"],what="median",n=260),period="week",select=dayofweek)[da,]
    res[,"prdol1"] <- AA3p1x[,"px_last"]
    #res[,"momentuml1"] <- extractxts(rollxts(retxts(AA3p1[,"px_last"]),what="meantri",n=260),period="week",select=dayofweek)[da,]
    #res[,"belowhighl1"] <- belowhigh(AA3p1x[,"px_last",drop=FALSE])
    res[,"bestl1"] <- as.xts(extractxts(rollxts(AA3p1[,"best_target_price"],what="median"),period="week",select=dayofweek)[da,,drop=FALSE])/
                        extractxts(AA3p1[,"px_last_noincome"],period="week",select=dayofweek)[da,,drop=FALSE]
    res[,"mcapl1"] <- AA3p1x[,"cur_mkt_cap"]                              
    #res[,"capuil1"] <- (putil1-callil1)/100
    #res[,"capuol1"] <- (AA3p1x[,"open_int_total_call"]-AA3p1x[,"open_int_total_put"]) / (AA3p1x[,"open_int_total_call"]+AA3p1x[,"open_int_total_put"]+1)
    incomeadjust <- AA3p1x[,"px_last"]/AA3p1x[,"px_last_noincome"] #this is the adjustment, always <=1 monotone increasing, to undo BB's error
    res[,"diprl1"] <- incomeadjust*AA3p1x[,"eqy_dvd_yld_ind"]/100.
    res[,"erprl1"] <- incomeadjust/(eps+AA3p1x[,"pe_ratio"])
    res[,"caprl1"] <- incomeadjust/(eps+AA3p1x[,"px_to_cash_flow"])
    res[,"boprl1"] <- incomeadjust/(eps+AA3p1x[,"px_to_book_ratio"])
    #res[,"cdsl1"] <- AA3p1x[,"cds_spread"]
    #res[,"rsi14l1"] <- AA3p1x[,"rsi_14d"]
    res[,"prdol2"] <- AA3p2x[,"px_last"]                                    
    res[,"bestl2"] <- as.xts(extractxts(rollxts(AA3p2[,"best_target_price"],what="median"),period="week",select=dayofweek)[da,,drop=FALSE])/
                        extractxts(AA3p2[,"px_last_noincome"],period="week",select=dayofweek)[da,,drop=FALSE]
    res[,"mcapl2"] <- AA3p2x[,"cur_mkt_cap"]                              
    incomeadjust <- AA3p2x[,"px_last"]/AA3p2x[,"px_last_noincome"] #this is the adjustment, always <=1 monotone increasing, to undo BB's error
    res[,"diprl2"] <- incomeadjust*AA3p2x[,"eqy_dvd_yld_ind"]/100.
    res[,"erprl2"] <- incomeadjust/(eps+AA3p2x[,"pe_ratio"])
    res[,"caprl2"] <- incomeadjust/(eps+AA3p2x[,"px_to_cash_flow"])
    res[,"boprl2"] <- incomeadjust/(eps+AA3p2x[,"px_to_book_ratio"])
    res[,"prdonoincome"] <- AA3p1x[,"px_last_noincome"]         #nb this is a lagged field despite no clue in the name!
    data.frame(lapply(res,coredata))
}
deraamacro <-
function()
{
    #mysqlQuery("
    #    CREATE TABLE `aamacro` (
    #      `date`   date NOT NULL,
    #      `btk`    char(17) NOT NULL,
    #      `value`  double,
    #      /* Keys */
    #      PRIMARY KEY (`date`, `btk`)
    #    ) ENGINE = MyISAM;
    #")
    mysqlQuery("DELETE FROM aamacro")
    bigcur <- psz(c("AUD","CAD","CHF","EUR","GBP","JPY","NOK","NZD","SEK","USD","CNY","DKK","HKD","KRW","SGD"),"USD curncy")
    bigrates <- psz(c("au0001m","cd0001m","sf0001m","dk0001m","ee0001m","bp0001m","jy0001m","nibor1m","ndbb1m","stib1m","us0001m")," index")
    bigyields <- psz(c("gacgb10","gcan10yr","gswiss10","gecu10yr","gukg10","gjgb10","gnor10yr","gnzgb10","gsgb10yr","usgg10yr")," index")
    bigother <- psz(c("vix","eucrbrdt","uscrwtic","$$swap10","bpss10")," index")
    smallcur <- psz(c("ZAR","MYR","BRL","INR","TWD"),"USD curncy")
    smallrates <- psz(c("bccdbae","hihd01m","irdra","mrdra","sadrc","fda fix","kwcr1z","sorf1m")," index") #nb fda fix becomes fda 4 lines down
    btk <- c(bigcur,bigrates,bigyields,bigother,smallcur,smallrates) #btk <- benchmarks
    mat <- bbts(bbcode=btk,bbitems="px_last",startday="1990-12-31",endday=thiswed("%Y-%m-%d"))
    colnames(mat) <- lapply(strsplit(colnames(mat)," "),"[",1)
    tab <- mattotab(as.matrix(mat),field="value",rclab=c("date","btk"))
    sqlSave(channel = DBcon, dat = tab, tablename = "aamacro", append = TRUE, rownames = FALSE, safer = TRUE)
    #worldcur <- c("CNY","KRW","PLN","HKD","USD","TWD","JPY","CAD","EUR","GBp","DKK","SAR","INR","CHF","AUD","ZAr","ILs","SGD","BMD","KWd","CLP","TRY","NZD","PHP","ARS","BRL","MYR","MXN","JOD","IDR","SEK","PEN","THB","HUF","KES","COP","CZK","RUB","MAD","NOK","AED","PKR","NGN","EGP","QAR","HRK")
}
newne <-
function() 
{
    droptab("ne")
    mysqlQuery("
        CREATE TABLE `ne` (
            `strategy` CHAR(60) NOT NULL,
            `date` DATE NOT NULL,
            `peraw` DOUBLE NULL DEFAULT NULL,
            `mkt` DOUBLE NULL DEFAULT NULL,
            `pecor` DOUBLE NULL DEFAULT NULL,
            `ponet` DOUBLE NULL DEFAULT NULL,
            `pogross` DOUBLE NULL DEFAULT NULL,
            `potrade` DOUBLE NULL DEFAULT NULL,
            PRIMARY KEY (`strategy`, `date`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
derne <-
function(
                    strats      #named list, each element is a vector of iga with name=strategy
                    )   
{
    stopifnot(is.list(strats) && length(strats)>0 && all(unlist(strats)%in%getive("ga",howmany="all")))
    stopifnot(!is.null(names(strats)) && !any(nchar(names(strats))==0) && !any(duplicated(names(strats))))
    newne()
    allga <- NULL
    for(i in seq_along(strats)) {
        njobs <- length(strats[[i]])
        peraw <- neperf(iga1=strats[[i]],gsf="AVG")
        mkt <- nemkt(iga1=strats[[i]],gsf="AVG")
        pecor <- nedebeta(y=peraw,x=mkt)
        ga <-   merge(
                    peraw,
                    mkt,
                    pecor,
                    nepos(iga1=strats[[i]],gsf="AVG",comp="gross"),
                    nepos(iga1=strats[[i]],gsf="AVG",comp="net"),
                    nepos(iga1=strats[[i]],gsf="AVG",comp="trade")
                    )
        colnames(ga) <- c("peraw","mkt","pecor","pogross","ponet","potrade")
#        allga <- rbind(allga,data.frame(cbind(strategy=names(strats)[i],date=as.character(index(ga)),data.frame(coredata(ga)))))
        allga <- data.frame(cbind(strategy=names(strats)[i],date=as.character(index(ga)),data.frame(coredata(ga))))
        sqlSave(channel=DBcon,
                dat=allga,
                tablename="ne",
                append=TRUE,
                rownames=FALSE,
                safer=FALSE)
    }
    chkne()
}
chkne <-
function() 
{
    "ne"%in%dirtab() && nrow(tip("ne"))>0
}
statusqu <-
function(
                ijo=getijo(),
                status="done",
                jo=TRUE,
                channel=chve()
                ) 
{
    for(i in seq_along(ijo)) {
        mysqlQuery(psz("
            UPDATE qu
            SET status = '",status,"'
            WHERE ijo = ",ijo[i]
            ))
        if(jo) {
            mysqlQuery(psz("
                UPDATE jo
                SET status = '",status,"'
                WHERE ijo = ",ijo[i]
                ),channel=channel)
        }
    }
}
newqu <-
function() 
{
    mysqlQuery("
        CREATE TABLE `qu` (
            `datestamp` CHAR(20) NULL DEFAULT NULL,
            `status` CHAR(6) NULL DEFAULT NULL,
            `comment` CHAR(255) NULL DEFAULT NULL,
            `ijo` SMALLINT(6) NULL DEFAULT NULL,
            `isu` SMALLINT(6) NULL DEFAULT NULL,
            `ili` SMALLINT(6) NULL DEFAULT NULL,
            `isi` SMALLINT(6) NULL DEFAULT NULL,
            `ipo` SMALLINT(6) NULL DEFAULT NULL,
            `ilo` SMALLINT(6) NULL DEFAULT NULL,
            `ira` SMALLINT(6) NULL DEFAULT NULL,
            `iga` SMALLINT(6) NULL DEFAULT NULL,
            `ima` SMALLINT(6) NULL DEFAULT NULL,
            `ifa` SMALLINT(6) NULL DEFAULT NULL,
            `iti` SMALLINT(6) NULL DEFAULT NULL,
            `ico` SMALLINT(6) NULL DEFAULT NULL,
            `ice` SMALLINT(6) NULL DEFAULT NULL,
            `ire` SMALLINT(6) NULL DEFAULT NULL
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
derqu <-
function(...,channel=chve(),channel2=DBcon,ijoselect=getijo(what="all")) 
{
    runli(beforeclass="qu")
    droptab("qu")
    #deliberate rule violation: name the channel2 database
    mysqlQuery(psz("CREATE TABLE ",nameve(channel2),".qu AS (SELECT * FROM jo WHERE status = 'todo' AND ijo IN (",flds(ijoselect),"))"),channel=channel)
    runli(beforeclass="qu",when="after")
    chkqu()
}
chkqu <-
function() 
{
    TRUE    
    #chkjo("qu") #removed 2011-11-07 when database split
}
statusme <-
function(ime=aame(),status="todo",channel=chve(),...) {
    ijo <- mexx(ime=ime,...)
    statusqu(ijo=ijo,status=status,channel=channel)
}
newxx <-
function(xx=setdiff(allxx(),c("si","li","cu","su","vi","jo","qu","ve","ca","mi"))) {
    for(i in seq_along(xx)) {
        print(xx[i])
        newxx <- get(psz("new",xx[i]))
        droptab(xx[i])
        do.call(what=newxx,args=list())
    }
}
newme <-
function() 
{
    declare(tabname="me", 
            dec=decme(),
            engine="MyISAM")
}
dermex <-
function(
                isu=117:123,
                ili=2:5,
                startafter=c("2000-01-01","2010-09-08",lastdate()),
                isi0=484,
                mycomment="-",
                mnem=psz("ime=",getive("me")+1),
                imepo=NA,   #copy po from this me
                imece=aame(),#derive ice from this me - needs ice then to be defaulted
                ice=extract("vemevi","distinct isu, max(ice) AS ice, max(ire) as ire",psz("WHERE ixx =",imece," GROUP BY isu"),channel=channel), #NA or matrix with unique rows and 3 cols: isu, ice, ire 
                autoice=TRUE,  #if TRUE and ice is not a valid argument (eg is NA), ice and ire will be taken from last job with same isu
                archive=TRUE,    #archive me
                channel=chve(),
                tightcycle=3
                ) 
{
    stopifnot(mode(isu) %in% c("integer","numeric")  && class(isu) %in% c("integer","numeric") && length(isu)>0 && all(isu %in% getive("su",how="all")))
    stopifnot(mode(ili) %in% c("integer","numeric")  && class(ili) %in% c("integer","numeric") && length(ili)>0 && all(ili %in% getive("li",how="all")))
    stopifnot(mode(isi0) %in% c("integer","numeric")  && class(isi0) %in% c("integer","numeric") && length(isi0)==1 && isi0 %in% getive("si",how="all"))
    stopifnot((length(ice)==1 && is.na(ice)) || (is.matrix(ice) && ncol(ice)==3 && identical(sort(colnames(ice)),c("ice","ire","isu")) && !any(duplicated(apply(ice,1,paste,collapse=" ")))) )
    stopifnot(length(imepo)==1 && (is.na(imepo) || imepo%in%getive("me",how="all")))
    icetable <- (is.matrix(ice) && ncol(ice)==3 && identical(sort(colnames(ice)),c("ice","ire","isu")))
    haveice <-  icetable && !any(is.na(ice[,"ice"]))
    haveire <- icetable && !any(is.na(ice[,"ire"]))
    if(1 %in% ili) {if(length(ili)>0) {ili <- ili[ili!=1]} else {stop("library 1 is reserved")}}
    nsu <- length(isu)
    nli <- length(ili)
    nper <- length(startafter)
    isilast <- getive("si")
    icolast <- getive("co")
    ipolast <- getive("po")
    #ibaisu <- unlist(splitsu(isu,ty="ixx",ndb=ceiling(nsu/2)))  #splits su into ndb groups for processing
    #ibalut <- as.numeric(substr(names(ibaisu),2,2))
    #names(ibalut)<-ibaisu
    #           unit    neutral     combo   1-report    n-report    [description]
    #           re      po          pn      ti          ti          [mnemonic]
    #           1       1           nsu     1           nper        [number of si, total]
    npersu <-  (1   +   nli +       1   +   nli +       nper)      #[number of jobs per su]
    #           totrep    totlib
    nperme <-   nper    +   nli
    njo <- nsu*npersu   +   nperme
    me <- matrix(0,njo,11,dimnames=list(NULL,c("ijo","isu","ili","isi","ice","ire","ico","ipo","iba","iho","method")))
    me[,"ijo"] <- getijo("last") + (1:njo)
    me[,"isu"] <- c(rep(isu,each=npersu),rep(1,nperme))
    me[,"iba"] <- floor(naa()*tightcycle/nsu) # #processors allocated for this job
    me[,"iho"] <- me[,"isu"]%%tightcycle
    me[,"ili"] <- c(rep(c(1,ili,1,ili,rep(1,nper)),nsu),rep(1,nper),ili)
    #sequence of jo, per su
    #   sequence        start           desc        njo         isi         steps   
    #   1                               unit        1           isi1        tore    
    #   1+(1:nli)       2               neutral     nli         isi2        topo1   
    #   n1+(1:nsu)      n1=1+nli        combo       1           isi3(isu)   topon   
    #   n2+(1:nli)      n2=n1+nsu       report      nli         isi4(1)     toga    
    #   n3+(1:nper)     n3=n2+nli       report      nper        isi4(iper)  toti    
    #si
    #sequence of si after isilast                                                   
    #   sequence        start           desc        nsi         name        steps   
    #   1                               unit        1           isi1        tore    
    #   2                               neutral     nsu         isi2(isu)   topo1   
    #   2+(1:nsu)                       combo       nsu         isi3(isu)   topon   
    #   n1+(1:nper)     n1=2+nsu        report      1           isi4        toga    
    #   n1+nper+1                       report      nper        isi5(iper)  toti    
    #the following not repeated, overall aggregate reports
    #   nper                            totrep      nper        isi6        toga    
    #   nlib                            totlib      nli         isi7        toga    
    isi1 <- isilast+1
    isi2 <- isi1+(1:nsu)
    isi3 <- max(isi2)+(1:nsu)
    isi4 <- max(isi3)+1
    isi5 <- isi4+(1:nper)
    isi6 <- max(isi5)+(1:nper)
    isi7 <- max(isi6)+(1:nli)
    meisi <- NULL
    for(i in 1:nsu) {
        meisi <- c( meisi, c(isi1,rep(isi2[i],nli),isi3[i],rep(isi4,nli),isi5) )
    }
    meisi <- c( meisi, isi6, isi7 )
    me[,"isi"] <- meisi
    me[,"method"] <- c(
                        rep(
                            c(
                            "unit",
                            rep("neutral",nli),
                            "combo",
                            rep("librep",nli),
                            rep("report",nper)
                            ),
                        nsu),
                        c(rep("totrep",nper),rep("totlib",nli))
                        )
    #ce
    #ice is either the last set of ice for these isu, or or derived from arg ice, a named list(isu1=ice1, isu2=ice2)
    if(haveice) {
        isuice <- ice[,c("isu","ice"),drop=FALSE]
    } else {
        isuice <- extract("jo","isu, MAX(ice) AS ice",psz("WHERE isu IN (",fldlst(isu),") AND ice IS NOT NULL GROUP BY isu ORDER BY isu"),channel=channel)
        if(nrow(isuice)==0 || !autoice) {#no ce yet for this su, so do directory entries
            isuice <- cbind(isu,isu*NA)
            colnames(isuice) <- c("isu","ice")
            for(i in 1:nsu) {
                isuice[i,"ice"] <- updvedi(xx="ce",myco=psz(mnem," isu=",isu[i]))
            }
        }
        stopifnot(nrow(isuice)==nsu && !any(is.na(isuice)))
    }
    if(haveire) {
        isuire <- ice[,c("isu","ire"),drop=FALSE]
    } else {
        isuire <- extract("jo","isu, MAX(ire) AS ire",psz("WHERE isu IN (",fldlst(isu),") AND ire IS NOT NULL GROUP BY isu ORDER BY isu"),channel=channel)
        if(nrow(isuire)==0 || !autoice) {#no re yet for this su, so do directory entries
            isuire <- cbind(isu,isu*NA)
            colnames(isuire) <- c("isu","ire")
            for(i in 1:nsu) {
                isuire[i,"ire"] <- updvedi(xx="re",myco=psz(mnem," isu=",isu[i]))
            }
        }
        stopifnot(nrow(isuire)==nsu && !any(is.na(isuire)))
    }
    stopifnot(all.equal(isuice[,"isu"],isuire[,"isu"]))
    isui <- cbind(isuice[,c("isu","ice"),drop=FALSE],isuire[,"ire",drop=FALSE])
    i <- match(me[,"isu"],isui[,"isu"])
    i[is.na(i)] <- max(i,na.rm=TRUE)
    stopifnot(length(isui[i,"ice"])==nrow(me) && all(isui[i,"ice"] %in% getive(xx="ce",how="all")) && all(isui[i,"ire"] %in% getive(xx="re",how="all")))
    me[,"ice"] <- isui[i,"ice"]
    me[,"ire"] <- isui[i,"ire"]
    #co - some interdependencies could exist, but clearer to to have one co per jo, at least until they are actively used in DRB
    me[,"ico"] <- icolast+(1:nrow(me))
    #ba
    #ho
    #po
    #sequence of ipo after ipolast                                      
    #   sequence        start           desc        number  name        
    #   1                               unit        nsu     ipo1        
    #   2                               neutral     nsu*nli ipo2        
    #   2+(1:nsu)                       combo       nsu     ipo3(isu)   
    meipo <- NULL
    ipo0 <- ipolast
    for(i in 1:nsu) {
        ipo1 <- ipo0+1              #unit
        ipo2 <- ipo1+(1:nli)        #neutral
        ipo3 <- max(ipo2)+1         #combo
        meipo <- c( meipo, c(ipo1,ipo2,ipo3,ipo2,rep(ipo3,nper)) )
        ipo0 <- max(ipo3)
    }
    meipo <- c(meipo,rep(max(meipo),(nper+nli))) #use this dummy ipo entry for the game reports
    me[,"ipo"] <- meipo
    me[,"method"] <- c(
                    rep(c("unit",rep("neutral",nli),"combo",rep("librep",nli),paste(rep("report from",nper),startafter)),nsu),
                    paste(rep("totrep from",nper),startafter),
                    psz("totlib-",ili)
                    )
    newme()
    sqlSave(channel=DBcon,
            dat=data.frame(me),
            tablename="me",
            append=TRUE,
            rownames=FALSE,
            safer=FALSE)
    #si editing
    doarch=TRUE
    edsimex(isi=isi0,pass="1",arch=doarch,mnem=mnem)             
    for(i in 1:nsu) {
        edsimex(isi=isi0,pass="2",arch=doarch,mnem=mnem,tightoffset=isu[i]%%tightcycle)            
    }
    for(i in 1:nsu) {
        edsimex(isi=isi0,pass="3",ijo=extract("me","ijo",psz("WHERE isu=",isu[i]," AND method='neutral'")),arch=doarch,mnem=mnem)        
    }
    edsimex(isi=isi0,pass="4",arch=doarch,mnem=mnem)              
    for(i in 1:nper) {
        edsimex(isi=isi0,pass="5",startafter=startafter[i],arch=doarch,mnem=mnem)
    }
    reper <- reperiodme()  
    for(i in seq_along(reper)) {
        ijo <- as.numeric(reper[[i]])
        edsimex(isi=isi0,pass="6",startafter=names(reper)[i],arch=doarch,mnem=psz(mnem," ",names(reper)[i]),ijo=ijo)        
    }
    relib <- relibme()
    for(i in seq_along(relib)) {
        ijo <- as.numeric(relib[[i]])
        edsimex(isi=isi0,pass="7",startafter=names(reper)[i],arch=doarch,mnem=psz(mnem," ",names(relib)[i]),ijo=ijo)        
    }
    #intermediate archive entries - these are always new & empty and so just do directory entry
    for(ipo in as.numeric(extract("me","DISTINCT ipo","ORDER BY ipo ASC"))) {
        myco <- psz(mnem," isu=",extract("me","MAX(isu)",psz("WHERE ipo=",ipo))," ",extract("me","MIN(method)",psz("WHERE ipo=",ipo))) #MAX(isu) because may have dummy isu=1 for totrep
        updvedi(xx="po",ixx=ipo,myco=myco)
    }
    for(ico in as.numeric(extract("me","DISTINCT ico","ORDER BY ico ASC"))) {
        myco <- psz(mnem," isu=",extract("me","isu",psz("WHERE ico=",ico))," ",extract("me","method",psz("WHERE ico=",ico)))
        updvedi(xx="co",ixx=ico,myco=myco)
    }
    if(!is.na(imepo)) {
        mysqlQuery(psz("
            UPDATE me, vemevi
            SET me.ipo=vemevi.ipo, me.ico=vemevi.ico
            WHERE vemevi.ixx=",imepo," AND vemevi.isu=me.isu AND vemevi.ili=me.ili AND vemevi.method=me.method AND me.method IN ('neutral', 'librep')
        "))
    }
    njo <- nrow(me)
    for(i in 1:njo) {
        method <- extract("me","method",psz("WHERE ijo=",me[i,"ijo"]))
        addjo(
            datestamp=format(Sys.time()),
            status="todo",
            comment=psz("me ",mnem," seq=",i,"/",njo," method=",method),
            ijo=me[i,"ijo"],
            isu=me[i,"isu"],
            isi=me[i,"isi"],
            ili=me[i,"ili"],
            ice=me[i,"ice"],
            ire=me[i,"ire"],
            ico=me[i,"ico"],
            ipo=me[i,"ipo"],
            disable=FALSE
            )
    }
    if(archive) { arcve("me",mycomment=mycomment) }
    return(TRUE) 
}
dermepelapo <-
function(
                    lapo=-13:13,
                    ime=getive("me"),
                    njo=nrow(jo),
                    lacetoo=FALSE, #reset lace, otherwise leave as is
                    lace=c(-30,0,30),
                    qupanel=c("none","year"), #only these allowed
                    xx=c("xo","xt"),
                    ...
                    ) {
    qupanel <- match.arg(qupanel)
    group <- "combopo" # the other option, neutral, generates ce, re for each, so better to do a new me
    xx <- match.arg(xx)
    resve(xx="me",ixx=ime)
    resve(xx="si",ixx=as.numeric(extract("me","DISTINCT isi","WHERE method='neutral' LIMIT 1")))
    if(lacetoo) { edsilace(lace=lace) }
    edsilapo(lapo=lapo,runma=lacetoo,xx=xx,qupanel=qupanel,...)
    mycomment <- psz("me ime=",ime," method=pe(lapo)")
    isi <- arcve(xx="si",mycomment=mycomment)
    #ijo <- as.numeric(extract("me","ijo","WHERE method='combo'"))
    ijo <- mexx(ime=ime,xx="jo",group=group)
    jo <- gettab("jo",qual=psz("WHERE ijo IN (",fldlst(ijo),")"))
    for(i in 1:njo) {
        addjo(
            status="todo",
            comment=psz(mycomment," seq=",i,"/",nrow(jo)),
            isu=jo[i,"isu"],
            isi=isi,
            ice=NA, 
            ico=jo[i,"ico"],
            ili=jo[i,"ili"],
            ipo=jo[i,"ipo"],
            ira=NA,
            iga=NA, 
            ima=NA,
            ilo=NA,
            ifa=NA,
            iti=NA,
            saveit=TRUE
        )
    }
}
dermemalace <-
function(
                    ime=getive("me"),
                    lace=sort(c(-1,1,seq(from=-50,to=50,by=10))),
                    njo=nrow(jo)
                    ) {
    resve(xx="me",ixx=ime)
    edsilace(lace=lace)
    mycomment <- psz("me ime=",ime," method=ma(lace)")
    isi <- arcve(xx="si",mycomment=mycomment)
    ijo <- as.numeric(extract("me","ijo","WHERE method='unit'"))
    jo <- gettab("jo",qual=psz("WHERE ijo IN (",fldlst(ijo),")"))
    for(i in 1:njo) {
        addjo(
            status="todo",
            comment=psz(mycomment," seq=",i,"/",nrow(jo)),
            isu=jo[i,"isu"],
            isi=isi,
            ice=NA,
            ico=NA,
            ili=jo[i,"ili"],
            ipo=NA,
            ira=NA,
            iga=NA, 
            ima=NA,
            ilo=NA,
            ifa=NA,
            iti=NA,
            saveit=TRUE
        )
    }
}
newjo <-
function() 
{
    declare(tabname="jo", 
            dec=decjo())
    flds <- dirfld("jo")
    for(i in seq_along(flds)) {addidx("jo",flds[i])}
}
derjo <-
function() 
{
    runli(beforeclass="jo")
    newjo()
    addjo(ijo=1,ice=NA,ire=NA,ico=NA,ipo=NA,ira=NA,ima=NA,iga=NA,ilo=NA,ifa=NA,iti=NA,status="done")
    runli(beforeclass="jo",when="after")
    chkjo()
}
chkjo <-
function(tab="jo",action=c("check","report","ijo"),channel=chve()) 
{
    action <- match.arg(action)
    ijo <- sort(getijo("all",channel=channel))
    ijotot <- NULL
    for (i in ijo) {
        joall <- extract(tab,"isu, ili, isi, iga, ima, ilo, ipo, ice, ico",psz("WHERE ijo =",i),channel=channel)
        if(!(nrow(joall)==1)) return(FALSE)
        jo <- joall[,!is.na(joall),drop=FALSE] #zero is allowed for li, to do beta strategies in me
        if(!(nrow(jo)>0)) return(FALSE)
        if(!all(jo>0)) return(FALSE)
        myclass <- substr(colnames(jo),2,3)
        for(j in seq(along=jo)) {
            if(!(nrow(
                    extract(psz("ve",myclass[j],"di"),
                            "*",
                            psz("WHERE ixx=",jo[,j]),
                            channel=channel)
                    ) 
            ==1))  {
                if(action=="check") {
                    return(FALSE)
                } else if(action=="report") {
                    print(psz("ijo=",i," ",psz(myclass[j])," ixx=",jo[,j]," missing"))
                }else if(action=="ijo") {
                    ijotot <- c(ijotot,i)
                }
            
            }
        }
    }
    if(action=="check") {return(TRUE)} else {return(ijotot)}
}
newvedi <-
function(
                xx=allvexx(),
                channel=chve()
                ) 
{
    xx <- match.arg(xx)
    vedi <- psz("ve",xx,"di")
    dec <- rbind(          
        c(  "ixx"           ,   "SMALLINT PRIMARY KEY AUTO_INCREMENT"         ),    #cannot use auto_increment however, as R crashes on sqlsave if any column missing
        c(  "datestamp"     ,   "CHAR(20)"          ),  #datetime fails (R bug)
        c(  "status"        ,   "CHAR(6)"           ),
        c(  "comment"       ,   "CHAR(255)"         )
        )
    dec <- cbind(dec,"NOT NULL") 
    colnames(dec) <- c("xfield","xtype","xnull") 
    declare(tabname=vedi, 
            dec=dec,channel=channel)
}
newve <-
function(
                xxvec=allvexx()
                ) 
{
    stopifnot(all(xxvec %in% allvexx()))
    for(xx in xxvec) {
        newvedi(xx)
        if(xx=="su") {
            declare(tabname="vesu",
                    dec=decsu()[,1:3])
            mysqlQuery("ALTER TABLE vesu ADD COLUMN (datestamp CHAR(20))")
            mysqlQuery("ALTER TABLE vesu ADD PRIMARY KEY (datestamp, bui, date)")
            }
        if(xx=="li") {
            declare(tabname="veli",
                    dec=decli())                
            #mysqlQuery("ALTER TABLE veli ADD COLUMN (line SMALLINT)")
            mysqlQuery("ALTER TABLE veli ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="si") {
            declare(tabname="vesi", 
                    dec=decsi(),
                    engine="MyISAM")
            mysqlQuery("ALTER TABLE vesi ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="cu") {
            newcu(tab="vecu")
            mysqlQuery("ALTER TABLE vecu ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="ra") {
            declare(tabname="vera",
                    dec=decra())                
            mysqlQuery("ALTER TABLE vera ADD COLUMN (datestamp CHAR(20))")
            mysqlQuery("ALTER TABLE vera ADD PRIMARY KEY (datestamp, domain, date, lag, comp, qu)")
            }
        if(xx=="ga") {
            declare(tabname="vega",
                    dec=decga())                
            mysqlQuery("ALTER TABLE vega ADD COLUMN (datestamp CHAR(20))")
            mysqlQuery("ALTER TABLE vega ADD PRIMARY KEY (datestamp, domain, field1, field2, value1, value2, type)")
            }
        if(xx=="ma") {
            declare(tabname="vema",
                    dec=decma())                
            mysqlQuery("ALTER TABLE vema ADD COLUMN (datestamp CHAR(20))")
            mysqlQuery("ALTER TABLE vema ADD PRIMARY KEY (datestamp, lag, te1, te2, comp, type)")
            }
        if(xx=="jo") {
            declare(tabname="vejo",
                    dec=decjo())                
            mysqlQuery("ALTER TABLE vejo ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="lo") {
            declare(tabname="velo",
                    dec=declo())                
            mysqlQuery("ALTER TABLE velo ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="fa") {
            declare(tabname="vefa",
                    dec=decfa())                
            mysqlQuery("ALTER TABLE vefa ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="me") {
            declare(tabname="veme",
                    dec=decme())                
            mysqlQuery("ALTER TABLE veme ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="be") {
            declare(tabname="vebe",
                    dec=decbe())                
            mysqlQuery("ALTER TABLE vebe ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="ti") {
            newti(tab="veti")
            mysqlQuery("ALTER TABLE veti ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="po") {
            newpo(tab="vepo",pkey=FALSE)
            mysqlQuery("ALTER TABLE vepo ADD COLUMN (datestamp CHAR(20))")
            mysqlQuery("ALTER TABLE vepo ADD PRIMARY KEY (datestamp, date, bui)")
            }
        if(xx=="co") {
            newco(tab="veco")
            mysqlQuery("ALTER TABLE veco ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="ce") {
            newce(tab="vece")
            mysqlQuery("ALTER TABLE vece ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="re") {
            newre(tab="vere")
            mysqlQuery("ALTER TABLE vere ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="nu") {
            newnu(tab="venu")
            mysqlQuery("ALTER TABLE venu ADD COLUMN (datestamp CHAR(20))")
            }
        if(xx=="fo") {
            newfo(tab="vefo",pkey=FALSE)
            mysqlQuery("ALTER TABLE vefo ADD COLUMN (datestamp CHAR(20))")
            mysqlQuery("ALTER TABLE vefo ADD PRIMARY KEY (datestamp, date, bui)")
            }
        if(xx=="ge") {
            newge(tab="vege",pkey=FALSE)
            mysqlQuery("ALTER TABLE vege ADD COLUMN (datestamp CHAR(20))")
            }
        droptab(psz("ve",xx,"vi"),keepthose="zz")
        mysqlQuery(psz("
            CREATE VIEW ve",xx,"vi AS 
                SELECT ve.*, vedi.ixx, vedi.status, vedi.comment
                FROM ve",xx," AS ve 
                    INNER JOIN ve",xx,"di AS vedi 
                    ON ve.datestamp = vedi.datestamp"))
        mysqlQuery(psz("ALTER TABLE ve",xx," ADD INDEX (datestamp)"))      #for join in xxvi
        mysqlQuery(psz("ALTER TABLE ve",xx,"di ADD INDEX (datestamp)"))
        mysqlQuery(psz("ALTER TABLE ve",xx,"di ADD INDEX (ixx)"))          #for where clause on retrieval
        }
}
derve <-
function(channel=chve()) 
{
    runli(beforeclass="ve")
    for (i in allvexx()) {
        newve(i,channel=channel)
        #if(i=="li") {
        #    mycomment <- "mkt" 
        #    declare("li",decli())   #dummy function, ili=1, used for all 'unit' strategies in meta
        #    mysqlQuery("INSERT INTO li (code, fun, beforeclass, line) VALUES('\\'lipo000000a0\\'<-function(...){addpa('mo');exprpa('mo','prem')}','lipo000000a0','li',1)")
        #} else {
        #    mycomment <- ""
        #}
        arcve(i,status="auto",mycomment="auto",channel=channel)
        }
    runli(beforeclass="ve",when="after")
    chkve()
}
chkvexx <-
function(
                xx=allvexx(),
                channel=chve()
                ) 
{
    stopifnot(all(xx %in% allvexx()))
    res <- TRUE
    for(i in seq(along=xx)) {
        d1 <- extract(psz("ve",xx[i],"di"),"DISTINCT datestamp","ORDER BY datestamp",channel=channel)
        d2 <- extract(psz("ve",xx[i]),"DISTINCT datestamp","ORDER BY datestamp",channel=channel)
        #print(paste(xx[i],setdiff(d2,d1))) #orphaned entries
        res <- res && all(d2%in%d1)#identical(d1,d2)
    }
    res
}
chkve <-
function(
            xx=substr(dirtab("^ve..$"),3,4),
            channel=chve()
            )
{
    res <- all(psz("ve",outer(xx,c("","di","vi"),paste,sep="")) %in% dirtab("^ve",channel=channel))
    for(i in seq(along=xx)) { # check that archive and index share same max index
        #res <- res && chkvexx(xx[i]) # remove 2011-04-01 to allow empty archive entries
    }
    res
}
newsi <-
function() 
{
    droptab("si")
    mysqlQuery("
        CREATE TABLE `si` (
            `mytable` CHAR(2) NOT NULL,
            `parameter` CHAR(20) NOT NULL,
            `ipa` SMALLINT(6) NOT NULL,
            `value` CHAR(100) NOT NULL,
            `mode` CHAR(10) NOT NULL,
            PRIMARY KEY (`mytable`, `parameter`, `ipa`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
    droptab("sise")
    mysqlQuery("
        CREATE TABLE `sise` (
            `mytable` CHAR(2) NULL DEFAULT NULL,
            `runseq` SMALLINT(6) NULL DEFAULT NULL
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
    dat <- data.frame(mytable=allxx(),runseq=1:length(allxx()))
    sqlSave(channel=DBcon,
                dat=dat,    
                tablename="sise",
                append=TRUE,
                rownames=FALSE,
                safer=TRUE)
}
dersi <-
function(over=TRUE) 
{
    runli(beforeclass="si")
    if(over) newsi()
    addsi()
    runli(beforeclass="si",when="after")
    chksi(whencreated=TRUE)
}
chksi <-
function(whencreated=FALSE) { TRUE }
newlo <-
function() 
{
    declare(tabname="lo",
            dec=declo(),
            engine="MyISAM")
    queries <- psz("INSERT INTO lo (mytable) VALUES ('",allxx(),"')")
    for(i in seq(along=queries)) mysqlQuery(queries[i])
}
derlo <-
function() 
{
    runli(beforeclass="lo")
    newlo()
    runli(beforeclass="lo",when="after")
    chklo(whencreated=TRUE)
}
chklo <-
function(whencreated=FALSE) 
{
    if( !("lo" %in% dirtab()) ) return(FALSE)
    all(allxx() %in% extract("lo","mytable"))
}
newti2 <-
function(tab="ti2") 
{
    newti1(tab)
    mysqlQuery(psz("ALTER TABLE ",tab," ADD COLUMN (drawdown     double)"))
    mysqlQuery(psz("ALTER TABLE ",tab," ADD COLUMN (perfee    double)"))
    mysqlQuery(psz("ALTER TABLE ",tab," ADD COLUMN (fundret    double)"))
    mysqlQuery(psz("ALTER TABLE ",tab," ADD COLUMN (fundcum   double)"))
}
newti1 <-
function(tab="ti1") 
{
    newti(tab)
    mysqlQuery(psz("ALTER TABLE ",tab," ADD COLUMN (datexo      date)"))
    mysqlQuery(psz("ALTER TABLE ",tab," ADD COLUMN (cashacc     double)"))
    mysqlQuery(psz("ALTER TABLE ",tab," ADD COLUMN (tcostacc    double)"))
    mysqlQuery(psz("ALTER TABLE ",tab," ADD COLUMN (fcostacc    double)"))
    mysqlQuery(psz("ALTER TABLE ",tab," ADD COLUMN (manfeeacc   double)"))
}
newti <-
function(tab="ti") 
{
    droptab(tab)
    myquery <- psz("
        CREATE TABLE `",tab,"` (
            `date` DATE NULL DEFAULT NULL,
            `year` SMALLINT(6) NULL DEFAULT NULL,
            `month` TINYINT(4) NULL DEFAULT NULL,
            `datew` DATE NULL DEFAULT NULL,
            `cashdollar` DOUBLE NULL DEFAULT NULL,
            `xn` DOUBLE NULL DEFAULT NULL,
            `xg` DOUBLE NULL DEFAULT NULL,
            `xt` DOUBLE NULL DEFAULT NULL,
            `pe` DOUBLE NULL DEFAULT NULL,
            `pev` DOUBLE NULL DEFAULT NULL COMMENT 'not used - deprecated'
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
     ")
     mysqlQuery(myquery)
}
newgad <-
function(tab="gad") 
{
    droptab(tab)
    mysqlQuery(psz("
        CREATE TABLE `",tab,"` (
            `date` DATE NULL DEFAULT NULL,
            `datexo` DATE NULL DEFAULT NULL,
            `dateminus1` DATE NULL DEFAULT NULL,
            `year` SMALLINT(6) NULL DEFAULT NULL,
            `month` TINYINT(4) NULL DEFAULT NULL,
            `datew` DATE NULL DEFAULT NULL,
            `bui` CHAR(18) NULL DEFAULT NULL,
            `pr` DOUBLE NULL DEFAULT NULL COMMENT '$ close',
            `cash` DOUBLE NULL DEFAULT NULL,
            `cashdollar` DOUBLE NULL DEFAULT NULL,
            `ret` DOUBLE NULL DEFAULT NULL,
            `turn` DOUBLE NULL DEFAULT NULL COMMENT '$ median daily turnover 5 days',
            `xo` DOUBLE NULL DEFAULT NULL,
            `xt` DOUBLE NULL DEFAULT '0',
            `xc` DOUBLE NULL DEFAULT NULL,
            `pe` DOUBLE NULL DEFAULT NULL,
            `vat` DOUBLE NULL DEFAULT NULL
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    "))
}
derti3 <-
function(
                tcost=0.0010,
                fcost=0.0025,
                manfee=0.02,
                includecash=TRUE,
                perfee=0.2,
                lever=1,
                ...
                )
{
    droptab("ti3")
    newti2("ti3")
    mysqlQuery("ALTER TABLE ti3 ADD COLUMN (freq CHAR(5))")
    derti1(tcost=tcost,fcost=fcost,manfee=manfee,lever=lever)
    for(freq in c("day","week","month")) {
        derti2(perfee=perfee,freq=freq,includecash=includecash)
        dfr <- data.frame(gettab("ti2",ret="dataframe"),freq=freq)
        sqlSave(channel=DBcon,
                    dat=dfr,    
                    tablename="ti3",
                    append=TRUE,
                    rownames=FALSE,
                    safer=TRUE)
    }       
}
derti2 <-
function(
                perfee=0.2,
                freq=c("week","day","month","year"),
                includecash=TRUE
                )
{
    freq <- match.arg(freq)
    stopifnot(includecash) #because the following line is incorrect in this case 2010-08-25
    pfields <- if(includecash) {"cashacc-tcostacc-manfeeacc+"} else {"tcostacc-manfeeacc+"}
    expr <- psz(pfields,"pe")
    freq <- match.arg(freq)
    gsffield <- c(day="date",week="datew",month="year, month",year="year")
    newti2()
    mysqlQuery(psz("
        INSERT INTO ti2 (date, year, month, datew, datexo, cashacc, xn, xg, xt, pe, pev, tcostacc, fcostacc, manfeeacc)
        SELECT MAX(date), MAX(year), MAX(month), MAX(datew), MIN(date) AS datexo, SUM(cashacc), NULL AS xn, NULL AS xg, SUM(xt), SUM(pe), SUM(pev), SUM(tcostacc), SUM(fcostacc), SUM(manfeeacc)
        FROM ti1
        GROUP BY ",gsffield[freq]
    ))
    mysqlQuery(psz("
        UPDATE ti2, ti1
        SET ti2.xn=ti1.xn, ti2.xg=ti1.xg
        WHERE ti2.datexo=ti1.date #opening values
    "))
    mysqlQuery(psz("
        UPDATE ti2
        SET pe=pe-xn*cashacc-fcostacc, pev=pev-xn*cashacc-fcostacc    #these costs are referred to opening xn and xg, as well as program size
    "))
    tab <- extract("ti2",c("date",expr)) #gsffield[freq]
    da <- as.Date(tab[,1])
    perf <- as.numeric(tab[,2])
    fundret <- fundcum <- fundperfee <- perf*0
    cumcompound <- hightodate <- 1
    for(i in which(!is.na(perf))) {
        if(0<perf[i] && hightodate<cumcompound*(1+perf[i])) {
            feepayingreturn <- (cumcompound*(1+perf[i])-hightodate)/cumcompound
            feeaccruedreturn <- feepayingreturn*perfee
        } else {
            feepayingreturn <- 0
            feeaccruedreturn <- 0
        }
        fundret[i] <- perf[i]-feeaccruedreturn
        cumcompound <- cumcompound*(1+fundret[i])
        fundcum[i] <- cumcompound
        hightodate <- max(hightodate,cumcompound)
        fundperfee[i] <- feeaccruedreturn
    }
    drawdown <- belowhigh(zoo(as.matrix(fundcum)))-1
    dfr <- data.frame(date=da,drawdown,perfee,fundret,fundcum)
    sqlUpdate(channel=DBcon,
            dat=dfr,
            tablename="ti2",
            index=c("date"))
    mysqlQuery("ALTER TABLE ti2 ENGINE=MEMORY")
    addidx("ti2","date")
}
derti1 <-
function(
                tcost=0.0025,
                fcost=0.0025,
                manfee=0.02,
                lever=1
                )
{
    ti <- gettab("ti",ret="dataframe")
    jlev <- c("xn","xg","xt","pe","pev")
    ti[,jlev] <- ti[,jlev]*lever
    newti1()
    #ti1 <- gettab("ti1")
    da <- ti[,"date",drop=FALSE]
    cost <- zoo(matrix(0,length(da),2),as.Date(da[,]))
    colnames(cost) <- c("fcost","manfee")
    cost[,"fcost"] <- fcost*as.numeric(ti[,"xg"])
    cost[,"manfee"] <- manfee
    #cols <- c("date",colnames(ti1[,-which(colnames(ti)=="date")]))
    acost <- ti
    acost[,c("fcostacc","manfeeacc")] <- accruexts(cost[,c("fcost","manfee"),drop=FALSE])
    acost[,"cashacc"] <- as.numeric(ti[,"cashdollar"])
    acost[,"tcostacc"] <- tcost*as.numeric(ti[,"xt"])
    acost[,"datexo"] <- NA
    sqlSave(channel=DBcon,
        dat=acost,    
        tablename="ti1",
        append=TRUE,
        rownames=FALSE,
        safer=TRUE)
    mysqlQuery("
        UPDATE ti1
        SET datexo = DATE_ADD(date, INTERVAL 6 DAY) #this is the weekly date whose open = date
    ")
    mysqlQuery("ALTER TABLE ti1 ENGINE=MEMORY")
    addidx("ti1","date")
}
derti <-
function(
            type=c("close","vwap"),
            ijo=NA,
            relever=1,
            ...)
{
    type <- match.arg(type)
    runli(beforeclass="ti")
    if(!any(is.na(ijo))) { #handle the 'combine' action
        stopifnot(all(ijo%in%extract("jo","DISTINCT ijo",channel=chve())))
        gati(ixx=ijo,sumtreatment="sum",gear=relever,filt=NULL)
        return(chkti())
    }
    newti()
    newgad()
    stopifnot(dergad(type=type))
    mysqlQuery(psz("
        INSERT INTO ti (date, year, month, datew, cashdollar, xn, xg, xt, pe)
        SELECT date, year, month, datew, cashdollar, SUM(xo) AS xn, SUM(ABS(xo)) AS xg, SUM(ABS(xt)) AS xt, SUM(pe) AS pe
        FROM gad
        GROUP BY date
    "))
    runli(beforeclass="ti",when="after")
    type=="vwap" || chkti(whencreated=TRUE)#cannot check for vwap
}
dergad <-
function(hedge=getsione(parameter="hedge",tab="pa",default="dollar"),type=c("close","vwap"))
{
    type <- match.arg(type)
    ret <- ifelse(type=="close","ret","revt")
    newgad()
    d1 <- extract("ty","MIN(dapo)","WHERE lapo=0")[1,1]
    d2 <- extract("ty","MAX(dapo)","WHERE lapo=0")[1,1]
    bui <- extract("po","DISTINCT bui")[,1]
    myfields <- list(
                    hedge   =psz("aared.prlo AS pr, aared.cashlo AS cash, aared.",ret,"lo AS ret"),
                    loc     =psz("aared.prlo AS pr, aared.cashlo AS cash, aared.",ret,"lo AS ret"),
                    dollar  =psz("aared.prdo AS pr, aared.cash, aared.",ret," AS ret"),
                    vwap   =psz("aared.prlo AS pr, aared.cashlo AS cash, aared.",ret,"lo AS ret")
                    )
    mysqlQuery(gadquery(myfields=myfields[[hedge]],d1=d1,d2=d2,bui=bui))
    if(hedge%in%c("hedge","vwap")) {#for minor currencies without interbank rates, use dollar premia
        mysqlQuery(gadquery(myfields=myfields[["dollar"]],d1=d1,d2=d2,bui=minorcurbui(mytab="po")))
    }
    mysqlQuery("
        UPDATE gad, va
        SET gad.vat=va.vat
        WHERE gad.datew=va.date AND gad.bui=va.bui AND va.lava=0
    ")
    mysqlQuery("
        UPDATE gad
        SET datexo = DATE_ADD(gad.date, INTERVAL 6 DAY) #this is the weekly date whose open = date
    ") 
    addidx("gad","datexo")
    system.time(mysqlQuery("
        UPDATE gad, po
        SET gad.xo=po.xo, gad.xt=po.xt
        WHERE gad.datexo=po.date AND gad.bui=po.bui
    "))
    mysqlQuery(psz("
        UPDATE gad
        SET gad.xc=gad.xo*(1+gad.ret)
    "))
    for(i in 1:4) {
        droptab("tmp1")
        mysqlQuery("CREATE TABLE tmp1 AS SELECT date, bui, xc, vat FROM gad WHERE xc IS NOT NULL")
        addidx("tmp1",c("date","bui"))
        mysqlQuery("
            UPDATE gad AS current, tmp1 AS previous
            SET current.xo=previous.xc
            WHERE current.dateminus1=previous.date AND current.bui=previous.bui
        ")
        mysqlQuery(psz("
            UPDATE gad
            SET gad.xc=gad.xo*(1+gad.ret)
        "))
    }
    mysqlQuery("UPDATE gad SET xt=0 WHERE xt IS NULL")
    mysqlQuery("UPDATE gad SET pe=gad.xo*gad.ret")
    if(type=="close") { idxgad() }
    type=="vwap"||chkgad()
}
chkti <-
function(whencreated=FALSE)
{
    if(!whencreated) {return(sqlnotempty("ti"))}
    return(sqlnotempty("ti"))   #had to do this 2010-08-04, not sure why mean performance differs with hedge=FALSE which should be the default, reason could be (a) changing cash rates (b) useage of closing & not opening cash rates
    derti3(tcost=0,fcost=0,manfee=0,perfee=0)
    tab <- as.matrix(mysqlQuery("
        SELECT a.value, ti3.pe
        FROM ti3 INNER JOIN 
            (
            SELECT value2 AS date, value
            FROM ga
            WHERE domain='pe' AND field1='comp' AND value1='tot' AND field2='date' AND type='SUM'
            ) AS a
        ON a.date=ti3.date
        WHERE ti3.freq='week'
        "))
    mode(tab)<-"numeric"
    res <- abs(cor(tab)[1,2]-1)<1.e-2 && abs(diff(apply(tab,2,sum)))<.01
    res && all(is.na(extract("ti","pev")))
}
chkgad <-
function(cancheck=FALSE) #because fails on occasion
{
    tab1 <- mysqlQuery("
        SELECT gad.datew, gad.bui, gad.xo AS gadxo, po.xo AS poxo
        FROM gad INNER JOIN po
        ON gad.bui=po.bui AND gad.datexo=po.date
        ")
    i <- apply(!is.na(tab1),1,all)
    res <- all.equal(as.numeric(tab1[i,3]),as.numeric(tab1[i,4]))    #opening positions match
    tab2 <- as.matrix(mysqlQuery("
        SELECT gad.xc AS gadxc, po.xc AS poxc
        FROM gad INNER JOIN po
        ON gad.bui=po.bui AND po.date = gad.date
        "))
    mode(tab2)<-"numeric"
    i <- apply(!is.na(tab2),1,all)
    if(cancheck) {
        res <- res && all.equal(tab2[i,1],tab2[i,2]) #closing positions match
    } else {
        res <- res && 0.995 < cor(tab2,use="c")[1,2]    #closing positions correlated
    }
    res
}
newma <-
function() 
{
    declare(tabname="ma",
            dec=decma(),
            engine="MyISAM")
}
derma <-
function(            
            te1='tall',
            te2='tall',
            minrow=20,
            kgroup=floor(getsione("nfac")/2)
            ) 
{
    runli(beforeclass="ma")
    if(sqlnrow("rece")==0) {return(FALSE)}
    newma()
    ga1derive(domain="fa",te1=te1,te2=te2)
    xyfields <- c("ret",setdiff(dirfld("ma1",c("^re","^qua$","^dev$")),c("ret","rer")))
    ma2(xyfields=xyfields,minrow=minrow,donotab=TRUE,equation=0)
    if(getsione(tab='xy',par='run'))    {   #proxy baskets1-3
        ga1derive(domain="xy",te1=te1,te2=te2)
        idxma1forma2()
        if(getsione(tab='ro',par='borrowlend')=="both") {
            ma2(minrow=minrow,xyfields=c("ret","re1"),donotab=FALSE,equation=1)
            ma2(minrow=minrow,xyfields=c("yit","yi1"),donotab=FALSE,equation=4)
        } else if(getsione(tab='ro',par='borrowlend')=="yes") {
            ma2(minrow=minrow,xyfields=c("ret","re2"),donotab=FALSE,equation=2)
            ma2(minrow=minrow,xyfields=c("yit","yi2"),donotab=FALSE,equation=5)
        } else if(getsione(tab='ro',par='borrowlend')=="no") {
            ma2(minrow=minrow,xyfields=c("ret","re3"),donotab=FALSE,equation=3)
            ma2(minrow=minrow,xyfields=c("yit","yi3"),donotab=FALSE,equation=6)
        }
    }
    #ma4(kgroup=kgroup)
    remove2from <-  c("te1","te2","lag","type","comp")  #benefit of the indexes dubious since use a view later
    for(dim1 in remove2from) {
        for(dim2 in remove2from) {
            addidx("ma",remove2from[!remove2from%in%c(dim1,dim2)] )
        }
    }    
    runli(beforeclass="ma",when="after")
    chkma(whencreated=TRUE)
}
chkma <-
function(whencreated=FALSE) 
{
    if( !("ma" %in% dirtab()) ) return(FALSE)
    !any(duplicated(apply(extract("ma","equation, lag, te1, te2, comp, type"),1,paste,collapse="-"))) && nrow(getma())>0
}
newga <-
function(tab="ga") 
{
    declare(tabname=tab, 
            dec=decga(),
            engine="MyISAM")#MEMORY
}
derga <-
function(
            te1="industry_sector",
            te2="countryfull",
            te1def="ALL",
            te2def="ALL",
            lagdef=0,
            lapodef=0,
            compdefpe=c("totx","tot","mktlong","syslong","reslong","mktshort","sysshort","resshort","syslong+sysshort"),
            compdefva=c("totx","tot","mktlong","syslong","reslong","mktshort","sysshort","resshort","syslong+sysshort"),
            compdefpo=c("gross","net","trade"),
            compdeff1=c("f1","f1short","f1long"),
            type=c("STD","SUM","AVG"),
            compound=FALSE,
            qupanel='none',#union(getsi("re")$qupanel,"year"),  #panels that have been converted to quantile panels with names prepended qqq; none -> none
            ijo=NA,
            relever=1
            ) 
{
    runli(beforeclass="ga")
    if(!any(is.na(ijo))) { #handle the 'combine' action
        stopifnot(all(ijo%in%extract("jo","DISTINCT ijo",channel=chve())))
        gaga(ixx=ijo,sumtreatment="sum",verbose=FALSE,gear=relever,collate=TRUE,filt=NULL)
        return(chkga())
    }
    notrequired <- list(po=c("STD","AVG"),va=c("STD","AVG"),f1=c("STD","AVG"))
    doqu <- is.character(qupanel) && any( psz("qqq",qupanel) %in% dirfld() ) && !any(qupanel=='none')
    if(doqu) {
        qupanel <- qupanel[which(psz("qqq",qupanel) %in% dirfld())]
        qu <- psz("qqq",qupanel)
    } else {
        qu <- NULL
    }
    newga()
    for(domain in domains()) {
        ga3args <- list(
            te1def=te1def,
            te2def=te2def,
            lagdef=lagdef,
            lapodef=lapodef,
            compdefpe=compdefpe,
            compdefpo=compdefpo,
            compdefva=compdefva,
            type=setdiff(type,notrequired[[domain]]),
            compound=compound
            )
        tt <- system.time(ga1derive(domain=domain, te1=te1,te2=te2,qu=qu))[3]
        if(getsione("verbose"))  print(psz("ga1 : ",floor(tt),"s"))
        tt <- system.time( ga2derive() )[3]
        if(getsione("verbose"))  print(psz("ga2 : ",floor(tt),"s"))
        tt <- system.time( do.call(what="ga3",args=ga3args))[3]
        if(getsione("verbose"))  print(psz("ga3 : ",floor(tt),"s"))
        if(doqu) {
            for(i in seq_along(qu)) {   #add quantiles
                query <- psz("UPDATE ga1 SET te2=CONCAT('",qupanel[i],"-',qqq",qupanel[i],")")  #te2 is assigned values like mcap-q1, where 'q1' is taken from field qqqmcap
                mysqlQuery(query)
                tt <- system.time( ga2derive() )[3]
                if(getsione("verbose"))  print(psz("ga2 qu ",qu[i],": ",floor(tt),"s"))
                tt <- system.time( do.call(what="ga3",args=c(ga3args,list(qu=qu[i]))))[3]
                if(getsione("verbose"))  print(psz("ga3 qu ",qu[i],": ",floor(tt),"s"))
            }
        }
    }
    mysqlQuery("    #insert rows for IR
        INSERT INTO ga (domain, field1, field2, value1, value2, type, value, nper)
            (
            SELECT g1.domain, g1.field1, g1.field2, g1.value1, g1.value2, 'IR' AS type , g1.value/g2.value, g1.nper
            FROM  ga AS g1, ga AS g2 
            WHERE g1.field1=g2.field1 AND g1.field2=g2.field2 AND g1.value1=g2.value1 AND g1.value2=g2.value2 AND g1.domain IN ('pe', 'ic') AND g2.domain = g1.domain AND g1.type='AVG' AND g2.type='STD'
            )
        ")
    gamkt()  #insert rows for mkt
    for(fld in setdiff(dirfld("ga"),"value")) addidx("ga",fld)
    runli(beforeclass="ga",when="after")
    chkga(whencreated=TRUE)
}
chkga <-
function(whencreated=FALSE) 
{
    if( !("ga" %in% dirtab()) ) return(FALSE)
    res <- TRUE
    ic <- extract("ga","value","WHERE domain='ic' AND type IN ('SUM' , 'AVG')")
    if(length(ic)>0 && !all(is.na(ic))) res <- res && max(abs(ic),na.rm=TRUE)<1.00001
    res <- res && sqlnotempty("ga")
    #if(sqlnotempty("ga") && whencreated) res <- res&gasumsaddup()
    return(res)
}
newvi <-
function() 
{
    droptab("pevi")
    droptab("povi")
    droptab("vavi")
    droptab("f1vi")
    droptab("favi")
    droptab("xyvi")
    droptab("icvi")
}
dervi <-
function(field="industry_sector",
                xtype=c("","IN","BETWEEN","="),
                xvalue=as.character(extract("pa",paste("DISTINCT",field))),
                dtype=c("","BETWEEN","=","IN"),
                dvalue=c("1900-01-01","2050-01-01"),
                type=c("TABLE","VIEW"),
                n=getsione("nfac")    #this arg is only included for lazy evaluation of previous two
                ) 
{
    runli(beforeclass="vi")
    newvi()
    if(getsione(par='run',tab='pe')) fovi(domain="pe",field=field,xtype=xtype,xvalu=xvalue,dtype=dtype,dvalue=dvalue,type=type)
    if(getsione(par='run',tab='pe')) fovi(domain="ic",field=field,xtype=xtype,xvalu=xvalue,dtype=dtype,dvalue=dvalue,type=type)
    fovi(domain="po",field=field,xtype=xtype,xvalu=xvalue,dtype=dtype,dvalue=dvalue,type=type) 
    if(getsione(par='run',tab='va')) fovi(domain="va",field=field,xtype=xtype,xvalu=xvalue,dtype=dtype,dvalue=dvalue,type=type)
    if(getsione(par='run',tab='va')) fovi(domain="f1",field=field,xtype=xtype,xvalu=xvalue,dtype=dtype,dvalue=dvalue,type=type)
    mysqlQuery(psz("DELETE FROM povi WHERE date NOT IN (",fldlst(getda("po"),"'","'"),")")) #because po from archive may be longer than dapo
    fovi(domain="fa",field=field,xtype=xtype,xvalu=xvalue,dtype=dtype,dvalue=dvalue,type=type)
    if(getsione(par='run',tab='xy')) fovi(domain="xy",field=field,xtype=xtype,xvalu=xvalue,dtype=dtype,dvalue=dvalue,type=type)
    runli(beforeclass="vi",when="after")
    chkvi()
}
chkvi <-
function() 
{
    res <- TRUE
    if(getsione(par='run',tab='pe')) res <- res&identical(sort(c("bui", "date","lace","lapo","ls",  "mktlong", "reslong", "syslong", "tot","totx",  "mktshort", "resshort", "sysshort")),sort(dirfld("pevi")))
    if(getsione(par='run',tab='po')) res <- res&identical(sort(c("bui",  "date", "gross","ls",   "net",  "trade")),sort(dirfld("povi")))
    if(getsione(par='run',tab='va')) res <- res&identical(sort(c("bui",  "date", "f1",   "lava", "ls",  "mktlong", "reslong", "syslong", "tot","totx",  "mktshort", "resshort", "sysshort")),sort(dirfld("vavi")))
    if(getsione(par='run',tab='va')) res <- res&identical(sort(c("bui",  "date", "f1","f1short","f1long", "lava", "ls")),sort(dirfld("f1vi")))
    if(getsione(par='run',tab='va')) res <- res&all(c("bui",  "date", "lace", "ret", "tare")%in% dirfld("favi"))
    res
}
newva <-
function() 
{
    droptab("va")
    mysqlQuery("
        CREATE TABLE `va` (
            `date` DATE NOT NULL,
            `bui` CHAR(18) NOT NULL,
            `lava` SMALLINT(6) NOT NULL,
            `f1` DOUBLE NOT NULL,
            `vam` DOUBLE NOT NULL,
            `vas` DOUBLE NOT NULL,
            `var` DOUBLE NOT NULL,
            `vat` DOUBLE NOT NULL,
            `ls` TINYINT(4) NOT NULL,
            PRIMARY KEY (`date`, `bui`, `lava`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
derva <-
function(
                xx=c("xo","xt"), #component to analyse
                cetab="ce"
                )
{

    xx <- match.arg(xx)
    runli(beforeclass="va")
    newva()
    mydace <- unique(as.character(as.matrix(mysqlQuery("SELECT dace from ty WHERE lava IN (SELECT lag FROM lava) ORDER BY dace"))))
    for(dace in mydace) {
        putdano(dace)
        mylava <- unique(as.numeric(as.matrix(mysqlQuery(psz("SELECT lava from ty WHERE lava IN (SELECT lag FROM lava) AND dace = '",dace,"' ORDER BY lava")))))
        ce <- getce(dace,tab=cetab)
        for(lava in mylava) {
            dapo <- offda(x=dace,lags=lava) #TC+lava
            po <- getpo(da=dapo,field=xx)
            prd <- prdce(x=ce,po=po,scaletovol=FALSE)
            tab <- data.frame(prd,bui=rownames(po),date=dapo,lava=lava,"ls"=as.numeric(sign(po)))
            sqlSave(channel=DBcon,      
                dat=tab,
                tablename="va",
                append=TRUE,
                rownames=FALSE,
                safer=TRUE)
        }
    }
    runli(beforeclass="va",when="after")
    chkva()
}
chkva <-
function()
{
    res <- TRUE
    res <- res && keytyva() && keypova()
    res <- res && all(sort(dirfld("va"))==sort(c("date","bui","lava","f1","vam","vas","var","vat","ls")))
    res
}
newpe <-
function() 
{
    droptab("pe")
    mysqlQuery("
        CREATE TABLE `pe` (
            `date` DATE NOT NULL,
            `bui` CHAR(18) NOT NULL,
            `lace` SMALLINT(6) NOT NULL,
            `lapo` SMALLINT(6) NOT NULL,
            `pem` DOUBLE NOT NULL,
            `pes` DOUBLE NOT NULL,
            `per` DOUBLE NOT NULL,
            `pet` DOUBLE NOT NULL,
            `re2m` DOUBLE NOT NULL,
            `re2s` DOUBLE NOT NULL,
            `re2r` DOUBLE NOT NULL,
            `re2t` DOUBLE NOT NULL,
            `po2` DOUBLE NOT NULL,
            `ls` TINYINT(4) NOT NULL,
            PRIMARY KEY (`date`, `bui`, `lace`, `lapo`),
            INDEX `lace` (`lace`),
            INDEX `lapo` (`lapo`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
derpoco <-
function(
                ijo=as.numeric(extract("me1v215","ijo","WHERE method='combo'")),
                freq=c("daily","weekly"),
                minlag=maxlag-99,
                maxlag=switch(freq,weekly=-1,daily=-4),  #this is the lowest value, for daily used with +c(-1,0,1)
                curhedge=TRUE, #applies in case of daily, otherwise pick up hedged or unhedged from vere using ijo
                dapo=rev(as.character(extract("mytmppo","DISTINCT date","ORDER BY date desc"))[-1]) ,
                pocodelete=c("all","current")   #rows to delete in table poco
                )
{
    freq <- match.arg(freq)
    retab <- switch(freq,weekly="mytmpre",daily="aared")
    if(freq=="weekly") { resve(xx="re",ixx=jolu(xx="re",ijo=ijo),to="mytmpre") } 
    bui <- as.character(extract("mytmppo","DISTINCT bui","ORDER BY bui ASC"))
    stopifnot(all(bui%in%extract(retab,"DISTINCT bui")))
    po <- getpa("tr","mytmppo")
    re <- getpa(
            field=switch(freq,weekly="rer+res",daily=ifelse(curhedge,"retlo","ret")),
            tab=retab,
            qual=psz("WHERE bui IN (",fldlst(bui,"'","'"),")")
            )
    if(freq=="daily") { re <- zoo(t(scale(t(re),scale=F)),index(re)) } #remove daily mean return as proxy for rem
    stopifnot(identical(colnames(po),colnames(re)))
    coredata(po)[is.na(coredata(po))] <- 0
    coredata(re)[is.na(coredata(re))] <- 0
    nre <- ncol(re)
    ira <- 1:nre
    ii <- nre+ira
    mysqlQuery(psz("DELETE from poco",ifelse(pocodelete=="all","",psz(" WHERE date IN (",fldlst(offda(dapo,1),"'","'"),")"))))
    for(i in seq_along(dapo)) {
        print(dapo[i])
        ipo <- as.Date(dapo[i])
        if(freq=="weekly") {
            ire1 <- offda(dapo[i],(minlag:maxlag))
            ire <- as.Date(intersect(as.Date(ire1),index(re)))
            wgts <- seq(from=1/3,to=1,len=length(ire))
        } else {
            ire1 <- as.Date(dapo[i])+(minlag:maxlag)
            ire <- match(as.Date(intersect(ire1,index(re))),index(re))+1
            wgts <- seq(from=1/3,to=1,len=length(ire))
        }
        pe <- po[ipo,,drop=FALSE]%*%t(coredata(re[ire,,drop=FALSE]))*wgts
        pet <- pe-mean(pe)
        x <- pet%*%coredata(re[ire,])
        if(freq=="daily") {
            x <- x+pet%*%coredata(re[ire-1,])
            x <- x+pet%*%coredata(re[ire+1,])
        }
        sqlSave(channel=DBcon,      
            dat=data.frame(date=offda(dapo[i],1),bui=bui,cov=as.numeric(x)), #T+1: so can be picked up next cycle in derce
            tablename="poco",
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
    }
}
derpe <-
function(
            checkpe=FALSE,
            xx=c("xo","xt") #component to analyse
            )  {
    xx <- match.arg(xx)
    runli(beforeclass="pe")
    newpe()
    droptab("tmp1")
    mysqlQuery("
            CREATE TABLE tmp1 (
            SELECT DISTINCT ty.da, re.bui, ty.lace, ty.lapo, re.rem, re.res, re.rer
            FROM ty,re 
            WHERE re.date=ty.da AND re.lace=ty.lace AND re.tare=1
            ORDER BY re.bui, re.date
            )
            ")
    droptab("tmp2")
    mysqlQuery(psz("
            CREATE TABLE tmp2 (
            SELECT DISTINCT ty.da, po.bui, ty.lapo, po.",xx," AS xx
            FROM ty,po 
            WHERE po.date=ty.dapo 
            ORDER BY po.bui, po.date
            )
            "))
    addidx("tmp1",c("da","bui","lapo"))
    addidx("tmp2",c("da","bui","lapo")) 
    mysqlQuery("
            INSERT INTO pe (date, bui, lace, lapo, pem, pes, per, pet, re2m, re2s, re2r, re2t, po2, ls)
                (
                SELECT 
                    tmp1.da as date, 
                    tmp1.bui, 
                    tmp1.lace, 
                    tmp2.lapo, 
                    tmp1.rem*tmp2.xx AS pem, 
                    tmp1.res*tmp2.xx AS pes, 
                    tmp1.rer*tmp2.xx AS per, 
                    (tmp1.rem+tmp1.res+tmp1.rer)*tmp2.xx as pet, 
                    tmp1.rem*tmp1.rem AS re2m, 
                    tmp1.res*tmp1.res AS re2s, 
                    tmp1.rer*tmp1.rer AS re2r, 
                    (tmp1.rem+tmp1.res+tmp1.rer)*(tmp1.rem+tmp1.res+tmp1.rer) as re2t, 
                    tmp2.xx*tmp2.xx AS po2, 
                    SIGN(tmp2.xx) AS ls
                FROM tmp1,tmp2
                WHERE tmp1.bui=tmp2.bui AND tmp1.da=tmp2.da AND tmp1.lapo=tmp2.lapo 
                ORDER BY tmp1.da, tmp1.bui, tmp1.lace
                )
            ")
    addidx("pe","lace")
    addidx("pe","lapo")
    droptab(c("tmp1","tmp2","tmp3"))
    runli(beforeclass="pe",when="after")
#mysqlQuery(psz("CREATE TABLE pe",currentjo()," AS SELECT * FROM pe"))
    chkpe(checkpe)
}
chkpe <-
function(chk=FALSE) 
{
    res <- TRUE
    res <- res && all(sort(dirfld("pe"))==sort(c("bui","date","lace","lapo","ls","pem","per","pes","pet","po2","re2m","re2r","re2s","re2t")))
    if(chk) res <- res && pepore()
    res
}
newpo <-
function(tab="po",pkey=TRUE) 
{
    declare(tabname=tab,
            dec=decpo(),
            engine="MyISAM")
    if(pkey) {mysqlQuery(psz("ALTER TABLE ",tab," ADD PRIMARY KEY (date,bui)"))}
    mysqlQuery("ALTER TABLE po MODIFY tapo TINYINT NULL  DEFAULT 1")
    mysqlQuery("ALTER TABLE po MODIFY tapo1 TINYINT NULL DEFAULT 1")
    mysqlQuery("ALTER TABLE po MODIFY polarity TINYINT NULL DEFAULT 1")
    mysqlQuery("ALTER TABLE po MODIFY xt DOUBLE NOT NULL DEFAULT 0")
    mysqlQuery("ALTER TABLE po MODIFY xo DOUBLE NOT NULL DEFAULT 0")
    mysqlQuery("ALTER TABLE po MODIFY xc DOUBLE NOT NULL DEFAULT 0")
    mysqlQuery("ALTER TABLE po MODIFY reason CHAR(60) NULL DEFAULT 'NA - app default'")
    mysqlQuery("ALTER TABLE po MODIFY lever DOUBLE NULL DEFAULT 1")
    mysqlQuery("ALTER TABLE po MODIFY neutral DOUBLE NULL DEFAULT 0")
    mysqlQuery("ALTER TABLE po MODIFY minxo DOUBLE NULL DEFAULT NULL")
    mysqlQuery("ALTER TABLE po MODIFY maxxo DOUBLE NULL DEFAULT NULL")
    mysqlQuery("ALTER TABLE po MODIFY alpha DOUBLE NULL DEFAULT 1")
    mysqlQuery("ALTER TABLE po MODIFY evret DOUBLE NULL DEFAULT 0")
}
derxipo <-
function(minsam=10,extendtrade=0.1,ttyp=c("gross","vol"),hasprevious=FALSE) 
{
    ttyp <- match.arg(ttyp)
    dapo <- getda("po")
    co <- gettab("co")
    if(!hasprevious) { #initialise by trading to the model tilt
        mysqlQuery(psz("
            UPDATE po
            SET po.xo=po.ti, po.xt=po.ti
            WHERE date = '",dapo[1],"'"
            ))
    }
    for(i in 1:(length(dapo))) {
        da <- dapo[i]
        if(ttyp=="vol") {
            ce <- getce(da)
        } else {
            ce <- NA
        }
        popda("no",da)
        crit <- gettab("co",psz("WHERE date = '",da,"'"))
        mysqlQuery(psz("
            UPDATE po, dano
            SET po.xc=po.xo*(1+po.evret)
            WHERE po.date=dano.date #AND po.tapo=1 #if in a tradeout, this is the last guaranteed valid premium
            "))
        xc <-  mysqlQuery("      #all tests are done on this periods closing value
            SELECT po.bui, po.xc, po.xc*po.lodir AS f1, po.ti, abs(po.ti-po.xc)*lodir AS absf1
            FROM po, dano
            WHERE po.date=dano.date
            ")
        universereduction <- as.character(extract(fields="bui",tab="po,dano","WHERE po.date=dano.date AND po.tapo=1 AND po.tapo1=0"))
        absadjust <- tradefraction <- 0  
        trade <- FALSE  
        exittrade <- FALSE
        reason <- NULL
        if(length(universereduction)>0) {
            xc <- xc[!xc[,"bui"]%in%universereduction,,drop=FALSE] #causes remaining tests to exclude universereduction
            exittrade <- TRUE
            reason <- "securityexit"
        }
        crit <- gettab(tab="co",qual=psz("WHERE control='lever' AND date = '",da,"'"),jrownames="apply",ret="d")
        #lever <- sum(abs(xc[,"xc"]))       #closing gross
        lever <- volpo(da=da,field="xc",ce=ce,ttyp=ttyp) #this should switch to testing ttyp='gross' when method='unit', but does not.  should be amended if unit is ever used...
        if(lever<crit["min","value"] || lever>crit["max","value"]) {
            trade <- TRUE
            reason <- paste(reason,"lever")
            underlevered <- 0<(crit["min","value"]-lever)
            tradefraction <- max(crit["min","value"]-lever,lever-crit["max","value"])/abs(crit["mid","value"]-lever)
        }   
        crit <- gettab(tab="co",qual=psz("WHERE control='neutral' AND date = '",da,"'"),jrownames="apply",ret="d")
        neutral <- sum(xc[,"f1"])           #closing f1
        if(neutral<crit["min","value"] || neutral>crit["max","value"]) {
            trade <- TRUE
            reason <- paste(reason,"neutral")
            tradefraction <- max(tradefraction,max(crit["min","value"]-neutral,neutral-crit["max","value"])/abs(crit["mid","value"]-neutral))
        }    
        crit <- gettab(tab="co",qual=psz("WHERE control='alpha' AND date = '",da,"'"),jrownames="apply",ret="d")
        cancomputealphacor <- (nrow(xc) - sum(apply(is.na(xc[,c("ti","xc")]),1,any)))>minsam && 
                            sum(abs(xc[,c("ti","xc")]),na.rm=TRUE)>1.e-10 && 
                            !any(apply(is.na(xc[,c("ti","xc")]),2,all))>0 && #neither column (ti, xc) is all na
                            !any(is.na(apply(xc[,c("ti","xc")],2,var,na.rm=TRUE))) &&  #both columns (ti, xc) have non-na var
                            !any(apply(xc[,c("ti","xc")],2,var,na.rm=TRUE)==0)  #both columns (ti, xc) have non-zero var
        if(cancomputealphacor) {
            alpha <- cor(xc[,c("ti","xc")],use="complete")[1,2] 
        } else { 
            alpha <- mean(c(crit["min","value"],1))
        }
        if(alpha<crit["min","value"] ) {
            trade <- TRUE
            reason <- paste(reason,"alpha") 
            tradefraction <- max(tradefraction,(crit["min","value"]-alpha)/(1-alpha))
        }
        nextda <- offda(getda("no"),1)
        if(trade) { #this section could be folded into the while loop
            f1after <- neutral*(1-min(tradefraction+extendtrade,1))
            sumabsf1 <- sum(xc[,"absf1"])
            absadjust <- ifelse((sumabsf1>1e-10)&&(sumabsf1>abs(f1after)),f1after/sumabsf1,0) #adjust trade so takes to f1=0
            nextpo.xt <- psz("(thispo.ti-thispo.xc)*",min(tradefraction+extendtrade,1)," - ABS(thispo.ti-thispo.xc)*",absadjust)
            nextpo.xo <- psz("thispo.xc+",nextpo.xt)
        } else{    
            nextpo.xo <- "thispo.xc"
            nextpo.xt <- "0"
        }
        mysqlQuery(psz("
            UPDATE po as thispo, dano
            SET thispo.reason='",reason,"', 
                thispo.neutral='",neutral,"', 
                thispo.lever='",lever,"', 
                thispo.alpha='",alpha,"'
            WHERE thispo.date = dano.date "
            ))
        firsttime <- TRUE
        regeared <- FALSE
        crit <- gettab(tab="co",qual=psz("WHERE control='lever' AND date = '",da,"'"),jrownames="apply",ret="d")
        while(firsttime || volpo(da=nextda,field="xo",ce=ce,ttyp=ttyp)<crit["min","value"] && tradefraction<1) { 
            if(firsttime) { firsttime <- FALSE } else  {
                regeared <- TRUE
                tradefraction <- min(tradefraction+extendtrade,1)
            }
            f1after <- neutral*(1-min(tradefraction,1))
            sumabsf1 <- sum(xc[,"absf1"])
            absadjust <- ifelse((sumabsf1>1e-10)&&(sumabsf1>abs(f1after)&&trade),f1after/sumabsf1,0)
            nextpo.xt <- psz("(thispo.ti-thispo.xc)*",tradefraction," - ABS(thispo.ti-thispo.xc)*",absadjust)
            nextpo.xo <- psz("thispo.xc+",nextpo.xt)
            mysqlQuery(psz("
                UPDATE po as thispo, po as nextpo, dano
                SET nextpo.xo=",nextpo.xo,", 
                    nextpo.xt=",nextpo.xt,"
                WHERE thispo.date = dano.date AND nextpo.date = '",nextda,"' AND thispo.bui = nextpo.bui
                "))            
            if(exittrade) {
                mysqlQuery(psz("
                    UPDATE po as thispo, po as nextpo, dano
                    SET nextpo.xo=0, 
                        nextpo.xt=-thispo.xc
                    WHERE thispo.date = dano.date AND nextpo.date = '",nextda,"' AND thispo.bui = nextpo.bui 
                    AND thispo.bui IN (",fldlst(universereduction,"'","'"),")
                    "))
            }
        }
        if(regeared) print(psz(da," regeared tradefraction=",round(tradefraction,3)," absadjust=",round(absadjust,3)))
    }
}
dertipo <-
function(
                method=c("neutral","unit","asis"),
                driver=c("si","me"),
                poshorttyp=c("evp","mcp","fmp1"),
                jcon=1,
                pomaxmult=NA,
                ngroups=NA,
                para=TRUE,
                ttyp=c("gross","vol"),
#                voltarget=1e-3,
                invert=TRUE,
                noshortco=NA,
                dirallowed=-1,
                dollar=FALSE,
                region=FALSE,
                poco=FALSE,
                maxnet=NA,
                ...
                ) 
{
    method <- match.arg(method)
    driver <- match.arg(driver)
    poshorttyp <- match.arg(poshorttyp)
    ttyp <- match.arg(ttyp)
    dapo <- getda("po")
    if(usesnow(length(dapo))&&para) {
        sfInit( parallel=TRUE, cpus=max(ncpus()-1,1) )
        result <- sfLapplyWrap(    
                    X=dapo, 
                    FUN=tipofun, 
                    method=method,
                    driver=driver,
                    poshorttyp=poshorttyp,
                    ttyp=ttyp,
                    jcon=jcon,pomaxmult=pomaxmult,ngroups=ngroups,invert=invert,noshortco=noshortco,dirallowed=dirallowed,dollar=dollar,region=region,poco=poco,maxnet=maxnet
                    )
        sfStop()
    } else {
        result <- lapply(    
                    X=dapo, 
                    FUN=tipofun, 
                    method=method,
                    driver=driver,
                    poshorttyp=poshorttyp,
                    ttyp=ttyp,
                    jcon=jcon,pomaxmult=pomaxmult,ngroups=ngroups,invert=invert,noshortco=noshortco,dirallowed=dirallowed,dollar=dollar,region=region,poco=poco,maxnet=maxnet
                    )
    }
    if(driver=="me") {  #apply polarity to tilt: note that polarity can be zero
        mysqlQuery("
            UPDATE po, co 
            SET po.polarity=co.value
            WHERE po.date=co.date AND co.control='polarity'
            ")
        mysqlQuery("
            UPDATE po, co 
            SET po.ti=po.ti*po.polarity, po.tr=po.tr*po.polarity
            WHERE po.date=co.date AND co.control='polarity'
            ")    }
    mysqlQuery("UPDATE po SET ti=0, tr=0 WHERE ti IS NULL") #this handles the rare condition where the stock enters but not for more than 1 period
}
derraropo <-
function(
                    form=c("linear","quadratic","asis"),
                    method=c("neutral","unit","asis"),
                    para=TRUE) 
{
    form <- match.arg(form)
    method <- match.arg(method)
    if(method=="unit") {
        copypa(ita="ce",ipa="fmp1/sdev",ota="po",opa="ra")
    } else {
        mysqlQuery(psz("
            UPDATE po,
                (SELECT DISTINCT po.date, po.te, x.ro
                    FROM po INNER JOIN
                        (SELECT date, te, count(te) as ro
                        FROM po
                        WHERE pa IS NOT NULL
                        GROUP BY date, te) AS x
                        ON x.te = po.te AND x.date = po.date
                ) as y
            SET po.ro = y.ro
            WHERE po.date = y.date AND po.te=y.te
            "))
        dapo <- getda("po")
        if(para && usesnow(length(dapo)) ) {
            sfInit( parallel=TRUE, cpus=max(ncpus()-1,1) )
            result <- sfLapplyWrap(
                        X=dapo, 
                        FUN=raropofun,
                        form=form)
            sfStop()
        } else {
            result <- lapply(
                        X=dapo,
                        FUN=raropofun,
                        form=form)
        }
        grosstest <- abs((as.numeric(extract("po","SUM(ABS(ra))","GROUP BY date")))-1)
        #if(invert) stopifnot(all(grosstest[!is.na(grosstest)]<1.e-4)) #should already be unit gross (but only if targeting gross)
    }
}
derpo <-
function(
                driver=c("si","me"),
                method=c("neutral","unit","asis"), #this argument only used if driver="si", otherwise taken from me2
                te="industry_subgroup",
                motab=c("pa","ce"),
                mo="mo",
                form=c("linear","asis","quadratic"),
                me2band=getsione(tab="co",par="leverband",def=0.15)*extract("me2","AVG(tgt)","WHERE tgt !=0"),
                me2alphamin=getsione(tab="co",par="alphamin",def=0.85),
                me2neutralband=getsione(tab="co",par="neutralband",def=0.002)*extract("me2","AVG(tgt)","WHERE tgt !=0"),
                extendtrade=0.1,
                #mosdv=FALSE,    #flag to apply mo2co which scales mo.lever by sdv(mo)
                lodirtyp=c("loadings1","betaevp","betamcp","betamvp"), #betamvp sets po.lodir <- 1; this used to determine if trade triggered (but see po.dollar)
                poshorttyp=c("evp","mcp","fmp1"),   #applies if invert=FALSE
                invert=TRUE,    #optimise
                jcon=1,         #if invert, constrain these factors to zero
                poco=FALSE,     #constrain ce.poco=0
                dollar=FALSE,   #if dollar, lambda1 is set to unity in optimisation constraint - combine this with lodirtyp=dollar for cash-neutral
                pomaxmult=-1,   #defines how closely constrained to equal weights: a value of unity will just bind on a linear ranking, 0.5 is just feasible
                ngroups=-1,     #groups between which return rankings and position rankings must correspond
                wmax=-1,        #max weight, for index weight ranked r1, -1 -> ignore these constraints
                wmin=wmax,      #min weight as a +ve number, for index weight ranked r2
                r1=-1.,         #cutoff where wmin is applied 
                r2=2.,         #cutoff where wmax is applied
                noshortco="none",   #vector te to apply dirallowed constraint to, NA means none
                region=FALSE,
                dirallowed=-1,   #1=long-only, 0=zero only, -1=no constraint
                #qu="mcap",     #quantile panel
                tapoextend=TRUE,#extend tapo by 1 period beyond end of mo for operational use without tradeout
                ttyp=c("gross","vol"), #target type if invert=T
                #voltarget=1,    #target value if invert=T : overrides DRB in me
                minte=1,        #industry tree pruning
                maxte=1e4,      #exclude using tapo if category too big
                ijo=NA,         #if non-NA, flags that portfolios are to be combined from archive
                relever=1,
                maxnet=NA,
                evolve=TRUE,    #if true, xc=xo*(1+evret)
                liquid=NA,      #if non-NA, apply constraint that |xo|<=liquid*turnover/1B, also have dirallowed=-1
                ...) 
{
    driver <- match.arg(driver)
    method <- match.arg(method)
    form <- match.arg(form)
    poshorttyp <- match.arg(poshorttyp)
    motab <- match.arg(motab)
    lodirtyp <- match.arg(lodirtyp)
    ttyp <- match.arg(ttyp)
    if(!any(is.na(ijo))) { #handle the 'combine' action
        stopifnot(all(ijo%in%extract("jo","DISTINCT ijo",channel=chve())))
        ipo <- jolu(ijo=ijo,xx="po")
        compo(ipo,relever=relever)
        return(chkpo())
    }
    stopifnot(invert||ttyp=="gross")
    if(1<minte && te =="industry_subgroup") { #prune industry tree
        agtepa(nmin=minte,ifield=te,field="industry1",reda=getda("po"))
        exprpa(te,"industry1")  #industry_subgroup is overwritten in pa and will be used thus later eg in ga #stetted 2011-03-29
    }
    if(length(jcon)==1 && jcon==0) jcon <- 1:getsione("nfac")
    runli(beforeclass="po")
    editty(pa=mo)    #restrict dapo to start with sufficient data
    #tapo
    exprpa(field="tapo",expr="tasu")
    if(method!="unit") {tapaany(field=mo,prior=FALSE,ta="tapo")}    #update pa.tapo with the validity check on model panel at lag 0 only
    if(tapoextend) {taextend(tab="pa",mycol="tapo")}   #extend tradeset 1 period
    if(maxte<1e4) {
        tecountpa(myte=te,mypa="tecount")
        exprpa("tapo",psz("(",maxte,">CAST(tecount AS DECIMAL)) AND tapo"))
    }
    domerge <- 0<nrow(gettab("po"))
    if(domerge) stopifnot(all(extract("po","distinct bui")%in%getsu()))
    if(!identical(as.character(getda("po")) , as.character(extract("po","DISTINCT date","WHERE xc!=0 ORDER BY date ASC")))) {
        if(getsione("verbose"))  print(psz(nrow(gettab("po"))," rows found"))
        if(domerge) {#copy forward and restrict dapo to those not already done
            copytabs(way="forward",tabs=c("po","ty"))
            on.exit(copytabs(way="back",tabs=c("po","ty"))) #in case of failure
            daxo <- as.character(extract("po","DISTINCT date", "WHERE xo!=0")) #dates with xo already calculated ie all
            daxc <- as.character(extract("po","DISTINCT date", "WHERE xc!=0")) #dates with xc already calculated
            transitiondate <- max(daxc)
            if(length(transitiondate)==1 && is.na(transitiondate)) transitiondate <- min(getda("po"))
            stopifnot(length(transitiondate)==1 && transitiondate%in%getda("po"))
            print(psz("p/f recalc from final provisional xc date ",transitiondate))
            mysqlQuery(psz("DELETE FROM ty WHERE dapo < '",transitiondate,"'")) #starting from transitiondate, create from scratch
            mysqlQuery("DELETE FROM po")
        }
        newpo()
        putdasupo()
        puttepapo(myte=te,motab=motab,pa=mo)
        puttatapo(evolve=evolve)
        putwgtpo(wmax=wmax,wmin=wmin,r1=r1,r2=r2,liquid=liquid)
        putcepo(poshorttyp=poshorttyp)
        derraropo(form=form,method=method)
        putlopo(lodirtyp=lodirtyp)
        if(domerge) { #combine new rows with existing
            copytabs(way="back",tabs="ty")
            mysqlQuery(psz("DELETE FROM ty WHERE dapo <'",transitiondate,"'")) #include transitiondate in dertipo & derxipo
            mysqlQuery(psz("INSERT INTO po SELECT * FROM pocopy WHERE date <'",transitiondate,"'"))
            mysqlQuery(psz("    #initialise xo on transitiondate
                UPDATE po, pocopy
                SET po.xo=pocopy.xo, po.xt=pocopy.xt
                WHERE po.date=pocopy.date AND po.bui=pocopy.bui AND po.date='",transitiondate,"'
            ")) 
        }
        dertipo(method=method,driver=driver,poshorttyp=poshorttyp,ttyp=ttyp,jcon=jcon,pomaxmult=pomaxmult,ngroups=ngroups,invert=invert,noshortco=noshortco,dirallowed=dirallowed,dollar=dollar,region=region,poco=poco,maxnet=maxnet)
        derxipo(extendtrade=0.1,ttyp=ttyp,hasprevious=domerge)
        notnullpo(method=method)
        if(domerge) { copytabs(way="back",tabs="ty") }
        on.exit()
    } else {print("no additional portfolio dates")}
    runli(beforeclass="po",when="after")
    #chkpo(fullcheck=driver=='si',ttyp=ttyp,dapo=dapo) #fullcheck not valid when editty() operates to reduce dapo
    chkpo(fullcheck=TRUE,ttyp=ttyp,dapo=getda("po")) #don't do it on merge because evret not populated
}
chkpo <-
function(
            fullcheck=FALSE, #can only be applied if driver=si and cannot be applied for composites due to multiple po.reason#default changed 2010-11-10
            ttyp="gross",
            dapo=getda("po")
            ) 
{
    tst <- TRUE
    clean <- mysqlQuery(psz("       #open(t+1) = close(t)+trade(t+1)
                SELECT this.xc+next.xt, next.xo, this.date
                FROM po AS this, po AS next, cala
                WHERE this.date=cala.minus0 AND next.date=cala.plus1 AND this.bui=next.bui AND this.date IN (",fldlst(dapo,"'","'"),")
                "))
    tst <- tst && all.equal(clean[,1],clean[,2]) #suspended 11-05-26 after error on ijo=1386
    accrue <- mysqlQuery(psz("      #close = open * (1+return)
                SELECT xo*(1+evret), xc
                FROM po
                WHERE date<'",max(dapo),"' #last period has xc=0
                "))
    i <- !apply(is.na(accrue),1,any)
    tst <- tst && all.equal(accrue[i,1],accrue[i,2],tol=1e-4)
    tradein <- mysqlQuery(psz("     #tradein means no position, maybe open one next period
                SELECT po.date, po.xo, po.xt, po.xc
                FROM po
                WHERE po.tapo=0 AND po.tapo1=TRUE AND po.date IN (",fldlst(dapo,"'","'"),")
                "))
    if(nrow(tradein)>0) { if(any(tradein[,-1]!=0)) {print("ERROR IN po - UNEXPECTED ROW");print(tradein)} }
    tradeout <- mysqlQuery(psz("    #tradeout means close position next period
                SELECT next.xo, next.xt, this.xc
                FROM po AS this, po AS next, cala
                WHERE this.tapo=1 AND this.tapo1=0 AND this.date=cala.minus0 AND next.date=cala.plus1 AND this.bui=next.bui AND this.date IN (",fldlst(dapo,"'","'"),")
                "))
    if(nrow(tradeout)>0) tst <- tst && all(tradeout[,"xo"]==0) && all.equal(tradeout[,"xc"]*-1,tradeout[,"xt"])
    if(fullcheck){  #check that control always triggers trades - only valid if co corresponds
        droptab("tmp0")
        mysqlQuery(psz("       
        CREATE TABLE tmp0
                (SELECT this.*, next.xo AS nextxo
                FROM po AS this, po AS next, cala
                WHERE this.date=cala.minus0 AND next.date=cala.plus1 AND this.bui=next.bui AND this.date IN (",fldlst(dapo,"'","'"),")
                )"))
        droptab("tmp1")
        mysqlQuery("
            CREATE TABLE tmp1
            (SELECT co.control, co.apply, co.value, posum.* FROM
                co INNER JOIN  
                    (SELECT tmp0.date, sum(tmp0.nextxo*tmp0.lodir) AS f1o, sum(tmp0.xc*tmp0.lodir) AS f1c, sum(abs(tmp0.nextxo)) AS levero, sum(abs(tmp0.xc)) AS leverc, alpha, reason
                    FROM tmp0
                    GROUP BY date) as posum
                ON co.date = posum.date
            )
        ")
        if(ttyp=="gross") {
            grosstest <- nrow(mysqlQuery("SELECT * FROM tmp1 WHERE control='lever' AND apply='min' AND levero<value-1e-10"))==0 
        }
        neutraltest <- nrow(mysqlQuery("SELECT * FROM tmp1 WHERE control='neutral' AND apply='min' AND f1o<value-1e-10"))==0 
        alphatest <- nrow(mysqlQuery("SELECT * FROM tmp1 WHERE control='alpha' AND apply='min' AND alpha<value AND reason NOT LIKE '%alpha%' AND reason NOT LIKE '%securityexit%'"))==0 
        tapotest <-  length(extract("po","ti","WHERE tapo1=0"))==0 || all(extract("po","ti","WHERE tapo1=0")==0 || is.na(extract("po","ti","WHERE tapo1=0")))
        xotest <- all(extract("po","xo","WHERE tapo=0")==0)
        if(FALSE) {
        key1 <- mysqlQuery(psz("       #extent is as documented, dapo x tapo[-1,0,1]
                    SELECT DISTINCT cala.minus0 AS date, pa.bui 
                    FROM pa, cala
                    WHERE ( pa.tapo = '1' AND (pa.date = cala.minus0 OR pa.date = cala.plus1 OR pa.date = cala.minus1)) 
                    #AND cala.minus0 >= (SELECT MIN(dapo) FROM ty GROUP BY NULL)
                    #AND cala.minus0 <= (SELECT MAX(dapo) FROM ty GROUP BY NULL)
                    AND cala.minus0 IN (",fldlst(dapo,"'","'"),")
                    ORDER BY date, bui
                    "))
        key2 <- mysqlQuery(psz("
                    SELECT DISTINCT date, bui 
                    FROM po
                    # WHERE date IN (SELECT DISTINCT dapo FROM ty)
                    WHERE date IN (",fldlst(dapo,"'","'"),")
                    ORDER BY date, bui
                    "))
        tst <- tst && all.equal(key1,key2)
        } #this removed 2011-02 because not obeyed for updates
    }
    reason <- NULL
    #reason <- paste(paste(c("neutraltest","alphatest","tapotest","xotest")[!c(neutraltest,alphatest,tapotest,xotest)],collapse=" ")," failed") # removed 11-03-19 - dependencies between si,co,po not desirable
    if(!is.null(reason)) print(reason)
    if(exists("alphatest") && !alphatest) print(mysqlQuery("SELECT * FROM tmp1 WHERE control='alpha' AND apply='min' AND alpha<value AND reason NOT LIKE '%alpha%' AND reason NOT LIKE '%securityexit%'"))
    is.null(reason)
}
newco <-
function(tabname="co") 
{
    dec <- decco()
    declare(tabname=tabname,
            dec=dec,
            engine="MyISAM")
}
derco <-
function(
                levermid=1.0,
                leverband=0.02,
                neutralmid=0.0,
                neutralband=0.02,
                alphamin=0.9,
                fresh=TRUE,     #need this until better cyclical extension implemented
                tightfactor=0, #0 for no cyclical tightening, 1 for maximum tightness on cycle
                tightper=5,
                tightoffset=0           
                ) 
{
    runli(beforeclass="co")
    if(fresh) { newco() } #removed 2011-01-28
    oldco <- gettab("co")
    if(getsione("verbose"))  print(psz(nrow(oldco)," rows found"))
    if(nrow(oldco)==0) { #fresh
        co <- cofromsi(
                levermid=levermid,
                leverband=leverband,
                neutralmid=neutralmid,
                neutralband=neutralband,
                alphamin=alphamin,
                tightfactor=tightfactor,
                tightper=tightper,
                tightoffset=tightoffset
                )
    } else { #update by extending final values [not correct if applying tightening]
        newco <- do.call(what="cofromsi",args=finalco())
        i <- which(!(newco[,"date"]%in%oldco[,"date"]))
        co <- newco[i,]
    }
    sqlSave(channel=DBcon,      
            dat=data.frame(co),
            tablename="co",
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
    runli(beforeclass="co",when="after")
    chkco()
}
chkco <-
function() {
    res <- TRUE
    res <- res && all.equal(sort(dirfld("co")),sort(c("date","control","apply","value")))
    res <- res && all(getda("po")%in%as.character(extract("co","DISTINCT date")))
    res
    }
newxy <-
function() 
{
    droptab("xy")
    mysqlQuery("
        CREATE TABLE `xy` (
            `date` DATE NOT NULL,
            `bui` CHAR(18) NOT NULL,
            `lacexy` SMALLINT(6) NOT NULL,
            `ret` DOUBLE NOT NULL,
            `re1` DOUBLE NOT NULL,
            `re2` DOUBLE NOT NULL,
            `re3` DOUBLE NOT NULL,
            `yit` DOUBLE NOT NULL,
            `yi1` DOUBLE NOT NULL,
            `yi2` DOUBLE NOT NULL,
            `yi3` DOUBLE NOT NULL,
            `tare` SMALLINT(6) NOT NULL,
            `tayi` SMALLINT(6) NOT NULL
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
derxy <-
function(...) 
{
    runli(beforeclass="xy")
    newxy()
    dace <- unique(getda("ce"))
    lacexy <- getla("ce") #getla("cexy") #a subset of lace, but if not full set then problems occur in prepend
    derrete()
    for(mydace in dace) {
        if(getsione("verbose")) print(mydace)
        teset <- gettexy(dace=mydace)
        for(myte in teset) {
            ro <- getro(dace=mydace,myte=myte)
            for(mylacexy in intersect(as.numeric(getty(key="dace",dates=mydace,ret="lace")),lacexy)) {
                re <- getrete(dace=mydace,myte=myte,lacexy=mylacexy)
                stopifnot(nrow(re[[1]])==nrow(ro[[1]]))
                stopifnot(all(rownames(re[[1]])==rownames(ro[[1]])))
                dfr <- data.frame(
                    date=mydace,
                    bui=rownames(re$ret),
                    lacexy=mylacexy,
                    ret=re$ret,
                    re1=ro$x1%*%re$ret,
                    re2=ro$x2%*%re$ret,
                    re3=ro$x3%*%re$ret,
                    yit=re$yit,
                    yi1=ro$x1%*%re$yit,
                    yi2=ro$x2%*%re$yit,
                    yi3=ro$x3%*%re$yit,
                    tare=re$tare,
                    tayi=re$tayi
                    )
                colnames(dfr) <- c("date","bui","lacexy","ret","re1","re2","re3","yit","yi1","yi2","yi3","tare","tayi")
                sqlSave(channel=DBcon, 
                    dat=dfr,
                    tablename="xy",
                    append=TRUE,
                    rownames=FALSE,
                    safer=TRUE
                    )
            }
        }
    }
    prependxy()
    mysqlQuery("ALTER TABLE xy ADD PRIMARY KEY (date,bui,lacexy)")
    runli(beforeclass="xy",when="after")
    #chkxy()
    return(TRUE)
}
chkxy <-
function()
{
    droptab("tmp1")      #xy has same (bui,date) extent as ce, subject to lace=lacexy
    mysqlQuery("CREATE TABLE tmp1 AS SELECT ty.* FROM ty INNER JOIN lacexy ON ty.lace=lacexy.lag")
    addidx("tmp1","dace")
    droptab("tmp")      
    mysqlQuery("CREATE TABLE tmp AS SELECT ce.date, ce.bui FROM ce INNER JOIN tmp1 ON ce.date=tmp1.dace")
    addidx("tmp",c("date","bui"))
    addidx("tmp",c("date"))
    addidx("tmp",c("bui"))
    res <- all.equal(
        mysqlQuery("SELECT DISTINCT tmp.bui, tmp.date FROM tmp INNER JOIN ro ON tmp.bui=ro.bui AND tmp.date=ro.dace"),
        mysqlQuery("SELECT DISTINCT tmp.bui, tmp.date FROM tmp ")
        )
    res <- res && valda(as.character(extract("xy","DISTINCT date","ORDER BY date")),contiguous=TRUE)
    droptab("tmp")
    droptab("tmp1")
    res    
}
newro <-
function() 
{
    droptab("ro")
    mysqlQuery("
        CREATE TABLE `ro` (
            `dace` DATE NOT NULL,
            `bui` CHAR(18) NOT NULL,
            `buix` CHAR(18) NOT NULL,
            `x1` DOUBLE NOT NULL,
            `x2` DOUBLE NOT NULL,
            `x3` DOUBLE NOT NULL,
            INDEX `dace` (`dace`),
            INDEX `bui` (`bui`),
            INDEX `dace_bui_buix` (`dace`, `bui`, `buix`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
derro <-
function(
                borrowlend=c("no","both","yes")    #2009-01-28 determines whether x2 (borrowlend=yes), x3 (borrowlend=no) are populated, or both (borrowlend=both)
                ) 
{
    borrowlend <- match.arg(borrowlend)
    runli(beforeclass="ro")
    newro()
    for(tc in getda("ce")) {    
        if(getsione("verbose")) print(paste("ro",tc))
        te <- as.matrix(extract(tab="texy",fields="te, bui",qual=psz("WHERE dace='",tc,"'")))
        ce <- getce(dat=tc)
        stopifnot(all.equal(sort(buice(ce)),as.character(sort(te[,"bui"]))))
        allvcv <- vcvce(ce)$T
        allldg <- ldgce(ce)
        for(thiste in unique(te[,"te"])) {     #loop through categories
            buite <- te[te[,"te"]==thiste,"bui"]
            jte <- which( as.logical(rownames(allvcv) %in% buite ))
            vcv <- allvcv[jte,jte,drop=FALSE]
            ldg <- allldg[jte,,drop=FALSE]
            stopifnot(all(rownames(vcv)==buite) && all(rownames(ldg)==buite))
            n <- nrow(vcv)
            Dmat <- vcv
            Amat <- diag(n-1)
            Amat1 <- cbind(rep(1,n-1),Amat)
            bvec <- c(1,rep(0,(n-1)))
            for(i in seq(along=buite)) {       #loop through stocks, solve for proxy portfolios
                bui <- as.character(buite[i])
                dvec <- vcv[,bui]
                if(borrowlend%in%c("both","yes")) {
                    solpos <- solve.QP(
                                Dmat=Dmat[-i,-i,drop=FALSE], 
                                dvec=dvec[-i,drop=FALSE], 
                                Amat=Amat)
                    unconstrained <- solpos$unconstrained.solution
                }
                if(borrowlend%in%c("both","no")) {
                    solposunit <- solve.QP(
                                Dmat=Dmat[-i,-i,drop=FALSE], 
                                dvec=dvec[-i,drop=FALSE], 
                                Amat=Amat1, 
                                bvec=bvec, 
                                meq=1)
                    unconstrained <- solposunit$unconstrained.solution
                }
                if(borrowlend=="yes") { #these lines supply data for the unsolved case
                    solposunit <- solpos 
                    solposunit$solution <- solposunit$solution*0 
                }
                if(borrowlend=="no") { 
                    solpos <- solposunit 
                    solpos$solution <- solpos$solution*0 
                }
                xpos <- zapsmall(solpos$solution)                   #positive
                xposunit <- zapsmall(solposunit$solution)           #positive, sum to unity
                xunc <- zapsmall(unconstrained)                    #unconstrained 
                stopifnot(all(names(dvec[-i])==buite[-i]))
                sqlSave(channel=DBcon,      
                    dat=data.frame(dace=tc,bui=bui,buix=buite[-i],x1=xunc,x2=xpos,x3=xposunit),
                    tablename="ro",
                    append=TRUE,
                    rownames=FALSE,
                    safer=TRUE)
            }
        }
    }
    addidx(tab="ro",field="dace")
    addidx(tab="ro",field="bui")
    addidx(tab="ro",field=c("dace","bui","buix"))
    droptab("rovi")
    mysqlQuery("CREATE TABLE rovi AS 
            SELECT  ro.*, texy.te AS te
            FROM ro, texy
            WHERE ro.bui=texy.bui AND ro.dace=texy.dace
            ")
    addidx("rovi",c("te","dace"))
    runli(beforeclass="ro",when="after")
    chkro()
}
chkro <-
function()
{
    res <- TRUE
    if(FALSE) {
        droptab("tmp")      #ro has same (bui,date) extent as ce
        mysqlQuery("CREATE VIEW tmp AS SELECT ce.* FROM ce INNER JOIN ty ON ce.date=ty.dace")
        res <- res&all.equal(
            mysqlQuery("SELECT DISTINCT tmp.bui, tmp.date FROM tmp INNER JOIN ro ON tmp.bui=ro.bui AND tmp.date=ro.dace"),
            mysqlQuery("SELECT DISTINCT tmp.bui, tmp.date FROM tmp ")
            )
        tmp <- mysqlQuery("SELECT rovi.te, texy.te AS tex #all bui, buix share same te.te
                FROM rovi INNER JOIN texy
                ON rovi.buix=texy.bui AND rovi.dace=texy.dace")
        res <- res && all(tmp[,1]==tmp[,2])
    }
    res <- res && all.equal(as.character(extract('ro','DISTINCT dace','ORDER BY dace')),getda('ce'))
    res
}
newrece <-
function(n=getsione("nfac"))
{
    dec <- rbind(
        c(  "date"              ,   "DATE"              ),
        c(  "bui"               ,   "CHAR(18)"          ),
        c(  "dace"              ,   "DATE"              ),        
        c(  "lace"              ,   "SMALLINT"          ),
        c(  "tare"              ,   "TINYINT"           ),  
        c(  "ret"               ,   "DOUBLE"              ),
        c(  "rer"               ,   "DOUBLE"              ),
        c(  "qua"               ,   "DOUBLE"              ),
        c(  "dev"               ,   "DOUBLE"              ),
        c(  "vat"               ,   "DOUBLE"              ),
        c(  "var"               ,   "DOUBLE"              )
        )
    for(i in 1:n) dec <- rbind(dec,c(psz("re",i),"DOUBLE"))
    for(i in 1:n) dec <- rbind(dec,c(psz("va",i),"DOUBLE"))
    dec <- cbind(dec,"NOT NULL")
    colnames(dec) <- c("xfield","xtype","xnull")
    declare(tabname="rece",
            dec=dec,
            engine="MyISAM")
}
newre <-
function(tab="re") 
{
    declare(tabname=tab,
            dec=decre(),
            engine="MyISAM")
}
newfa1 <-
function()
{
    mysqlQuery("
        CREATE TABLE `fa1` (
          `date`   date,
          `fnum`   int,
          `score`  double,
          `lace`   int,
          `gross`  double
        ) ENGINE = MyISAM
    ")
}
newfa <-
function() 
{
    declare(tabname="fa",
            dec=decfa(),
            engine="MyISAM")
}
derre <-
function(
                mnem=c("re","yi"),  #order of the arguments is important as only the first gets rece = custom factor aggregation and reno = normalisation
                from=c("prem","yi"),
                rece=FALSE,          #normalised returns
                qupanel="mcapl1",#c("mcap","cem","cev"),    #panels to be processed into quantile panels with names preceded by qqq; 'none' -> none
                daily=FALSE,
                para=TRUE,
                fresh=TRUE, #changed from FALSE 2012-01-03
                updatefinal=TRUE    #flags for deletion of final period return in previous update = current penultimate period
                )
{
    stopifnot(length(mnem)==length(from))
    stopifnot(all(from %in% dirfld()))
    runli(beforeclass="re")
    if(fresh || rece) { 
        newre() 
        newrece()
        droptab("fa1")
        newfa1()
    } 
    if(updatefinal) { 
        finaldate <- offda(max(getda("ce")),-4)
        mysqlQuery(psz("DELETE FROM re WHERE date>='",finaldate,"'"))
    } 
    daexist <- extract("re","distinct date")
    if(0<length(daexist)) stopifnot(all(extract("re","distinct bui")%in%getsu()))
    X <- setdiff(getda("ce"),daexist)
    print(psz(length(X)," rows to do"))
    if(!any(qupanel=='none')) {
        qupanel <- qupanel[which((qupanel!="year") & (qupanel %in% dirfld()))]
        for(i in seq_along(qupanel)) {
            qupa(
                field=qupanel[i],
                qu=psz("qqq",qupanel[i]), #reserved names
                ta="tace"
                )
        }
        yearpa() #for qqqyear
    }
    nn <- length(mnem)
    k <- getsione("nfac")
    if(daily) {myfun <- get("refundly")} else {myfun <- get("refun")}
    if(usesnow(length(X)) && para) {
        sfInit( parallel=TRUE, cpus=max(ncpus()-1,1) )
        result <- sfLapplyWrap(
                    X=X, 
                    FUN=myfun,
                    mnem=mnem,
                    from=from,
                    rece=rece,
                    nn=nn,
                    k=k)
        sfStop()
    } else {
        result <- lapply(
                    X=X, 
                    FUN=myfun,
                    mnem=mnem,
                    from=from,
                    rece=rece,
                    nn=nn,
                    k=k)
    }
    if(daily) {
        addidx(tab="red",fields=c("date","bui"))        
    } else {
        addidx(tab="re",fields=c("date","bui"))
        addidx(tab="re",fields=c("date","lace","tare"))
        addidx(tab="re",fields=c("date"))
        addidx(tab="re",fields=c("bui"))
        addidx(tab="re",fields=c("dace"))
        addidx(tab="re",fields=c("lace"))
        addidx(tab="re",fields=c("tayi"))
        addidx(tab="re",fields=c("tare"))
        if(rece) {
            addidx(tab="rece",fields=c("date","bui"))
            addidx(tab="rece",fields=c("date"))
            addidx(tab="rece",fields=c("bui"))
            addidx(tab="rece",fields=c("dace"))
            addidx(tab="rece",fields=c("lace"))
            addidx(tab="rece",fields=c("tare"))
            derfa()
        }
        repa()
        mysqlQuery(psz("UPDATE pa SET retpa = rem+res+rer WHERE retpa IS NULL AND date = '",rev(getda("po"))[1],"'")) #enables approximate evolution of positions when price missing
        if(!any(qupanel=='none')) {chkqqq() && chkre()} else {chkre()}
    }
    runli(beforeclass="re",when="after")
    chkre()
}
derfa <-
function() 
{
    newfa()
    k <- getsione("nfac")
    expost <- mysqlQuery("SELECT 'expost' AS type, fnum, lace, POW(STD(score),2) AS var FROM fa1 INNER JOIN lace ON fa1.lace=lace.lag GROUP BY fnum, lace ")
    expost[,1] <- paste(expost[,1],"lag",expost[,3])
    x <- unlist(getcedecomp(ca=list(fnam=1:k,fnum=1:k)))
    exante <- data.frame(type="exante",fnum=1:k,lace=-999,var=x)
    res <- data.frame(rbind(exante,expost))[,c("type","fnum","var")]
    sqlSave(channel=DBcon,      
        dat=res,
        tablename="fa",
        append=TRUE,
        rownames=FALSE,
        safer=TRUE)
    chkfa(whencreated=TRUE)
}
chkre <-
function() 
{
    res <- TRUE
    res <- res && identical(sort(dirfld("re")),sort(c("date","bui","dace","lace","tare","rem","res","rer","yim","yis","yir","tayi")))
    key1 <- extract(fields="DISTINCT date, bui",
                    tab="pa",
                    qual="WHERE tace='1' AND date IN (SELECT DISTINCT da FROM ty) AND bui!='EQ0012941200001000' ORDER BY date, bui") #prices erratic, causes problem 2011-06-15 - FIX!
    key2 <- extract(fields="DISTINCT dace, bui",
                    tab="re",
                    qual="WHERE bui!='EQ0012941200001000' ORDER BY dace, bui")
    res <- res && all(paste(key1[,1],key1[,2]) %in% paste(key2[,1],key2[,2]))
    res <-  res && max(mysqlQuery("SELECT MAX(ABS(prem-(rem+res+rer))) FROM pa GROUP BY date, bui"),na.rm=TRUE)<1.e-4 #1.e-10 #changed 11-05-11, a few rogue numbers, not sure why
    res
}
chkqqq <-
function()
{
    return(TRUE)    #bypassed because fails when there are a lot of ties, maybe even just 3?  this happens when stocks enter with short hist eg JPA IN TCS IN
    qqq <- dirfld(pat="^qqq")
    res <- TRUE
    for(i in seq_along(qqq)) {
        x<-getpa(qqq[i])
        x[x=="NoQ"] <- NA
        x[] <- as.numeric(as.factor(coredata(x)))
        mode(x) <- "numeric"
        xx <- data.frame(lapply(apply(x,1,hist,plot=FALSE,breaks=.5:5.5),"[[","counts"))
        #jmax <- xx[,which.max(lapply(xx,var))]
        res <- res && (all(apply(xx,2,sum)<50) || all(lapply(xx,var)<2))    #seems to work OK
    }
    res
}
chkfa <-
function(whencreated=FALSE) {!any(is.na(gettab("fa")))}
newce <-
function(k=getsione("nfac"),
                tabname="ce") 
{
    stopifnot(mode(tabname) %in% c("character")  && class(tabname) %in% c("character") && length(tabname)==1)
    dec <- decce(k=k)
    declare(tabname=tabname,
            dec=dec,
            engine="MyISAM")
    mysqlQuery("ALTER TABLE ce MODIFY poco DOUBLE NULL  DEFAULT 0")
}
derce <-
function(
                field="prem",
                win=getla("we"), 
                center=FALSE,
                da=sort(union(getda("ce"),getda("po"))),#include dapo because needed for pfcon
                nfac=20,
                lambda=0.1,
                normalise=c("NONE","xs","ts","tsxs"),   #rearrange on a normal cdf
                applyvix=TRUE,                          #apply inverse VIX prior to estimation
                mixvix=0.5,                             #bayesian adjustment toward 500-week mean, 1=pure vix, 0=pure 500-week mean
                bench=c("mcap","equal","minvar"),       #defines factor polarity
                vixldg=FALSE,                           #estimate panel vixce (systematic sensitivity to vix)
                vixsmooth=1,
                shrinkb=0.3,
                fresh=FALSE,
                pocoupdate=c("none","all","current"),   #update poco from poco.cov
                ...
                ) 
{
    pocoupdate <- match.arg(pocoupdate)
    stopifnot(valla(win))
    stopifnot(is.logical(center) && length(center)==1)
    stopifnot(valda(da))
    normalise <- match.arg(normalise)
    bench <- match.arg(bench)
    runli(beforeclass="ce")
    if(fresh) { newce() } #removed 2011-01-28
    daexist <- extract("ce","distinct date")
    if(0<length(daexist)) stopifnot(all(extract("ce","distinct bui")%in%getsu()))
    X <- setdiff(da,daexist) #added 2011-01-28
    print(psz(length(X)," rows to do"))
    stopifnot(field %in% dirfld())
    if("mcapl1"%in%dirfld()) {
        mcap <- lag(getpa("mcapl1"),-1) #-ve lag is right shift (for zoo, which is what getpa() returns, note that the opposite applies for xts)
    } else {
        mcap <- NA*getpa(field)
    }
    weight <- gettab("lawe")[,"weight"]
    if(usesnow(n=length(X))) {
        idxce(create="DROP")
        sfInit( parallel=TRUE, cpus=max(ncpus()-1,1) )
        result <- sfLapplyWrap(    #lapply(
                    X=X, 
                    FUN=ceWrapper, 
                    field=field, 
                    win=win, 
                    normalise=normalise, 
                    center=center, 
                    weight=weight, 
                    nfac=nfac, 
                    lambda=lambda, 
                    mcap=mcap, 
                    bench=bench,
                    applyvix=applyvix,
                    mixvix=mixvix,
                    vixsmooth=vixsmooth,
                    shrinkb=shrinkb,
                    ...)
        sfStop()
    } else {
        result <- lapply(
                    X=X, 
                    FUN=ceWrapper, 
                    field=field, 
                    win=win, 
                    normalise=normalise, 
                    center=center, 
                    weight=weight, 
                    nfac=nfac, 
                    lambda=lambda, 
                    mcap=mcap, 
                    bench=bench,
                    applyvix=applyvix,
                    mixvix=mixvix,
                    vixsmooth=vixsmooth,
                    shrinkb=shrinkb,
                    ...)
    }
    #stopifnot(identical(sort(extract("ce","DISTINCT bui")),sort(extract("su","DISTINCT bui"))))
    #stopifnot(identical(sort(extract("ce","DISTINCT date")),sort(da)))
    idxce(create="CREATE")
    if(pocoupdate!="none" && "poco"%in%dirtab()) {
        myquery <- psz("
            UPDATE ce, poco
            SET ce.poco=poco.cov
            WHERE ce.date=poco.date AND ce.bui=poco.bui
            ",ifelse(pocoupdate=="current",psz("AND date IN (",fldlst(X,"'","'"),")"),""))
        mysqlQuery(myquery)
    }
    msrv() #transfer to pa for mo
    if(vixldg) { vixce(lawe=win) }
    runli(beforeclass="ce",when="after")
    chkce()
}
chkce <-
function() 
{
    res <- TRUE
    dace <- getda("ce")
    res <- res && all(c("bui","date","fmp1","loadings1","method1","sdev","uniqueness","mvp","evp","mcp","betaevp","betamcp","betamvp","poco") %in% dirfld("ce"))
    res <- res&valce(getce(dace[1]))
    res <- res&valce(getce(dace[length(dace)]))
    key1 <- extract(fields="DISTINCT date, bui",
                    tab="pa",
                    qual="WHERE tace='1' AND ( (date IN (SELECT DISTINCT dapo FROM ty WHERE dapo IS NOT NULL)) OR (date IN (SELECT DISTINCT dace FROM ty))) ORDER BY date, bui")
    key2 <- extract(fields="DISTINCT date, bui",
                    tab="ce",
                    qual="WHERE (date IN (SELECT dace FROM ty)) OR (date IN(SELECT dapo FROM ty WHERE dapo IS NOT NULL)) ORDER BY date, bui")
    #res <- res&identical(key1,key2) #extent is (dapo+dace) x tace #disabled 11-05-09 for pe(lapo) without re-run of derce()
    res
}
newte <-
function(tab="texy") 
{
    droptab(tab)
    mysqlQuery(psz("
        CREATE TABLE `",tab,"` (
            `dace` DATE NOT NULL,
            `bui` CHAR(18) NOT NULL,
            `te` CHAR(60) NOT NULL,
            `rawte` CHAR(60) NOT NULL,
            PRIMARY KEY (`dace`, `bui`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    "))
}
dertexy <-
function(
            texy="industry_sector",
            checktete=FALSE
            ) 
{
    stopifnot(texy %in% dirfld(tab="pa"))
    stopifnot("tace" %in% dirfld(tab="pa"))
    newte()
    for(tc in getda("ce")) {    
        putdano(tc)
        rawte <- te <- getfr(texy,tacondition=TRUE,ta="tace",tab="pa")
        if(checktete) stopifnot(all(!is.na(rawte)))
        tevec <- as.character(te)
        if(any(summary(as.factor(tevec),maxsum=1e9)<=2)) {           #essential fix for categories with only one member - get rolled into largest category, should be nearest neighbour category
            itemerge <- which(summary(as.factor(tevec))<=2)
            itemergewith <- which.max(summary(as.factor(tevec),maxsum=1e9))
            te[,tevec%in%names(itemerge)] <- names(itemergewith)
        }
        for(thiste in unique(te)) {     #loop through categories
            jte <- which(as.logical(te==thiste))
            buite <- colnames(te[,jte,drop=FALSE])
            sqlSave(channel=DBcon,      
                dat=data.frame(dace=tc,bui=buite,te=thiste,rawte=as.character(rawte[,jte,drop=FALSE])),
                tablename="texy",
                append=TRUE,
                rownames=FALSE,
                safer=TRUE)
        }
    }
    addidx(tab="texy",field="dace")
    addidx(tab="texy",field="bui")
}
derte <-
function(
            texy="industry_sector",
            tepo="industry_sector",
            tega1=getsione("te1","ga"),
            tega2=getsione("te2","ga"),
            texychk=FALSE,
            tepochk=FALSE,
            tegachk=FALSE,
            indmin=3,
            indname="industry1",
            tree=ifelse(texy%in%tetree()[["bb"]],"bb","icb")
            )
{
    runli(beforeclass="te")
    dertexy(texy)
    if(!is.null(indmin)) agte1(nmin=indmin,field=indname,tree=tree) #but nb this does not make any mods to pa, only generates tmp table
    runli(beforeclass="te",when="after")
    chkte(
        texy=texy,
        tepo=tepo,
        tega1=tega1,
        tega2=tega2,        
        texychk=texychk,
        tepochk=FALSE,  #tepochk,   #this check cannot validly be done yet as mo does not exist
        tegachk=tegachk
        )
}
derrete <-
function()
{
    droptab("rete")    #rete is accessed by xy - it is easier to do this CREATE of a new table than to add to re
    mysqlQuery("CREATE TABLE rete AS 
            SELECT  re.*, texy.te AS te
            FROM re INNER JOIN texy
            ON re.bui=texy.bui AND re.dace=texy.dace
            ")
    addidx(tab="rete",fields=c("dace","te","lace"))
}
chktexy <-
function(texy="industry_sector") 
{
    stopifnot(texy %in% dirfld())
    teset <- extract("texy","DISTINCT rawte","ORDER BY te")
    res <- TRUE
    for(i in seq(along=teset)) {    #te.rawte matches pa.texy
        pate <- mysqlQuery(psz("
            SELECT pa.",texy," AS pate, texy.te, texy.rawte
            FROM texy INNER JOIN  pa
            ON pa.bui = texy.bui AND pa.date=texy.dace
            WHERE texy.rawte='",teset[i],"'
            "))
        res <- res&all(pate[,"pate"]==teset[i])
    }
    #res <- res && keysubset(x=extract("pa","date, bui","WHERE pa.tasu=1"),table=extract("texy","dace, bui")) #all tasu have valid texy [fails if ty is restricted by data]
    res
}
chktepo <-
function(tepo="industry_sector") 
{
    droptab("tmp")
    newpo(tab="tmp")     
    putdasupo(tab="tmp")
    puttepapo(tab="tmp",myte=tepo,pa="mo")    #nb this section is incomplete
    !any(is.na(extract("po","DISTINCT te")))    #tepo is valid over the basis of po, which includes the lags {-1,0,1}
}
chktega <-
function(
            tega1="industry_sector",
            tega2="countryfull"
            ) 
{
    res <- TRUE
    droptab("tmp")
    mysqlQuery(psz("
    CREATE TABLE tmp AS
        SELECT  pa.",tega1,", pa.",tega2,"
        FROM pa INNER JOIN (SELECT DISTINCT da AS date FROM ty WHERE dapo IS NOT NULL UNION SELECT DISTINCT dapo AS date FROM ty WHERE dapo IS NOT NULL) as tyda
        ON tyda.date=pa.date
        WHERE pa.tasu='1'
    "))
    res <- res && !any(is.na(gettab("tmp"))) #the tega are not null over dato, dapo
    res <- res && keysubset(                 #all tasu have valid tega    
                    x=extract("pa","DISTINCT date, bui","WHERE pa.tasu=1"),
                    table=extract("pa","DISTINCT date, bui",psz("WHERE pa.",tega1," IS NOT NULL AND pa.",tega2," IS NOT NULL")))
    res
}
chkte <-
function(
            texy=getsione("texy"),
            tepo=getsione("tepo"),
            tega1=getsione("tega1"),
            tega2=getsione("tega2"),
            texychk=getsione("texychk"),
            tepochk=getsione("tepochk"),
            tegachk=getsione("tegachk")
            ) 
{
    res <- TRUE
    if(texychk) res <- res && chktexy(texy=texy)  
    if(tepochk) res <- res && chktepo(tepo=tepo) 
    if(tegachk) res <- res && chktega(tega1=tega1,tega2=tega2)
    res
}
newta <-
function() 
{
    delfld(tabname="pa",field=dirfld(tabname="pa",pattern="^ta"))
    mysqlQuery("ALTER TABLE pa ADD (tare tinyint(1) NOT NULL)")
    mysqlQuery("ALTER TABLE pa ADD (tayi tinyint(1) NOT NULL)")
    mysqlQuery("ALTER TABLE pa ADD (tasu tinyint(1) NOT NULL)")
    mysqlQuery("ALTER TABLE pa ADD (tapo tinyint(1) NOT NULL)")
    mysqlQuery("ALTER TABLE pa ADD (tace tinyint(1) NOT NULL)")
    mysqlQuery("ALTER TABLE pa ADD (tall tinyint(1) NOT NULL)")
}
derta <-
function(
                prior=TRUE,
                ...
                ) 
{
    runli(beforeclass="ta")
    newta()
    exprpa("tall","1")                              #tall
    tapaany(field="prem",ta="tare",logic="OR")      #tare
    tapaany(field="yi",ta="tayi",logic="OR")        #tayi
    tasuderive(ta="tasu")                           #tasu
    tapaany(field="prem",ta="tasu",logic="AND")     #prem is approximated for operational use, uses prdol1, is available for T
    taextend(mycol="tasu",k=1)                      #extend to T+1
    tacederive()                                    #tace
    runli(beforeclass="ta",when="after")
    chkta()
}
chkta <-
function() 
{
    taset <- dirfld("pa","^ta")
    res <- TRUE
    for(ta in taset){ 
        res && valta(getpa(ta)) 
    }
    res
}
newpala <-
function(k=5,tab="pala") {
    stopifnot(is.numeric(k) && 0<=k)
    droptab(tab)
    mysqlQuery(psz("
            CREATE TABLE `",tab,"` (
            `date` DATE NULL,
            `date0` DATE NULL,
            `date1` DATE NULL,
            `bui` CHAR(18) NULL,
            `x0` DOUBLE NULL,
            INDEX `date1_bui` (`date1`, `bui`),
            INDEX `date0` (`date0`),
            PRIMARY KEY (`date`, `bui`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM
        ROW_FORMAT=DEFAULT
    "))
    if(0<k) {
        for(i in 1:k) {
            mysqlQuery(psz("ALTER TABLE ",tab," ADD COLUMN (x",i," DOUBLE)"))
        }
    }
}
newpa <-
function(tab="pa",su="su") 
{
    droptab("tmppa")
    dec <- cbind(xfield=c("date","bui"),xtype=c("DATE","CHAR(18)"),xnull=c("NOT NULL","NOT NULL"))#,xkey=c("1","1"))
    mysqlQuery("
        CREATE TABLE `tmppa` (
            `date` DATE NOT NULL,
            `bui` CHAR(18) NOT NULL
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
    mysqlQuery(psz("
        INSERT INTO tmppa (bui, date)
        SELECT a.bui, b.date
        FROM
            (SELECT DISTINCT bui FROM su) AS a , 
            (SELECT DISTINCT date FROM dapa) AS b
        ")) 
    addidx("tmppa","date")
    droptab(tab)
    mysqlQuery(psz("CREATE TABLE ",tab," AS SELECT * FROM tmppa INNER JOIN cala ON tmppa.date=cala.minus0"))
    nn <- dirfld()
    jdates <- nn[union(grep("minus",nn),grep("plus",nn))]
    for(j in jdates) {
        addidx(tabname=tab,fields=j)
        addidx(tabname=tab,fields=c(j,"bui"))
    }
    addidx(tabname=tab,fields=c("date"))
    addidx(tabname=tab,fields=c("bui"))
    addidx(tabname=tab,fields=c("date","bui"))
    stopifnot(chkpa(tab=tab))
}
derpa <-
function(
            hedge=c("dollar","hedge","loc","vwap"),        #determines whether local or dollar premium, return
            yi="boprl1",        #book/price
            icu=aacu(),
            ...
            ) 
{
    hedge <- match.arg(hedge)
    runli(beforeclass="pa")
    newpa()
    fi <- tabfi(c("aats","aaxs"))
    stopifnot(mode(fi) %in% c("character","list")  && class(fi) %in% c("matrix","data.frame") && nrow(fi)>0)
    alter(tabname="pa",dec=fi[!duplicated(fi[,"xfield"]),])   
    fi <- extract("fi","*","WHERE xinclude AND xhasdate ORDER BY xseq ASC")
    copypa(
        ita=unique(as.character(fi[,"xsourcetab"])),
        ipa=as.character(fi[,"xfield"]),
        ota="pa",
        opa=fi[,"xfield"],
        hasdate=TRUE
        )
    fi <- extract("fi","*","WHERE xinclude AND !xhasdate ORDER BY xseq ASC")
    copypa(
        ita=unique(as.character(fi[,"xsourcetab"])),
        ipa=as.character(fi[,"xfield"]),
        ota="pa",
        opa=fi[,"xfield"],
        hasdate=FALSE
        )
    numpa(c('prem','retpa','yi'))
    exprpa(field='prem',expr=switch(hedge,hedge="rrloto",dollar="rrdoto",loc="reloto",vwap="rrdotovw"))
    if(hedge=="hedge") {#for minor currencies without interbank rates, use dollar premia
        stopifnot(all(c("rrloto","reloto","rrdoto","redoto")%in%dirfld()))
        bui <- minorcurbui()
        mysqlQuery(psz("
            UPDATE pa
            SET rrloto=rrdoto, reloto=redoto
            WHERE bui IN (",fldlst(bui,"'","'"),")
        "))
    }
    finalretpa(hedge=hedge) #nb this is further modified in derre, where null 
    exprpa(field='yi',expr=yi)
    if(!is.na(getive(xx="cu",how="all")) && icu%in%getive(xx="cu",how="all")) {
        weightpa(icu=icu)
    } else {
        numpa("weight")
        exprpa("weight","1")
    }
    runli(beforeclass="pa",when="after")
    chkpa()
}
dermipa <-
function(
                opt=FALSE,              #option to apply inverse of crossmoment
                type=c("var","cor"),    #crossmoment type
                tab="pa",               
                pa="prem",              #panel to analyse
                out=psz(pa,components)
                ) 
{
    components <- c("m","s","r")
    iprintevery <- 20
    stopifnot(tab%in%dirtab())
    stopifnot(pa%in%dirfld(tab))
    type <- match.arg(type)
    if(!all(out%in%dirfld())) addpa(out)
    dace <- getda("ce")
    ra <- getpa(pa,tab)
    rem <- res <- rer <- ra*NA
    nfac <- getsione("nfac")
    for(i in seq_along(dace)) {
        if(getsione("verbose") && floor(i/iprintevery)*iprintevery==i) print(psz("po.mi ",dace[i]))
        suce <- extract("ce","DISTINCT bui",psz("WHERE date='",dace[i],"'"))
        supa <- colnames(ra)[!is.na(ra[as.Date(dace[i]),,drop=FALSE])]
        su <- intersect(suce,supa)
        nsu <- length(su)
        if(nsu>nfac) {
            jr <- (nfac+1):nsu
            v <- vcvce(ce,units=type)$T[su,su]
            if(opt) {
                myeig <- eigen(v)
                d <- 1/myeig$values
                al <- sweep(myeig$vectors,M=2,STAT=d,FUN="*")
                aam <- al[,1,drop=FALSE]%*%t(myeig$vectors[,1,drop=FALSE])
                aas <- al[,2:nfac,drop=FALSE]%*%t(myeig$vectors[,2:nfac,drop=FALSE])
                aar <- al[,-(1:nfac),drop=FALSE]%*%t(myeig$vectors[,jr,drop=FALSE])
                if(!all.equal(as.numeric(aam+aas+aar),as.numeric(solve(v)))) warning("ill-conditioning in vcv")
            } else {
                egn <- eigen(v)
                aam <- egn$vectors[,1,drop=FALSE]%*%t(egn$vectors[,1,drop=FALSE])
                aas <- egn$vectors[,2:nfac,drop=FALSE]%*%t(egn$vectors[,2:nfac,drop=FALSE])
                aar <- egn$vectors[,jr,drop=FALSE]%*%t(egn$vectors[,jr,drop=FALSE])
                if(!all.equal(as.numeric(aam+aas+aar),as.numeric(diag(nsu)))) warning("ill-conditioning in vcv")
            }
            rem[match(as.Date(dace[i]),index(rem)),su] <- as.numeric(ra[as.Date(dace[i]),su,drop=FALSE]%*%aam)
            res[match(as.Date(dace[i]),index(res)),su] <- ra[as.Date(dace[i]),su,drop=FALSE]%*%aas
            rer[match(as.Date(dace[i]),index(rer)),su] <- ra[as.Date(dace[i]),su,drop=FALSE]%*%aar
        }
    }
    putpa(rem,out[1])
    putpa(res,out[1])
    putpa(rer,out[1])
}
chkpa <-
function(tab="pa",key1="date",key2="bui",cartesian=TRUE) 
{
    pa <- extract(tab,psz(key1,",",key2))
    !any(duplicated(paste(pa[,key1],pa[,key2]))) && ifelse(cartesian,length(unique(pa[,key1]))*length(unique(pa[,key2])) == nrow(pa),TRUE) 
}
newda <-
function(ext=daex())
{
    for(x in ext) {
        droptab(psz("da",x))
        mysqlQuery(psz("
            CREATE TABLE `da",x,"` (
                `date` DATE NOT NULL,
                PRIMARY KEY (`date`)
            )
            COLLATE='latin1_swedish_ci'
            ENGINE=MyISAM;
        "))
    }
}
derda <-
function(
            ext=daex(),
            value=NULL
            ) 
{
    runli(beforeclass="da")
    newda()
    popda(ext=ext,value=value)
    runli(beforeclass="da",when="after")
    chkda()
}
chkda <-
function() 
{
    daset <- dirtab("^da")
    res <- TRUE
    for(da in gsub(patt="da",rep="",x=daset)) { 
        res <- res&valda(getda(da)) 
    }
    res
}
newty <-
function(name="ty",...)
{
    droptab(name)
    mysqlQuery(psz("
        CREATE TABLE `",name,"` (
            `datum` DATE NULL DEFAULT NULL,
            `date` DATE NULL DEFAULT NULL,
            `lag` SMALLINT(6) NULL DEFAULT NULL,
            INDEX `datum` (`datum`),
            INDEX `date` (`date`),
            INDEX `lag` (`lag`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    "))
}
dertypo <-
function(j="date", 
        orderby=j,
        ext="po",
        driver="si",
        startafter='1996-12-31',
        ...) 
{
    tynam <- psz("ty",ext)
    putty(ext=ext)
    retdates <- as.character(as.matrix(extract("tyce","DISTINCT date")))
    modates <- intersect(validtc(),validtp(startafter=startafter))
    if(driver=="me") modates <- intersect(modates,extract("me2","DISTINCT date"))
    universedates <- getda("su")
    putty(ext=ext,
        dateseries=retdates,
        datumseries=intersect(universedates,modates),
        lagseries=getla(ext))
    filty(j=j,orderby=orderby,ext=ext,nullity="NULL")
    addidx(tynam,"date")
    stopifnot(valty(gettab(tynam)))
}
dertycexy <-
function(j="date", 
        orderby=j)
{
    droptab("tycexy")
    mysqlQuery("CREATE TABLE tycexy AS SELECT * FROM ty LIMIT 0")
    dacexy <- offda(getda("po"),getla("wo"))
    lace <- rep(0,length(dacexy))
    dfr <- data.frame(da=dacexy,dace=dacexy,lace=lace,dapo=lace*NA,lapo=lace*NA,lava=lace*NA)
    sqlSave(channel=DBcon,
        dat=dfr,
        tablename="tycexy",
        append=TRUE,
        rownames=FALSE,
        safer=TRUE)
    droptab("tmp")
    mysqlQuery("CREATE TABLE tmp (SELECT DISTINCT ty.da FROM tycexy INNER JOIN ty ON ty.da=tycexy.da AND ty.dace=tycexy.dace)")
    mysqlQuery("DELETE FROM tycexy WHERE da IN (SELECT da FROM tmp)")
}
dertyce <-
function(j="date",
        orderby=j,
        ...) 
{
    putty(ext="ce")
    filty(j=j,orderby=orderby,ext="ce")
    addidx("tyce","date")
    stopifnot(valty(gettab("tyce")))
}
derty <-
function(
                j="date",
                orderby="da",
                driver="si",
                startafter='2000-01-01',
                detectstartfrompo=TRUE,
                extend=TRUE,    #extends su and applies event exclusions
                ...) 
{
    runli(beforeclass="ty")
    if(extend) {
        extendsu()    #extend su to the last date in aats
        if("me2"%in%dirtab()) extendsu("me2","ijo1")  #also me2
    }
    if(detectstartfrompo && 0<sqlnrow("po")) {startafter <- max(offda(as.character(extract("po","MIN(date)")),-1),startafter)}
    newty()
    dertyce(j=j,...)
    dertypo(j=j,driver=driver,startafter=startafter,...)#,include=include,...)
    dertypo(j=j,driver=driver,ext="ra")
    droptab("ty")
    mysqlQuery(psz("
        CREATE TABLE ty AS
        SELECT tyce.date AS da, tyce.datum AS dace, tyce.lag AS lace, typo.datum AS dapo, typo.lag AS lapo, tyce.lag-typo.lag AS lava
        FROM tyce INNER JOIN typo
        ON tyce.date = typo.date"," 
        ORDER BY ",orderby,"
        "))
    dertycexy()
    #mysqlQuery("INSERT INTO ty (SELECT * FROM tycexy)")
    addidx("ty","da")
    addidx("ty","dace")
    addidx("ty","dapo")
    addidx("ty","lace")
    addidx("ty","lapo")
    derlava()
    stopifnot(valty(gettab("ty"),jdate="da",jdatum="dapo",jlag="lapo"))
    stopifnot(valty(gettab("ty"),jdate="da",jdatum="dace",jlag="lace"))
    runli(beforeclass="ty",when="after")
    chkty()
}
derlava <-
function(lapo=0,lace=getla("ce")) 
{
    stopifnot(all(lapo %in% getla("po")) && all(lace %in% getla("ce")))
    lava <- sort(unique(as.numeric(outer(X=lace,Y=lapo,FUN="-"))))
    lage(la=lava,tabname="lava")
}
chkty <-
function() 
{
    res <- TRUE
    res <- res && valty(x=gettab("typo"))
    res <- res && valty(x=gettab("tyce"))
    res <- res && valty(x=gettab("ty"),jdatum="dace",jdate="da",jlag="lace")
    res <- res && valty(x=gettab("ty"),jdatum="dapo",jdate="da",jlag="lapo")
    res
}
dersu <-
function(
            excludeafter=6,  #period in months, added to max(cu.idate) for that bui -> idate2; expiry/exclusion date is min(idate2, t2)
            minstocks=5,
            check=TRUE
            )
{
    runli(beforeclass="su")
    droptab("su")
    mysqlQuery(psz("
        CREATE TABLE su AS
            SELECT c.bui, cuca.date
            FROM cuca INNER JOIN 
                (
                SELECT MAX(bui) AS bui, MAX(date) AS idate
                FROM cu
                WHERE big or bigish
                GROUP BY bui, date
                ) AS c
            ON c.idate=cuca.idate
    "))
    mysqlQuery("
        DELETE FROM su
        WHERE bui IN
            (SELECT bui FROM bbexclude)
    ")
    runli(beforeclass="su",when="after")
    if(check) {chksu(minstocks=minstocks,whencreated=TRUE)} else {return(TRUE)}
}
chksu <-
function(
            minstocks=30,
            whencreated=FALSE
            ) 
{ 
    mysu <- gettab("su")
    if(nrow(mysu)<1) return(FALSE)
    res <- valsu(mysu) 
    if("bbexclude"%in%dirtab()) {
        bbexclude <- extract("bbexclude","bui")
    } else {
        bbexclude <- NULL
    }
    res <- res && !any(getsu()%in%bbexclude)
    res <- res && all(as.numeric(extract("su","COUNT(*)","GROUP BY date"))>=minstocks)  #need to start with at least minstocks
    if(whencreated) res <- res && all(setdiff(extract("cu","DISTINCT bui","WHERE big OR bigish"),bbexclude) %in% mysu[,"bui"])
    res <- res && all(nchar(mysu[,"bui"])==18)
    res <- res && all(mysu[,"date"]%in%getca())
    res
}
newcu <-
function(tabname="cu") 
{
    droptab(tabname)
    mysqlQuery(psz("
        CREATE TABLE `",tabname,"` (
            `date` CHAR(10) NOT NULL,
            `btk` CHAR(17) NOT NULL,
            `weight` DOUBLE NULL DEFAULT NULL,
            `bui` CHAR(18) NULL DEFAULT NULL,
            `countryfull` CHAR(100) NULL DEFAULT NULL,
            `countryissue` CHAR(6) NULL DEFAULT NULL,
            `bti` CHAR(17) NOT NULL DEFAULT '',
            `btklocal` CHAR(17) NULL DEFAULT NULL,
            `weightlocal` DOUBLE NULL DEFAULT NULL,
            `big` INT(1) NULL DEFAULT NULL,
            `bigish` INT(1) NOT NULL DEFAULT '0',
            INDEX `date` (`date`),
            INDEX `bui` (`bui`),
            INDEX `big` (`big`),
            INDEX `bigish` (`bigish`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    "))
}
dercu <-
function(big=minmaxweight("big"), bigish=minmaxweight("bigish"), minconstit=30,validfor=6)
{
    runli(beforeclass="cu")
    droptab("cu")
    mysqlQuery(psz("
        CREATE TABLE cu AS
            SELECT aauniv.*, weight>",big," AS big, 0 AS bigish
            FROM aauniv, du
            WHERE aauniv.bti=du.bti AND 
                DATEDIFF(du.t1, aauniv.date) <= 183 AND
                DATEDIFF(du.t2, aauniv.date) >= 0
        "))
    addidx("cu",c("date","bui","weight","big"))
    mysqlQuery(psz("
        UPDATE cu, 
            (
            SELECT cu.bui, cu.date, cu.weight, b.indate
            FROM cu INNER JOIN 
                (
                SELECT DISTINCT bui, MIN(date) AS indate
                FROM cu
                WHERE big
                GROUP BY bui
                ) AS b
            ON cu.bui=b.bui
            WHERE big=FALSE 
                AND b.indate<cu.date 
                AND ",bigish,"<cu.weight
            ) AS a
        SET cu.bigish=1 
        WHERE cu.bui=a.bui AND cu.date=a.date
        "))
    cucaop(validfor=validfor)
    cucapriorop()
    runli(beforeclass="cu",when="after")
    chkcu(big=big, bigish=bigish, minconstit=minconstit)
}
chkcu <-
function(big=minmaxweight(), bigish=big*0.4, minconstit=30) 
{ 
    cuca <- gettab("cuca")
    #du <- gettab("du")
    #minconstit<=min(as.numeric(extract("cu","date, count(*) AS count","WHERE big OR bigish GROUP BY date")[,"count"])) && 
    #all(big < as.numeric(extract("cu","weight","WHERE big"))) &&
    #all(bigish < as.numeric(extract("cu","weight","WHERE bigish"))) &&    
    #all(as.numeric(extract("cu","weight","WHERE bigish")) <= big) &&    
    #all(min(du[,"t1"])<=cuca[,"date"]) &&
    #all(max(du[,"t2"])>=cuca[,"date"]) &&
    all(cuca[,"date"]%in%getca()) &&
    all(diff(match(sort(unique(cuca[,"date"])),getca()))==1) &&    
    valcu(gettab("cu")) 
}
newdu <-
function()
{
    droptab("du")
    mysqlQuery("
        CREATE TABLE `du` (
            `bti` CHAR(8) NOT NULL,
            `t1` CHAR(10) NOT NULL,
            `t2` CHAR(10) NOT NULL,
            `include` TINYINT(1) NOT NULL,
            PRIMARY KEY (`bti`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
derdu <-
function(du=rbind(t(c("FR","2008-12-31","2009-12-31","1")),t(c("BE","2008-12-31","2009-12-31","1")))) 
{
    colnames(du) <- c("bti","t1","t2","include")
    stopifnot(valdu(du))
    runli(beforeclass="du")
    newdu()
    xx <- data.frame(du)
    xx[,"include"] <- as.integer(xx[,"include"])           
    xx[,"t1"] <- as.character(xx[,"t1"])                   
    xx[,"t2"] <- as.character(xx[,"t2"])                   
    #putdu(xx)
    sqlSave(channel=DBcon, 
                dat=xx,
                tablename="du",
                append=TRUE,
                rownames=FALSE,
                safer=TRUE
                )
    runli(beforeclass="du",when="after")
    chkdu()
}
chkdu <-
function() 
{ 
    valdu(getdu())
}
newla <-
function() {}
derla <-
function(
            wi=230,
            laca=-1:1,
            lapo=c(-20,-10,-1,0,1,10),
            lawo=-1,
            lace=-1:3,
            lava=lace,
            lacexy=0,
            lami=-3:3
            )
{
    runli(beforeclass="la")
    #newla()
    caladerive(la=sort(union(laca,-1:1)))
    lawo <- union(lawo,0)
    lawederive(la=-1*(wi:1))
    lage(tabname="lapo",la=lapo)
    lage(tabname="lace",la=lace)
    lage(tabname="lawo",la=min(lawo):max(lawo))
    lage(tabname="lava",la=lava)        #lava is a member of lace, TP-TC, for variance attribution 
    lage(tabname="lacexy",la=lace)    #lacexy is a stub, not implemented fully (gaps occur in xy if not same as lace)
    lage(tabname="lara",la=min(lapo):max(lapo))
    lage(tabname="lami",la=lami)
    runli(beforeclass="la",when="after")
    chkla()
}
chkla <-
function(la=laex()) 
{
    res <- TRUE
    for(i in seq(along=la)) {
        res <- res&valla(getla(la[i]))
    }
    res
}
newca <-
function()
{
    droptab("ca")
    mysqlQuery("
        CREATE TABLE `ca` (
            `date` DATE NOT NULL,
            PRIMARY KEY (`date`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
derca <-
function(start='1996-01-03',                 #coercible to Date
                   end='2014-12-24',                    #coercible to Date
                   by="day",
                   weekday=TRUE,                    
                   find="all",    
                   period="week",
                   partials=FALSE,
                   firstlast=FALSE,
                   select=3
                   )
{
    mysqlQuery("SET default_week_format=3")
    value <- extractDates(dates=seq(from=as.Date(start),by=by,to=as.Date(end)),
                    weekday=weekday,find=find,period=period,partials=partials,firstlast=firstlast,select=select)
    stopifnot(class(value)%in%c("Date","character"))
    if(class(value)=="Date") stopifnot(value==round(value))
    runli(beforeclass="ca")
    newca()
    query <- paste("INSERT INTO ca (date)
                    VALUES ",
                    explicitvalues(sort(as.character(value)))
                    )
    mysqlQuery(query)
    runli(beforeclass="ca",when="after")
    chkca()
}
chkca <-
function()
{
    valca(getca())
}
newfi <-
function() 
{
    droptab("fi")
    mysqlQuery("
        CREATE TABLE `fi` (
            `xfield` CHAR(60) NOT NULL,
            `xsourcetab` CHAR(20) NOT NULL,
            `xhasdate` TINYINT(1) NOT NULL,
            `xseq` TINYINT(4) NOT NULL,
            `xtype` CHAR(60) NOT NULL,
            `xnull` CHAR(8) NOT NULL,
            `xkey` TINYINT(1) NOT NULL,
            `xindex` TINYINT(1) NOT NULL,
            `xinclude` TINYINT(1) NOT NULL,
            `xtkr` CHAR(18) NOT NULL,
            PRIMARY KEY (`xfield`, `xsourcetab`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
derfi <-
function(
            myfixa=c("countryfull","countryissue","industry_sector","industry_group","industry_subgroup"),
            myfipa=c("reloto","redoto","rrdoto","prdo","prdol1","boprl1") 
            ) 
{
    myfixa <- union(myfixa,c("countryfull","countryissue","industry_sector","industry_group","industry_subgroup","name","btimax","icb_industry_name","icb_supersector_name","icb_sector_name","icb_subsector_name")) #expanded set for release
    myfipa <- union(myfipa,c("reloto","redoto","rrdoto","prdo","prdol1","boprl1","prlo","bestl1","mcapl1","diprl1","erprl1","caprl1","rrloto","turn","rrdotovw"))
    runli(beforeclass="fi")
    newfi()
    x <- rbind(
            fixa(tab="aats",seq=1),
            fixa(tab="aaxs",seq=2)
            )
    sqlSave(channel=DBcon,
            dat=x,
            tablename="fi",
            append=TRUE,
            rownames=FALSE,
            safer=FALSE)
    edtfi(field="weightmax",xtype="DOUBLE",include=0)
    edtfi(field=myfipa,xsourcetab="aats",xhasdate="1")
    edtfi(field=myfixa,xsourcetab="aaxs")#,xtkr="id_bb_unique")
    runli(beforeclass="fi",when="after")
    chkfi()
}
chkfi <-
function(tab="fi") 
{
    tsfields <- extract("fi","DISTINCT xfield","WHERE xsourcetab LIKE '%ts'")
    res <- all(tsfields%in%dirfld("aats"))
    xsfields <- extract("fi","DISTINCT xfield","WHERE xsourcetab LIKE '%xs'")
    res <- res && all(xsfields%in%dirfld("aaxs"))
    res <- res && identical(sort(dirfld(tab)),sort(c("xsourcetab","xhasdate","xseq","xfield","xtype","xnull","xkey","xindex","xinclude","xtkr")))
    res
}
newli <-
function(
                filepath=psz(Aaroot,"lib/"),
                archive=FALSE       #flag to auto-archive each file within the directory
                ) 
{
    if(substr(filepath,start=nchar(filepath),stop=nchar(filepath))!="/") psz(filepath,"/")
    ddlli()
    #declare("li",decli())
    for(myclass in allxx()) {
        classpath <- psz(filepath,myclass,"/")
        fnams <- suppressWarnings(dir(classpath))
        if(length(fnams)>0) {
            for(fnam in fnams) {
                if(archive) {
                    declare("li",decli())
                }
                declare(tabname="tmp",dec=decli()[1,,drop=FALSE])
                mysqlQuery(psz("
                    LOAD DATA LOCAL INFILE '",classpath,fnam,"'
                    INTO TABLE tmp
                    LINES TERMINATED BY '\r\n' 
                    "))
                mysqlQuery(psz("ALTER TABLE tmp ADD COLUMN (fun CHAR(60) DEFAULT '",strsplit(fnam,split='\\.')[[1]][1],"' )"))
                mysqlQuery(psz("ALTER TABLE tmp ADD COLUMN (beforeclass CHAR(10) DEFAULT '",myclass,"')"))
                mysqlQuery("ALTER TABLE tmp ADD COLUMN (line SMALLINT AUTO_INCREMENT PRIMARY KEY )")
                mysqlQuery("INSERT INTO li (fun, line, code, beforeclass) SELECT fun, line, code, beforeclass FROM tmp")
                if(archive) {
                    arcve("li",mycomment=fnam)
                }
            }
        }
    }
    derli()
}
derli <-
function(
                loc=FALSE     #to the global environment
                ) 
{
    runli(beforeclass="li")
    sourcecodein(tab='li',loc=loc)
    runli(beforeclass="li",when="after")
    valli()  
}
chklisi <-
function()
{
    x=extract("li","DISTINCT fun")
    arg <- sourcecodein('li')
    for(i in seq_along(arg)) {
        xx <- substr(names(arg)[i],3,4)
        si <- gettab("si",qual=psz("WHERE mytable='",xx,"'"))
        if(!all(arg %in% si[,'parameter'])) return(FALSE)
    }
    return(TRUE)
}
chkli <-
function(whencreated=FALSE) {TRUE}
newcon <-
function(odbcname=c("bloomberg","aa0","aa1","aa2","aa3","aa4","aa5","aa6","aa7","bb1"))
{
    odbcname <- match.arg(odbcname)
    DBcon <<- odbcConnect(odbcname)
    if(odbcname=="bloomberg") { bloomberg <<- DBcon }
}
dirtab <-
function(pattern="",channel=DBcon) 
{ 
    res <- NULL
    for(patt in pattern)
        res <- union(res,grep(pattern=patt,
                            x=as.matrix(mysqlQuery("SHOW TABLES",channel=channel)),
                            value=TRUE)
                    )
    res
}
dirfld <-
function(tabname="pa",pattern="",channel=DBcon,check=TRUE)    
{ 
    if(check) {stopifnot(tabname %in% dirtab(channel=channel))}
    res <- NULL
    for(i in seq_along(pattern)) {
        res <- union(res,grep(pattern=pattern[i],
            x=mysqlQuery(psz("SHOW COLUMNS FROM ",tabname),channel=channel)[,1],
            value=TRUE) )
    }
    res
}
outxx <-
function() 
{
    c("ma","lo","fa")#,"ra","be","me")
}
intxxwithdates <-
function() #these int are checked for date overlap on archive
{
    c("ce","re","co","po")
}
intxx <-
function() 
{
    c("ce","re","co","po","ga","ti")
}
inpxx <-
function() 
{
    c("su","li","si")
}
allxxlong <-
function() 
{
    long <- c(li="lib",fi="field",ca="calendar",la="lag",du="index",cu="constituent",su="security",ty="temporal",da="date",pa="panel",ta="tradeset",ce="covariance",re="return",co="control",po="portfolio",pe="performance",va="variance",vi="views",te="category",ro="proxyportfolio",xy="proxyvalue",ra="rank",ga="aggregate",ti="timeseries",ma="summary",lo="log",si="simulation",ve="archive",jo="job",qu="queue",mi="optimise")
    long[allxx()]
}
allxx <-
function() 
{
    c("li","fi","ca","la","du","cu","su","ty","da","pa","ta","te","ce","re","ro","xy","co","po","pe","va","vi","ra","ga","ti","ma","lo","si","ve","jo","qu","mi")
    c("li","fi","ca","la","du","cu","su","ty","da","pa","ta","te","ce","re","ro","xy","co","po","pe","va","vi",     "ga","ti","ma","lo","si","ve","jo","qu"     )
}
allvexx <-
function() 
{
    c(inpxx(),outxx(),intxx(),c("cu","me"))#,c("cu","be","me","nu","fo","ge"))
}
tgt.solve.QP <-
function(
                    ce,         #ce
                    dvec,       #return, colnames(dvec) in ce
                    constr,     #constraints list(Am,bv,meq) - nrows(constr$Am)=ncol(dvec)
                    tgt=.1,     #target for vol or gross
                    ttyp=c("gross","vol"),   #target type
                    tol=.1,      #tolerance
                    vcomp=c("T","S","R","M")
                    )
{
    stopifnot(tgt>0)
    stopifnot(tol>1.e-18)
    stopifnot(!is.null(rownames(dvec))&& all(rownames(dvec)%in%buice(ce)))
    `volqp` <- function(w,Dmat) {sqrt(as.numeric(t(w)%*%Dmat%*%w))} #these functions inside due to limited error checking for performance
    `grossqp` <- function(w,Dmat) {sum(abs(w))}
    `tgtqp` <- function(x, Dmat, dvec, constr, tgt, ttyp, objfun) {
        w <- solve.QP(Dmat=x*Dmat, dvec=dvec, Amat=constr$Am, bvec=constr$bv, meq=constr$meq)$solution
        return(objfun(w=w,Dmat=Dmat) - tgt)
    }
    bui <- rownames(dvec)
    ttyp <- match.arg(ttyp)
    vcomp <- match.arg(vcomp)
    objfun <- switch(ttyp,'vol'=volqp,'gross'=grossqp)
    Dmat <- vcvce(ce)[[vcomp]][bui,bui]
    w0 <- solve(Dmat,dvec) 
    scal <- objfun(w=w0,Dmat=Dmat)
    upr <- 5*scal/tgt #gives tgt-tgt*<0 because tgt decreases with lambda (checked on next line)
    if(tgtqp(x=upr, Dmat=Dmat, dvec=dvec, constr=constr, tgt=tgt, ttyp=ttyp, objfun=objfun)>0) stop("unexpected condition in tgt.solve.QP")
    lwr <- .05*scal/tgt #first estimate for upper bound
    while(tgtqp(x=lwr, Dmat=Dmat, dvec=dvec, constr=constr, tgt=tgt, ttyp=ttyp, objfun=objfun)<0) {lwr <- lwr*.2;print(paste("lowering lambda for soln. -",lwr))}
    estim.prec <- root <- 1
    while(estim.prec>0.01*root && tol>1.e-18) {
        sol <- uniroot(
                    f=tgtqp,
                    interval=c(upr,lwr),
                    Dmat=Dmat, 
                    dvec=dvec, 
                    constr=constr,
                    tgt=tgt,
                    ttyp=ttyp,
                    tol=tol,
                    objfun=objfun)
        root <- sol$root
        estim.prec <- sol$estim.prec
        tol <- tol/10
        if(estim.prec>0.01*root) print("lowering tolerance to achieve accuracy")
    }
    sol <- solve.QP(Dmat=root*Dmat, dvec=dvec, Amat=constr$Am, bvec=constr$bv, meq=constr$meq)
    list(root=root,solution=sol) 
}
poslim <-
function(
                n=5,            #number of assets
                lwr=-1*abs(upr),#lower constraint
                upr=NA,       #upper constraint
                lambda=NA,    #exposures, these equated to zero within categ
                categ=NA,   #within categ, f1 nets to zero (column vector) (these normally are industries)
                categ2=NA,  #within these categories, those identified by noshortco have the dirallowed constraint applied (these normally are countryissue)
                ngroups=NA, #number of groups, between groups ranking is invariant so quantile portfolios based on fo have ascending net,-ve means disable, 0 means n
                fo=NA,      #required for ngroups option
                noshortco=NA,#values of te with dirallowed constraint
                dirallowed=-1, #direction of dirallowed constraint
                currencyblock=FALSE,
                region=FALSE,   #if TRUE, neutral within neutralregions
                neutralregions=c("Other","Europe"),
                zero=is.na(upr[1])&is.na(lwr[1])&all(is.na(lambda))&all(is.na(categ))&all(is.na(ngroups)&!currencyblock&!region),
                maxnet=NA
                )
{
    stopifnot(is.numeric(n) && length(n)==1)
    stopifnot( (length(lwr)==length(upr)) && length(lwr)%in%c(1,n) )
    stopifnot(!((-1<dirallowed) && (!is.na(upr[1])))) #can't have both dirallowed and upr/lwr constraints
    stopifnot(length(maxnet)==1)
    if(length(ngroups)==0) ngroups <- NA # this occurs when a parameter is set to NA in gui
    if(!any(is.na(lambda)) && !any(is.na(fo))) {stopifnot(all.equal(rownames(lambda),colnames(fo)))}
    if(!any(is.na(categ)) && !any(is.na(fo))) {stopifnot(all.equal(rownames(categ),colnames(fo)))}
    if(!any(is.na(categ2)) && !any(is.na(fo))) {stopifnot(all.equal(rownames(categ2),colnames(fo)))}
    if(!is.na(ngroups) && ngroups==0) ngroups <- n
    if(!zero) {
        Am <- NULL
        bv <- NULL
        meq <- 0
        if(!is.na(dirallowed) && -1<dirallowed && any(categ2%in%noshortco)) { #noshortco is applied
            i <- which(categ2%in%noshortco)
            Am <- diag(rep(1,n))[,i]
            epsilon <- 1e-12
            bv <- rep(ifelse(dirallowed==0,0,-1*epsilon),length(i)) #not clear why this delta-minus is needed as 0 should be OK
            if(dirallowed==0) meq <- meq+length(i)
        } else if(!is.na(upr[1])) { #or upper and lower is applied
            stopifnot((is.numeric(upr) && is.numeric(lwr)) )
            if(length(lwr)==1) lwr <- rep(lwr,n)
            if(length(upr)==1) upr <- rep(upr,n)
            Am <- cbind(diag(rep(1,n)),diag(rep(-1,n)))
            bv <- c(lwr,-1*upr)
        }
        if(region) { #region neutral
            xm <- regionflags(co=categ2,simple=TRUE)[,neutralregions,drop=FALSE] * lambda[,1] #for f1-neutral regions - modified from $-neutral 2012-02-01
            Am <- cbind(xm,Am)
            bv <- c(rep(0,ncol(xm)),bv)
            meq <- meq+ncol(xm)
        }
        if(!any(is.na(categ))) {    #factor 1 neutral within each categ
            stopifnot(length(categ)==n)
            #xlev <- attr(as.factor(categ),"levels")
            nincat <- summary(as.factor(categ),maxsum=1e9)
            xlev <- names(summary(as.factor(categ),maxsum=1e9)[1<nincat])#no constraint for singleton
            xm <- matrix(NA,n,length(xlev),dimnames=list(NULL,xlev))
            for(j in 1:ncol(xm)) xm[,j] <- as.numeric(categ==xlev[j])*lambda[,1]
            Am <- cbind(xm,Am)
            bv <- c(rep(0,ncol(xm)),bv)
            meq <- meq+ncol(xm)
            if(!any(nincat==1)) lambda <- lambda[,-1,drop=FALSE] #no further f1 constraint needed if no singleton
        }
        if(currencyblock && any(categ2%in%c("JP","US","CA",euroiso()))) { #currency block neutral
            xm <- cbind(as.numeric(categ2=="JP"),as.numeric(categ2%in%c("US","CA")),as.numeric(categ2%in%euroiso()))
            xm <- xm[,which(as.logical(apply(xm,2,max))),drop=FALSE]
            Am <- cbind(xm,Am)
            bv <- c(rep(0,ncol(xm)),bv)
            meq <- meq+ncol(xm)
        }
        if(any(!is.na(lambda))) {
            stopifnot(inherits(lambda,"matrix") && nrow(lambda)==n )
            Am <- cbind(lambda,Am)
            bv <- c(rep(0,ncol(lambda)),bv)
            meq <- meq+ncol(lambda)
        }
        if(!is.na(ngroups) && 1<ngroups && 2<n) {
            stopifnot(!any(is.na(fo)) && length(fo)==n)
            ngroups <- min(ngroups,n)
            igroup <- ceiling(seq(from=1.e-10,to=ngroups,length=n))
            jord <- order(fo)
            Am1a <- Am1b <- matrix(0,ngroups-1,n)
            for(i in 1:nrow(Am1a)) {
                Am1a[i,which(igroup==i)] <- -1/sum(igroup==i)
                Am1a[i,which(igroup==i+1)] <- 1/sum(igroup==(i+1))
            }
            Am1b[,jord] <- Am1a
            stopifnot(  all(Am1b%*%t(fo)>=0) && #each row has positive fo
                        all(apply(abs(Am1b),1,sum)>0) && #each row has a constraint
                        all.equal(apply(Am1b,1,sum),rep(0,nrow(Am1b))) #each row is zero net
                        )
            Am <- cbind(t(Am1b),Am)
            bv <- c(rep(0,nrow(Am1b)),bv)
        }
        if(!is.na(maxnet)) {
            Am <- cbind(Am,rep(-1,nrow(Am)))
            bv <- c(bv,-1*maxnet)
        }
        result <- list(Am=Am,bv=bv,meq=meq)
    } else {
        result <- list(Am=matrix(rep(0,n)),bv=0,meq=1)
    }
    result
}
con.solve.QP <-
function(
                ce,
                fo=t(ldgce(ce)[,1,drop=FALSE]),
                tgt=1,
                ttyp=c("gross","vol"),
                tol=.1,
                vcomp="T",
                jcon=1,
                pomaxval=NA,
                maxxo=NA,
                minxo=NA,
                ngroups=NA,
                categ=NA,
                categ2=NA,
                noshortco=NA,
                dirallowed=-1,
                dollar=FALSE,
                region=FALSE,
                poco=FALSE,
                maxnet=NA
                )
{
    stopifnot(all(colnames(fo)%in%buice(ce)) && !any(is.na(fo)))
    stopifnot(all(is.na(categ)) || all(colnames(fo)==rownames(categ)))
    if(!is.na(pomaxval) && pomaxval<=0) pomaxval <- NA #0 or -ve flags no position limits via this method
    stopifnot(is.na(pomaxval) || ttyp!="gross" || tgt<=pomaxval*length(fo))
    stopifnot(is.na(pomaxval) || is.na(maxxo[1]))   #cannot use pomaxmult and maxxo
    if(length(ngroups)==0) ngroups<-NA
    if(!is.na(ngroups) && ngroups<0) ngroups<-NA #-ve flags NA, 0 flags n
    bui <- colnames(fo)
    x <- ldgce(ce)[bui,,drop=FALSE]
    if(length(jcon)==1 && jcon<=0) {lambda <- NULL} else {
        lambda <- ldgce(ce)[bui,jcon,drop=FALSE]
        if(dollar) {lambda[,1] <- 1}
    }
    if(poco) {lambda <- cbind(lambda,pococe(ce)[bui,,drop=FALSE])}
    if(is.null(lambda)) {lambda <- NA}
    lwr <- ifelse(!is.na(pomaxval) && pomaxval!=0,-pomaxval,NA)  
    upr <- ifelse(!is.na(pomaxval) && pomaxval!=0,pomaxval,NA)
    if(is.na(lwr) && !is.na(minxo[1]) && dirallowed==-1) lwr <- minxo
    if(is.na(upr) && !is.na(maxxo[1]) && dirallowed==-1) upr <- maxxo
    con <- poslim(
            n=length(bui),
            lwr=lwr,
            upr=upr,
            lambda=lambda,
            ngroups=ngroups,
            fo=fo,
            categ=categ,
            categ2=categ2,
            noshortco=noshortco,
            region=region,
            dirallowed=dirallowed,
            maxnet=maxnet)
    tsq <- tgt.solve.QP(
            ce=ce,
            dvec=t(fo),
            constr=con,
            tgt=tgt,
            ttyp=ttyp,
            tol=tol,
            vcomp=vcomp) 
    sol <- tsq$solution$solution
    if(any(jcon>=0)) stopifnot(all(sol%*%lambda<1.e-10)) 
    sol <- zapsmall(sol)#applying zapsmall in prev line can cause failure of the neutrality test
    if(!is.na(pomaxval)) stopifnot(all(abs(sol)<=pomaxval+1.e-10))
    if(!is.na(ngroups)) {#constraints may become infeasible if !is.na(categ)
        A <- poslim(ngroups=ngroups,fo=fo,n=length(fo),categ=categ)$Am
        stopifnot(all(fo%*%A>=0) && all(sol%*%A>-1.e-10))
    }
    sol
}
tabsumm <-
function(tab)
    {
    tab <- as.matrix(tab)
    tab <- cbind(tab,apply(tab,1,paste,collapse=""))
    colnames(tab)[ncol(tab)] <- "pasted cols"
    result <- matrix(NA,4,ncol(tab),dimnames=list(c("noNAunique","noNArepeat","NA","SUM"),colnames(tab)))
    result[1,] <- (unlist(lapply(apply(rbind(tab,rep(NA,ncol(tab))),2,unique),length))-1)
    for(j in 1:ncol(tab)) 
        {
        notna <- tab[which(!is.na(tab[,j])),j]
        result[2,j] <- length(notna)-length(unique(notna))
        }
    result[3,] <- apply(is.na(tab),2,sum)
    result[4,] <- apply(result[1:3,],2,sum)
    result
    }
capwords <-
function(s, strict = FALSE) {
     cap <- function(s) paste(toupper(substring(s,1,1)),
                   {s <- substring(s,2); if(strict) tolower(s) else s},
                              sep = "", collapse = " " )
     sapply(strsplit(s, split = " "), cap, USE.NAMES = !is.null(names(s)))
 }
lags <-
function(x,           #vector, mode preserved in output
                la=0,           #lagseries
                pad=FALSE)      #flag to retain rows where x is NA
{
    if(!valla(la)) stop("lags not valid")
    x <- x[seq_along(x)]
    la0 <- sort(union(la,0))
    o1 <- max(la0)
    o2 <- min(la0)
    oo <- o1-o2
    suppressWarnings(z <- matrix(data=c(x,rep(NA,1+oo)), # the warnings arise from the data being nrows+1 in length, which is deliberate
                nrow=(length(x)+oo),
                ncol=oo+1,
                dimnames=list(NULL,latotxt(o1:o2))))
    if(pad) {
        i <- 1:nrow(z)
    } else {
        i <- which(!is.na(z[,latotxt(0)]))
    }
    res <- z[i,latotxt(la),drop=FALSE]
    rownames(res) <- as.character(z[i,latotxt(0),drop=FALSE])
    res
}
buialigned <-
function(x,ret) { all(colnames(ret)==rownames(x$loadings)) }
addq <-
function(x)
{
    stopifnot(is.matrix(x))
    cbind(x,(x[,1,drop=FALSE]-mean(x[,1,drop=FALSE],na.rm=TRUE))**2)
}
dumpti1 <-
function(
                ipo=getive(xx="po"),
                myfile="z:/-Amberalpha/contact/brummer/weekly/po.csv",
                gearing=1.5,
                append=FALSE,
                li="li",
                da=extract("po","DISTINCT date","where ti is not null and ti !=0 ORDER BY date DESC")[2] #select required
                ) 
                {
    options(scipen=100)  #force no scientific notation
    resve(xx="po",ixx=ipo)
    tab <- extract("po",psz("date, bui, te, lodir, ti*",gearing," AS ti"),psz("WHERE date='",da,"' ORDER BY te, ti"))
    tab1 <- extract("aaxs","bui, countryissue, name",psz("WHERE bui IN (",fldlst(tab[,"bui"],"'","'"),")"))
    tab2 <- cbind(tab,tab1[match(tab[,"bui"],tab1[,"bui"]),])
    stopifnot(all(tab2[,2]==tab2[,6]))
    #tab2[,"ti"] <- tab2[,"ti"]*gearing
    j <- c("date","bui","name","te","countryissue","lodir","ti")
    tab3 <- tab2[,j]
    tab3[,"bui"] <- paste(tab2[,"bui"],"equity")
    for(i in 1:2) tab3[,"date"] <-  sub(x=tab3[,"date"],patt="-",rep="")
    tab3 <- cbind(tab3,pf=ifelse(as.numeric(tab3[,"ti"])<0,psz("SHORT-ti-",tab3[1,"date"],"-",li),psz("LONG-ti-",tab3[1,"date"],"-",li)))
    tab3 <- cbind(tab3,absweight=psz(abs(as.numeric(tab3[,"ti"]))*100,"%"))
    ishort <- which(as.numeric(tab3[,"ti"])<0)
    ilong <- which(as.numeric(tab3[,"ti"])>=0)
    shortweight <- -1*sum(as.numeric(tab3[ishort,"ti"]))
    longweight <- sum(as.numeric(tab3[ilong,"ti"]))
    shortcash <- c(date=tab3[1,'date'],bui='USD Curncy',name='USD Curncy',te=' ',countryissue=' ',lodir=0,ti=1-shortweight,pf=psz("SHORT-ti-",tab3[1,"date"],"-",li),absweight=psz((1-shortweight)*100,"%"))
    longcash <- c(date=tab3[1,'date'],bui='USD Curncy',name='USD Curncy',te=' ',countryissue=' ',lodir=0,ti=1-longweight,pf=psz("LONG-ti-",tab3[1,"date"],"-",li),absweight=psz((1-longweight)*100,"%"))
    tab4 <- rbind(tab3,shortcash,longcash)
    ishort <- which(tab4[,"pf"]==psz("SHORT-ti-",tab4[1,"date"],"-",li))
    ilong <- which(tab4[,"pf"]==psz("LONG-ti-",tab4[1,"date"],"-",li))
    #stopifnot(all.equal(sum(as.numeric(tab4[ishort,"absweight"])),1)&all.equal(sum(as.numeric(tab4[ilong,"absweight"])),1))
    write.table(tab3,file=gsub(".csv","-nocash.csv",myfile),col.names=FALSE,sep=",",append=append)
    write.table(tab4,file=myfile,col.names=FALSE,sep=",",append=append)
    options(scipen=NULL)
}
dumpti <-
function(ipo=as.numeric(extract("me1v211","DISTINCT ipo","WHERE method='combo'")),mydir="c:/temp/temp/",gearing=1,li="combo") {
    for(i in seq_along(ipo)) {
        print(ipo[i])
        dumpti1(ipo=ipo[i],myfile=psz(mydir,li,"-ti.csv"),gearing=gearing,append=i!=1,li=li)
    }
}
bbreinvest <-
function(
                    reinvest=TRUE,
                    splitadjust=TRUE,
                    sleepsec=5
                    ) {
    if(reinvest) {
        if(splitadjust) {
            system(psz(Aaroot,"deployment/automation/dpdf-yyyyy.exe"))
        } else{
            stop("reinvest without split adjust not implemented")
        }
    } else {
        if(splitadjust) {
            system(psz(Aaroot,"deployment/automation/dpdf-nnnyy.exe"))
        } else{
            system(psz(Aaroot,"deployment/automation/dpdf-nnnny.exe"))
        }
    }
    Sys.sleep(sleepsec)
}
vvdir <-
function(){psz(userdir(),"vvarchive/")}
userdir <-
function() {psz(gsub(patt="/documents",rep="",x=getwd()),"/")}
tradedir <-
function(mydate=thiswed(...),...){aadir(mydate,subdir="trade\\")}
sourcedir <-
function() {psz(userdir(),"RSQL/")}
peerdir <-
function(mydate=thiswed()) {aadir(mydate,subdir="peer\\")}
livedir <-
function(){"\\\\sabre\\dfs\\models\\giles heywood\\"}
imgdir <-
function() {psz(aahome(),"\\img\\")}
imagedir <-
function() {psz(userdir(),"documents/RSQL/")}
dumpdir <-
function(mydate=thiswed()) {aadir(mydate,subdir="dump\\")}
aaupdate <-
function() {format(as.Date(Sys.time())-365*1.5,"%Y-%m-%d")}
aasu <-
function(
            ime=c(3,11:13,aame(),72,75,76,92,114,116,124),
            othersu=c(52:58,74:80,81:87,88:94)
            ) {
    isu <- NULL
    for(i in seq_along(ime)) {isu <- union(isu,as.numeric(extract("vemevi","distinct isu",psz("WHERE ixx IN (",fldlst(ime),")"),result=as.matrix)))}
    sort(union(isu,othersu))    
}
aasnapdir <-
function(mydate=thiswed()){aadir(mydate,subdir="nodivs\\")}
aarddir <-
function(){psz(userdir(),"rdata/")}
aame <-
function() {92}
aahome <-
function() { Sys.getenv("R_USER")}
aagetwd <-
function() {aahome()}
aafile <-
function(fnam="tmp",mydir=aahome()) {psz(mydir,"\\",fnam)}
aadir <-
function(mydate=thiswed(),subdir=""){psz(aagetwd(),"\\~wip\\aa",mydate,"\\",subdir)}
myco <-
function(use=c("other","sfm","CN","pacific","all","allexcn"))
{
    use <- match.arg(use)
    inuniv <- as.character(extract("aauniv","distinct countryissue"))
    if(use=="CN") return(intersect(inuniv,"CN"))
    if(use=="pacific") return(intersect(inuniv,c("AU","KR","SG","CA")))
    if(use=="sfm") return(sort(intersect(inuniv,as.character(extract("sabretk","distinct countryissue")))))
    if(use=="other") {
        outuniv <- union(c("CN","AU","KR","SG","CA"),as.character(extract("sabretk","distinct countryissue")))
        return(setdiff(inuniv,outuniv))
    }
    if(use=="allexcn")  return(setdiff(inuniv,"CN"))
    if(use=="all") return(inuniv)
}
aaunivf1 <-
function(minmaxw=0.02,onlylatest=FALSE)
{
    x <- rbind(
        aabuitop(univ="bworld",minmaxw=minmaxw/2,onlylatest=onlylatest),
        #aabuitop(univ="bworld",minmaxw=minmaxw/2,onlylatest=onlylatest,countries="pacific"),
        #aabuitop(univ="bworld",minmaxw=minmaxw/2,onlylatest=onlylatest,countries="sfm"),
        aabuitop(univ="sfm",minmaxw=minmaxw/2,onlylatest=onlylatest,countries="sfm")        
        )
    res <- x[order(x[,"mw"],decreasing=FALSE),]
    res[!duplicated(res[,"bui"]),]
}
aasize1 <-
function(minweight=2.1,universe='aabworld',nmincountry=-1,...)
{
    x <- data.frame(mattotab(aasize(tab=TRUE,co="countryissue",minweight=minweight,universe=universe,nmincountry=nmincountry,...)))
    droptab("tmp")
    sqlSave(channel=DBcon,
        dat=x,
        tablename="tmp",
        append=TRUE,
        rownames=FALSE,
        safer=FALSE)
    d1 <- as.character(extract("tmp","max(date)"))
    tab <- mysqlQuery(psz("
        SELECT aaindices.region, aaindices.country, aaindices.development, tmp.field, aaindices.bti
        FROM tmp INNER JOIN aaindices
        ON tmp.bui=aaindices.bti
        WHERE date='",d1,"' AND 0<field
        ORDER BY region, development, field DESC
    "))
    tab[,2] <- capwords(tolower(tab[,2]))
    tab1 <- 3*(tab[,1]=="Europe")+4*(tab[,1]=="MEA")+1*(tab[,1]=="North America")+2*(tab[,1]=="Latin America")+5*(tab[,1]=="Far East")
    tab[order(tab1),]
}
aasize <-
function(
                minweightbp=1,  #minimum weight in b.p.
                nmincountry=4,  #minimum in country at inception, negative means country is ignored
                co=c("countryfull","countryissue"),
                tabular=TRUE,
                universe=c("aauniv","aabworld","backupaauniv"),  
                comparator=c("<=",">="), #<= means this is a min weight, >= means a max
                idate="2003-08-31"
                )
{
    co <- match.arg(co)
    universe <- match.arg(universe)
    comparator <- match.arg(comparator)
    countryqual <- ifelse(nmincountry<1,"",psz("AND ",co," IN 
            (
            SELECT ",co," FROM
                (
                SELECT ",co,", count(*) AS n
                    FROM ",universe,"
                    WHERE date='",idate,"' AND ",minweightbp*.01,"<= weight
                    GROUP BY ",co,"
                ) AS a
            WHERE n>=",nmincountry,"
            ORDER BY n desc
            )
        "))
    x <- mysqlQuery(psz("
        SELECT date, ",ifelse(tabular,psz(co,", count(*)"),"bui, countryfull, countryissue, weight"),"
        FROM ",universe,"
        WHERE ",minweightbp*.01,comparator,"weight ",countryqual,"
        ",ifelse(tabular,psz("GROUP BY date, ",co),""),"
    "))
    if(tabular) {
        x<-tabtomat(x)
        mode(x) <- "numeric"
        x[is.na(x)] <- 0
        x <- cbind(x,TOTAL=apply(x,1,sum))
    }
    x    
}
aabuitop <-
function(
                countries=c("sfm","allexcn","other","CN","pacific","all"),
                univ=c("all","bworld","sfm"),
                minmaxw=minmaxweight(),
                onlylatest=FALSE
                )
{
    countries <- match.arg(countries)
    univ <- match.arg(univ)
    bti <- aabti(univ=univ)
    if(onlylatest) {
        datequal <- psz(" AND date='",extract("aauniv","DISTINCT DATE","ORDER BY date DESC LIMIT 1"),"' ")
    } else {
        datequal <- ""
    }
    incco <- myco(countries)
    res <- mysqlQuery(psz("
                SELECT * FROM
                    (SELECT bui, btk, max(weight) AS mw, min(weight)
                    FROM aauniv
                    WHERE countryissue IN (",fldlst(incco,"'","'"),")
                    AND bti IN (",fldlst(bti,"'","'"),")
                    ",datequal,"
                    GROUP BY bui
                    ORDER BY mw DESC, bui ASC) AS a
                WHERE mw>=",minmaxw," OR mw IS NULL
                ORDER BY mw DESC
                "))
    rownames(res) <- res[,"bui"]
    res[order(res[,"mw"],decreasing=TRUE),]
}
aabti <-
function(univ=c("bworld","sfm","all"))
{
    univ <- match.arg(univ)
    if(univ=="bworld") {
        bti <- extract("aauniv","DISTINCT bti","WHERE char_length(bti)=2")
    } else if(univ=="sfm") {
        bti <- "SFM"
    } else if(univ=="all") {
        bti <- extract("aauniv","DISTINCT bti")
    }
    sort(bti)
}
thiswed <-
function(myformat="%Y%m%d",myoffset=0) {
    #i <- which.min(abs(Sys.Date()-as.Date(getca())))
    i <- max(which(as.Date(getca()) <= Sys.Date()))
    format(as.Date(getca())[i+myoffset],myformat)
}
getstir <-
function(stir=unique(crncytostir()),locf=TRUE)
{
    res <- getpa(field="value",tab="aamacro",ifield="date",jfield="btk",qual="WHERE btk LIKE '%1m'")[,stir]/100
    if(locf) {
        i <- which(is.na(res[,"ee0001m"]) & index(res)<=as.Date("1998-01-01")) #euro inception
        res[i,"ee0001m"] <- res[i,"us0001m"]-.03 #could use DM but here use $-3%
        res <- na.locf(res)
    }
    res
}
getaamonthly <-
function(type=c("level","return","premium","accrued"))
{
    type <- match.arg(type)
    aamonthly <- mz(tabtomat(gettab("aamonthly")))
    mode(aamonthly) <- "numeric"
    cashaccrued <- accruexts(aamonthly[,"US0001M",drop=FALSE]/100)[-1,,drop=FALSE]
    ret <- exp(diff(log(aamonthly)))-1
    if(type=="level") {
        res <- aamonthly
    } else if(type=="return") {
        res <- ret
        res[,"US0001M"] <- cashaccrued
    } else if(type=="premium") {
        res <- sweep(ret,MAR=1,FUN="-",STAT=cashaccrued)
        res[,"US0001M"] <- 0
    } else if(type=="accrued") {
        res <- cashaccrued
    }
    res
}
dpdftest <-
function(income=c("adj","noadj","any"),splits=c("adj","noadj","any"))
{
    income <- match.arg(income)
    splits <- match.arg(splits)
    if(income=="any") {
        res1 <- TRUE
    } else {
        itest <- switch(income,adj=get(">"),noadj=get("<"))
        res1 <- do.call(itest,list(diff(bbts(bbco="ULVR LN equity",bbi="px_last",start="2009-05-19",end="2009-05-20"))[2],0))
    }
    if(splits=="any") {
        res2 <- TRUE
    } else {
        stest <- switch(splits,adj=get(">"),noadj=get("<"))
        res2 <- TRUE #do.call(stest,list(min(bbts(bbco="VIP LN equity",bbi="px_last",start="2010-01-01",end="2010-08-17"),na.rm=TRUE),1))
    }   #had to change this 2012-02-24
    as.logical(res1&res2)
}
aazeroprice <-
function(
                    AA,
                    jprice=c("px_last_local","px_open","px_high","px_low","px_last","eqy_weighted_avg_px")
                    )
{
    ina<-!is.na(coredata(AA[[3]]))
    izer<-which(ina)[which(coredata(AA[[3]])[ina]==0)]
    if(0<length(izer))  {
        coredata(AA[[3]])[izer]<-NA    
    }
    AA
}
aaylyhf <-
function(
            AA,
            dat=index(AA[[3]]),
            reportlag=13,
            selected=FALSE
            )
{
    AA1w <- xts(matrix(NA,nrow=length(dat),ncol=ncol(AA[[1]])),dat)
    colnames(AA1w) <- colnames(AA[[1]])
    mode(AA1w) <- "numeric"
    if(all(is.na(AA[[1]])) || is.na(AA[[2]][,"eqy_fiscal_yr_end"]) || AA[[2]][,"eqy_fiscal_yr_end"] %in% nullbb()) { return(AA1w) }
    reyr <- as.numeric(substr(AA[[2]][,"eqy_fiscal_yr_end"],4,7))
    remo <- substr(AA[[2]][,"eqy_fiscal_yr_end"],1,2)
    lastfy <- as.numeric(sort(format(index(AA[[1]]),"%Y"),dec=T)[1])
    AA[[1]]  <- AA[[1]][!apply(is.na(AA[[1]]),1,all),]  #remove any all-NA rows
    yearends <- data.frame(
            period=rev(seq(from=lastfy,by=-1,length.out=nrow(AA[[1]]))),
            whenyear=rev(seq(from=reyr,by=-1,length.out=nrow(AA[[1]]))),
            whenmonth=rep(remo,nrow(AA[[1]]))
            )
    ime <- match(extractDates(              #index of monthends in dates
                            dates=dat,
                            weekday=TRUE,
                            find="last",
                            period="month",
                            partials=TRUE,
                            firstlast=FALSE
                            )
                ,dat)
    monthends <- data.frame(                #monthend table
            year=format(dat[ime],format="%Y"),
            month=format(dat[ime],format="%m"),
            index=ime
            )
    iw <- monthends[match(  
                        paste(yearends[,2],yearends[,3]),   #yyyymm
                        paste(monthends[,1],monthends[,2])  #yyyymm
                        )
                    ,3] + reportlag
    if(length(iw)==1 && iw>length(dat)) iw <- length(dat) #only one yearend, passed recently, assume reported
    i <- which(iw<=length(dat))               #index into AA1
    iw <- iw[i]                             #revised index into AA1w
    AA1w[iw,] <- as.numeric(AA[[1]][i,])
    stopifnot(zapsmall(max(unclass(AA1w[iw,])-unclass(AA[[1]][i,]),na.rm=TRUE))==0)
    if(selected) AA1w <- AA1w[!apply(is.na(AA1w),1,all),]
    return(AA1w)
}
aatssplit <-
function(
                    bui,
                    adjfactor,
                    mytab=c("aats","aared")
                    )
{
    mytab <- match.arg(mytab)
    stopifnot(length(bui)==1 && nchar(bui)==18)
    if(abs(adjfactor-1)<1.e-6) return()
    if(mytab=="aats") {
        cols <- c("prdo","prdovw","prlo","prdol1","prdol2","prdonoincome")
    } else {
        cols <- c("prdo","prdov","prlo","prlov")
    }
    whereclause <- psz(" WHERE bui='",bui,"'")
    for(i in seq_along(cols)) {
        mysqlQuery(psz("UPDATE ",mytab," SET ",cols[i]," = ",cols[i],"*",adjfactor,whereclause))
    }
}
aatsmonthtkr <-
function(tkr=c("fund","index","rate"))
{
    btkfund <- extract("peer3","DISTINCT btk")
    btkindex <- c(
            "HEDGNAV",      #cs/tremont
            "HEDGNEUT",     #cs/tremont equity MN
            "HEDGLSEQ",     #cs/tremont Long/short equity
            "SPX",
            "SPTR"         #S&P total return
            )
    btkrate <- c(
            "US0001M"
            )
    btk <- NULL
    if("fund"%in%tkr) btk <- c(btk,btkfund)
    if("index"%in%tkr) btk <- c(btk,btkindex)
    if("rate"%in%tkr) btk <- c(btk,btkrate)
    if(is.null(btk)) stop()
    psz(btk," index")
}
aatsmonthrep <-
function()
{
    mysqlQuery("DROP TABLE aamonthly")
    mysqlQuery("CREATE TABLE aamonthly AS SELECT * FROM aamacro LIMIT 0")
    mysqlQuery("ALTER TABLE aamonthly ADD PRIMARY KEY (date, btk)")
    sqlSave(channel = DBcon, dat = aatsmonthdlv0(), tablename = "aamonthly", append = TRUE, rownames = FALSE, safer = TRUE)
    sqlSave(channel = DBcon, dat = aatsmonthdlv2(), tablename = "aamonthly", append = TRUE, rownames = FALSE, safer = TRUE)
    if(!is.null(aatsmonthextras())) sqlSave(channel = DBcon, dat = aatsmonthextras(), tablename = "aamonthly", append = TRUE, rownames = FALSE, safer = TRUE)
    #chkaatsmonth()
}
aatsmonthextras <-
function(fnam="z:/-fundmanager/bluecrest/allblue.csv")
{
    
    if(!file_test(op="-f",x=fnam)) return(NULL)
    x <- read.csv("z:/-fundmanager/bluecrest/allblue.csv")
    if(!(nrow(x)>0 && identical(colnames(x),c("date","btk","value")))) return(NULL)
    eomwday <- as.Date(extractDates(as.POSIXct(as.Date(seq(from=as.Date(x[1,"date"]),to=as.Date(x[nrow(x),"date"]),by=1))),per="m",find='l',weekday=T))
    stopifnot(length(eomwday)==nrow(x))
    x[,"date"] <- as.character(eomwday)
    x
}
aatsmonthdlv2 <-
function(bui=peerbui(),startday="1990-12-31")
{
    mbench <- getpa("px_last","aapeerdaily")
    mat1 <- mbench[extractDates(index(mbench),per="month",find="last",weekd=FALSE),]
    lut <- extract("peer3","btk,bui")
    colnames(mat1) <- firsttoken(lut[match(colnames(mat1),lut[,"bui"]),"btk"])
    mat <- mat1[,which(!is.na(colnames(mat1)))]
    i <- nrow(mat) #dispose of trailing flat data, set to NA
    j <- 1:ncol(mat)
    while(any(coredata(mat[i,j])==coredata(mat[i-1,j])) && 1<i) {
        j <- intersect(which(coredata(mat[i,])==coredata(mat[i-1,])),j)
        mat[i,j] <- NA
        i <- i-1
        }
    tab1 <- mattotab(as.matrix(mat[]),field="value",rclab=c("date","btk"))
    tab <- data.frame(tab1[!is.na(tab1[,"value"]),])
    tab
}
aatsmonthdlv1 <-
function(bui=peerbui(),mydir=peerdir(),startday="2000-01-01")
{
    bbreinvest(reinvest=TRUE,splitadjust=TRUE)
    bbcon(NonTradingDayValue=NULL) #carries over the previous trading day's value - important for monthly <index> tickers
    aabuiseq(
                    mydir=peerdir(),
                    verbose=TRUE,
                    FUN=aabui7,
                    totalreturn=TRUE,
                    splitcorrect=TRUE,
                    bui=bui,
                    para=FALSE,     #this is required because otherwise bbcon has the default settings
                    xstable="peer3",
                    orderby='bui',
                    startday=startday
                    )
    bbreinvest(reinvest=FALSE,splitadjust=TRUE)
    bbcon()
}
aatsmonthdlv0 <-
function(btk=aatsmonthtkr(tkr=c("index","rate")),startday="1990-12-31")
{
    bbcon(NonTradingDayValue=NULL) #carries over the previous trading day's value - important for monthly <index> tickers
    mbench <- bbts(bbcode=btk,bbitems="px_last",startday=startday,endday=format(Sys.time(),"%Y-%m-%d"))
    bbcon() #revert to defaults
    mat<-mbench[extractDates(index(mbench),per="month",find="last",weekd=FALSE),]
    colnames(mat) <- lapply(strsplit(colnames(mat)," "),"[",1)
    i <- nrow(mat) #dispose of trailing flat data, set to NA
    j <- 1:ncol(mat)
    while(any(coredata(mat[i,j])==coredata(mat[i-1,j])) && 1<i) {
        j <- intersect(which(coredata(mat[i,])==coredata(mat[i-1,])),j)
        mat[i,j] <- NA
        i <- i-1
        }
    tab1 <- mattotab(as.matrix(mat),field="value",rclab=c("date","btk"))
    tab <- data.frame(tab1[!is.na(tab1[,"value"]),])
    tab #added this 2010-08-25!
}
aatsmodes <-
function(x)
{
    stopifnot(identical(sort(union(colnames(x),"bui")),sort(aatsfields())))
    modes <- lapply(x,mode)
    if(any(modes=="logical")) {
        for(i in which(modes=="logical")) mode(x[[i]]) <- "numeric"
    }
    x
}
aatsfields <-
function()
{
    c(
        c("date","bui","prdo","prlo","reloto","rrloto","redoto","rrdoto","prdovw","rrdotovw","prdonoincome","turn"),
        psz(c("prdo","mcap","dipr","erpr","capr","bopr","best"),"l1",coll=""),
        psz(c("prdo","mcap","dipr","erpr","capr","bopr","best"),"l2",coll="")
       # psz(c("prdo","momentum","belowhigh","mcap","capui","capuo","dipr","erpr","capr","bopr","cds","rsi14","best"),"l1",coll="")
    )
}
aatsdaily <-
function(
            AAloc=AA,
            cash=getstir()#getpa(field="value",tab="aamacro",ifield="date",jfield="btk",qual="WHERE btk='us0001m'")/100
            )
{
    eps <- 1.e-6 #for preventing singularity
    stopifnot(is.list(AAloc) && c("AA1","AA2","AA3")%in%names(AAloc))
    stir <- crncytostir(buicrncy(bui=rownames(AA[[2]])))
    carry <- accruexts(cash)
    da <- as.Date(intersect(index(AAloc[[3]]),index(carry)))
    #da <- index(carry)
    resnams <- c("date","bui","prdo","prdov","cash","turn","ret","revt","prlo","prlov","cashlo","retlo","revtlo")
    res <- data.frame(matrix(NA,nrow=length(da),ncol=length(resnams)))
    colnames(res) <- resnams
    AA3 <- AAloc[[3]]
    AA3[is.na(AA3[,"px_volume"]),"px_volume"] <- 0
    ivwaptest <- which(abs(AA3[,"eqy_weighted_avg_px"]/AA3[,"px_last"] -1)>0.1) # added 2010-06-15
    if(length(ivwaptest)>0 && !any(is.na(ivwaptest))) AA3[ivwaptest,"eqy_weighted_avg_px"] <- AA3[ivwaptest,"px_last"]
    AA3 <- focb.local(AA3,maxper=4)
    #AA1  <- aaylyhf(AAloc,dat=da)
    res[,"date"] <- da
    res[,"bui"] <- rownames(AAloc[[2]])
    res[,"prdo"] <- AA3[,"px_last"][da,]                             
    res[,"prdov"] <- AA3[,"eqy_weighted_avg_px"][da,]
    res[,"cash"] <- carry[da,"us0001m"]
    res[,"turn"] <-  rollxts(AA3[da,"px_last"]*AA3[da,"px_volume"],what="median",n=5)
    res[,"ret"] <- retxts(AA3[,"px_last"])[da,]
    res[,"revt"] <- retxts(AA3[,"eqy_weighted_avg_px"])[da,]
    res[,"prlo"] <- AA3[,"px_last_local"][da,]            
    myfx <- AA3[,"px_last_local"][da,]/AA3[,"px_last"][da,]
    myfx[1e6<myfx] <- NA
    res[,"prlov"] <- AA3[,"eqy_weighted_avg_px"][da,]*myfx
    res[,"cashlo"] <- carry[da,stir]
    res[,"retlo"] <- retxts(AA3[,"px_last_local"])[da,]
    res[,"revtlo"] <- retxts(AA3[,"eqy_weighted_avg_px"][da,]*myfx)[da,]
    res <- cbind(res,datew=NA,dateminus1=NA,xo=NA,xt=NA,xc=NA,pe=NA,pev=NA,year=NA,month=NA)    
    data.frame(lapply(res,coredata))
}
aatoday <-
function(iaddweek=0)
{
    day <- as.character(as.Date(Sys.time())+iaddweek*7)
    getca()[max(which(getca()<=day))]
}
aaredfields <-
function()
{
    c(
    "date",       "dateminus1", "year",       "month",      "datew",      "bui",        "prdo" ,     
    "prdov",      "cash",       "ret",        "revt",       "prlo",       "prlov",      "cashlo",    
    "retlo",      "revtlo",     "turn",       "xo",         "xt",         "xc",         "pe",        
    "pev"       )
}
aareddates <-
function()
{
    mysqlQuery("UPDATE aared SET datew=date WHERE DAYOFWEEK(date)=4")
    mysqlQuery("UPDATE aared SET datew=date + INTERVAL 1 DAY WHERE DAYOFWEEK(date)=3")
    mysqlQuery("UPDATE aared SET datew=date + INTERVAL 2 DAY WHERE DAYOFWEEK(date)=2")
    mysqlQuery("UPDATE aared SET datew=date + INTERVAL 5 DAY WHERE DAYOFWEEK(date)=6")
    mysqlQuery("UPDATE aared SET datew=date + INTERVAL 6 DAY WHERE DAYOFWEEK(date)=5")
    mysqlQuery("UPDATE aared SET dateminus1=date - INTERVAL 1 DAY WHERE DAYOFWEEK(date)!=2")
    mysqlQuery("UPDATE aared SET dateminus1=date - INTERVAL 3 DAY WHERE DAYOFWEEK(date)=2")
    mysqlQuery("UPDATE aared SET year=YEAR(date)")
    mysqlQuery("UPDATE aared SET month=MONTH(date)")
}
aaread1 <-
function(
            i=sample(1:length(done),1), #bui is also permitted
            mydir=peerdir(),
            ret=FALSE
            )
{
    done <- aabuidone(mydir=mydir)
    if(is(i,"numeric")) {stopifnot(i<=length(done))} else {i <- match(i,done)}
    dd <- dir(mydir)
    load(psz(mydir,done[i],".rdata"),.GlobalEnv)
    attributes(AA) <<- c(attributes(AA),bui=done[i])
    if(ret) return(AA)
}
aaread <-
function(
            i=sample(1:length(done),1), #bui is also permitted
            mydir=aadir(),
            snapdir=aasnapdir(),
            ret=FALSE
            )
{
    done <- aabuidone(mydir=mydir)
    if(is(i,"numeric")) {stopifnot(i<=length(done))} else {i <- match(i,done)}
    dd<-dir(mydir)
    load(psz(snapdir,done[i],".rdata"),.GlobalEnv) #assumption: use latest dated directory
    BB <- AA[,c("best_target_price","px_last")]
    colnames(BB) <- c("best_target_price","px_last_noincome")
    load(psz(mydir,done[i],".rdata"),.GlobalEnv)
    attributes(AA)<<-c(attributes(AA),bui=done[i])
#    AA[[2]] <<- aadots(AA[[2]],patt=" ") #temp fix
#    AA[[3]] <<- cbind(AA[[2]],BB)
#    colnames(AA[[3]])[ncol(AA[[3]])] <- "px_last_noincome"
    if(!is.null(AA$AA2) && !all(is.na(AA$AA2))) AA$AA2 <<- aadots(AA$AA2,patt=" ") #temp fix
    AA$AA3 <<- cbind(AA$AA3,BB)
    AA <<- c(NA,AA) #because AA1 is now skipped - would be better to have this done in aabui0
    names(AA)[1] <<- "AA1"
    if(ret) return(AA)
}
aaloadfundly <-
function(todo,cash)
{
    aaread(todo)
    x <- aatsdaily(cash=cash[index(AA[[3]]),,drop=FALSE])
    x <- x[x[,"date"]>as.Date("2003-08-01"),]
    sqlSave(channel=DBcon,
        dat=x,    
        tablename="aared",
        append=TRUE,
        rownames=FALSE,
        safer=TRUE)
}
aaloadfun1 <-
function(
                    todo=aabuidone(mydir=peerdir())[1],
                    aatstab="aapeerdaily"
                    )
{
    stopifnot(length(todo)==1)
    aaread1(todo)
    x <- data.frame(date=as.character(index(AA)),bui=todo,px_last=as.numeric(coredata(AA)))
    donets <- as.character(extract(aatstab,"DISTINCT bui"))
    if(!todo%in%donets) {
        sqlSave(channel=DBcon,
            dat=x,
            tablename=aatstab,
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
    }
}
aaloadfun <-
function(
                    todo,
                    dayofweek,
                    cash,
                    aatstab="aats",
                    aaxstab="aaxs",
                    dots=TRUE,
                    doxs=FALSE      #aaxs is now done once onle in deraaxs
                    )
{
    aaread(todo)
    donets <- as.character(extract(aatstab,"DISTINCT bui"))
    #donexs <- as.character(extract(aaxstab,"DISTINCT bui","ORDER BY bui")) 
    #if(doxs && !todo%in%donexs) {
    #    x <- getaaxsdf(AA)
    #    colnames(x) <- lapply(strsplit(colnames(x),".",fix=TRUE),paste,collapse="_")
    #    sqlSave(channel=DBcon,
    #        dat=x,
    #        tablename=aaxstab,
    #        append=TRUE,
    #        rownames=FALSE,
    #        safer=TRUE)
    #}
    if(dots && !todo%in%donets) {
        sqlSave(channel=DBcon,
            dat=deraats(AA,cash,dayofweek=dayofweek),    
            tablename=aatstab,
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
    }
}
aaloaddly <-
function(
                    mydir=aadir(),
                    verbose=TRUE,
                    para=length(todo)>10,
                    inuniv=latestsu(onlylatest=FALSE)   #this should be replaced with latestsu(onlylatest=FALSE)
                    )
{
    avail <- aabuidone(mydir=mydir)
    done <- as.character(extract("aared","DISTINCT bui"))#,"WHERE prdo IS NOT NULL ORDER BY bui")) # removed 11-05-21
    todo <- intersect(setdiff(inuniv,done),avail)
    cash <- getstir()
    #mysqlQuery("ALTER TABLE aared DROP PRIMARY KEY")  #speed it up
    #mysqlQuery("ALTER TABLE aared DROP INDEX date")    
    #mysqlQuery("ALTER TABLE aared DROP INDEX datew")    
    #mysqlQuery("ALTER TABLE aared DROP INDEX bui")    
    if(!para) {
        result <- lapply(
                    X=todo, 
                    FUN=aaloadfundly,
                    cash=cash
                    )
    } else {
        sfInit( parallel=TRUE, cpus=max(ncpus(),1) )
        result <- sfLapplyWrap(
                    X=todo, 
                    FUN=aaloadfundly,
                    cash=cash
                    )
        sfStop()
    }
    #mysqlQuery("ALTER TABLE aared ADD PRIMARY KEY (date, bui)")    
    #addidx("aared","date")
    #addidx("aared","datew")
    #addidx("aared","bui")
    aareddates()
}
aaload1 <-
function(
                mydir=peerdir(),
                verbose=TRUE,
                aatstab="aapeerdaily",
                inuniv=aabuidone(mydir=peerdir()),
                para=FALSE
                )
{
    avail <- aabuidone(mydir=mydir)
    done <- as.character(extract(aatstab,"DISTINCT bui","ORDER BY bui"))
    todo <- intersect(setdiff(inuniv,done),avail)
    if(length(todo)<1) return()
    if(!para) {
    result <- lapply(
                X=todo, 
                FUN=aaloadfun1
                )
    } else {
    sfInit( parallel=TRUE, cpus=max(ncpus(),1) )
    result <- sfLapplyWrap(
                X=todo, 
                FUN=aaloadfun1
                )
    }
    sfStop()
}
aaload <-
function(
                mydir=aadir(),
                verbose=TRUE,
                dayofweek=3,
                aatstab="aats",
                aaxstab="aaxs",
                dots=TRUE,
                doxs=FALSE, #aaxs is NOT loaded here - see deraaxs
                para=10<length(todo),
                #inuniv=aabuitop()[,"bui"]
                inuniv=latestsu(onlylatest=FALSE)
                )
{
    avail <- aabuidone(mydir=mydir)
    donets <- as.character(extract(aatstab,"DISTINCT bui","ORDER BY bui"))
    donexs <- as.character(extract(aaxstab,"DISTINCT bui","ORDER BY bui"))
    done <- intersect(donets,donexs)
    todo <- intersect(setdiff(inuniv,done),avail)
    if(length(todo)<1) return()
    cash <- getstir() #getpa(field="value",tab="aamacro",ifield="date",jfield="btk",qual="WHERE btk='us0001m'")/100
    #mysqlQuery("ALTER TABLE aats DROP PRIMARY KEY") # not a good idea - see aabuidone() in aaread()
    if(!para) {
    result <- lapply(
                X=todo, 
                FUN=aaloadfun,
                dayofweek=dayofweek,
                cash=cash,
                aats=aatstab,
                aaxs=aaxstab,
                dots=dots,
                doxs=doxs
                )
    } else {
    sfInit( parallel=TRUE, cpus=max(ncpus(),1) )
    result <- sfLapplyWrap(
                X=todo, 
                FUN=aaloadfun,
                dayofweek=dayofweek,
                cash=cash,
                aats=aatstab,
                aaxs=aaxstab,
                dots=dots,
                doxs=doxs
                )
    }
    sfStop()
    #mysqlQuery("ALTER TABLE aats ADD PRIMARY KEY (date, bui)")
}
aajoinfun <-
function(
                X=intersect(aabuidone(where="rda",what="aats"),extract("aats","distinct bui"))[1],
                verbose=TRUE,
                specified=NULL,
                ...
                )
{
    stopifnot(is.null(specified) || (length(specified)==1 && specified%in%rawt()))
    stopifnot(length(X)==1)
    aatstemp <- psz("tstemp",X)
    aadeclare(aats=aatstemp)
    allflds <- dirfld("aats")
    flds <- setdiff(allflds,c("date","bui"))
    fail <- NULL
    print(X)
    mytest <- TRUE
    xa <- aagetrdb(X,"rda",getaaxs=FALSE)$ts   #new data
    xb <- aagetrdb(X,"rdb",aats="aats",getaaxs=FALSE)$ts
    xa <- xa[,sort(colnames(xa))]
    xb <- xb[,sort(colnames(xb))]
    mytest1 <- all(colnames(xa)==colnames(xb))  #elementary
    stopifnot(min(xb[,"date"])<=min(xa[,"date"])) #assumed on next line
    ixb <- match(xa[,"date"],xb[,"date"])   #some will be NA (at the end)
    ixa <- 1:nrow(xa)                       #same length as ixb
    bdates <- xb[!is.na(xb[,"prdo"]),"date"]
    adates <- xa[!is.na(xa[,"prdo"]),"date"]
    alladates <- xa[!apply(is.na(xa),1,all),"date"] #includes those with NA prices but valid other data, esp last period
    allbdates <- xb[!apply(is.na(xb),1,all),"date"] 
    if((0<length(adates) && !all(alladates%in%allbdates))) {
        finaloverlapdate <- NULL
        if(is.null(specified)) {
            finaloverlapdate <- as.Date(max(intersect(bdates,adates)))
        } else if ((specified %in% bdates) && (specified %in% adates)){
            finaloverlapdate <- specified
        }
        if(!is.null(finaloverlapdate)) {
            ixbmax <- match(finaloverlapdate,xb[,"date"])
            ixamax <- match(finaloverlapdate,xa[,"date"])
            ixanew <- which(finaloverlapdate<=xa[,"date"])
            adjfactor <- round(xa[ixamax,"prdo"]/xb[ixbmax,"prdo"],digits=5) #use dollar because local is rounded by BB to pennies after split adjust (!)
            stopifnot(!is.na(adjfactor))
            print(psz(finaloverlapdate," ",adjfactor))
            qc <- data.frame(
                    today=as.Date(Sys.time()),
                    name="aats",
                    bui=X,
                    adjfactor=adjfactor,
                    overlap=sum(!is.na(ixb)),
                    newfromdate=finaloverlapdate,
                    nvalidold=sum(!is.na(xb[ixbmax,])),
                    nvalidnew=sum(!is.na(xa[ixamax,]))
                    )
            sqlSave(channel = DBcon, dat = qc, tablename = "aajoin", append = TRUE, rownames = FALSE, safer = TRUE)
            whereclause <- psz("WHERE bui='",X,"' AND date >= '",finaloverlapdate,"'")
            mysqlQuery(psz("ALTER TABLE aatsops ALTER  today SET DEFAULT '",as.character(Sys.Date()),"'"))
            mysqlQuery(psz("INSERT INTO aatsops (",fldlst(dirfld("aats")),")SELECT ",fldlst(dirfld("aats"))," FROM aats ",whereclause))
            aatssplit(bui=X,adjfactor=adjfactor,mytab="aats")            #apply adjustment factor to history
            sqlSave(channel = DBcon, #all the new data
                    dat = xa[ixanew,], 
                    tablename = aatstemp, 
                    append = TRUE, 
                    rownames = FALSE, 
                    safer = TRUE)
            for(j in seq_along(flds)) { #populate null values in the update where existing is not null
                mysqlQuery(psz("
                    UPDATE aats, ",aatstemp,"
                    SET ",psz(aatstemp,".",flds[j]),"=",psz("aats.",flds[j])," 
                    WHERE ",aatstemp,".",flds[j]," IS NULL AND aats.bui=",aatstemp,".bui AND aats.date=",aatstemp,".date
                    "))
            }
            mysqlQuery(psz("DELETE FROM aats ",whereclause)) #delete the rows
            mysqlQuery(psz("INSERT INTO aats (",fldlst(allflds),")SELECT ",fldlst(allflds)," FROM ",aatstemp))
        }
    }
    droptab(aatstemp)
}
aajoinf1 <-
function(
                bui=intersect(aabuidone(where="rda",what="aats"),extract("aats","distinct bui")),
                specified=NULL,
                para=TRUE,
                ...
                )
{
    stopifnot(is.null(specified) || (length(specified)==1 && specified%in%rawt()))
    if(para) {
        sfInit( parallel=TRUE, cpus=max(1.5*ncpus(),1) )
        result <- sfLapplyWrap(
                    X=bui, 
                    FUN=aajoinfun,
                    verbose=FALSE,
                    specified=specified,
                    ...
                    )
        sfStop()
    } else {
        result <- lapply(
                    X=bui, 
                    FUN=aajoinfun,
                    verbose=TRUE,
                    specified=specified,
                    ...
                    )
    }
}
aajoindailyfun <-
function(
                X=intersect(aabuidonedaily(where="rda"),extract("aared","distinct bui")),
                verbose=TRUE
                )
{
    stopifnot(length(X)==1)
    fail <- NULL
    jchk <- c("bui","date","prdo","prdov","prlo","prlov","turn")
    xa <- aagetrdbdaily(X,"rda")
    xb <- aagetrdbdaily(X,"rdb")
    xa <- xa[,sort(colnames(xa))]
    xb <- xb[,sort(colnames(xb))]
    ixb <- match(xa[,"date"],xb[,"date"])   #some will be NA (at the end, possibly at beginning also)
    ixa <- 1:nrow(xa)                       #same length as ixb
    bdates <- xb[!is.na(xb[,"prdo"]),"date"]
    adates <- xa[!is.na(xa[,"prdo"]),"date"]
    if(0<length(adates) && !all(adates%in%bdates)) {
        abdates <- intersect(bdates,adates)
        finaloverlapdate <- as.Date(max(setdiff(abdates,max(abdates)))) #actually penultimate to avoid intraday issues
        ixbmax <- match(finaloverlapdate,xb[,"date"])
        ixamax <- match(finaloverlapdate,xa[,"date"])
        ixanew <- which(finaloverlapdate<=xa[,"date"])
        adjfactor <- round(xa[ixamax,"prdo"]/xb[ixbmax,"prdo"],digits=5)
        stopifnot(!is.na(adjfactor))
        print(psz(finaloverlapdate," ",adjfactor))
        qc <- data.frame(
                today=as.Date(Sys.time()),
                name="aared",
                bui=X,
                adjfactor=adjfactor,
                overlap=sum(!is.na(ixb)),
                newfromdate=finaloverlapdate,
                nvalidold=sum(!is.na(xb[ixbmax,jchk])),
                nvalidnew=sum(!is.na(xa[ixamax,jchk]))
                )
        sqlSave(channel = DBcon, dat = qc, tablename = "aajoin", append = TRUE, rownames = FALSE, safer = TRUE)
        aatssplit(bui=X,adjfactor=adjfactor,mytab="aared")            #apply adjustment factor to history
        mysqlQuery(psz("DELETE FROM aared WHERE bui='",X,"' AND date >= '",finaloverlapdate,"'"))
        sqlSave(channel = DBcon, 
                dat = xa[ixanew,], 
                tablename = "aared", 
                append = TRUE, 
                rownames = FALSE, 
                safer = TRUE)            
    }
}
aajoindaily <-
function(
                bui=intersect(aabuidonedaily(where="rda"),extract("aared","distinct bui")),
                para=TRUE
                )
{
    if(para) {
        sfInit( parallel=TRUE, cpus=max(1.5*ncpus(),1) )
        result <- sfLapplyWrap(
                    X=bui, 
                    FUN=aajoindailyfun,
                    verbose=FALSE
                    )
        sfStop()
    } else {
        result <- lapply(
                    X=bui, 
                    FUN=aajoindailyfun,
                    verbose=TRUE
                    )
    }
    aareddates()
}
aagetrdbdaily <-
function(
            i=sample(1:length(done),1), #bui is also permitted
            where=c("rdb","rdata"),  #source
            aared="aared"
            )
{
    where <- match.arg(where)
    done <- aabuidone(where=where)
    if(is(i,"numeric")) {stopifnot(i<=length(done))} else {i <- match(i,done)}
    if(where=="rdb") {
        myts <- extract(aared,aaredfields(),psz("WHERE bui = '",done[i],"' ORDER BY date asc"),res=data.frame)
        myts[,"date"] <- as.Date(myts[,"date"])
    } else if(where=="rdata") {
        aaread(done[i])
        myts <- aatsdaily()
    }
    myts
}
aagetrdb <-
function(
            i=sample(1:length(done),1), #bui is also permitted
            where=c("rdb","rdata"),  #source
            aats="aats",
            getaaxs=TRUE
            )
{
    where <- match.arg(where)
    done <- aabuidone(where=where)
    if(is(i,"numeric")) {stopifnot(i<=length(done))} else {i <- match(i,done)}
    if(where=="rdb") {
        myts <- extract(aats,aatsfields(),psz("WHERE bui = '",done[i],"' ORDER BY date asc"),res=data.frame)
        myts[,"date"] <- as.Date(myts[,"date"])
    } else if(where=="rdata") {
        aaread(done[i])
        #if(getaaxs) myxs <- getaaxsdf() #this is no longer available - xsect data always comes from aaxsect, is not stored in rdata
        myts <- aatsmodes(deraats())
    }
    if(getaaxs) {
        myxs <- extract("aaxs","*",psz("WHERE bui = '",done[i],"'"),res=data.frame)    
        attr(myxs,"row.names") <- "1"
        myxs <- myxs[,sort(colnames(myxs))]
    } else {myxs <- NULL}
    list(xs=myxs,ts=myts) 
}
aadump <-
function(tabs=setdiff(dirtab(),dirtab("vi")),mydir=dumpdir())
{
    for(i in seq_along(tabs)) {
        print(i)
        cmd <- psz("mysqldump -uroot -p7159 bloomberg ",tabs[i]," > ",mydir,tabs[i])
        shell(cmd)
    }
}
aadots <-
function(x,patt=".")
{
    colnames(x) <- lapply(strsplit(colnames(x),patt,fix=TRUE),paste,collapse="_")
    x
}
aadeclare <-
function(
                aatstab="aatssafe", #defaults set for safety
                ...
                )
{
    mysqlQuery(psz("
    DROP TABLE IF EXISTS ",aatstab,";
    "))
    mysqlQuery(psz("
    CREATE TABLE ",aatstab," (
      `bui`     char(18) NOT NULL,
      `date`    date NOT NULL,
      `prdo`    double,
      `prdovw`    double,
      `prlo`    double,
      `reloto`  double,
      `rrloto`  double,
      `redoto`  double,
      `rrdoto`  double,
      `rrdotovw`  double,
      `prdol1`    double,
      `bestl1`    double,
      `mcapl1`    double,
      `diprl1`    double,
      `erprl1`    double,
      `caprl1`    double,
      `boprl1`    double,
      `prdol2`    double,
      `bestl2`    double,
      `mcapl2`    double,
      `diprl2`    double,
      `erprl2`    double,
      `caprl2`    double,
      `boprl2`    double,
      `prdonoincome`    double,
      `turn`    double,
      /* Keys */
      PRIMARY KEY (`date`, `bui`)
    ) ENGINE = MyISAM
    "))

}
aabuiseq <-
function(
                mydir=aadir(),
                verbose=TRUE,
                FUN=aabui0,
                totalreturn=TRUE,
                splitcorrect=TRUE,
                bui=latestsu(),
                para=4*ncpus()<length(bui),
                xstable=c("aaxs","peer3"), #static data
                orderby="weightmax DESC",
                ...
                )
{
    xstable <- match.arg(xstable)
    stopifnot(dpdftest(income=ifelse(totalreturn,"adj","noadj"),splits=ifelse(splitcorrect,"adj","noadj")))
    stopifnot(all(bui%in%extract(xstable,"Distinct bui")))
    xs <- gettab(xstable)
    X <- as.character(
            as.matrix(mysqlQuery(psz("
                SELECT bui 
                FROM ",xstable," 
                WHERE bui IN (",fldlst(setdiff(bui,aabuidone(mydir=mydir)),"'","'"),") 
                ORDER BY ",orderby
                ))))
    if(para) {
        sfInit( parallel=TRUE, cpus=max(2*ncpus(),1) )
        result <- sfLapplyWrap(
                    X=X, 
                    FUN=aabuiseqfun,
                    xs=xs,
                    mydir=mydir,
                    aafun=FUN,
                    verbose=verbose,
                    ...
                    )
        sfStop()
    } else {
        result <- lapply(
                    X=X, 
                    FUN=aabuiseqfun,
                    xs=xs,
                    mydir=mydir,
                    aafun=FUN,
                    verbose=verbose,
                    ...
                    )
    }
}
aabuidonedaily <-
function(
                    where=c("rdata","rdb"),
                    mydir=aasnapdir()
                    )
{
    where <- match.arg(where)
    if(where=="rdata") {
        bui <- sort(unique(gsub(".rdata","",dir(path=mydir))))
    } else {
        bui <- as.character(as.matrix(mysqlQuery("SELECT DISTINCT bui FROM aared")))
    }
    bui[which(nchar(bui)==18 & substr(bui,1,2)=="EQ")]
}
aabuidone <-
function(
                    where=c("rdata","rdb"),
                    mydir=ifelse(what=="aats",aadir(),aasnapdir()),
                    what=c("aats","aared")
                    )
{
    where <- match.arg(where)
    what <- match.arg(what)
    if(where=="rdb") {
        bui1 <- mysqlQuery(psz("
                        SELECT DISTINCT bui, weightmax 
                        FROM aaxs 
                        WHERE bui IN (",flds(extract(what,"DISTINCT bui")),") 
                        ORDER BY weightmax DESC, bui ASC
                        "))
        rdbbui <- as.character(as.matrix(bui1[,"bui"]))
    } else {
        bui <- sort(unique(gsub(".rdata","",dir(path=mydir))))
    }
    switch(where,
        rdata=bui[which(nchar(bui)==18 & substr(bui,1,2)=="EQ")],
        rdb=rdbbui
        )
}
aabui7 <-
function(
                bui="EQ0010054600001000",
                startday=aaupdate(),
                endday=thiswed(myformat="%Y-%m-%d")
                )
{
    items <- c(
                "px_last"
                )
    loc <- bbts(
                 bbcodes=paste(bui," Equity"),
                 bbitems="px last",
                 startday=startday,
                 endday=endday
                 )
    colnames(loc) <- "px_last_local"
    AA <- loc
    AA[format(as.Date(Sys.time()),"%Y-%m-%d") ,] <- NA #this to prevent undesired intraday prices which will not subsequently be matched
    aazeroprice(AA)
}
aabui6 <-
function(
                bui="EQ0010054600001000",
                startday=aaupdate(),#"1990-12-31",  #need a year for turnover calcs
                endday=thiswed(myformat="%Y-%m-%d")#'2011-03-24'#'format(as.Date(Sys.time()),"%Y-%m-%d") #goes to the current day (wed).
                )
{
    items <- c(
                "px_last",
                "best_target_price",
                "px_to_book_ratio"#,
                #"best_px_bps_ratio",
                #"best_pe_ratio",
                #"best_div_yld"
                )
    dlr <- bbts(
                 bbcodes=paste(bui," Equity"),
                 bbitems=items,
                 startday=startday,
                 endday=endday,
                 currency="USD"
                 )
    dlr[format(as.Date(Sys.time()),"%Y-%m-%d") ,] <- NA #this to prevent undesired intraday prices which will not subsequently be matched
    dlr
}
aabui3 <-
function(
                bui="EQ0010054600001000",
                startday=aaupdate(),#"1990-12-31",  #need a year for turnover calcs
                endday=thiswed(myformat="%Y-%m-%d")#format(as.Date(Sys.time()),"%Y-%m-%d") 
                )
{
    items <- c(
                "px_open",
                "px_high",
                "px_low",
                "px_last",
                "eqy_weighted_avg_px",
                "px_volume",
                "cur_mkt_cap",
                "hist_call_imp_vol",
                "hist_put_imp_vol",
                "eqy_dvd_yld_ind",
                "pe_ratio",
                "px_to_cash_flow",
                "px_to_book_ratio",
                "open_int_total_put",
                "open_int_total_call",
                "rsi_14d",
                "cash_st_investments_to_tot_asset",
                "total_debt_to_ev"
                )
    dlr <- bbts(
                 bbcodes=paste(bui," Equity"),
                 bbitems=items,
                 startday=startday,
                 endday=endday,
                 currency="USD"
                 )
    loc <- bbts(
                 bbcodes=paste(bui," Equity"),
                 bbitems="px last",
                 startday=startday,
                 endday=endday
                 )
    colnames(loc) <- "px_last_local"
    cdstkr <- as.character(bbxsect(bbcodes=paste(bui," Equity"),bbitems="cds spread ticker 5y"))
    if(is.na(cdstkr)) {
        cds <- loc*NA
    } else {
        cds <- bbts(
                     bbcodes= paste(cdstkr," index"),
                     bbitems="px last",
                     startday=startday,
                     endday=endday
                     )
    }
    colnames(cds) <- "cds_spread"
    AA <- cbind(loc,dlr,cds)
    AA[format(as.Date(Sys.time()),"%Y-%m-%d") ,] <- NA #this to prevent undesired intraday prices which will not subsequently be matched
    aazeroprice(AA)
}
aabui2 <-
function(bui="EQ0010054600001000")
{
    items <-  c(
                "name",
                "gics_industry_group_name",
                "gics_industry_name",
                "gics_sub_industry_name",
                "icb_industry_name",
                "icb_supersector_name",
                "icb_sector_name",
                "icb_subsector_name",
                "eqy_prim_exch",
                "industry_sector",
                "industry_group",
                "industry_subgroup",
                "crncy",
                "eqy_fiscal_yr_end"
                )
    x <- bbxsect(
                bbcodes=paste(bui," Equity"),
                bbitems=items
                )
    rownames(x) <- bui
    x
}
aabui1 <-
function(bui="EQ0010054600001000",years=2002:2009)
{
    items <- c(
                "bs_tot_asset",
                "tot_shrhldr_eqy",
                "bs_sh_out"
                )
    bbxsectts(
                bbcodes=paste(bui," Equity"),
                bbitems=items,
                years=years,
                currency="USD"
                )
}
aabui0 <-
function(bui="EQ0010054600001000",
                startday=aaupdate(),#"1990-12-31",
                years=2002:2009
            )
{
    list(
        #AA1=aabui1(bui,years=years),
        AA2=matrix(NA,1,1,dimnames=list(bui,NULL)),#aabui2(bui),
        AA3=aabui3(bui,startday=startday), 
        AA5=NULL
        )        
}
buievent <-
function(
                keepbui=TRUE    #prevents deletion of initial date, avoids failure of checks eg chkce
                ) {
    buidate <- mysqlQuery("SELECT date, bui FROM aabuievent WHERE bui IN (SELECT DISTINCT bui FROM su)")
    if(0<nrow(buidate)) {
        for(i in 1:nrow(buidate)) {
            if(keepbui) {
                sustart <- as.character(extract("su","MIN(date)",psz("WHERE bui=",flds(buidate[i,2]))))
                buidate[buidate[,"date"]<sustart,"date"] <- sustart
            }
            mysqlQuery(psz("
                DELETE FROM su
                WHERE su.bui=",flds(buidate[i,2])," AND ",flds(buidate[i,1]),"<su.date
            "))
        }
    }
}
sumne1 <-
function(
            type=c("peraw","mkt","pecor"),
            strategy=as.character(extract("ne","DISTINCT strategy")),
            annfactor=getsione("annfactor"),
            percent=ifelse(getsione("percent"),100,1),
            transcost=.01
            ) 
{ 
    type <- match.arg(type)
    strategy <- match.arg(strategy)
    qual <- psz("WHERE strategy = '",strategy,"'")
    pa <- coredata(merge(
                        getpa(type,"ne",jfield="strategy",qual=qual),
                        getpa("pogross","ne",jfield="strategy",qual=qual),
                        getpa("potrade","ne",jfield="strategy",qual=qual),
                        getpa("mkt","ne",jfield="strategy",qual=qual)
                        ))
    colnames(pa) <- c("strategy","pogross","potrade","mkt")
    myret <- mean(pa[,"strategy"],na.rm=TRUE)*annfactor*percent
    mycosts <- mean(pa[,"potrade"],na.rm=TRUE)*(type!='mkt')*transcost*annfactor*percent #nb zero costs assumed for mkt
    myvol <- sqrt(var(pa[,"strategy"],na.rm=TRUE)*annfactor)*percent
    mycapm <- summary(lm(pa[,"strategy"] ~ pa[,"mkt"]))$coefficients #nb no lags this time
    res <- data.frame(
            strategy=strategy,
            type=type,
            turnover=mean(pa[,"potrade"],na.rm=TRUE)*annfactor*percent,
            gross=mean(pa[,"pogross"],na.rm=TRUE),
            return=myret,
            costs=-1*mycosts,
            returnaftercosts=myret-mycosts,
            volatility=myvol,
            sharpe=(myret-mycosts)/myvol,
            beta=mycapm[2,1],
            beta.se=mycapm[2,2]
            )
    if(type=="pecor") {
        res[,"turnover"] <- NA
        res[,"gross"] <- NA
    } else if (type=="mkt") {
        res[,"turnover"] <- NA
        res[,"gross"] <- 1
    }
    res
}
sumne <-
function(
            type=sort(rep(c("peraw","mkt","pecor"),length(extract("ne","DISTINCT strategy")))),
            strategy=rep(as.character(extract("ne","DISTINCT strategy")),length(type)/length(as.character(extract("ne","DISTINCT strategy")))),
            annfactor=getsione("annfactor"),
            percent=ifelse(getsione("percent"),100,1),
            transcost=.01
            ) 
{ 
    stopifnot(length(type)==length(strategy) && !any(duplicated(paste(type,strategy))))
    res <- NULL
    for(i in 1:length(type)) {
        res <- rbind(res,sumne1(type=type[i],strategy=strategy[i],annfactor=annfactor,percent=percent,transcost=transcost))
    }
    res
}
plotne <-
function(x,
                legend=TRUE,
                label=FALSE,
                color=TRUE,
                cumul=TRUE,
                point=FALSE,
                dogrid=TRUE,
                ylab="performance",
                colstart=0,
                lwd=1,
                sleepytime=1
                ) 
{ 
    stopifnot(is.zoo(x) && all(as.Date(as.character(index(x)))==index(x)))
    if(color) {
        if(length(colstart)==1) {
            mycol <- colstart + (1:ncol(x))
        } else {
            mycol <- colstart
        }
    } else {
         mycol <- 1
    }
    #mycol <- c("purple","dark orange")
    xmat <- coredata(x)
    i <- is.na(xmat)
    xmat[i] <- 0
    if(cumul) x2 <- apply(xmat,2,cumsum) else x2 <- xmat
    x2[i] <- NA
    x1 <- zoo(x2,index(x))
    #par(xaxt="n")
    Sys.sleep(sleepytime)
    plot(x1,sc=1,col=mycol,xlab="",ylab=ylab,lwd=lwd)
    #par(xaxt="s")
    #axis.Date(side=1,at=index(x1)[seq(from=1,to=length(x1),by=1)],cex.axis=.5,format="%Y-%b-%d",las=2)    
    if(point) {
        for(i in 1:ncol(x1)) {
            points(index(x1),x1[,i],col=mycol[i],cex=1.,pch=16)
        }
    }
    if(label) {
        curves <- vector("list",ncol(x1))
        for(i in 1:ncol(x1)) {
            curves[[i]] <- list(x=as.Date(index(x1)),y=as.numeric(x1[,i]))
        }
        labcurve(curves=curves,labels=colnames(x1),col.=mycol)
    }
    if(legend) {
        legend(x='topleft',legend=colnames(x1),col=mycol,lty=1,bty='n')
    }    
    if(dogrid) {grid(col='darkgray')}
}
nepos <-
function(iga1,gsf="AVG",comp="gross") 
{
    stopifnot(all(iga1%in%getive("ga",how="all")))
    idatestamp <- ixxtods(xx="ga",ixx=iga1)
    qual <- psz("WHERE domain='po' AND field1='comp' AND value1= '",comp,"' AND field2='date' AND datestamp IN(",fldlst(idatestamp,"'","'"),") GROUP BY value1, value2")
    x <- mz(tabtomat(extract("vegavi",psz("value2, value1, ",gsf,"(value)"),qual)))
    mode(x) <- "numeric"
    return(x)
}
neperf <-
function(iga1,gsf="AVG") 
{
    stopifnot(all(iga1%in%getive("ga",how="all")))
    idatestamp <- ixxtods(xx="ga",ixx=iga1)
#    qual <- psz("WHERE domain='pe' AND field1='lag' AND value1=0 AND field2='date' AND ixx IN(",fldlst(iga1),") GROUP BY value1, value2")
    qual <- psz("WHERE domain='pe' AND field1='comp' AND value1='tot' AND field2='date' AND datestamp IN(",fldlst(idatestamp,"'","'"),") GROUP BY value1, value2")
    x <- mz(tabtomat(extract("vegavi",psz("value2, value1, ",gsf,"(value)"),qual)))
    mode(x) <- "numeric"
    return(x)
}
nemkt <-
function(iga1,gsf="AVG") 
{
    stopifnot(all(iga1%in%getive("ga",how="all")))
    idatestamp <- ixxtods(xx="ga",ixx=iga1)
    qual <- psz("WHERE domain='pe' AND field1='mkt' AND value1= 'mkt' AND field2='date' AND datestamp IN(",fldlst(idatestamp,"'","'"),") GROUP BY value1, value2")
    x <- mz(tabtomat(extract("vega",psz("value2, value1, ",gsf,"(value)"),qual)))
    mode(x) <- "numeric"
    return(x)
}
nedebeta <-
function(
                y,        #returns
                x,         #market returns
                investable=FALSE
                )
{
    yx <- merge(y,x,lag(x,-1),lag(x,1)) #because symmetric, would work for xts or zoo
    beta1 <- summary(lm(yx[,1] ~ yx[,2] + yx[,3] + yx[,4]))
    if(investable) {
        yx[,1]- beta1$coefficients[2,1]*yx[,2] -
                beta1$coefficients[3,1]*yx[,2] -
                beta1$coefficients[4,1]*yx[,2]
    } else {
        yx[,1]- beta1$coefficients[2,1]*yx[,2] -        
                beta1$coefficients[3,1]*yx[,3] -    #uses lagged/leading market returns so not investable
                beta1$coefficients[4,1]*yx[,4]
    }
}
getne <-
function(
            type=sort(rep(c("peraw","mkt","pecor"),length(extract("ne","DISTINCT strategy")))),
            strategy=rep(as.character(extract("ne","DISTINCT strategy")),length(type)/length(as.character(extract("ne","DISTINCT strategy")))),
            annfactor=getsione("annfactor"),
            percent=ifelse(getsione("percent"),100,1),
            transcost=.01,
            adjustgross=FALSE
            ) 
{ 
    stopifnot(length(type)==length(strategy) && !any(duplicated(paste(type,strategy))))
    myscale <- ifelse(adjustgross,"/pogross","")
    res <- NULL
    for(i in seq_along(strategy)) {
        query <- psz("SELECT date, '",paste(sep="$",strategy[i],type[i]),"' AS strategytype, ",type[i],myscale," AS value FROM ne WHERE strategy = '",strategy[i],"'")
        res <- rbind(res,mysqlQuery(query))
    }
    mat <- tabtomat(res)
    colnames(mat) <- lapply(strsplit(colnames(mat),split="$",fixed=TRUE),paste,collapse="-")
    mz(mat)
}
tryautorestart <-
function()
{
    if("autorestart"%in%dirtab() && as.logical(gettab("autorestart"))) {
        derqu()
        startqu(disable=TRUE, irestart=TRUE)
    }
}
startqu <-
function(...,disable=TRUE,irestart=disable) 
{
    on.exit({
        if(disable) enablegu(TRUE)
        if(irestart) autorestartop(FALSE)
        pt("terminated")
    })
    if(irestart) autorestartop(TRUE)
    if(disable) enablegu(FALSE)
    pt("queue started")
    todo <- as.numeric(extract("qu","ijo",qual="ORDER BY ijo"))
    for(ijo in todo) {
        for(xx in intxx()) mysqlQuery(psz("DELETE FROM ",xx))
        resjo(ijo,disablegu=FALSE)
        for(xx in outxx()) mysqlQuery(psz("DELETE FROM ",xx))
        uncotaupdate(clear=TRUE,disable=FALSE)
        unseupdate(disable=FALSE)
        liliupdate(disable=FALSE)
        papaupdate(disable=FALSE)
        statusqu(ijo=ijo,status="start",jo=FALSE)
        ruquupdate(disable=FALSE)
        rujoupdate(disable=FALSE)
        rucuupdate(disable=FALSE)
        runjo()
        if(getsione("verbose")) print("start archiving")
        for(xx in inpxx())edtvedi(xx=xx,ixx=jolu(xx=xx,ijo=ijo),status="lock")      #update input status="lock"
        for(i in seq_along(outxx())) { 
            tt <- system.time( {
                repixx <- jolu(ijo=ijo,xx=outxx()[i])   #modified to overwrite outputs 2010-04-10
                ixx <- arcve(xx=outxx()[i],status="lock",mycomment=jofield(ijo=ijo,addijo=TRUE),ixx=repixx)  #archive outputs with comment from jo
                if(!is.na(ixx)) updjo(ijo=ijo,xx=outxx()[i],ixx=ixx)                    #post foreign key of output into jo
            })[3]
            if(getsione("verbose"))  print(psz("archive ",outxx()[i]," : ",floor(tt),"s"))
        }              
        for(i in seq_along(intxx())) {
            tt <- system.time( {
                repixx <- jolu(ijo=ijo,xx=intxx()[i])
                ixx <- arcve(xx=intxx()[i],status="lock",mycomment=jofield(ijo=ijo,addijo=TRUE),ixx=repixx,checkdates=intxx()[i]%in%intxxwithdates()) #replace with same index
                if(!is.na(ixx)) updjo(ijo=ijo,xx=intxx()[i],ixx=ixx)                    #post foreign key of output into jo
            })[3]
            if(getsione("verbose"))  print(psz("archive ",intxx()[i]," : ",floor(tt),"s"))
        }
        statusqu(ijo=ijo,status="done")
        ruquupdate(disable=FALSE)
    }
    #pt("adding cross-job comparisons")
    #gapeersadd(jobs=todo)   #this may take a long time
    pt("successful completion")
    chkjo()
}
delqu <-
function(ijo=getijo()) 
{
    stopifnot(ijo %in% getijo(what="all",where="qu"))
    mysqlQuery(psz("
        DELETE FROM qu
        WHERE ijo = ",ijo
        ))
}
autorestartop <-
function(istart=FALSE,verbose=TRUE)
{
    droptab("autorestart")
    mysqlQuery(psz("CREATE TABLE autorestart AS SELECT ",istart," AS istart"))
    if(verbose) {pt(psz("autorestart ",ifelse(as.logical(gettab("autorestart ")),"enable","disable")))}
}
updown1 <-
function(i=1:10,period=4){ramp(i,period)*(alternate(i,period)/2+.5) - ramp(-i+1,period)*(alternate(i,period)/2-.5)}
splitsu <-
function(
                isu=unique(mexx(xx="su")),
                ndb=naa(),
                type=c("datestamp","ixx"),
                channel=chve()
                ) {
    type <- match.arg(type)
    nsu <- length(isu)
    suda <- sort(ixxtods(ixx=as.numeric(isu),xx="su"))
    sunu <- mysqlQuery(psz("
                SELECT datestamp, size 
                FROM (
                    SELECT datestamp, COUNT(*) AS size
                    FROM vesu 
                    WHERE datestamp IN(",flds(suda),") 
                    GROUP BY datestamp) AS a
                ORDER BY size DESC
                "),channel=channel)
    su <- cbind(isu,suda)
    mult <- ceiling(nsu/ndb) #maximum number of su per db
    dsmat <- matrix(NA,min(ndb,nsu),mult)
    isumat <- numat <- matrix(NA,min(ndb,nsu),mult)
    for(i in 1:nsu) {
        jcol <- (i-1)%/%ndb + 1      #col
        irow <- updown1(i,ndb)       #row = database = processor
        dsmat[irow,jcol] <- sunu[i,1]
    }
    isumat[] <- dsmat
    stopifnot(identical(sort(suda),sort(isumat)))
    if(type!="datestamp") {
        for(i in 1:nrow(isumat)) {
            for(j in 1:ncol(isumat)) {
                if(!is.na(isumat[i,j])) isumat[i,j] <- dstoixx(ds=isumat[i,j],xx="su")
            }
        }
    }
    res <- as.list(data.frame(t(isumat)))
    for(i in seq_along(res)) {res[[i]] <- res[[i]][!is.na(res[[i]])]}
    res
}
splitaa <-
function(
                channel=chve(),
                aa=TRUE,
                qu=TRUE,
                isuselect=getive(xx="su",how="all")
                ) {
    stopifnot(all(c("aats","aared")%in%dirtab(channel=channel)))
    stopifnot(("qu"%in%dirtab(channel=channel)) && 0<sqlnrow(tab="qu",channel=channel)) 
    on.exit(newcon(nameve(DBcon)))
    isu <- sort(intersect(as.numeric(extract("qu","DISTINCT isu",channel=channel)),isuselect))
    splitargs <- list(isu=isu,type="datestamp",ndb=naa())
    spl <- do.call(what="splitsu",args=splitargs)
    splitargs$type <- "ixx"
    spi <- do.call(what="splitsu",args=splitargs)
    deleteaa(ruthless=TRUE)
    for(i in seq_along(spl)) {
        print(psz("split to schema #",i," ",Sys.time()))
        newcon(psz("aa",i))
        if(aa) {    #big tables: just the bui required
            bui <- as.character(as.matrix(mysqlQuery(psz("SELECT DISTINCT bui FROM ",nameve(channel),".vesu WHERE datestamp IN (",flds(spl[[i]],),")"))))
            copysubset(tab="aats",channel=channel,bui=bui)
            copysubset(tab="aared",channel=channel,bui=bui)
        }
        if(qu) {
            mysqlQuery("DROP TABLE IF EXISTS qu")
            newqu()
            quflds <- dirfld("qu")
            mysqlQuery(psz("
                INSERT INTO qu (",fldlst(quflds),")
                SELECT ",fldlst(quflds)," FROM ",nameve(channel),".qu
                WHERE status='todo' and isu IN (",flds(spi[[i]]),")
            "))
        }
        stopifnot(splitchk())
    }
    if(aa) { copyaa() }    #other: copy entire table
}
spawnaa <-
function(iaa=aawithqu()){
    #should check status first
    sfInit( parallel=TRUE, cpus=length(iaa) )#=min(max(naa(),1),length(iaa)) ) #length(iaa) should suffice
    result <- sfLapplyWrap(
                X=iaa, 
                FUN=runaa)
    sfStop()
}
sis <-
function(
            ){
    splitaa()
    initialiseaa()
    spawnaa()
}
runme <-
function(
            ime=NULL,    #can be vector
            group=c("all","allexcepttotal","total")
            ){
    if(is.null(ime))stop("supply argument: ime")
    group <- match.arg(group)
    newcon()
    if(group%in%c("all","allexcepttotal")) {
        statusme(ime=ime,status='todo')
        statusme(ime=ime,status='hold',group='total')
        derqu()
        sis()
    }
    newcon()
    if(group%in%c("all","total")) {
        statusme(ime=ime,status='todo',group='total')
        derqu()
        sis()
    }
    newcon()
    print(Sys.time())    
}
runaa <-
function(X){
    Sys.sleep(3*X)
    aa <- psz("aa",X)
    newcon(aa)
    n <- length(aawithqu())
    parent <- locwin(n=n)
    dergu(label=aa,parent=parent[X,],size=1000/ceiling(n/2))
    startqu()
}
reperiodme <-
function()
{
    da <- unlist(lapply(strsplit(extract("me","distinct method","where method like 'report from%'"),"report from "),"[[",2))
    ijo <- vector("list",length(da))
    for(i in seq_along(da)) {
        ijo[[i]] <- as.numeric(extract("me","ijo",psz("WHERE method LIKE 'report %",da[i],"'")))
    }
    names(ijo) <- da
    ijo
}
relibme <-
function()
{
    ili <- sort(unique(extract("me","DISTINCT ili","WHERE 1<ili")))
    ijo <- vector("list",length(ili))
    for(i in seq_along(ili)) {
        ijo[[i]] <- as.numeric(extract("me","ijo",psz("WHERE  method LIKE 'librep%' AND ili IN (",ili[i],")")))
    }
    names(ijo) <- ili
    ijo
}
ramp <-
function(i=1:10,period=4,ioff=0){ioff+(i-1)%%period+1}
mexx <-
function(
            ime=aame(),
            xx="jo",
            group=c("allexcepttotal","po","ga","combopo","neutral","comborep","unit","total","entire","longtotal","libtot","indiv"),    #preset groups of methods
            myqual=NULL,
            channel=chve()
            ) {
    group <- match.arg(group)
    methodqual <- switch(group,
                    allexcepttotal=" AND method NOT LIKE 'tot%'",
                    unit=" AND method in ('unit')",
                    po=" AND method in ('unit', 'neutral', 'combo')",
                    ga=" AND (method in ('librep') OR method LIKE 'report%')",
                    combopo=" AND method='combo'",
                    neutral=" AND method='neutral'",
                    comborep=" AND method LIKE 'report from 2000%'",
                    total=" AND method LIKE 'tot%'",
                    longtotal=" AND method LIKE 'totrep from 2000%'",
                    libtot=" AND method LIKE 'totlib-%'",
                    indiv=" AND method LIKE 'librep'",
                    entire="")
    ixx <- psz("jo.i",xx)
    sort(as.numeric(extract("vemevi INNER JOIN jo ON vemevi.ijo=jo.ijo",ixx,psz("WHERE ixx IN ( ",fldlst(ime)," )",methodqual,myqual),channel=channel))) #mod 2012-02-08 with join...
}
mepo <-
function(ipo=mexx(xx="po",group="combopo"),channel=chve()) {
    ds <- ixxtods(xx='po',ixx=ipo)
    extract("vepo","date, sum(abs(xt)), sum(abs(xt))/sum(abs(xo))",psz("WHERE datestamp IN (",flds(ds),") GROUP BY date"),channel=channel)
}
lastdate <-
function() {
    offda(as.Date(extract("aats","max(date)")),-2)
}
initialiseaa <-
function(iaa=aawithqu()) {
    on.exit(newcon("bloomberg"))  #leave unchanged
    for(i in iaa) {
        print(i)
        newcon(psz("aa",i))
        #resve("cu",upd=F) #needed for susummarystring()
        resjo(1)
        newsi() #to create sise
        newca() 
        derca() #for newxx
        caladerive()
        newxx()
        resjo(1)
        cucapriorop()
        newrece()
    }
}
edsimex <-
function(
                isi0=getive(xx="si"),                 #template parameters
                pass=as.character(1:7), #1=tore, 2=topo1, 3=topon, 4=toga, 5=toti, 6=togaga, 7=togati
                ijo=1:3,                #for pass=3,6 need ijo to aggregate
                startafter="2000-01-01",#for pass=4,5,6
                arch=FALSE,
                mnem="",
                tightoffset=0
                )
{
    pass <- match.arg(pass)
    resve(xx="si",ixx=isi0)
    mycomment <- psz(mnem," ",switch(pass,"1"="unit","2"="neutral","3"="combo","4"="librep","5"="report","6"="totrep","7"="totrep"))
    mysqlQuery("DELETE FROM si WHERE mytable='po' AND parameter='ijo'")
    mysqlQuery(psz("UPDATE si SET value = 'TRUE' WHERE parameter = 'run'"))
    mysqlQuery(psz("UPDATE si SET value = 'FALSE' WHERE parameter = 'run' AND mytable IN ('du','cu','su','ra','ti','ro','xy')"))
    mysqlQuery("DELETE FROM si WHERE mytable='po' AND parameter='method'")
    mysqlQuery("DELETE FROM si WHERE mytable='ty' AND parameter='detectstartfrompo'")
    mysqlQuery("DELETE FROM si WHERE mytable='ga' AND parameter='ijo'")
    mysqlQuery("DELETE FROM si WHERE mytable='ti' AND parameter='ijo'")
    mysqlQuery("DELETE FROM si WHERE mytable='co' AND parameter='tightoffset'")
    if(pass=="1") { #unit
        mysqlQuery("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('po','method','1','unit','character')")
        mysqlQuery(psz("UPDATE si SET value = 'FALSE' WHERE parameter = 'run' AND mytable IN ('co','po','pe','va','vi','ga','ma','ti')"))
    } else {
        mysqlQuery("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('po','method','1','neutral','character')")
        mysqlQuery("UPDATE si SET value = 'FALSE' WHERE parameter = 'rece' AND mytable ='re'")
        mysqlQuery("UPDATE si SET value = 'FALSE' WHERE parameter = 'run' AND mytable ='ma'")
    }
    if(pass%in%c("2","3")) {    #neutral
        fromtab("co",postpo="no")
        mysqlQuery(psz("UPDATE si SET value = 'TRUE' WHERE parameter = 'run' AND mytable='ty'"))
        mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('co','tightoffset','1',",tightoffset,",'numeric')"))
        #mysqlQuery(psz("UPDATE si SET value = ",tightoffset," WHERE parameter = 'tightoffset' AND mytable='co'"))
        if(pass=="3") { #combo
            for(ii in seq_along(ijo)) {
                mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('po','ijo',",ii,",",ijo[ii],",'numeric')"))
            }   
        }
    }
    if(pass=="4") { #reports
        fromtab("pe",postpo="ga")
    } else if(pass=="5") {
        fromtab("pe",postpo=c("ga","ti"))
    } else if(pass%in%c("6","7")) {
        fromtab("ga",postpo="no")
        mysqlQuery("UPDATE si SET value='TRUE' WHERE mytable ='ga' AND parameter='run'") #hack for game
        if(pass=="6") {mysqlQuery("UPDATE si SET value='TRUE' WHERE mytable = 'ti' AND parameter = 'run'") }
        for(ii in seq_along(ijo)) {
            mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('ga','ijo',",ii,",",ijo[ii],",'numeric')"))
            mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('ti','ijo',",ii,",",ijo[ii],",'numeric')"))
        }   
    }
    if(pass%in%c("4","5")) {
        mysqlQuery("DELETE FROM si WHERE mytable='ty' AND parameter='startafter'")
        mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('ty','startafter','1','",startafter,"','character')"))
        mysqlQuery(psz("UPDATE si SET value = 'TRUE' WHERE mytable='ty' AND parameter='run'"))
        mysqlQuery("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('ty','detectstartfrompo','1','TRUE','logical')")
    }
    if(arch) arcve(xx="si",myco=psz("me",isi0," ",mycomment),allowempty=TRUE)
}
edsilapo <-
function(
                lapo=-13:13,
                startafter='2000-01-01',
                runma=FALSE,
                qupanel=c("none","year"), #only these allowed
                xx=c("xo","xt"), #component of pe, va to lag-analyse
                te1cancel=TRUE
                ) {
    qupanel <- match.arg(qupanel)
    xx <- match.arg(xx)
    mysqlQuery(psz("UPDATE si SET value = '",startafter,"' WHERE parameter = 'startafter'"))
    mysqlQuery(psz("UPDATE si SET value = '1' WHERE parameter = 'alphamin'"))
    mysqlQuery(psz("UPDATE si SET value = 'TRUE' WHERE parameter = 'run'"))
    mysqlQuery(psz("UPDATE si SET value = 'FALSE' WHERE parameter = 'run' AND mytable IN ('du','cu','su','ra','ti','ro','xy')"))
    if(!runma) {mysqlQuery(psz("UPDATE si SET value = 'FALSE' WHERE parameter = 'run' AND mytable IN ('ma','co','po')"))} #co and po normally will be up-to-date already
    if(te1cancel) {mysqlQuery(psz("UPDATE si SET value = 'tall' WHERE parameter = 'te1' AND mytable='ga'"))}
    mysqlQuery(psz("UPDATE si SET value = 'tall' WHERE parameter = 'te2' AND mytable='ga'"))
    mysqlQuery("DELETE FROM si WHERE mytable IN ('ga', 're') AND parameter='qupanel'")
    mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('re', 'qupanel', 1, '",qupanel,"', 'character')"))        
    mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('ga', 'qupanel', 1, '",qupanel,"', 'character')"))        
    mysqlQuery("DELETE FROM si WHERE mytable='la' AND parameter='lapo'")
    for(i in seq_along(lapo)) {
        mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('la','lapo',",i,",",lapo[i],",'numeric')"))
    }   
    mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('pe', 'xx', '1', '",xx,"', 'character')"))
    mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('va', 'xx', '1', '",xx,"', 'character')"))
}
edsilace <-
function(lace=sort(c(-1,1,seq(from=-50,to=50,by=10))),startafter='2000-01-01') {
    mysqlQuery(psz("UPDATE si SET value = '",startafter,"' WHERE parameter = 'startafter'"))
    mysqlQuery(psz("UPDATE si SET value = 'TRUE' WHERE parameter = 'run'"))
    mysqlQuery(psz("UPDATE si SET value = 'FALSE' WHERE parameter = 'run' AND mytable IN ('du','cu','su','ra','ga','ti')"))
    mysqlQuery(psz("UPDATE si SET value = 'TRUE' WHERE parameter = 'run' AND mytable IN ('ro','xy')"))
    mysqlQuery("DELETE FROM si WHERE mytable='la' AND parameter='lace'")
    mysqlQuery("DELETE FROM si WHERE mytable='re' AND parameter='rece'")
    mysqlQuery(psz("UPDATE si SET value = 'tall' WHERE parameter = 'te1' AND mytable='ma'"))
    mysqlQuery(psz("UPDATE si SET value = 'tall' WHERE parameter = 'te2' AND mytable='ma'"))
    mysqlQuery(psz("UPDATE si SET value = 'none' WHERE parameter = 'qupanel' AND mytable='re'"))            
    mysqlQuery("DELETE FROM si WHERE mytable='ma' AND parameter IN ('te1', 'te2')")
    mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('ma','te1',1,'tall','character')"))
    mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('ma','te2',1,'tall','character')"))
    mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('re','rece',1,'TRUE','logical')"))
    for(i in seq_along(lace)) {
        mysqlQuery(psz("INSERT INTO si (mytable, parameter, ipa, value, mode) VALUES ('la','lace',",i,",",lace[i],",'numeric')"))
    }   
}
deleteaa <-
function(idb=1:naa(),ruthless=FALSE) {
    if(!ruthless) {return()}
    for(i in idb) {
        newcon(psz("aa",i))
        tabs <- dirtab()
        for(j in seq_along(tabs)) {
            mysqlQuery(psz("DROP TABLE IF EXISTS ",tabs[j]))
            mysqlQuery(psz("DROP VIEW IF EXISTS ",tabs[j]))
        }
    }
}
decme <-
function() 
{
structure(
        c(
        "ijo", "isu", "ili", "isi", "ice", "ire", "ico", "ipo", "iba", "iho", "method", 
        "SMALLINT ", "SMALLINT ", "SMALLINT ", "SMALLINT ", "SMALLINT ", "SMALLINT ", "SMALLINT ", "SMALLINT ", "SMALLINT ", "SMALLINT ", "CHAR(50)  ", 
        "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL"
        ), 
        .Dim = c(11L, 3L), 
        .Dimnames = list(NULL, c("xfield", "xtype", "xnull"))
        )
}
copysubset <-
function(bui,tab=c("aats","aared"),channel=chve()) {
    tab <- match.arg(tab)
    stopifnot(DBcon!=bloomberg)
    fld <- dirfld(tab,channel=channel)
    qual <- psz(" WHERE bui IN (",flds(bui),")")
    mysqlQuery(psz("DROP TABLE IF EXISTS ",tab))
    mysqlQuery(psz("CREATE TABLE ",tab," AS SELECT * FROM ",nameve(channel),".",tab," LIMIT 0"))
    mysqlQuery(psz("
        INSERT INTO ",tab," (",fldlst(fld,coll=", "),")
        SELECT ",fldlst(fld,,coll=", ")," 
        FROM ",nameve(channel),".",tab," ",qual
    ))
    addidx(tab,c("date","bui"))
    addidx(tab,"date")
    addidx(tab,"bui")
}
copyaa <-
function(iaa=1:naa(),excludetabs=dirtab(c("aats","aared","aasnap","copy","safe","10","11"),channel=bloomberg)) {
    on.exit(newcon("bloomberg"))  #leave unchanged
    aatab <- setdiff(tabsset(tab="aa",engine="MyISAM",channel=bloomberg),excludetabs)
    aatab <- union(aatab,"aaindices") #should not be required but aaindices keeps reverting to engine=MEMORY and hence getting missed
    for(i in iaa) {
        newcon(psz("aa",i))
        for(j in seq_along(aatab)) {
            print(psz("aa=",i," table=",aatab[j]))
            mysqlQuery(psz("DROP TABLE IF EXISTS ",aatab[j]))
            mysqlQuery(psz("CREATE TABLE ",aatab[j]," SELECT * FROM bloomberg.",aatab[j]))
        }
    }
}
alternate <-
function(i=1:10,period=4){1-((((i)-1)%/%period)%%2)*2}
aawithqu <-
function() {
    which(as.logical(sapply(sapply(1:naa(),FUN=function(i) {dirtab("qu",channel=odbcConnect(psz("aa",i)))}),length)))
}
updjo <-
function(
            ijo=getijo(),
            xx=c(outxx(),intxx()),
            ixx=getive(xx),
            channel=chve()
            ) 
{
    xx <- match.arg(xx)
    stopifnot(ixx %in% getive(xx,how="all") && ixx>0)
    mysqlQuery(psz("
                UPDATE jo 
                SET jo.i",xx," = ",ixx,
                " WHERE ijo = ", ijo
            ),
            channel=channel
            )
}
runjo <-
function(startfrom=getsirun()[1]) 
{
    stopifnot(startfrom%in%getsirun())
    irun <- getsirun()
    irun <- union("li",irun[match(startfrom,irun):length(irun)])    #the library is always sourced
    derlo()
    for(myclass in irun) {
        print(myclass)
        putlo(tab=myclass,event="start")
        derxx <- get(psz("der",myclass))
        stopifnot(do.call(what=derxx,args=getsi(myclass))) #ensures failure if chkxx() fails
        putlo(tab=myclass,event="end")
    }
}
resjo <-
function(
            ijo=getijo(),
            disablegu=FALSE,
            restorera=FALSE,     #table ra is not restored because huge
            channel=chve()
            ) 
{
    if(disablegu) {
        on.exit({enablegu(TRUE);pt("...archive restored")})
        pt("restoring archived job...")
        enablegu(FALSE)
    }
    for(xx in inpxx()) {
        ifo <- jolu(xx=xx,ijo=ijo,channel=channel)
        resve(xx=xx,ixx=ifo,updategu=FALSE,channel=channel)
    }
    for(xx in c(outxx(),intxx())) {
        ifo <- jolu(xx=xx,ijo=ijo,channel=channel)
        if(xx=='ra' && !restorera) ifo <- NA
        if(!is.na(ifo)) {
            resve(xx=xx,ixx=ifo,updategu=FALSE,channel=channel)
        } else {
            do.call(what=psz("new",xx),args=list())    #delete!
        } 
    }
}
jolusi <-
function(
                xx=allvexx(),      
                ijo=getijo(),    #ixx for jo
                filters=list(parameter="method",value="neut%"),
                ftab="si",
                channel=chve()
                ) 
{
    xx <- match.arg(xx)
    stopifnot( all(ijo %in% getijo("all",channel=channel)))
    stopifnot(ftab%in%allvexx())
    tab <- as.matrix(mysqlQuery(psz("
                                SELECT ijo, i",xx,"
                                FROM jo INNER JOIN ve",ftab,"vi
                                ON jo.i",ftab," = ve",ftab,"vi.ixx
                                WHERE ijo IN (",fldlst(ijo),") AND
                                ",psz(names(filters)," LIKE '",filters,"'",collapse=" AND ") 
                            ),channel=channel)
                        )
    ixx <- tab[match(ijo,tab[,1]),2,drop=TRUE]
    sort(unique(ixx[!is.na(ixx)]))
}
jolu <-
function(
                xx=allvexx(),      
                ijo=getijo(),
                channel=chve()
                ) 
{
    xx <- match.arg(xx)
    stopifnot( all(ijo %in% getijo("all",channel=channel)))
    tab <- as.matrix(mysqlQuery(psz("
        SELECT ijo, i",xx,"
        FROM jo
        WHERE ijo IN (",fldlst(ijo),")"
        ),channel=channel))
    ixx <- tab[match(ijo,tab[,1]),2,drop=TRUE]
    ifelse(valive(xx=xx,ixx=ixx,channel=channel),ixx,NA)
}
jofield <-
function(ijo=getijo(),field="comment",addijo=FALSE,channel=chve())  
{
    psz(extract("jo",field,psz("WHERE ijo = '",ijo,"'"),channel=chve()),ifelse(addijo,psz(" ijo=",ijo),""))
}
ipmult <-
function(
            ijo=getijo(),
            xx=inpxx(),
            channel=chve()
            ) 
{
    res <- NULL
    for(i in seq(along=xx)) {
        res <- c(res,
                as.numeric(
                    mysqlQuery(
                        psz("SELECT COUNT(*) FROM jo WHERE i",xx," IN 
                                    (SELECT i",xx,"
                                    FROM jo 
                                    WHERE ijo = ",ijo,")"),
                            channel=channel
                            )))
    }
    res
}
getijo <-
function(what=c("last","todo","all"),where="jo",channel=chve()) 
{
    if(nrow(gettab(tab=where,channel=channel))==0) {
        res <- 0 
    } else {
        what <- match.arg(what)
        if(what=="last") res <- max(extract(where,"ijo",channel=channel)) else
        if(what=="todo") res <- as.numeric(extract(where,"ijo","WHERE status = 'todo'",channel=channel)) else
        if(what=="all") res <- as.numeric(extract(where,"ijo",channel=channel))
    }
    res
}
deljo <-
function(
            ijo=getijo(),
            ikeepjo=NULL,
            deletefreeinputs=FALSE,  #option to delete inputs & intermediates not referenced elsewhere
            dependents=TRUE,
            channel=chve(),
            protectlastentry=FALSE
            ) 
{
    stopifnot(all(ijo%in%getijo("all",channel=channel)))
    ijo <- setdiff(ijo,ikeepjo)
    allxx <- setdiff(c(inpxx(),intxx()),"li")
    for(i in seq_along(ijo)) {
        print(psz(i,"/",length(ijo)))
        if(dependents) {
            for(xx in allxx) {
                ixx <- jolu(xx=xx,ijo=ijo[i],channel=channel)
                if(ipmult(ijo=ijo[i],xx=xx,channel=channel)==1  && !is.na(ixx) && xx!="su")  {#if input not referenced by other jobs
                    cat(psz("freeing ",xx," ",ixx))
                    edtvedi(xx=xx,
                            ixx=ixx,
                            status="free",channel=channel)
                    if(deletefreeinputs) {
                        cat(psz("deleting ",xx," ",ixx))
                        delve(xx=xx,ixx=ixx,channel=channel,protectlastentry=protectlastentry)
                    }
                }
            }
            for(xx in outxx()) { 
                ixx <- jolu(xx=xx,ijo=ijo[i],channel=channel)
                if(!is.na(ixx)) {
                    cat(psz("deleting ",xx," ",ixx))
                    delve(xx=xx,ixx=ixx,channel=channel,protectlastentry=protectlastentry)
                }
            }
        }
        mysqlQuery(psz("
            DELETE FROM jo WHERE ijo = ",ijo[i]            #delete job
            ),channel=channel)
    }
}
decjo <-
function() 
{
    structure(
        c(
            "datestamp", "status", "comment", "ijo", "isu", "ili","isi", "ice", "ire", "ico", "ipo", "ilo", "ira", "iga", "ima", "ifa", "iti", 
            "CHAR(20)", "CHAR(6)", "CHAR(255)", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", "SMALLINT", 
            "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"
        ), 
        .Dim = c(17L, 3L), 
        .Dimnames = list(NULL, c("xfield", "xtype", "xnull"))
        )
}
currentjo <-
function(
                    ijo=mexx(),
                    channel=chve()
                    )
{
    stopifnot(all(ijo%in%getijo(what="all")))
    ijo <- sort(ijo)
    tab <- gettab("jo",psz("WHERE ijo IN (",flds(ijo),") ORDER BY ijo")) #[,-1:-3,drop=FALSE]
    j <- which(nchar(colnames(tab))==3 & colnames(tab)!="ijo")
    jcomp <- setdiff(1:ncol(tab),j)
    xx <- substr(colnames(tab)[j],2,3)
    res <- subtab <- tab[,j,drop=FALSE]
    mode(res) <- "logical"
    for(jj in seq_along(j)) {
        ixx <- getive(xx=xx[jj],how="all")
        res[,jj] <- as.numeric(subtab[,jj])%in%ixx
    }
    data.frame(tab[,jcomp,drop=FALSE],res)
}
addjo <-
function(
            datestamp=format(Sys.time()),
            status="todo",
            comment="-",
            ijo=max(getijo(what="last",where="jo"),getijo(what="last",where="veme"))+1,
            isu=getive("su"),
            isi=getive("si"),
            ice=NA,
            ire=NA,
            ico=NA,
            ili=getive("li"),
            ipo=NA,
            ira=NA,
            iga=NA, 
            ima=NA,
            ilo=NA,
            ifa=NA,
            iti=NA,
            gu=TRUE,
            disable=TRUE,
            saveit=TRUE, 
            channel=chve()
            ) 
{
    stopifnot(isu %in% getive("su",how="all"))
    stopifnot(ili %in% getive("li",how="all"))
    stopifnot(length(isu)==1 && length(isi)==1 && length(ili)==1)
    if(saveit) {
        stopifnot(all(isi %in% getive("si",how="all")))
        resve(xx='li',ixx=ili)
        resve(xx='si',ixx=isi)
        stopifnot(chklisi())
    }
    jorow <- data.frame(
        datestamp=datestamp,
        status=status,
        comment=comment,
        ijo=ijo,
        isu=isu,                                                            
        ili=ili,                                                           
        isi=isi,
        ice=ice,
        ire=ire,
        ico=ico,
        ipo=ipo,                                  
        ira=ira,                                  
        iga=iga,                                                           
        ima=ima,
        ilo=ilo,
        ifa=ifa,
        iti=iti
        )
    if(saveit) {
        sqlSave(channel=channel,
            dat=jorow,
            tablename="jo",
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
        edtvedi("su",ixx=as.numeric(isu),status="lock",channel=channel)
        if(saveit) {edtvedi("si",ixx=as.numeric(isi),status="lock",channel=channel)}
        edtvedi("li",ixx=as.numeric(ili),status="lock",channel=channel)
        if(existsgu()&& gu) ruquupdate(disable=disable)
        return(as.numeric(jorow[,"ijo"]))
    } else { return(jorow) }
}
vefields <-
function(
                xx=allvexx(),
                k=getsione("nfac",def=20),
                what=c("all","index"),
                vitoo=FALSE,
                channel=chve()
                ) 
{
    xx <- match.arg(xx)
    what <- match.arg(what)
    if(what=="all") {
    res <- switch(xx,
                    su="bui, date",
                    li="code, line, fun, beforeclass",
                    si="mytable, parameter, ipa, value, mode",
                    cu="date, btk, weight, bui, countryfull, countryissue, bti, btklocal, weightlocal, big, bigish",
                    ga="domain, field1, field2, value1, value2, type, value, nper",
                    ma="equation, lag, te1, te2, comp, type, value",
                    jo="isu, isi, ili, iga, ima, ijo",
                    lo="mytable, mystart, myend, elapsed, mysize",
                    fa="type, fnum, var",
                    ra="domain, date, lag, comp, qu, value",
                    me="ijo, isu, ili, isi, ice, ire, ico, ipo, iba, iho, method",
                    be="ililhs, ilirhs, isu, type, value",
                    ti="date, year, month, datew, cashdollar, xn, xg, xt, pe, pev",
                    po="date, bui, te, lodir, poshort, pa, tapo, tapo1, polarity, ra, ro, tr, ti, xt, xo, xc, reason, lever, neutral, countryissue, weight, minxo, maxxo, alpha, evret",
                    co="date, control, apply, value",
                    ce=psz(c("date, bui, sdev, uniqueness, full, qua, evp, mcp, mvp, poco",
                            psz("beta",c("evp","mcp","mvp"),collapse=", "),
                            psz("loadings",1:k,collapse=", "),
                            psz("fmp",1:k,collapse=", "),
                            psz("hpl",1:k,collapse=", "),
                            psz("method",1:k,collapse=", ")
                            ),collapse=", "),
                    re="date, bui, dace, lace, tare, rem, res, rer, tayi, yim, yis, yir",
                    nu="dam1, dal1, date, dap1, bui, btk, xo, xop1, xc, xtp1, noti, prdom1, prdol1, prdovw, prlovw, prlol1, navm1, nav, re, ti, tim1, ipo, current, nu, nut, btkba, buiba, nuba, btkpms, qty, buipms, name",
                    fo="source, pfname, date, bui, btk, yellow, qty, sector",
                    ge="date, bui, type, forc, forcse, zcoef, ztstat, zrsq"
    )
    } else {
    res <- switch(xx,
                    su="bui, date",
                    li="line, fun, beforeclass",
                    si="mytable, parameter, ipa",
                    cu="date, bui, big, bigish",
                    ga="domain, field1, field2, value1, value2, type",
                    ma="equation, lag, te1, te2, comp",
                    jo="isu, isi, ili, iga, ima, ijo",
                    lo="mytable",
                    fa="type, fnum, var",
                    ra="domain, date, lag, comp, qu, value",
                    me="ijo, isu, ili, isi",
                    be="ililhs, ilirhs, isu, type",
                    ti="date, year, month, datew",
                    po="date, bui, te, tapo, tapo1, reason, countryissue",
                    co="date, control, apply",
                    ce="date, bui",
                    re="date, bui",
                    nu="date, bui",
                    fo="date, bui",
                    ge="date, bui"
    )
    }
    if(vitoo) res <- psz(res,", ixx, datestamp")
    res
}
valive <-
function(
            xx=allvexx(),
            ixx=getive(xx),
            channel=chve()
            ) 
{
    ixx %in% getive(xx,howmany="all",channel=channel)
}
updvedi <-
function(
            xx=allvexx(),
            mycomment="",
            ixx=getive(xx,channel=channel)+1,   #a manual auto_increment, because sqlSave requires all columns present or crashes
            status="free",
            channel=chve()
            ) 
{
    if(is.na(ixx)) ixx <- 1 #to initialise
    xx <- match.arg(xx)
    vexxdi <- psz("ve",xx,"di")
    stopifnot( !(ixx %in% getive(xx,how="all",channel=channel)) && is.numeric(ixx) && length(ixx)==1 )
    stopifnot(is.character(mycomment) && length(mycomment)==1)
    stopifnot(is.character(status) && length(status)==1)
    datestamp <- format(Sys.time())
    if(nrow(gettab(vexxdi,channel=channel))>0) while(extract(vexxdi,"datestamp","ORDER BY datestamp DESC LIMIT 1",channel=channel)==format(Sys.time())) datestamp <- format(Sys.time())
    datestamp <- format(Sys.time())
    tab <- data.frame(ixx=ixx,datestamp=datestamp,comment=mycomment,status=status) 
    sqlSave(channel=channel,
            dat=tab,
            tablename=vexxdi,
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
    ixx
}
resve <-
function(
            xx=allvexx(),
            ixx=getive(xx,"ixx",channel=channel),
            updategu=TRUE,
            to=xx,
            vitoo=FALSE,
            ixxtoo=FALSE,
            andqual=NULL,
            pre="",           #modifier to archive name
            channel=chve(),   #archive channel
            channel2=DBcon    #work channel ie destination
            ) 
{
    xx <- match.arg(xx)
    stopifnot(psz("ve",xx)%in%dirtab(channel=channel))
    stopifnot(all(ixx %in% getive(xx,what="ixx",how="all",channel=channel)) )
    droptab(to,channel=channel2)
    orderclause <- ifelse(xx=="li"," ORDER BY fun, line","")
    #deliberate rule violation: name the channel2 database
    myfields <- vefields(xx,vitoo=vitoo,channel=channel)
    if(ixxtoo) {myfields <- psz(myfields,", ixx")}
    query <- psz("
                CREATE TABLE ",nameve(channel2),".",to," AS 
                SELECT ",myfields," 
                FROM ",pre,"ve",xx,"vi 
                WHERE ixx IN (",fldlst(ixx),") ",andqual," ",orderclause
                )
    mysqlQuery(query,channel=channel)
    if(xx=="ga") {
        addidx(tabname=to,fields=c("field1","field2"),channel=channel2)
    } else if (xx=="gad") {
        for(fld in setdiff(dirfld(to,channel=channel2),c("turn","xo","xc","xt","pe","pev","vat"))) addidx(to,fld,channel=channel2)
    } else {
        for(fld in strsplit(vefields(xx=xx,what="index",vitoo=vitoo,channel=channel),",")[[1]]) addidx(to,fld,channel=channel2)  #takes time, but needed for responsiveness when browsing esp ra
        if(xx=="po") {mysqlQuery(psz("CREATE INDEX date_bui ON ",to," (date, bui)"),channel=channel2)}
        if(xx=="re") {
                mysqlQuery(psz("CREATE INDEX date_bui ON ",to," (date, bui)"),channel=channel2)
                mysqlQuery(psz("CREATE INDEX date_lace_tare ON ",to," (date, lace, tare)"),channel=channel2)
                }
    }
    nrow1 <- mysqlQuery(psz("SELECT count(*) FROM ",pre,"ve",xx,"vi WHERE ixx IN (",fldlst(ixx),")"),channel=channel)
    nrow2 <- mysqlQuery(psz("SELECT count(*) FROM ",to),channel=channel2)
    stopifnot(!is.null(andqual) || nrow1==nrow2)
    if(existsgu() && updategu && to==xx) gudepend(xx=xx)
    nrow1
}
nameve <-
function(channel=chve())  {
    gsub(x=strsplit(attributes(channel)$connection.string,split="=")[[1]][2],patter=";",rep="")
}
ixxtods <-
function(
            xx=allvexx(),
            ixx=getive(xx,howmany="one",channel=channel),
            channel=chve()
            ) 
{
    xx <- match.arg(xx)
    as.character(extract(psz("ve",xx,"di"),"datestamp",paste("WHERE ixx IN (",fldlst(ixx),")"),channel=channel))
}
idxga <-
function(option=c("add","drop"),channel=chve())
{
    option <- match.arg(option)
    if(option=="add") {
        mysqlQuery("DROP INDEX field2 ON vega",channel=channel)
        mysqlQuery("DROP INDEX value2 ON vega",channel=channel)   
        mysqlQuery("ALTER TABLE vega DROP PRIMARY KEY",channel=channel)   
    } else {
        addidx("vega",c("field2"),channel=channel)
        addidx("vega",c("value2"),channel=channel)
        mysqlQuery("ALTER TABLE vega ADD PRIMARY KEY (datestamp, domain, field1, field2, value1, value2, type)",channel=channel)   
    }
}
getvedi <-
function(
            xx=allvexx(),
            ixx=getive(xx,"ixx"),
            field="comment",
            channel=chve()
            ) 
{
    xx <- match.arg(xx)
    stopifnot(psz("ve",xx)%in%dirtab(channel=channel))
    stopifnot(ixx %in% getive(xx,what="ixx",how="all",channel=channel) )
    query <- psz(" SELECT ",field," FROM ve",xx,"di WHERE ixx = '",ixx,"'")
    mysqlQuery(query,channel=channel) 
}
getve <-
function(
            xx=allvexx(),
            ixx=getive(xx,"ixx"),
            qualifier=NULL,
            limit=NULL,
            channel=chve()
            ) 
{
    xx <- match.arg(xx)
    stopifnot(psz("ve",xx)%in%dirtab(channel=channel))
    stopifnot(ixx %in% getive(xx,what="ixx",how="all",channel=channel) )
    orderclause <- switch(xx,
                    li=" ORDER BY fun, line",
                    si=" ORDER BY mytable, parameter, ipa",
                    "")
    if(is.null(qualifier)) qual <- " WHERE " else qual <- paste(qualifier," AND ")
    qual <- paste(qual,psz("datestamp = '",ixxtods(xx=xx,ixx=ixx,channel=channel),"'"))
    query <- paste(" SELECT ",vefields(xx,channel=channel),psz(" FROM ve",xx),qual,orderclause,limit)
    mysqlQuery(query,channel=channel) 
}
getive <-
function(
            xx=allvexx(),
            what=c("ixx","datestamp"),
            howmany=c("one","all"),
            channel=chve()
            )
{
    what <- match.arg(what)
    howmany <- match.arg(howmany)
    xx <- match.arg(xx)
    i <- extract(psz("ve",xx,"di"),what,paste("ORDER BY ",what,ifelse(howmany=="one"," DESC LIMIT 1","")),channel=channel)
    if(length(i)>0) 
        res <- switch(what,
                "ixx"=as.numeric(i),
                "datestamp"=as.character(i)
                ) else 
        res <- NA
    res
}
edtvedi <-
function(
            xx=allvexx(),
            ixx=getive(xx,channel=channel),
            mycomment=NULL,
            status=NULL,
            channel=chve()
            ) 
{
    xx <- match.arg(xx)
    vexxdi <- psz("ve",xx,"di")
    vexx <- psz("ve",xx)
    stopifnot( (ixx %in% extract(vexxdi,"ixx",channel=channel)) && is.numeric(ixx))
    stopifnot(!is.null(mycomment) || !is.null(status))
    stopifnot(vexx%in%dirtab(channel=channel) && vexxdi%in%dirtab(channel=channel))
    stopifnot(ixx %in% extract(vexxdi,"ixx",channel=channel))
    if(!is.null(mycomment)) {
        mysqlQuery(psz("
        UPDATE ",vexxdi,"
        SET comment = '",mycomment,"'
        WHERE ixx = ",ixx
        ),channel=channel)
    }
    if(!is.null(status)) {
        mysqlQuery(psz("
        UPDATE ",vexxdi,"
        SET status = '",status,"'
        WHERE ixx IN (",fldlst(ixx),")"
        ),channel=channel)
    }
}
dstoixx <-
function(
            xx=allvexx(),
            ds=getive(xx,howmany="one",what="datestamp",channel=channel),
            channel=chve()
            ) 
{
    xx <- match.arg(xx)
    as.numeric(extract(psz("ve",xx,"di"),"ixx",paste("WHERE datestamp IN (",flds(ds),")"),channel=channel))
}
delve <-
function(
            xx=allvexx(),
            ixx=getive(xx,channel=channel),     #tolerates ixx not in archive
            directoryalso=TRUE,
            channel=chve(),
            protectlastentry=FALSE   #prevents deletion of max(ixx) to avoid re-use; used in arcvv
            )
{
    xx <- match.arg(xx)
    vexxdi <- psz("ve",xx,"di")
    stopifnot( any(ixx %in% getive(xx,howmany="all",channel=channel)) && is.numeric(ixx))
    stopifnot(psz("ve",xx)%in%dirtab(channel=channel))
    ixx <- ixx[which(ixx %in% getive(xx,howmany="all",channel=channel))]
    if(protectlastentry) {ixx <- setdiff(ixx,getive(xx,howmany="one",channel=channel))}
    if(xx=="li" && ixx==1) stop("library 1 is reserved and cannot be deleted")
    for(i in seq_along(ixx)) {
        datestamp <- ixxtods(xx=xx,ixx=ixx[i],channel=channel)
        if(xx=="jo") {  #delete dependent outputs and check for orphaned inputs
            if(!is.na(jolu(xx="ra",ijo=ixx[i]))) delve("ra",ixx=jolu(xx="ra",ijo=ixx[i]),channel=channel)
            if(!is.na(jolu(xx="ma",ijo=ixx[i]))) delve("ma",ixx=jolu(xx="ma",ijo=ixx[i]),channel=channel)
            if(!is.na(jolu(xx="ga",ijo=ixx[i]))) delve("ga",ixx=jolu(xx="ga",ijo=ixx[i]),channel=channel)
            for(ii in inpxx()) {
                mysqlQuery(psz("
                    UPDATE ve",ii,"di
                    SET status='free'
                    WHERE ixx NOT IN (SELECT i",ii," FROM vejo)
                    "),channel=channel)
                }
            }
        mysqlQuery(psz("
            DELETE FROM ve",xx," WHERE datestamp = '",datestamp,"'"
            ),channel=channel)
        if(directoryalso) {
            mysqlQuery(psz("
                DELETE FROM ve",xx,"di WHERE ixx = ",ixx[i]
                ),channel=channel)
        }
    }
}
commentupdate <-
function(mycomment="abc-update-0",channel=chve())
{
    mystring <- ifelse(0<length(grep(mycomment,patt="-update-")),"-update-"," update=")
    spl <- strsplit(mycomment,mystring)[[1]]
    if(1<length(spl) && is.numeric(as.numeric(spl[2])+1)) {
        spl <- paste(spl[1],mystring,as.character(as.numeric(spl[2])+1),sep="")
    } else {
        spl <- paste(spl[1]," update=0",sep="")
    }
    spl
}
cleanve <-
function(
            xx=c(outxx(),intxx()),
            keepme=TRUE,
            keepixx=NULL, 
            channel=chve(),
            protectlastentry=TRUE,
            doopt=FALSE
            )
{

    stopifnot(all(xx%in%c(outxx(),intxx(),inpxx())))
    #if("ga"%in%xx) idxga("drop")
    for(i in seq_along(xx)) {
        vexxdi <- psz("ve",xx[i],"di")
        ixx <- psz("DISTINCT i",xx[i])
        del <- setdiff(setdiff(extract(vexxdi,"DISTINCT ixx",channel=channel),extract("jo",ixx)),keepixx)
        if(keepme && xx%in%c("ga","ti")) {
            meixx <- as.character(extract(psz("ve",xx,"di"),"ixx","WHERE comment LIKE 'me %'",channel=channel))
            del <- setdiff(del,meixx)
        }
        if(length(del)>0) {
            psz("deleting ",xx[i]," ",psz(del,collapse=","))
            delve(xx=xx[i],ixx=del,protectlastentry=protectlastentry,channel=channel)
        }
        if(doopt) {mysqlQuery(psz("OPTIMIZE TABLE ve",xx[i]))}
    }
    #if("ga"%in%xx) idxga("add")
}
chve <-
function() {bloomberg}
arcve <-
function(
                xx=allvexx(),
                mycomment="",
                status="free",
                ixx=NA,           #non-NA means 'delete and re-archive'
                checkdates=FALSE, #if dates match, don't delete and rearchive
                allowempty=FALSE, #for placeholder entries
                chkxx=TRUE,       #chkxx at end
                channel=chve(),   #archive channel
                channel2=DBcon    #work channel
                ) 
{
    xx <- match.arg(xx)
    vexxdi <- psz("ve",xx,"di")
    if(!(xx%in%dirtab(channel=channel2) && psz("ve",xx)%in%dirtab(channel=channel))) return(NA)
    if(!allowempty && (sqlnrow(xx)==0)) return(NA)
    stopifnot(is.character(mycomment) && length(mycomment)==1)
    stopifnot(is.character(status) && length(status)==1)
    if(length(ixx)==0 || is.na(ixx)) {
        ixx <- updvedi(mycomment=mycomment,xx=xx,status=status,channel=channel)
    } else {
        stopifnot(ixx%in%getive(xx,how="all",channel=channel))
        if(checkdates) {
            vedates <- extract(
                            psz("ve",xx),
                            "distinct date",
                            psz("where datestamp='",ixxtods(xx=xx,ixx=ixx,channel=channel),"' ORDER BY date ASC"),
                            channel=channel
                        )
            tabdates <- extract(
                            xx,
                            "distinct date",
                            "ORDER BY date ASC",
                            channel=channel2
                        )
            mytest <- length(vedates)==length(tabdates) && identical(vedates,tabdates)
            print(psz("all dates equal: ",mytest))
            if(mytest && xx=="ce") {
                vepoco <- extract(
                            "vece",#psz("ve",xx),
                            "date, bui, poco",
                            psz("where datestamp='",ixxtods(xx=xx,ixx=ixx),"' ORDER BY date, bui DESC LIMIT 1000"),
                            channel=channel
                            )
                tabpoco <- extract(
                            "ce",#xx,
                            "date, bui, poco",
                            "ORDER BY date, bui DESC LIMIT 1000",
                            channel=channel2
                            )
                mytest <- identical(vepoco, tabpoco)
            }
            if(mytest) {return(ixx)}
        }
        mycomment <- commentupdate(extract(psz("ve",xx,"di"),"comment",psz("WHERE ixx=",ixx),channel=channel))
        edtvedi(xx=xx,ixx=ixx,mycomment=mycomment,status=status,channel=channel)
    }
    dstarget <- ixxtods(xx=xx,ixx=ixx,channel=channel)
    fields <- vefields(xx,channel=channel)
    mysqlQuery(psz("DELETE FROM ve",xx," WHERE datestamp='",dstarget,"'"),channel=channel)
    #necessary rule violation: here name the database using nameve
    mysqlQuery(psz("        
        INSERT INTO ",nameve(channel),".ve",xx," (datestamp, ",fields,")
            SELECT ","'",dstarget,"', ",fields,"
            FROM ",xx
        ),channel=channel2)
    if(is.na(ixx)) stopifnot(extract(psz("ve",xx),"datestamp","ORDER BY datestamp DESC LIMIT 1",channel=channel)==dstarget)
    if(chkxx && xx %in% dirtab() && sqlnrow(xx)>0) stopifnot(do.call(what=get(psz("chk",xx)),args=list()))
    return(ixx)
}
sisummarystring <-
function(limit=getsione("sisummary"))
{
    if(sqlnrow("si")==0 || is.logical(limit)) return(NULL)
    parval <- mysqlQuery(psz("
                            SELECT DISTINCT parameter, value
                            FROM si
                            WHERE ",fldlst(allxx(),pre="parameter LIKE 'li",post="%'",collapse=" OR "),"
                            LIMIT ",limit
                        ))
    paste(apply(as.matrix(parval),1,paste,collapse="="),collapse=",")
}
sisum <-
function(
                isi=getive("si",how="all"),
                pars=c("form","lodirtyp","poshorttyp","invert","jcon","lawi","lapo","lace","mycomment","run","lipocefield")
                )
{
    myquery <- psz("SELECT ixx, mytable, parameter, ipa, value FROM vesivi WHERE ixx IN (",fldlst(isi),") AND parameter LIKE (",fldlst(pars,"'","'"),")ORDER BY ixx ASC")
    tab1 <- mysqlQuery(myquery)
    tab <- cbind(tab1[,"ixx"],apply(as.matrix(tab1[,c("mytable","parameter","ipa")]),1,paste,collapse="."),tab1[,"value"])
    t(data.frame(tabtomat(tab)))
}
permsi <-
function(x=list(
#                        list(mytable='po',parameter='lipotauma',ipa=1,value=c(8,10,12,14,16,18,20,24,28,32,36,44,52,60,70,80),mode='numeric'),
#                        list(mytable='po',parameter='lipolatest',ipa=1,value=c("l"),mode='character'),
#                        list(mytable='po',parameter='lipotrend',ipa=1,value=c(-1),mode='numeric'),
#                        list(mytable='po',parameter='lipojfacmax',ipa=1,value=c(20),mode='numeric'),
#                        list(mytable='po',parameter='lipotype',ipa=1,value=c("fmp"),mode='character')
#                        list(mytable='po',parameter='mosdv',ipa=1,value=c(FALSE,TRUE),mode='logical'),
#                        list(mytable='po',parameter='form',ipa=1,value=c("linear","asis"),mode='character'),
#                        list(mytable='po',parameter='liporetpos',ipa=1,value=c("pos","ret"),mode='character')
#                        list(mytable='po',parameter='lipojfacmax',ipa=1,value=c(10,20),mode='numeric'),
#                        list(mytable='po',parameter='liponzero',ipa=1,value=c(4,0),mode='numeric'),
#                        list(mytable='po',parameter='liponapex',ipa=1,value=c(4,0),mode='numeric')
#                        list(mytable='po',parameter='lipojfacmax',ipa=1,value=c(2,4,8,16),mode='numeric'),
#                        list(mytable='po',parameter='lipotauma',ipa=1,value=c(52,104),mode='numeric'),
#                        list(mytable='po',parameter='lipobb',ipa=1,value=c(2,4),mode='numeric')
#                        list(mytable='po',parameter='lipotype',ipa=1,value=c('hpl','fmp'),mode='character'),
#                        list(mytable='po',parameter='liponorm',ipa=1,value=c(T,F),mode='logical'),
#                        list(mytable='po',parameter='liporetpos',ipa=1,value=c('ret','pos'),mode='character'),
#                        list(mytable='po',parameter='lipotype',ipa=1,value=c('hpl','fmp'),mode='character'),
#                        list(mytable='po',parameter='lodirtyp',ipa=1,value=c("loadings1","betaevp","betamcp","betamvp"),mode='character'),
#                        list(mytable='po',parameter='method',ipa=1,value=c("unit","neutral"),mode='character'),
#                        list(mytable='po',parameter='poshorttyp',ipa=1,value=c("evp","mcp","fmp1"),mode='character')#,
#                        list(mytable='po',parameter='invert',ipa=1,value=c(TRUE,FALSE),mode='logical'),
#                        list(mytable='po',parameter='jcon',ipa=1,value=c(0,1,2),mode='numeric'),
#                        list(mytable='po',parameter='pomaxmult',ipa=1,value=c(1,1000),mode='numeric')
#                        list(mytable='po',parameter='lodirtyp',ipa=1,value=c('betamcp','loadings1'),mode='character'),
#                        list(mytable='po',parameter='jcon',ipa=1,value=c(0,1),mode='numeric'),
#                        list(mytable='po',parameter='form',ipa=1,value='asis',mode='character'),
#                        list(mytable='po',parameter='invert',ipa=1,value=TRUE,mode='logical'),
#                        list(mytable='po',parameter='lipocefield',ipa=1,value='loadings',mode='character'),
#                        list(mytable='po',parameter='lipomod',ipa=1,value=c(TRUE,FALSE),mode='logical'),
#                        list(mytable='po',parameter='lipok1',ipa=1,value=1,mode='numeric'),
#                        list(mytable='po',parameter='lipok2',ipa=1,value=c(1,2,3,4,6,8,10,15,20),mode='numeric')
#                        list(mytable='po',parameter='poshorttyp',ipa=1,value=c("evp","mcp","fmp1"),mode='character'),
#                        list(mytable='po',parameter='jcon',ipa=1,value=c(-1,1),mode='numeric'),
#                        list(mytable='po',parameter='lodirtyp',ipa=1,value=c('betamvp','betamcp','loadings1'),mode='character'),
#                        list(mytable='po',parameter='lipocefield',ipa=1,value=c("loadings1","betamcp"),mode='character')
#                        list(mytable='te',parameter='texy',ipa=1,value=c("btimax"),mode='character')
#                        list(mytable='po',parameter='lipoyield',ipa=1,value=c("bopr","dipr","capr","dipr"),mode='character'),
#                        list(mytable='po',parameter='lipoimpute',ipa=1,value=c(TRUE,FALSE),mode='logical')
                        list(mytable='co',parameter='alphamin',ipa=1,value=c(0.85,0.75),mode='numeric'),
                        list(mytable='pa',parameter='hedge',ipa=1,value=c(FALSE,TRUE),mode='logical'),
                        list(mytable='po',parameter='invert',ipa=1,value=c(TRUE,FALSE),mode='logical'),
                        list(mytable='po',parameter='jcon',ipa=1,value=c(0,1),mode='numeric'),
                        list(mytable='po',parameter='te',ipa=1,value=c("industry_subgroup","icb_subsector_name"),mode='character')
                        ),
                    towhat="co",
                    howmany=c("once","never","asis")   #how many times the steps are run prior to (not incl) towhat
                    )
{
    howmany <- match.arg(howmany)
    if(howmany=="never") { fromtab(towhat) }
    dflst <- vector("list",length(x))
    for(i in seq_along(x))  { dflst[[i]] <- cbind(x[[i]]$mytable,x[[i]]$parameter,x[[i]]$ipa,x[[i]]$value,x[[i]]$mode) }
    ij <- do.call(what="expand.grid",lapply(lapply(lapply(x,expand.grid),nrow),seq,start=1))
    tablist <- vector("list",nrow(ij))
    for(i in 1:nrow(ij)) {
        for(j in 1:ncol(ij)) {
            tablist[[i]] <- rbind(tablist[[i]],dflst[[j]][ij[i,j],,drop=FALSE])
        }
        colnames(tablist[[i]]) <- names(x[[j]])
    }
    for(i in seq_along(tablist)) {
        for(j in seq_along(x)) {
            query <- psz("DELETE FROM si WHERE mytable ='",x[[j]]$mytable,"' AND parameter ='",x[[j]]$parameter,"'")
            mysqlQuery(query)
        }
        sqlSave(channel=DBcon,
            dat=data.frame(tablist[[i]]),
            tablename="si",
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
        arcve("si",mycomment=paste("permsi",paste(tablist[[i]][,"parameter"],tablist[[i]][,"value"],collapse=",")))
        for(j in seq_along(x)) {
            query <- psz("DELETE FROM si WHERE mytable ='",x[[j]]$mytable,"' AND parameter ='",x[[j]]$parameter,"'")
            mysqlQuery(query)
        }
        if(howmany=="once" && i==1) { fromtab(towhat) }
    }
}
getsirun <-
function() 
{
    unique(as.character(as.matrix(mysqlQuery("
                SELECT si.mytable 
                FROM si INNER JOIN sise 
                ON si.mytable=sise.mytable
                WHERE si.parameter='run' AND si.value IN ('TRUE', 'T')
                ORDER BY runseq
                "))))
}
getsione <-
function(parameter,tab=NULL,default=TRUE) 
{
    if(!is.null(tab)) qual <- psz("mytable = '",tab,"'") else qual <- NULL
    fullqual <- paste(collapse=" AND ",c(psz("WHERE parameter = '",parameter,"'"),qual))
    x <- extract("si","value",fullqual)
    if(length(x)!=1) return(default)
    xmode <- extract("si","mode",fullqual)
    as(x,xmode)
}
getsi <-
function(mytable=allxx(),run=FALSE) 
{
    mytable <- match.arg(mytable) 
    tab <- mysqlQuery(psz("SELECT parameter, ipa, value, mode FROM si WHERE mytable = '",mytable,"' ORDER BY parameter"))
    pars <- unique(tab[,"parameter"])
    if(!run) pars <- setdiff(pars,"run")
    res <- unclass(data.frame(matrix(NA,0,length(pars),dimnames=list(NULL,pars))))
    attr(res,"row.names")<-NULL
    for(ipar in pars) {
        if(tab[tab[,"parameter"]==ipar,"mode"][1]=="function") {
            res[[ipar]] <- do.call(tab[tab[,"parameter"]==ipar,"value"],list())
        } else {
            res[[ipar]] <- tab[tab[,"parameter"]==ipar,"value"]
            mode(res[[ipar]]) <- tab[tab[,"parameter"]==ipar,"mode"][1]
            res[[ipar]] <- sort(res[[ipar]])
        }
    }
    res
}
fromtab <-
function(
                co="co",
                postpo=NA, #allowed: NA,"no","ga","ti" flags which to run after po; NA means leave unchd, no means none
                where=c("si","vesivi"),
                ixx=NA
                )
{
    where <- match.arg(where)
    stopifnot(all(is.na(postpo)) || all(postpo%in%c("no","ga","ti")))
    stopifnot(length(co)==1 && co%in%allxx())
    stopifnot(where=="si" || (!any(is.na(ixx)) && where=="vesivi" && all(ixx%in%getive(xx="si",how="all"))))
    if(!is.na(ixx)) {qual <- psz(" AND ixx IN (",fldlst(ixx),")")} else {qual <- ""}
    priortoco <- allxx()[1:(match(co,allxx())-1)]
    mysqlQuery(psz("UPDATE ",where," SET value = 'FALSE' WHERE parameter = 'run' AND mytable IN (",fldlst(priortoco,"'","'"),")",qual))
    mysqlQuery(psz("UPDATE ",where," SET value = 'FALSE' WHERE parameter = 'run' AND mytable IN ('ro','xy','ma')",qual))
    if(!all(is.na(postpo))) {
        xxpostpo <- allxx()[(match("po",allxx())+1):length(allxx())]
        mysqlQuery(psz("UPDATE ",where," SET value = 'FALSE' WHERE parameter = 'run' AND mytable IN (",fldlst(xxpostpo,"'","'"),")",qual))
        gaxx <- c("pe","va","vi","ga")
        tixx <- c("pe","va","vi","ti")
        if("ga"%in%postpo)mysqlQuery(psz("UPDATE ",where," SET value = 'TRUE' WHERE parameter = 'run' AND mytable IN (",fldlst(gaxx,"'","'"),")",qual))
        if("ti"%in%postpo)mysqlQuery(psz("UPDATE ",where," SET value = 'TRUE' WHERE parameter = 'run' AND mytable IN (",fldlst(tixx,"'","'"),")",qual))
    }
}
decsi <-
function() 
{
    structure(
        c(
        "mytable", "parameter", "ipa", "value", "mode", 
        "CHAR(2)", "CHAR(20)", "SMALLINT", "CHAR(100)", "CHAR(10)", 
        "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL"
        ), 
        .Dim = c(5L, 3L), 
        .Dimnames = list(NULL, c("xfield", "xtype", "xnull"))
        )
}
addsi <-
function(path=psz(Aaroot,"par/current/")) 
{
    fnam <- dir(path)
    for(i in seq_along(fnam)) {
        mysqlQuery(psz(" LOAD DATA LOCAL INFILE '",path,fnam[i],"' #table saved as csv with no text enclosures and 1 header line
            INTO TABLE si
            FIELDS
                TERMINATED BY ','
                IGNORE 1 LINES
            "))
    }
}
putlo <-
function(tab="fi",event=c("end","start")) 
{
    stopifnot(tab %in% allxx())
    event <- match.arg(event)
    if(existsgu())  { i <- which(allxx()==tab) }
    if(event=="start") {
            mysqlQuery(psz("
                UPDATE lo SET mystart= '",format(Sys.time()),"'
                WHERE mytable = '",tab,"'"))
            if(existsgu()) {
                gnb$run$current$grucu3[i,4] <- format(Sys.time())
            }
    }
    if(event=="end") {
            x<-mysqlQuery(psz("SHOW TABLE STATUS LIKE '",tab,"'"))
            size <- as.numeric(x[,"Avg_row_length"])*as.numeric(x[,"Rows"])/2^20
            if(length(size)==0) size <- 0
            mysqlQuery(psz("
                UPDATE lo SET myend= '",format(Sys.time()),"', elapsed = timediff(myend,mystart), mysize='",size,"'
                WHERE mytable = '",tab,"'"))
            if(existsgu()) {
                #svalue(grucu2) <- allxx() %in% union(svalue(grucu2),tab) #check the done box
                gnb$run$current$grucu3[i,5] <- format(Sys.time())
            }
    }
}
melo <-
function(ime=aame(),categ="mytable",channel=chve(),summtable=TRUE,channel2=DBcon)
{
    resve(xx="me",ixx=ime)
    tab <- mysqlQuery(psz("
                SELECT ",ifelse(summtable,"method, elapsed, mytable, ijo","*"),"
                FROM velovi INNER JOIN 
                    (SELECT jo.ijo, ilo, method
                    FROM jo INNER JOIN ",nameve(channel2),".me
                    ON jo.ijo=me.ijo) AS a
                ON a.ilo=velovi.ixx
                WHERE elapsed IS NOT NULL
                "),channel=channel)
    tab[,"elapsed"] <- as.difftime(tab[,"elapsed"],units="hours")
    if(summtable) {
        grp <- unique(tab[,categ])
        res <- data.frame()
        for(i in seq_along(grp)) {
            res <- rbind(res,data.frame(grp[i],sum(tab[tab[,categ]==grp[i],"elapsed"])))
        }
        colnames(res) <- c("x","elapsed")
        if(categ=="mytable") {i <- order(res[,"elapsed"]) } else {i <- order(res[,"x"]) }
        res <- rbind(res[i,],data.frame(x="TOTAL",elapsed=sum(res[,"elapsed"])))
        colnames(res)[1] <- categ
        return(res)
    } else {
        return(tab)
    }
}
declo <-
function() 
{
    structure(
                c(
                "mytable", "mystart", "myend", "elapsed", "mysize", 
                "CHAR(2)", "DATETIME", "DATETIME", "TIME", "INTEGER", 
                "NULL", "NULL", "NULL", "NULL", "NULL"
                ), 
                .Dim = c(5L, 3L), 
                .Dimnames = list(NULL, c("xfield", "xtype", "xnull"))
            )
}
sulisum <-
function(xx=c("li","su","comp"),qual=ifelse(xx%in%c("su","comp"),"WHERE method='neutral'",""),keepmat=FALSE)
{
    xx <- match.arg(xx)
    ixx <- psz("i",xx)
    droptab("tmp")
    mysqlQuery("CREATE TABLE tmp AS SELECT value2 AS date, 1 AS ili, 1 AS isu, value AS pe, '12345678910' AS comp FROM vega LIMIT 1")
    mysqlQuery("DELETE FROM tmp")
    ili <- as.numeric(extract("me1","DISTINCT ili",qual))
    isu <- as.numeric(extract("me1","DISTINCT isu",qual))
    for(jli in seq_along(ili)) {
        for(jsu in seq_along(isu)) {
            ds <- ixxtods(xx="ga",ixx=jolu(xx="ga",ijo=as.numeric(extract("me1","ijo1",psz("WHERE ili=",ili[jli]," AND isu='",isu[jsu],"'")))))
            mysqlQuery(psz("
                INSERT INTO tmp (date, ili, isu, pe, comp)
                    SELECT value2 AS date, ",ili[jli]," AS ili, ",isu[jsu]," AS isu, value AS pe, value1 AS comp
                    FROM vega
                    WHERE datestamp='",ds,"' AND domain='pe' AND type='SUM' AND field2='date' AND field1='comp'
            "))
        }
    }
    if(xx %in% c("li","su")) {
        mat <- tabtomat(mysqlQuery(psz("
                    SELECT date, ",ixx,", AVG(pe)
                    FROM tmp
                    WHERE comp='tot'
                    GROUP BY date, ",ixx,"
                ")))
        mat <- cbind(mat,apply(mat[,colnames(mat)!=1],1,mean,na.rm=TRUE))
        lab <- extract(psz("ve",xx,"di"),"ixx, comment")
        ii <- match(as.numeric(colnames(mat)),as.numeric(lab[,"ixx"]))
        nam <- lab[ii,"comment"]
        nam[is.na(nam)] <- "all"
    } else {
        mat <- tabtomat(mysqlQuery(psz("
                    SELECT date, comp, AVG(pe)
                    FROM tmp
                    WHERE comp != 'totx'
                    GROUP BY date, comp
                ")))
        nam <- colnames(mat)
    }
    res <- rbind(
                cor(mat,use="c"),
                apply(mat,2,mean,na.rm=TRUE),
                sqrt(apply(mat,2,var,na.rm=TRUE)),
                apply(mat,2,mean,na.rm=TRUE)/sqrt(apply(mat,2,var,na.rm=TRUE))
                )
    extranam <- c("mean","vol","Sharpe")
    if(xx%in%c("li","comp")) {
        i <- which(apply(!is.na(mat),1,all))
        mm <- apply(mat[i,],2,scale,center=TRUE,scale=FALSE)
        res <- rbind(res,apply(mm*mm[,1],2,sum)/crossprod(mm[,1]))
        extranam <- c(extranam,"beta")
    }
    if(keepmat) sulisummat <<- mat[apply(!is.na(mat),1,all),]
    dimnames(res) <- list(c(nam,extranam),nam)
    res
}
peerti <-
function() {"MCANQIS"}
idxgad <-
function()
{
    addidx("gad",c("date","bui"))
    addidx("gad","date")
    addidx("gad","bui")
    addidx("gad","dateminus1")
    addidx("gad","datew")
    addidx("gad","date")
    addidx("gad","xo")
}
getti6 <-
function(
                peer=peerti(),
                t1=NULL,
                dogrid=getsione(parameter="grid",tab="gu",default=TRUE),
                simisoptimal=FALSE,
                ...)
{
    pct=ifelse(getsione("percent",def=TRUE),100,1)
    pw <- as.numeric(getti3(peer=peer,t1=t1)["optimalallocation"])
    x <- corser(peer=peer,t1=t1)
    xx <- cbind(x[,peer], x[,2]*pw + x[,peer]*(1-pw))
    xcum <- pct*cumsum(xx)
    ti3 <- getti3()
    if(simisoptimal) {
        colnames(xcum) <- c(peer,"sim")
        plotne(xcum,ylab="cumulated premium",cum=FALSE,colstart=c("purple","orange"),label=FALSE,legend=TRUE,point=TRUE,...)
    } else {
        colnames(xcum) <- c(peer,"ex-post optimal mix")
        plotne(xcum,ylab="cumulated premium",cum=FALSE,colstart=c("purple","brown"),label=FALSE,legend=TRUE,point=TRUE,...)
    }
    if(dogrid) {grid(col='darkgray')}
}
getti5 <-
function(
                ti1=getti1(freq=freq),
                freq='month',
                main=NULL,
                percent=TRUE,    #done this way because not considered to depend on gui settings
                pfac=100,
                breaks=10)
{
    pfac <- ifelse(percent,100,1)
    hist(ti1[,"fundret"]*pfac,xlab=psz(freq," return"),main=main,breaks=breaks,col="lightblue")
}
getti4 <-
function(
                ti1=getti1(...),
                expandline=(max(y)-min(y))*.1,
                percent=TRUE,    #done this way because not considered to depend on gui settings
                expandbar=((max(x)-min(x))*.3),main=NULL,
                ppt=getsione(parameter="ppt",tab="gu",default=FALSE),
                dogrid=getsione(parameter="grid",tab="gu",default=FALSE),
                ...
                )
{
    pfac <- ifelse(percent,100,1)
    x <- zoo(ti1[,"fundret",drop=FALSE],as.Date(ti1[,"date"]))
    y <- exp(cumsum(log(1+x)))*pfac
    par(mar=c(5,4,4,4)+.1,new=FALSE)
    pars <- list(height=x*pfac,yaxt="n",xaxt="n",border=NA,ylim=c(-.05-expandbar,.15+expandbar)*pfac)
    if(ppt) { pars <- c(pars,list(col="orange",density=30)) } else {pars <- c(pars,list(col="lightblue"))}
    do.call(what="barplot",args=pars)
    #barplot(x*pfac,yaxt="n",xaxt="n",col="orange",density=30,  border=NA,ylim=c(-.05-expandbar,.15+expandbar)*pfac)
    #barplot(x*pfac,yaxt="n",xaxt="n",col="lightblue",          border=NA,ylim=c(-.05-expandbar,.15+expandbar)*pfac)
    axis(4,ylab="month return %")
    par(new=TRUE)
    plot(y,ylab="NAV",xlab=NA,lwd=2,col=gray(.5),ylim=c(min(y)-expandline,max(y)+expandline),main=main)
    mtext("1 month return",4)
    if(dogrid)  {grid(col='darkgray',nx=8,ny=NULL)}
}
getti3 <-
function(
            digits=2,
            percent=TRUE,    #done this way because not considered to depend on gui settings
            stress1="2007-08-03",
            stress2="2007-08-09",
            peer=peerti(),
            freq=c("m","w","d"),
            ...)
{
    freq <- match.arg(freq)
    annfac <- switch(freq,m=12,w=52,d=260)
    ti1freq <- getti1(freq=freq)
    derti2(freq=freq)   #perfee should be passed but will have no impact due to HWM
    drawti <- drawdownti()
    ti1dly <- getti1(freq="day")
    ti1wkly <- getti1(freq="w")
    prembench <- extract("ga","value2, value","where type='MKT'")
    prem <- ti1freq[,"fundret",drop=FALSE]-ti1freq[,"cashacc",drop=FALSE]
    ret <- ti1freq[,"fundret",drop=FALSE]
    premw <- ti1wkly[,"fundret",drop=FALSE]-ti1wkly[,"cashacc",drop=FALSE]
    rownames(prem) <- ti1freq[,"date"]
    rownames(ret) <- ti1freq[,"date"]
    rownames(premw) <- ti1wkly[,"date"]
    logret <- log(1+ret)
    d1 <- as.character(prembench[,"value2"])
    d2 <- as.character(ti1wkly[,"date"])
    wda <- intersect(d1,d2)
    xy <- cbind(prembench[d1%in%wda,"value"],premw[d2%in%wda,"fundret"])
    mode(xy) <- "numeric"
    reg <- summary(lm(xy[,2] ~ xy[,1]))
    pfac <- ifelse(percent,100,1)
    cash <- ti1freq[,"cashacc"]
    xg <- ti1freq[,"xg"]
    monthlyreg <- corti(peer=peer,...)
    q5weekly <- quantile(premw[,"fundret"],.05)
    q5freq <- quantile(ret[,"fundret"],.05)
    worstper <- min(ret)
    worstperperiod <- as.Date(rownames(ret[ret==min(ret),,drop=FALSE]))
    bestper <- max(ret)
    bestperperiod <- as.Date(rownames(ret[ret==max(ret),,drop=FALSE]))
    stressret <- ifelse(nrow(ti1dly)<1 || !any(ti1dly[,"date"]<=stress2),
                    NA,
                    exp(sum(log(1+ti1dly[ti1dly[,"date"]>=stress1 & ti1dly[,"date"]<=stress2,"fundret",drop=FALSE])))-1)
    turn <- extract("ti3","AVG(xt)","WHERE freq='day'")[1,1]*260
    posfrac <- mean(0<ret)
    dfr <- data.frame(t(c(
        retcum=(exp(sum(logret))-1)*pfac,
        retann=(exp(mean(logret,na.rm=T)*annfac)-1)*pfac,
        premann=(exp(mean(log(1+prem),na.rm=T)*annfac)-1)*pfac,
        volann=(exp(sqrt(var(logret)*annfac))-1)*pfac,
        wklybeta=reg$coefficients[2,1],
        wklycorr=reg$r.squared**.5,
        sharpe=mean(prem)/sqrt(var(prem))[1,1]*(annfac**.5),
        sharpeg=(exp(mean(log(1+prem),na.rm=T)*annfac)-1) / (exp(sqrt(var(log(1+prem))*annfac))-1),
        sortino=SortinoRatio(prem*pfac)[1,1]*annfac**.5,
        cashann=mean(cash)*annfac*pfac,
        activeann=(exp(mean(logret,na.rm=T)*annfac)-1)*pfac-mean(cash)*annfac*pfac,
        gross=mean(xg)*pfac,
        maxdrawdown=drawti$maxdrawdown*pfac,
        maxdrawdowndate=drawti$maxdrawdowndate,
        maxdrawdownlength=drawti$daysdrawdown,
        maxdrawdownrecovery=drawti$daysrecovery,
        corhedgnav=monthlyreg$cor[1,"HEDGNAV"],
        corhedgneut=monthlyreg$cor[1,"HEDGNEUT"],
        corspx=monthlyreg$cor[1,"SPTR"],
        corpeer=monthlyreg$cor[1,peer],
        q5weekly=q5weekly*pfac,
        q5freq=q5freq*pfac,
        worstper=worstper*pfac,
        worstperperiod=worstperperiod,
        bestper=bestper*pfac,
        bestperperiod=bestperperiod,
        stressret=stressret*pfac,
        turn=turn*pfac,
        posfrac=posfrac,
        #annjensena=monthlyreg$reg3$coefficients[1,1]*12*pfac,
        #jensenb=monthlyreg$reg3$coefficients[2,1],
        annjensena=reg$coefficients[1,1]*52*pfac,
        jensenb=reg$coefficients[2,1],
        optimalallocation=as.numeric(rev(monthlyreg$efron[1,])[1]),
        mincordate=monthlyreg$d1,
        maxcordate=monthlyreg$d2
        )))
    datefields <- c("maxdrawdowndate","worstperperiod","bestperperiod")
    for(i in seq_along(datefields)) {dfr[datefields[i]] <- as.Date(unlist(dfr[datefields]))[i]}
    dfr
}
getti2 <-
function(
            domain=c("fundret","fundcum","xg","xn","xt"),
            addcol=TRUE,
            digits=2,
            percent=TRUE    #done this way because not considered to depend on gui settings
            )
{
    domain <- match.arg(domain)
    res <- getti1(freq="month")[,c("year","month",domain)]#getti1(domain=domain,...)
    tab <- tabtomat(res)
    tab <- tab[,order(as.numeric(colnames(tab))),drop=FALSE]
    if(addcol) {
        if(domain=="fundret") {
            ytd <- exp(apply(log(1+tab),1,sum,na.rm=TRUE)) -1
        } else if(domain=="xt"){
            ytd <- apply(tab,1,sum,na.rm=TRUE)
        } else {
            ytd <- apply(tab,1,mean,na.rm=TRUE)
        }
        tab <- cbind(tab,ytd)
        if(ncol(tab)==13)colnames(tab) <- c("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","YTD")
    }
    if(percent) tab <- tab*100
    round(tab,digits)
}
getti1 <-
function(
                freq=c("month","day","week","year")
                )
{
browser()
    freq <- match.arg(freq)
    res <- gettab("ti3",ret="dataframe",psz("WHERE freq='",freq,"'"))
    i <- which(apply(!is.na(res[,colnames(res)!="pev"]),1,all))
    stopifnot(all(diff(i)==1))
    rownames(res) <- res[,"date"]
    res[i,,drop=FALSE]
}
getpeerti <-
function(qual="",fundsonly=TRUE)
{
    btk <- sort(setdiff(
        as.character(extract("aamonthly","distinct btk",qual=qual)),
        c("US0001M","3LFUNDE")))
    if(fundsonly) {btk <- intersect(btk,as.character(extract("PEER3","distinct btk",qual="WHERE maintain")))}
    btk
}
gati <-
function(
            ixx=sort(getijo("all"),decr=TRUE)[1:2],
            xx="jo",
            sumtreatment=c("average","sum"),
            filt=list(parameter="method",value="neut%"),
            gearing=1,
            channel=chve(),
            channel2=DBcon
            )
{
    sumtreatment <- match.arg(sumtreatment)
    if(xx=="jo") {
        ijo <- ixx[ixx%in%getijo("all",channel=channel)]
        stopifnot(length(ijo)>=1)
        if(is.null(filt)) {
            iti <- jolu(xx="ti",ijo=ijo,channel=channel)
        } else {
            iti <- jolusi(xx="ti",ijo=ijo,filt=filt,channel=channel)
        }
    } else {
        iti <- ixx[ixx%in%getive(xx="ti",how="all",channel=channel)]
    }
    ag <- fldlst(c("xn","xg","xt","pe","pev"),pre=psz(gearing,"*",c(sum="SUM",average="AVG")[sumtreatment],"("),post=")")
    #if(sumtreatment=="sum") {ag <- "SUM(xn), SUM(xg), SUM(xt), SUM(pe), SUM(pev)"} else {ag <- "AVG(xn), AVG(xg), AVG(xt), AVG(pe), AVG(pev)"}
    stopifnot(length(iti)>0)
    itidatestamp <- ixxtods(xx="ti",ixx=iti,channel=channel)
    newti()
    mysqlQuery(psz("
        INSERT INTO ",nameve(channel2),".ti (date, year, month, datew, cashdollar, xn, xg, xt, pe, pev)
        SELECT date, year, month, datew, cashdollar, ",ag," FROM veti
        WHERE datestamp IN (",fldlst(itidatestamp,"'","'"),")
        GROUP BY date
    "),channel=channel)
}
gadquery <-
function(myfields,d1,d2,bui) {
    myquery <- psz("
            INSERT INTO gad (date, dateminus1, year, month, datew, bui, cashdollar, pr, cash, ret, turn, xo, xt, xc, pe, vat)
            SELECT aared.date, aared.dateminus1, aared.year, aared.month, aared.datew , aared.bui, aared.cash AS cashdollar, ",myfields,", aared.turn, aared.xo, aared.xt, aared.xc, aared.pe, NULL AS vat
            FROM aared INNER JOIN po
            ON aared.datew=po.date AND aared.bui=po.bui
            WHERE aared.datew<='",d2,"' AND '",d1,"'<=aared.datew AND aared.bui IN (",fldlst(bui,"'","'"),") AND po.tapo
            ")
}
fmtti3 <-
function(peer=peerti(),...)
{
    x <- getti3(peer=peer,...)
    digits <- 2
    col1 <- round(c(
        "return (g) p.a."=x$retann,         #geometric av
        "return cumulative"=x$retcum,
        "premium (g) p.a."=x$premann,       #geometric av
        "volatility p.a."=x$volann,
        "sharpe ratio (g)"=x$sharpeg,       #geometric av
        "sharpe ratio (a)"=x$sharpe.fundret,       #arithmetic av
        "sortino ratio"=x$sortino,
        "cash p.a."=x$cashann,
        "active return p.a."=x$activeann
        ),digits)
    col2 <- round(c(
        "maximum drawdown"=x$maxdrawdown,
        "maximum drawdown date"=x$maxdrawdowndate,
        "days to maximum drawdown"=x$maxdrawdownlength,
        "days recovery from max drawdown"=x$maxdrawdownrecovery,
        "5th percentile weekly"=x$q5weekly,
        "5th percentile monthly"=x$q5freq,
        "worst month"=x$worstper,
        "worst month date"=x$worstperperiod,
        "best month"=x$bestper,
        "best month date"=x$bestperperiod,
        "stress: 3-9th Aug 2007"=x$stressret,
        "average effective holding (weeks)"=2*52*x$gross/x$turn,
        "gross exposure"=x$gross,
        "fraction returns>0"=x$posfrac
        ),digits)
    col3 <- round(c(
        "jensen alpha p.a."=x$annjensena,
        "jensen beta"=x$jensenb,
        "cor. vs. CS/Tremont All"=x$corhedgnav,
        "cor. vs. CS/Tremont MN"=x$corhedgneut,
        "cor. vs. S&P500"=x$corspx,
        x$corpeer,        
        "optimal allocation to simulated"=x$optimalallocation
        ),digits)
    col3 <- c(col3,
        "start"=substr(as.character(as.Date(x$mincordate)),1,7),
        "end"=substr(as.character(as.Date(x$maxcordate)),1,7)
        )
    names(col3)[6] <- psz("cor. vs. ",peer)
    i <- 1:max(c(length(col1),length(col2),length(col3)))
    res <- cbind(names(col1)[i],col1[i],names(col2)[i],col2[i],names(col3)[i],col3[i])
    res[2,4] <- substr(as.character(as.Date(as.numeric(res[2,4]))),1,7)
    res[8,4] <- substr(as.character(as.Date(as.numeric(res[8,4]))),1,7)
    res[10,4] <- substr(as.character(as.Date(as.numeric(res[10,4]))),1,7)
    rownames(res) <- NULL
    colnames(res) <- rep(" ",ncol(res))
    res[is.na(res)] <- ""
    res
}
drawdownti <-
function()
{
    maxdrawdown <- extract("ti2","MIN(drawdown)")[1,1]
    maxdrawdowndate <- as.Date(mysqlQuery(psz("SELECT date FROM ti2 WHERE ABS(drawdown-",maxdrawdown,")<1.e-6"))[1,1])
    priordate <- as.Date(mysqlQuery(psz("
        SELECT MAX(date) 
        FROM ti2
        WHERE drawdown=0 AND date < '",maxdrawdowndate,"'
        "))[1,1])
    postdate <- as.Date(mysqlQuery(psz("
        SELECT MIN(date) 
        FROM ti2
        WHERE drawdown=0 AND date > '",maxdrawdowndate,"'
        "))[1,1])
    list(
        maxdrawdown=maxdrawdown,
        maxdrawdowndate=maxdrawdowndate,
        daysdrawdown=as.numeric(maxdrawdowndate)-as.numeric(priordate),
        daysrecovery=as.numeric(postdate)-as.numeric(maxdrawdowndate)
        )
}
corti <-
function(
            peer=peerti(),
            dw=0.05,
            maxweight=1,
            t1=NULL,
            ...)
{
    xx <- corser(peer=peer,t1=t1)
    reg5 <- summary(lm(xx[,1,drop=FALSE] ~ -1 + xx[,2,drop=FALSE] + xx[,peer,drop=FALSE]))
    npts <- 1 + floor(maxweight/dw)         #equally spaced in weight dimension
    wts <- seq(from=0,by=dw,length=npts)    #weight in fund
    wts[npts+1] <- max(reg5$coefficients[1,1],0)/max(1,max(reg5$coefficients[1,1],0)+max(reg5$coefficients[2,1],0))  #weight in fund for tangent point, ignore -ves
    efron <- matrix(NA,5,npts+1,dimn=list(c("fund wt","peer wt","vol","ret","ret/vol"),as.character(wts)))
    for(i in 1:(npts+1)) {
        po <- xx[,2]*wts[i]+xx[,peer]*(1-wts[i])
        efron[3,i] <- sqrt(var(po,na.rm=TRUE))
        efron[4,i] <- mean(po,na.rm=TRUE)
    }
    efron[1,] <- wts   
    efron[2,] <- 1-wts
    efron[5,] <- efron[4,]/efron[3,]
    list(
        reg1=summary(lm(coredata(xx[,2]) ~ coredata(xx[,"HEDGNAV"]))),
        reg2=summary(lm(coredata(xx[,2]) ~ coredata(xx[,"HEDGNEUT"]))),
        reg3=summary(lm(coredata(xx[,2]) ~ coredata(xx[,"SPTR"]))),
        reg4=summary(lm(coredata(xx[,2]) ~ coredata(xx[,peer]))),
        reg5=reg5,
        cor=cor(xx[,!colnames(xx)%in%c("US0001M","")],use="pair"),
        efron=efron,
        d1=as.Date(min(index(xx))),
        d2=as.Date(max(index(xx))),
        peer=peer
    )
}
corsum <-
function(peer=peerti(),t1=NULL,robust=TRUE,thresh=0.3,jselect=c("sim",peer,"HEDGNAV","HEDGLSEQ","HEDGNEUT","SPTR"),...)
{
    cc <- corser(peer=peer,t1=t1,thresh=thresh,robust=robust)[,jselect,drop=FALSE]
    cormat <- cor(cc,use='pair',...)
    vol <- sqrt(diag(cov(cc,use='pair',...)))
    av <- apply(cc,2,mean,na.rm=TRUE)
    list(cormat=cormat,vol=vol,mean=av)
}
corser <-
function(peer=peerti(),t1=NULL,robust=TRUE,thresh=0.3)
{
    ti1 <- getti1(freq="month")
    cash <-  zoo(ti1[,"cashacc",drop=FALSE],as.Date(ti1[,"date"]))
    mode(cash) <- "numeric"
    netprem <- zoo(ti1[,"fundret",drop=FALSE]-ti1[,"cashacc",drop=FALSE],as.Date(ti1[,"date"]))
    benchprem <- getaamonthly("prem")
    da <- as.Date(intersect(
        index(netprem[which(!is.na(netprem)),]),
        index(benchprem)[which(!is.na(apply(benchprem[,peer,drop=FALSE],1,sum)))]
        ))
    res <- zoo(cbind(rep(1,length(da)),coredata(cbind(netprem[da,],benchprem[da,]))),da)
    colnames(res)[2] <- "sim"
    if(robust) {
        res <- res[apply(abs(res[,c("HEDGNEUT","HEDGNAV","SPTR")])<thresh,1,all),]
    }
    if(!is.null(t1) && as.Date(t1)%in%index(res)) {istart <- match(as.Date(t1),index(res)) } else {istart <- min(nrow(res),1)}
    res[istart:nrow(res),,drop=FALSE]
}
sumareg <-
function(yx,
                eiv=1,              #errors in variables: numeric greater than one (1+VAR(e)/VAR(x))
                minobs=20
                ) 
{ 
    if(nrow(yx)>minobs) {
        stopifnot(all(names(eiv)==colnames(yx)[-1]) || all(eiv==1))
        if(all(eiv==1)) eiv <- rep(1,ncol(yx)-1)
        mylm <- summary(lm(yx[,1,drop=FALSE] ~ yx[,-1,drop=FALSE]))
        mylmcon <- summary(lm(yx[,1,drop=FALSE] ~ apply(yx[,c(-1,-ncol(yx)),drop=FALSE],1,sum))) #sum all components except int, qua
        cotab <- mylm$coefficients[,1:3,drop=FALSE]
        rsq <- c(mylmcon$r.squared,mylm$adj.r.squared)
        colnames(cotab) <- c("coef","se","tstat")
        comp <- colnames(yx)
        if(any(lapply(lapply(names(eiv),grep,x=rownames(mylm$coefficients)),length)==0)) stop(psz("only the following regressors were valid (categories too small?): ",fldlst(rownames(mylm$coefficients)),collapse=", "))
        res <- rbind(
                    data.frame(comp=comp,type="coadj",value=cotab[,"coef",drop=TRUE]*c(1,as.numeric(eiv))),
                    data.frame(comp=comp,type="coef",value=cotab[,"coef",drop=TRUE]),
                    data.frame(comp=comp,type="se",value=cotab[,"se",drop=TRUE]),
                    data.frame(comp=comp,type="tstat",value=cotab[,"tstat",drop=TRUE]),
                    data.frame(comp=comp,type="~rsq",value=rsq[2])
                    )
        rownames(res) <- NULL  
        res
    }
}
quanmom <-
function(x,quantiles=c(0,.05,.5,.95,1),maxmoment=4,center=FALSE,...) 
{
    res1 <- quantile(x,quantiles,...)
    names(res1) <- psz("q",floor(quantiles*100))
    res2 <- NULL
    for (i in 1:maxmoment) res2 <- c(res2,moment(x,i,central=((1<i)&&center),...))
    names(res2) <- psz("m",1:maxmoment)
    c(res1,res2)
}
ma4 <-
function(kgroup=10)
{
    mysqlQuery("DELETE FROM ma WHERE equation>6")
    dace <- getda("ce")
    for (i in seq_along(dace)) {
        tc1 <- ma3(da=dace[i],kgroup=kgroup)
        df1 <- data.frame(
            equation=6+as.numeric(substr(names(tc1$msttests),3,3)),
            lag=match(dace[i],getca()),
            te1="ALL",
            te2="ALL",
            comp=substr(names(tc1$msttests),1,1),
            type=substr(names(tc1$msttests),2,2),
            value=tc1$msttests
            )
        for(j in seq_along(tc1$xreg)) {
            df1 <- rbind(df1,data.frame(
                            equation=9+j,
                            lag=match(dace[i],getca()),
                            te1="ALL",
                            te2="ALL",
                            type=substr(names(tc1$xreg[[j]]),1,1),
                            comp=substr(names(tc1$xreg[[j]]),2,8),
                            value=tc1$xreg[[j]]
                            ))
        }
        df1[,"type"][df1[,"type"]=="f"] <- "Fstat"
        df1[,"type"][df1[,"type"]=="p"] <- "pval"
        df1[,"type"][df1[,"type"]=="c"] <- "coef"
        df1[,"type"][df1[,"type"]=="s"] <- "sdev"
        df1[,"type"][df1[,"type"]=="t"] <- "tstat"
        df1[,"type"][df1[,"type"]=="r"] <- "~rsq"
        sqlSave(channel=DBcon,      
                dat=df1,
                tablename="ma",
                append=TRUE,
                rownames=FALSE,
                safer=TRUE)
    }
}
ma3 <-
function(da=getda("no"),
                    tol=1e-6,
                    kgroup=10
                    )
{
    #                               M.DF    R.DF        
    #E1     r = b1.rtms             1       NT-1        
    #E2     rt = b2.rtms            N+1     N(T-1)-1    
    #E2a    r = a + b2a.rtms        N+1     N(T-1)-1    
    #E3     r = Lp + b3.rtms        k+1     NT-k-1      
    #E4     p = m                   1       NT-1        
    #C1     b1=b2                                       
    #C2     RSS1-RSS2=T m'm                             
    #C3     b2=b2a                                      
    #C4     b3=b2                                       
    #C5     linear.hypothesis()$F = manual calc         
    #C6     linear.hypothesis.Res.Df[1] = n*T-k-1       
    #F1     asset means = 0             (M1-M2)/(NM2-NM1)   N       R2/NR2      N(T-1)-1    
    #F2     means project onto loadings (M3-M2)/(NM2-NM3)   N-k     R2/NR2      N(T-1)-k-1  
    #F3     slope = mean                (M4-M3)/(NM3-NM4)   k       R3/NR3      N(T-1)-k-1  
    ce <- getce(da)
    prem <- getpi(field='prem',
            date=da,       
            ta='tace',
            we=getla('we')
            )[,fulce(ce),drop=FALSE]
    sco <- coredata(scoce(ce,prem))
    k <- ncol(sco)
    n <- length(fulce(ce))
    nper <- nrow(prem)
    w <- as.numeric(gettab("lawe")[,"weight"])
    scobar <- apply(sco,M=2,F=weighted.mean,w=w,na.rm=FALSE)
    rbar <- apply(prem,M=2,F=weighted.mean,w=w,na.rm=FALSE)
    rtwid <- sweep(prem,M=2,F="-",S=rbar)
    jms <- list(1,2:k)
    allres <- NULL
    for(j in c("m","s","t")) {
        if(j=="m") {
            rtms <- mktce(ce,rtwid)[,fulce(ce),drop=FALSE]
        } else if(j=="s") {
            rtms <- sysce(ce,rtwid)[,fulce(ce),drop=FALSE]
        } else {
            rtms <- msce(ce,rtwid)[,fulce(ce),drop=FALSE]
        }
        rtmstab <- as.matrix(mattotab(rtms))
        y <- as.numeric(prem)
        x <- as.numeric(rtmstab[,3])
        e1 <- lm(y ~ x -1,weight=rep(w,n))
        rss1 <- nper*n*weighted.mean(e1$residuals**2,w=rep(w,n))
    
        y <- as.numeric(rtwid)
        e2 <- lm(y ~ x -1,weight=rep(w,n))
        rss2 <- nper*n*weighted.mean(e2$residuals**2,w=rep(w,n))
        stopifnot(all.equal(as.numeric(e2$coefficients-e1$coefficients),0,tol=tol)) #c1
        stopifnot(isTRUE(all.equal((rss1-rss2),nper*as.numeric(crossprod(rbar)),tol=tol))) #c2
    
        y <- as.numeric(coredata(prem))
        dmat2 <- matrix(NA,nper*n,k+1)
        ldg <- ldgce(ce)
        for(i in 1:n) {
            i1 <- (i-1)*nper
            dmat2[(i1+1):(i1+nper),1:k] <- t(matrix(ldg[i,],k,nper))
        }
        dmat2[,k+1] <- as.numeric(rtmstab[,3])
        e3 <- lm(y ~ dmat2 -1,weight=rep(w,n))
        rss3 <- nper*n*weighted.mean(e3$residuals**2,w=rep(w,n))
        stopifnot(all.equal(as.numeric(e2$coefficients-e3$coefficients[k+1]),0,tol=tol)) #c4
        lh4 <- linear.hypothesis(e3,cbind(diag(k),rep(0,k)),scobar,weight=rep(w,n)) # weights ignored
        f4 <- ((lh4$RSS[2]-lh4$RSS[1])/k) / (lh4$RSS[1]/(lh4$Res.Df[1]))
        stopifnot(all.equal(f4,lh4$F[2],tol=tol)) #c5
        stopifnot(all.equal(n*nper-k-1,lh4[["Res.Df"]][1],tol=tol)) #c6
        f <- p <- rep(NA,3)
        nrdf2 <- n*(nper-1)-1
        f[1] <- ((rss1-rss2)/n)     / (rss2/nrdf2)  #joint: means differ from zero
        f[2] <- ((rss3-rss2)/(n-k)) / (rss2/nrdf2)  #test means restricted to k factors
        f[3] <- f4                                  #test means equal expost factor means
        p[1] <- pf(f[1],n,nrdf2)
        p[2] <- pf(f[2],n-k,nrdf2)
        p[3] <- pf(f[3],k,n*nper-k-1)
        ngroups <- floor(k/kgroup)
        res <- c(f,p)
        names(res) <- psz(j,c("f1","f2","f3","p1","p2","p3"))
        allres <- c(allres,res)
    }
    xreg <- vector("list",ngroups)
    names(xreg) <- letters[1:ngroups]
    for(i in 1:ngroups) {
        kmin <- (i-1)*kgroup+1
        kmax <- kmin+kgroup-1
        lm5 <- summary(lm(e3$coefficients[kmin:kmax] ~ scobar[kmin:kmax]))
        reg <- as.numeric(cbind(lm5$coefficients[,1:3],rep(lm5$r.squared,2)))
        names(reg) <- psz(c("ca","cb","sa","sb","ta","tb","ra","rb"),psz(kmin,"-",kmax))
        xreg[[i]] <- reg
        names(xreg)[[i]] <- psz(kmin,"-",kmax)
    }
    list(msttests=allres,xreg=xreg)
}
ma2 <-
function(
            xyfields,                                               #all fields for yx
            equation,
            minrow=20,
            donotab=TRUE                                            #flag for computation of normalised returns table (notab)
            ) 
{
    stopifnot(as.character(extract("ma1","domain","LIMIT 1"))%in%c("fa","xy"))
    if(equation%in%0:3) taqual <- " tare=1 " else taqual <- " tayi=1 " 
    te1set <- sort(union("ALL",extract("ma1","DISTINCT te1","ORDER BY te1")))
    te2set <- sort(union("ALL",extract("ma1","DISTINCT te2","ORDER BY te2")))
    lagset <- extract("ma1","DISTINCT lag","ORDER BY lag")
    if(donotab) {
        gcd <- getcedecomp()
        eiv <- c(gcd,qua=(2*gcd$rem-1),dev=1)
    } else {
        eiv <- 1  #same flag controls whether eiv correction is done
    }
    for(mylag in lagset) {
        lagqual <- psz(" lag='",mylag,"'")
        for(myte2 in te2set) {
            te2qual <- ifelse(myte2=="ALL","",psz("te2='",myte2,"'"))
            for(myte1 in te1set) {
                te1qual <- ifelse(myte1=="ALL","",psz("te1='",myte1,"'"))
                qual <- c(te1qual,te2qual,lagqual,taqual)
                whereclause <- paste(" WHERE ",paste(collapse=" AND ",qual[nchar(qual)>0]))
                yx <- extract("ma1",xyfields,qual=whereclause)
                colnames(yx)[1] <- psz("int",equation)
                if(donotab) {
                    nofields <- dirfld("ma1","^no")                         
                    yxno <- extract("ma1",nofields,qual=whereclause)        
                }
                if(nrow(yx)>minrow) {
                    tab <- cbind(equation=equation,lag=mylag,te1=myte1,te2=myte2,sumareg(yx=yx,minobs=minrow,eiv=eiv))
                    if(donotab) {
                        qm <- mattotab(apply(yxno,2,FUN=quanmom))
                        notab <- data.frame(equation=equation,lag=mylag,te1=myte1,te2=myte2,comp=gsub(patt="no",rep="",x=qm[,2]),type=qm[,1],value=qm[,3])    
                        tab <- rbind(tab,notab)                           
                    }
                    sqlSave(channel=DBcon,
                        dat=tab,    
                        tablename="ma",
                        append=TRUE,
                        rownames=FALSE,
                        safer=TRUE)
                }
            }
        }
    }
}
idxma1forma2 <-
function() {
    for(fld in c("te1","te2","lag")) { 
        tt <- system.time(addidx("ma1",fld))[3]
        if(getsione("verbose")) print(paste("ma1",fld,"indexed in",floor(tt),"s"))
    }
}
getma4r <-
function(
            comp=as.character(extract("ma","DISTINCT comp","WHERE equation in (10,11)")),
            type=as.character(extract("ma","DISTINCT type","WHERE equation in (10,11)")),
            equation=10:11
            )
{
    query <- psz("
            SELECT equation, comp, type, value 
            FROM ma
            WHERE comp IN (",fldlst(comp,"'","'"),")
            AND type IN (",fldlst(type,"'","'"),")
            AND equation IN (",fldlst(equation),")
            ")
    mysqlQuery(query)
}
getma4f <-
function(
            comp=as.character(extract("ma","DISTINCT comp","WHERE equation in (7,8,9)")),
            type=as.character(extract("ma","DISTINCT type","WHERE equation in (7,8,9)")),
            equation=7:9
            )
{
    query <- psz("
            SELECT equation, comp, type, value 
            FROM ma
            WHERE comp IN (",fldlst(comp,"'","'"),")
            AND type IN (",fldlst(type,"'","'"),")
            AND equation IN (",fldlst(equation),")
            ")
    mysqlQuery(query)
}
getma4 <-
function(
            compf=as.character(extract("ma","DISTINCT comp","WHERE equation in (7,8,9)")),
            typef=as.character(extract("ma","DISTINCT type","WHERE equation in (7,8,9)")),
            equationf=7:9,
            compr=as.character(extract("ma","DISTINCT comp","WHERE equation in (10,11)")),
            typer=as.character(extract("ma","DISTINCT type","WHERE equation in (10,11)")),
            equationr=10:11
            )
{
    rbind(
        getma4f(comp=compf,type=typef,equation=equationf),
        getma4r(comp=compr,type=typer,equation=equationr)
        )
}
getma <-
function(rows=c("te1","te2","lag","type","comp","ixx"),
                cols=c("te2","te1","lag","type","comp","ixx"),
                te1=extract("ma","DISTINCT te1","ORDER BY te1 LIMIT 1"),
                te2=extract("ma","DISTINCT te2","ORDER BY te2 LIMIT 1"),
                lag=0,
                comp=extract("ma","DISTINCT comp","WHERE equation<7 ORDER BY comp LIMIT 1"),
                type="coef",
                tab="ma",
                ijo=FALSE
                                ) 
{
    rows <- match.arg(rows)
    cols <- match.arg(cols)
    if(rows==cols) stop("rows, cols must differ")
    fields <- c("te1","te2","lag","type","comp")
    default <- fields[!fields%in%c(rows,cols)] 
    if("ixx"%in%c(rows,cols)) { tab <- psz("ve",tab,"vi") }    
    query <- psz("
        SELECT ",rows,", ",cols,", value FROM ",tab,"
        WHERE   ",default[1]," = '",get(default[1]),"' 
        AND     ",default[2]," = '",get(default[2]),"' 
        AND     ",default[3]," = '",get(default[3]),"'
        ")
    if("ixx"%in%c(rows,cols)) { 
        #query <- psz(query," AND ",default[4]," = '",get(default[4]),"'"," AND ixx IN (",fldlst(as.character(extract("jo","DISTINCT ima","WHERE ima IS NOT NULL"))),")") 
        query <- psz(query," AND ",default[4]," = '",get(default[4]),"'"," AND ixx IN (",flds(getive("ma",how="all")),")") 
        }
    mytab <- mysqlQuery(query)
    if("ixx"%in%c(rows,cols) && ijo) {
        lut <- extract("jo",c("ima","ijo"),psz("WHERE ima IN (",fldlst(mytab[,"ixx"]),")"))
        mytab[,"ixx"] <- lut[match(mytab[,"ixx"],lut[,"ima"]),"ijo"] 
    } 
    res <- tabtomat(mytab)
    mode(res) <- "numeric"
    res
}
decma <-
function() 
{
    structure(
        c("equation", "lag", "te1", "te2", "comp", "type", "value", 
        "SMALLINT", "SMALLINT", "CHAR(30)", "CHAR(30)", "CHAR(6)", "CHAR(6)", "DOUBLE", 
        "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL"
        ), 
        .Dim = c(7L, 3L), 
        .Dimnames = list(NULL, c("xfield", "xtype", "xnull"))
    )
}
whereforpassive <-
function(
                    clauses,
                    activeset
                    )
{
    passiveset <- !(names(clauses) %in% activeset) & nchar(clauses)>0 #fields which are not one of the loop variables i.e. not part of the key
    mywhere <- paste(collapse=" AND ",clauses[passiveset])
    if(nchar(mywhere)>0) whereclause <- paste("WHERE ",mywhere) else whereclause <- ""
    return(whereclause)
}
qufigaplus <-
function()
{
    c(psz(setdiff(qufiga(),"year"),"-q"),"year")
}
qufiga <-
function()
{
    res <- unlist(unique(lapply(strsplit(as.character(extract("ga","DISTINCT value2",qual="WHERE field2='te2' AND value2 LIKE '%-q%'")),"-q"),"[",1)))
    if(length(res)>0) res <- c(res,"year")
    res
}
pega <-
function(iga=getive("ga"),domain=c("pe","va"))
{
    domain <- match.arg(domain)
    res <- tabtomat(
        mysqlQuery(psz("
            SELECT a.date,  vegadi.ixx AS iga, a.value
            FROM
                (SELECT datestamp, value2 AS date, value
                FROM vega
                WHERE datestamp IN (",fldlst(ixxtods(xx="ga",ixx=iga),"'","'"),")
                AND field1='comp'
                AND value1='tot'
                AND field2='date'
                AND domain='",domain,"'
                AND type='sum') AS a
            INNER JOIN vegadi
            ON a.datestamp=vegadi.datestamp
        "))
    )
    res[,match(iga,colnames(res)),drop=FALSE]
}
getga <-
function(
            domain=c("po","pe","va","f1","ic"),
            rows=c("te1","te2","lag","lapo","comp"),
            cols=c("te2","te1","lag","lapo","comp","date","job"),
            colstart=NULL,  #if non-NA, value must start with this string
            type=c("AVG","SUM","STD","IR"),     #nb IR gets done in derga
            sums=TRUE,
            avgs=TRUE,
            iinc=as.character(extract("ga","DISTINCT value1",psz("WHERE field1='",rows,"'"))),   #to select specific rows
            variance=c('v','v/(V^.5)','v/V'), 
            percent=getsione("percent"),
            annfactor=getsione("annfactor"),
            channel=DBcon
            ) 
{
    domain <- match.arg(domain)
    rows <- match.arg(rows)
    cols <- match.arg(cols)
    type <- match.arg(type)
    variance <- match.arg(variance)
    incclause <- psz(" AND value1 IN (",fldlst(iinc,"'","'"),")")
    if(!is.null(colstart)) {colvalclause <- psz(" AND value2 LIKE '",colstart,"%'")} else {colvalclause <- " AND value2 NOT LIKE '%_q%' AND value2 NOT LIKE 'year-%'"} #this for quantiles
    if(type=="AVG" && domain!="ic") {
        rawtype <- switch(type,AVG="SUM",SUM="SUM",STD="STD",IR="IR")
        qual <- psz("WHERE field1='",rows,"' AND field2='",cols,"' AND type='",rawtype,"' AND domain='",domain,"'",incclause,colvalclause)
        myfields <- "DISTINCT value1, value2, value/nper"
    } else {
        rawtype <- switch(type,AVG="AVG",SUM="SUM",STD="STD",IR="IR")
        qual <- psz("WHERE field1='",rows,"' AND field2='",cols,"' AND type='",rawtype,"' AND domain='",domain,"'",incclause,colvalclause)
        myfields <- "DISTINCT value1, value2, value"
    }
    res <- tabtomat(texttonumdf(extract("ga",myfields,qual=qual,result=data.frame,channel=channel)))
    if(domain==c("pe") && (type %in% c("AVG")) )            {res <- res*annfactor   } 
    if(domain==c("pe") && (type %in% c("STD","IR")) )       {res <- res*annfactor^.5} 
    if(domain==c("f1") && (type %in% c("AVG","SUM")))             {res <- res*annfactor^.5} 
    if(domain%in%c("pe","f1") && (type!="IR") && percent)   {res <- res*100         }
    if(domain=="va")                                        {res <- res*annfactor   }
    if(domain=="va" && percent)                             {res <- res*10000       }
    if(domain=="va") {
        pwr <- switch(variance,'v/(V^.5)'=.5,'v/V'=1)
        vadd <- variance %in% c('v/V','v/(V^.5)') && rows%in%c("te1","te2","comp")
        hadd <- variance %in% c('v/V','v/(V^.5)') && cols%in%c("te1","te2","comp")
        i <- !rownames(res)%in%c("tot","totx")
        j <- !colnames(res)%in%c("tot","totx")
        if(vadd && !hadd) {
            vsum <- apply(res[i,,drop=FALSE],MARGIN=2,FUN=sum,na.rm=TRUE)^pwr
            res <- sweep(res,MARGIN=2,STATS=vsum,FUN="/")
        } 
        if(!vadd && hadd) {
            hsum <- apply(res[,j,drop=FALSE],MARGIN=1,FUN=sum,na.rm=TRUE)^pwr
            res <- sweep(res,MARGIN=1,STATS=hsum,FUN="/")
        } 
        if(vadd && hadd) { res <- res/(sum(res[i,j],na.rm=TRUE)^pwr) } 
        if(variance %in% c('v/V','v/(V^.5)') && !vadd && !hadd) { res <- res/(res^pwr) }
    }
    res
}
gatiga <-
function(
            ixx=sort(getijo("all"),decr=TRUE)[1:2],
            sumtreatment=c("sum","average"),
            verbose=TRUE,
            xx="jo",
            filt=list(parameter="method",value="neut%"),
            gearing=1,
            collate=FALSE
            )
{
    gaga(ixx=ixx,sumtreatment=sumtreatment,verbose=verbose,xx=xx,filt=filt,gearing=gearing,collate=collate)
    gati(ixx=ixx,sumtreatment=sumtreatment,xx=xx,filt=filt,gearing=gearing)
}
gate <-
function(ijo=as.numeric(extract("me1","DISTINCT ijo2")))
{
    iga <- jolu(ijo=ijo,xx="ga")
    newga()
    dst <- as.character(extract("vegadi","DISTINCT datestamp",psz("WHERE ixx IN (",fldlst(iga),")")))
    foundte1 <- NULL
    for(i in seq_along(iga)) {
        te1 <- extract("vega","DISTINCT value1",psz("WHERE field1='te1' AND datestamp='",dst[i],"' AND field2!='job'"))
        stopifnot(!any(te1%in%foundte1))
        foundte1 <- c(foundte1,te1)
    }
    myquery <- psz("
        INSERT INTO ga (domain, field1, field2, value1, value2, type, value, nper)
        SELECT domain, field1, field2, value1, value2, type, value, nper 
        FROM vega
        WHERE datestamp in (",fldlst(dst,"'","'"),")
        AND field1='te1'
        AND field2!='job'
    ")
    mysqlQuery(myquery)
}
gatab <-
function(ime=getive("me")) {
    tab1 <- gaparse(domain="po",field1="lag",field2="comp",value1="0",value2="gross",type="SUM",ime=ime)
    tab2 <- tab2a <- gaparse(domain="po",field1="lag",field2="comp",value1="0",value2="net",type="SUM",ime=ime)
    tab3 <- tab3a <- gaparse(domain="po",field1="lag",field2="comp",value1="0",value2="trade",type="SUM",ime=ime)
    tab4 <- tab4a <- tab4b <- gaparse(domain="pe",field1="lag",field2="comp",value1="0",value2="tot",type="AVG",ime=ime)
    tab5 <- tab5a <- gaparse(domain="pe",field1="lag",field2="comp",value1="0",value2="tot",type="STD",ime=ime)
    tab6 <- gaparse(domain="pe",field1="lag",field2="comp",value1="0",value2="tot",type="IR",ime=ime)
    tab4[,"value"] <- as.character(as.numeric(tab4[,"value"])*52)
    tab5[,"value"] <- as.character(as.numeric(tab5[,"value"])*52^.5)
    tab6[,"value"] <- as.character(as.numeric(tab6[,"value"])*52^.5)
    tab2a[,"value"] <- tab2[,"value"]/tab1[,"value"]
    tab2a[,"value2"] <- "net/gross"
    tab2a[,"domain"] <- "ratio"
    tab3a[,"value"] <- 2*tab1[,"value"]/tab3[,"value"]
    tab3a[,"value2"] <- "hold"
    tab3a[,"domain"] <- "ratio"
    tab5a[,"value"] <- as.numeric(tab5[,"value"])/tab1[,"value"]*tab1[,"nper"]
    tab5a[,"value2"] <- "vol/gross"
    tab5a[,"domain"] <- "ratio"
    tab4a[,"value"] <- as.numeric(tab4[,"value"])/tab1[,"value"]*tab1[,"nper"]
    tab4a[,"value2"] <- "mean/gross"
    tab4a[,"domain"] <- "ratio"
    tab4b[,"value"] <- as.numeric(tab4[,"value"])/tab3[,"value"]*tab1[,"nper"]/52
    tab4b[,"value2"] <- "mean/trade"
    tab4b[,"domain"] <- "ratio"
    tab <- cbind(ime,rbind(tab1,tab2,tab3,tab4,tab5,tab6,tab3a,tab2a,tab5a,tab4a,tab4b))
    isum <- which(tab[,"type"]=="SUM" & tab[,"domain"]=="po")
    tab[isum,"value"] <- as.numeric(tab[isum,"value"])/tab[isum,"nper"]
    tab[isum,"type"] <- "AVG"
    tab[tab[,"domain"]=="ratio","type"] <- "ratio"
    tab
}
gasumsaddup <-
function(eps=1.e-10)
{
    res <- TRUE
    domains <- unique(extract("ga","domain"))
    hasva <- "va"%in%domains
    hasf1 <- "f1"%in%domains
    hasic <- "ic"%in%domains
#    totcomp1 <- c("mktlong","mktshort","reslong","resshort","syslong","sysshort")
#    totcomp2 <- c("mktlong","mktshort","totx")
    totcomp1 <- c("reslong","resshort","syslong","sysshort")
    totcomp2 <- c("totx")
    type <- "SUM"
    for(rows in c("te1","te2")) {
        res <- res&var(c(
            sum(getga(dom="po",rows=rows,cols="lag",type=type)[,"0"],na.rm=TRUE),
            sum(getga(dom="po",rows=rows,cols="lapo",type=type)[,"0"],na.rm=TRUE),
            sum(getga(dom="po",rows=rows,cols="comp",type=type)[,"gross"],na.rm=TRUE),
            sum(getga(dom="po",rows=rows,cols="te1",type=type),na.rm=TRUE),
            sum(getga(dom="po",rows=rows,cols="te2",type=type),na.rm=TRUE)
            )) < eps
        res <- res&var(c(
            sum(getga(dom="pe",rows=rows,cols="lag",type=type)[,"0"],na.rm=TRUE),
            sum(getga(dom="pe",rows=rows,cols="lapo",type=type)[,"0"],na.rm=TRUE),
            sum(getga(dom="pe",rows=rows,cols="comp",type=type)[,"totx"],na.rm=TRUE),
            sum(getga(dom="pe",rows=rows,cols="comp",type=type)[,totcomp1],na.rm=TRUE),
            sum(getga(dom="pe",rows=rows,cols="comp",type=type)[,totcomp2],na.rm=TRUE)
            )) < eps
        if(hasva) {
            res <- res&var(c(
                sum(getga(dom="va",rows=rows,cols="lag")[,"0"],na.rm=TRUE),
                sum(getga(dom="va",rows=rows,cols="lapo")[,"0"],na.rm=TRUE),
                sum(getga(dom="va",rows=rows,cols="comp")[,"totx"],na.rm=TRUE),
                sum(getga(dom="va",rows=rows,cols="comp")[,totcomp1],na.rm=TRUE),
                sum(getga(dom="va",rows=rows,cols="comp")[,totcomp2],na.rm=TRUE)
                )) < eps
        }
        if(hasf1) {
            res <- res&var(c(
                sum(getga(dom="f1",rows=rows,cols="lag")[,"0"],na.rm=TRUE),
                sum(getga(dom="f1",rows=rows,cols="lapo")[,"0"],na.rm=TRUE),
                sum(getga(dom="f1",rows=rows,cols="comp")[,"f1"],na.rm=TRUE),
                sum(getga(dom="f1",rows=rows,cols="comp")[,c("f1long","f1short")],na.rm=TRUE)
                )) < eps
        }
        if(hasic) {
            res <- res&all(apply(cbind(
                sum(getga(dom="ic",rows=rows,cols="lag")[,"0"],na.rm=TRUE),
                sum(getga(dom="ic",rows=rows,cols="lapo")[,"0"],na.rm=TRUE),
                sum(getga(dom="ic",rows=rows,cols="comp")[,"tot"],na.rm=TRUE)
                ),1,var) < eps)
        }
    }
    res
}
gasum <-
function(iga=getive("ga"),lever=1,ic=FALSE)
{
    iga <- sort(as.numeric(iga))
    if(all(iga%in%extract("jo","DISTINCT iga"))) {
        isu <- as.numeric(as.matrix(mysqlQuery(psz("SELECT isu FROM jo WHERE iga IN (",fldlst(iga),") ORDER BY iga ASC"))))
        tab <- mysqlQuery(psz("
                SELECT ixx, AVG(n) AS n
                FROM 
                    (
                    SELECT ixx, date, COUNT(*) AS n
                    FROM vesuvi
                    WHERE ixx IN (",fldlst(isu),")
                    GROUP BY ixx, date
                    ) AS a
                GROUP BY ixx
                ORDER by ixx ASC
                "))
        #mode(tab) <- "numeric"
        c5n <- as.numeric(tab[match(isu,tab[,"ixx"]),"n"])
    } else {
        c5n <- rep(NA,length(iga))
    }
    c1mu <- mysqlQuery(psz("
            SELECT * FROM vegavi WHERE field1='lag' AND field2='comp' AND value1=0 and value2='tot' AND domain='pe' AND type='AVG' AND
            ixx IN (",fldlst(iga),") ORDER BY ixx ASC
        "))[,"value",drop=FALSE]
    c2vol <- mysqlQuery(psz("
            SELECT * FROM vegavi WHERE field1='lag' AND field2='comp' AND value1=0 and value2='tot' AND domain='pe' AND type='STD' AND
            ixx IN (",fldlst(iga),") ORDER BY ixx ASC
        "))[,"value",drop=FALSE]
    c3ir <- mysqlQuery(psz("
            SELECT * FROM vegavi WHERE field1='lag' AND field2='comp' AND value1=0 and value2='tot' AND domain='pe' AND type='IR' AND
            ixx IN (",fldlst(iga),") ORDER BY ixx ASC
        "))[,"value",drop=FALSE]
    c6ic <- mysqlQuery(psz("
            SELECT * FROM vegavi WHERE field1='lag' AND field2='comp' AND value1=0 and value2='tot' AND domain='ic' AND type='AVG' AND
            ixx IN (",fldlst(iga),") ORDER BY ixx ASC
        "))[,"value",drop=FALSE]
    if(!ic || nrow(c6ic)<length(iga)) c6ic <- rep(NA,length(iga))
    c7xg <- mysqlQuery(psz("
            SELECT value/nper AS value FROM vegavi WHERE field1='comp' AND field2='comp' AND value1='gross' and value2='gross' AND domain='po' AND type='SUM' AND
            ixx IN (",fldlst(iga),") ORDER BY ixx ASC
        "))[,"value",drop=FALSE]
    c8xt <- mysqlQuery(psz("
            SELECT value/nper AS value FROM vegavi WHERE field1='comp' AND field2='comp' AND value1='trade' and value2='trade' AND domain='po' AND type='SUM' AND
            ixx IN (",fldlst(iga),") ORDER BY ixx ASC
        "))[,"value",drop=FALSE]
    c9cmt <- extract("vegadi","comment",psz("WHERE ixx IN (",fldlst(iga),") ORDER BY ixx ASC"))
    c10icvol <- mysqlQuery(psz("
            SELECT * FROM vegavi WHERE field1='lag' AND field2='comp' AND value1=0 and value2='tot' AND domain='ic' AND type='STD' AND
            ixx IN (",fldlst(iga),") ORDER BY ixx ASC
        "))[,"value",drop=FALSE]
    if(!ic || nrow(c10icvol)<length(iga)) c10icvol <- rep(NA,length(iga))
    cc <- cbind(
        iga=iga,
        comment=c9cmt,
        mu=lever*c1mu*52,
        vol=lever*c2vol*52^.5,
        ir=c3ir*52^.5,
        fundlaw=c6ic*
                (
                #(c8xt*52)/(2*c7xg)* #turns pa
                c5n*52  #n
                )^.5,
        n=c5n,
        ic=c6ic,
        xg=lever*c7xg,      
        xt=lever*c8xt*52,   
        hold=2*c7xg/c8xt,
        earn=c1mu*2/c8xt,
        icvol=c10icvol,
        fundlaw2=1/c5n^.5
        )
    colnames(cc) <- c("iga","comment","mu","vol","ir","flaw1","n","ic","xg","xt","hold","earn","vol(ic)","flaw2")
    rownames(cc) <- as.character(sort(iga))
    cc
}
gaselect <-
function(domain="pe",field1="lag",field2="comp",value1="0",value2="tot",type="IR",ime=getive("me")) {
    ixx <- mexx(ime=ime,xx="ga",gr="tot")
    ds <- ixxtods(xx="ga",ixx=ixx)
    query <- psz("
        SELECT ixx, comment, domain, type, value2, value, nper
        FROM
            (SELECT * FROM vega WHERE
            domain=",flds(domain)," AND
            field1=",flds(field1)," AND
            value1=",flds(value1)," AND
            field2=",flds(field2)," AND
            value2=",flds(value2)," AND
            type=",flds(type)," AND
            datestamp IN (",flds(ds),")) AS a
            INNER JOIN vegadi
            ON vegadi.datestamp=a.datestamp
            ")
    mysqlQuery(query)
}
garegion <-
function(group=c("region","development"),dolabel="pretty",aaindices="aaindices")
{
    group <- match.arg(group)
    indices <- extract(aaindices,"bti, region, development","WHERE char_length(bti)=2")
    if(group=="region") {
        group <- "region"
    } else {
        group <- "development"
    }
    rnams <- extract(aaindices,psz("DISTINCT ",group))
    regions <- vector("list",length(rnams))
    names(regions) <- rnams
    for(i in seq_along(regions)) regions[[i]] <- extract(aaindices,"bti",psz("WHERE CHAR_LENGTH(bti)=2 AND ",group,"='",rnams[i],"'"))    
    if(dolabel!="pretty") {names(regions) <- lapply(regions,paste,collapse="-")}
    droptab("tmp0")
    mysqlQuery("CREATE TABLE tmp0 AS SELECT * FROM ga")
    i <- 1
    for(i in seq_along(regions)) {
        mysqlQuery(psz("
                UPDATE tmp0
                SET value1='",names(regions)[i],"'
                WHERE field1='te2' AND value1 IN (",fldlst(as.character(regions[[i]]),"'","'"),")
                "))
    }
    gaga(mytab="tmp0",sum="sum")
}
gapeersdel <-
function(
                jobs=getijo("all")
                )
{
    mysqlQuery(psz("DELETE FROM vega WHERE field2='job' AND value2 IN (",fldlst(jobs),")"))
}
gapeersadd <-
function(
                jobs=mexx(group='comborep'),
                delete=TRUE
                )
{
    if(length(jobs)<2) return()
    jobs <- jobs[jobs%in%getijo("all")]
    if(delete) gapeersdel(jobs)
    droptab("tmp1")
    mysqlQuery(psz("
        CREATE TABLE tmp1 AS 
        SELECT DISTINCT vegadi.ixx, vegadi.datestamp, jo.ijo
        FROM vegadi INNER JOIN jo
        ON vegadi.ixx=jo.iga
        WHERE jo.ijo IN (",fldlst(jobs),")
        "))
    addidx("tmp1","datestamp")
    droptab("tmp2")
    mysqlQuery("
        CREATE TABLE tmp2
        SELECT DISTINCT vega.domain, vega.field1, 'job' AS field2, vega.value1, tmp1.ijo AS value2, vega.type, vega.nper, vega.datestamp, vega.value
        FROM vega, tmp1
        WHERE 
            (field2='lapo' OR field2='lag') AND value2=0 
            AND 
            vega.datestamp=tmp1.datestamp
    ")
    mysqlQuery("
        ALTER TABLE tmp2 
        ADD PRIMARY KEY (domain, field1, field2, value1, value2, type, datestamp)
        ")
    mysqlQuery("
        INSERT INTO vega (domain, field1, field2, value1, value2, type, value, nper, datestamp)
        SELECT tmp2.domain, tmp2.field1, tmp2.field2, tmp2.value1, tmp2.value2, tmp2.type, tmp2.value, tmp2.nper, tmp1.datestamp
        FROM tmp2, tmp1
        ")
}
gaparse <-
function(...) {
    tab <- gaselect(...)
    ilib <- grep(pattern="totlib",x=tab[,"comment"])
    iper <- grep(pattern="totrep",x=tab[,"comment"])
    stopifnot(all.equal(sort(c(ilib,iper)),1:nrow(tab)))
    libs <- unlist(lapply(lapply(lapply(
                    lapply(strsplit(x=tab[ilib,"comment"],split="totlib-"),"[[",2),
                    strsplit,
                    split=" "
                    ),
            "[[",1),"[[",1))
    pers <- unlist(lapply(
                    lapply(
                    lapply(strsplit(x=tab[iper,"comment"],split="totrep from "),"[[",2),
                    substr,1,10),
            "[[",1))
    ili <- rep(paste(libs,collapse=","),nrow(tab))
    startdate <- rep(min(pers,na.rm=TRUE),,nrow(tab))
    ili[ilib] <- libs
    startdate[iper] <- pers
    cbind(tab[,c("domain","type","value","value2","nper")],ili,startdate)
}
gamkt <-
function()
{
    mysqlQuery("
    INSERT INTO ga (domain, field1, field2, value1, value2, type, value, nper)
        SELECT 'pe' AS domain, 'mkt' AS field1, 'date' AS field2, 'mkt' AS value1, date AS value2, 'mkt' AS type, SUM(sco)/sum(gross) AS value, 1 AS nper
            FROM
                (SELECT aats.date, aats.bui, aats.redoto * ce.fmp1 AS sco, ABS(fmp1) AS gross
                FROM aats, ce
                WHERE aats.date=ce.date AND aats.bui=ce.bui
                ) AS a
        GROUP BY date
    ")
}
game3 <-
function(
            ime=getive("me"),
            arch=TRUE,
            gear=1,
            note="",
            ili=as.numeric(extract("me","DISTINCT ili","WHERE ili!=1"))
            )
{
    resve(xx="me",ixx=ime)
    for(i in seq_along(ili)) {
        ijo <- as.numeric(extract("me","ijo",psz("WHERE method='librep' AND ili=",ili[i])))
        mycomment <- psz("me ime=",ime," all sectors ili=",ili[i]," ",note)
        gaga(ixx=ijo,sum="sum",collate=TRUE,gear=gear)
        if(arch) {arcve(xx="ga",mycomment=mycomment)}
    }
}
game2 <-
function(arch=TRUE,ime=getive("me"),gear=1,note="")
{
    resve(xx="me",ixx=ime)
    reper <- reperiodme()
    da <- names(reper)
    if(0<length(da) && !arch) {da <- min(da)}
    for(i in seq_along(da)) {
        ijo <- as.numeric(reper[[i]])
        mycomment <- psz("me ime=",ime," from ",da[i]," all sectors ijo=",paste(ijo,collapse=",")," ",note)
        gatiga(ixx=ijo,sum="sum",collate=TRUE,gear=gear)
        if(arch) {
            arcve(xx="ga",mycomment=mycomment)
            arcve(xx="ti",mycomment=mycomment)
        }
    }
}
game1 <-
function(sumtreatment=c("average","sum"))
{
    sumtreatment <- match.arg(sumtreatment)
    ili <- setdiff(extract("me1","DISTINCT ili"),1)
    stopifnot(length(ili)>0)
    for(i in seq_along(ili)) {
        print(ili[i])
        ijo <- extract("me1","ijo1",psz("WHERE method LIKE 'neut%' AND ili=",ili[i]))
        gaga(ixx=ijo,filt=NULL,sumtreatment=sumtreatment)
        arcve("ga",mycomment=psz(extract("velidi","comment",psz("WHERE ixx=",ili[i]))))
    }
}
game <-
function(ime=aame(),...)
{
    stopifnot(is.numeric(ime) && all(ime%in%getive("me",how="all")))
    for(i in seq_along(ime)) {
        game2(ime=ime[i],...)
        game3(ime=ime[i],...)
    }
}
gaga <-
function(
            ixx=sort(getijo("all"),decr=TRUE)[1:2],
            sumtreatment=c("average","sum"),
            verbose=TRUE,
            xx="jo",
            filt=list(parameter="method",value="neut%"),
            gearing=1,        #-ve values not prevented but position will be incorrect (an maybe others)
            collate=FALSE,    #if collate, retain un-aggregated rows keyed on field1=te1 (only suitable for non-overlapping te1 as in me2)
            mytab="vega",
            channel=chve(),
            channel2=DBcon
            )
{
    sumtreatment <- match.arg(sumtreatment)
    if(mytab!="vega") {  #this flags an intra-ga aggregation
        stopifnot(is.na(match(mytab,psz("tmp",1:9))))
        datestampqual <- "WHERE "
        sumcode <- ""
        iga <- 0
    } else {
        if(xx=="jo") {
            ijo <- ixx[ixx%in%getijo("all")]
            stopifnot(length(ijo)>=1)
            if(is.null(filt)) {
                iga <- jolu(xx="ga",ijo=ijo)
            } else {
                iga <- jolusi(xx="ga",ijo=ijo,filt=filt)
            }
        } else {
            iga <- ixx[ixx%in%getive(xx="ga",how="all")]
        }
        stopifnot(length(iga)>0)
        idatestamp <- ixxtods(xx="ga",ixx=iga)
        datestampqual <- psz(" WHERE datestamp IN (",fldlst(idatestamp,"'","'"),") AND ")
        sumcode <- ifelse(sumtreatment=="average",psz("/",length(iga)),"")
        stopifnot(length(idatestamp)==length(iga))    
        stopifnot(all(idatestamp%in%getive(xx="ga",what="datestamp",howmany="all")))
        if(collate) {   #check for non-overlapping te1
            foundte1 <- NULL
            for(i in seq_along(idatestamp)) {
                te1 <- extract("vega","DISTINCT value1",psz("WHERE field1='te1' AND datestamp='",idatestamp[i],"' AND field2!='job'"),channel=channel)
                collate <- collate && !any(te1%in%foundte1) #if overlapping, disallow collation
                foundte1 <- c(foundte1,te1)
            }
        }
    }
    if(verbose) print(paste(Sys.time(),"gaga avg"))
    newga(tab="tmp1")   #TS avg(pe) no log option
     mysqlQuery(psz("
        INSERT INTO ",nameve(channel2),".tmp1 (domain, field1, field2, value1, value2, type, value, nper)
        SELECT domain,  'lag' AS field1, field1 AS field2, 0 AS value1,  value1 AS value2, 'AVG' AS type, ",gearing,"*AVG(value) AS value, count(value) AS nper
        FROM (
            SELECT domain, field1, value1, field2, value2, type, SUM(value)",sumcode," AS value, MAX(nper) #MAX treatment is important, needs documenting
            FROM ",mytab,"
            ",datestampqual,"
            domain IN ('pe')
            AND field2='date'
            AND type='SUM'
            GROUP BY field1, value1, value2
        ) AS a
        GROUP BY field1, value1
    "),channel=channel)
    if(verbose) print(paste(Sys.time(),"gaga std"))
    newga(tab="tmp2")   #TS std(pe) no log option
    mysqlQuery(psz("
        INSERT INTO ",nameve(channel2),".tmp2 (domain, field1, field2, value1, value2, type, value, nper)
        SELECT domain,  'lag' AS field1, field1 AS field2, 0 AS value1,  value1 AS value2, 'STD' AS type, ",gearing,"*STD(value) AS value, count(value) AS nper
        FROM (
            SELECT domain, field1, value1, field2, value2, type, SUM(value)",sumcode," AS value, MAX(nper)
            FROM ",mytab,"
            ",datestampqual,"
            domain='pe'
            AND field2='date'
            AND type='SUM'
            GROUP BY field1, value1, value2
        ) AS a
        GROUP BY field1, value1
    "),channel=channel)
    if(verbose) print(paste(Sys.time(),"gaga IR"))
    newga(tab="tmp3")   #IR(pe) from the above
    mysqlQuery(psz("
        INSERT INTO ",nameve(channel2),".tmp3 (domain, field1, field2, value1, value2, type, value, nper)
            SELECT a.domain, a.field1, a.field2, a.value1, a.value2, 'IR' AS type, a.value/b.value AS value, a.nper
            FROM ",nameve(channel2),".tmp1 AS a INNER JOIN ",nameve(channel2),".tmp2 AS b
            ON 
                a.field1 = b.field1  AND
                a.value1 = b.value1  AND
                a.field2 = b.field2  AND
                a.value2 = b.value2 
             WHERE a.domain = 'pe' AND b.domain='pe'
    "),channel=channel)
    if(verbose) print(paste(Sys.time(),"gaga sum(pe)/n"))
    newga(tab="tmp4")   #sum(pe) optionally *1/njobs
    mysqlQuery(psz("
        INSERT INTO ",nameve(channel2),".tmp4 (domain, field1, field2, value1, value2, type, value, nper)
            SELECT domain, field1, field2, value1, value2, type, ",gearing,"*SUM(value)",sumcode," AS value, MAX(nper) #MAX is important, needs doc
            FROM ",mytab,"
            ",datestampqual,"
            domain IN ('pe')
            AND type IN ('SUM')
            AND field2 != 'job'
            GROUP BY field1, value1, field2, value2
    "),channel=channel)
    if(verbose) print(paste(Sys.time(),"gaga sum(po)/n"))
    newga(tab="tmp5")   #sum(po) optionally *1/njobs
    mysqlQuery(psz("
        INSERT INTO ",nameve(channel2),".tmp5 (domain, field1, field2, value1, value2, type, value, nper)
            SELECT domain, field1, field2, value1, value2, type, ",gearing,"*SUM(value)",sumcode," AS value, MAX(nper)
            FROM ",mytab,"
            ",datestampqual,"
            domain IN ('po')
            AND type IN ('SUM')
            AND field2 != 'job'
            GROUP BY field1, value1, field2, value2
    "),channel=channel)
    if(verbose) print(paste(Sys.time(),"gaga sum(mkt)/n"))
    newga(tab="tmp6")   #sum(mkt)/njobs
    mysqlQuery(psz("
        INSERT INTO ",nameve(channel2),".tmp6 (domain, field1, field2, value1, value2, type, value, nper)
            SELECT domain, field1, field2, value1, value2, type, SUM(value)/",length(iga)," AS value, MAX(nper) #SUM(value)",sumcode,"
            FROM ",mytab,"
            ",datestampqual,"
            domain IN ('pe')
            AND field1 IN ('mkt')
            AND type IN ('mkt')
            AND field2 != 'job'
            GROUP BY field1, value1, field2, value2
    "),channel=channel)
    if(verbose) print(paste(Sys.time(),"gaga sum(f1)/n"))
    newga(tab="tmp7")   #sum(f1) optionally *1/njobs
    mysqlQuery(psz("
        INSERT INTO ",nameve(channel2),".tmp7 (domain, field1, field2, value1, value2, type, value, nper)
            SELECT domain, field1, field2, value1, value2, type, ",gearing,"*SUM(value)",sumcode," AS value, MAX(nper)
            FROM ",mytab,"
            ",datestampqual,"
            domain IN ('f1')
            AND type IN ('sum')
            AND field2 != 'job'
            GROUP BY field1, value1, field2, value2
    "),channel=channel)
    if(verbose) print(paste(Sys.time(),"gaga sum(va)/n"))
    newga(tab="tmp8")   #sum(va) optionally *1/njobs (correlation implicitly assumed zero)
    mysqlQuery(psz("
        INSERT INTO ",nameve(channel2),".tmp8 (domain, field1, field2, value1, value2, type, value, nper)
            SELECT domain, field1, field2, value1, value2, type, POW(",gearing,",2)*SUM(value)",sumcode," AS value, MAX(nper)
            FROM ",mytab,"
            ",datestampqual,"
            domain IN ('va')
            AND type IN ('sum')
            AND field2 != 'job'
            GROUP BY field1, value1, field2, value2
    "),channel=channel)
    newga()
    if(verbose) print("gaga ga")        
    mysqlQuery("
        INSERT INTO ga
        SELECT * FROM tmp1 UNION
        SELECT * FROM tmp2 UNION
        SELECT * FROM tmp3 UNION
        SELECT * FROM tmp4 UNION
        SELECT * FROM tmp5 UNION
        SELECT * FROM tmp6 UNION
        SELECT * FROM tmp7 UNION
        SELECT * FROM tmp8
    ")
    if(mytab=="vega" && collate) {
        if(verbose) print(paste(Sys.time(),"gaga collate"))
        mysqlQuery(psz("
            DELETE FROM ",nameve(channel2),".ga
            WHERE field1='te1'
            AND field2!='job'
        "),channel=channel)
        mysqlQuery(psz("
            INSERT INTO ",nameve(channel2),".ga (domain, field1, field2, value1, value2, type, value, nper)
            SELECT domain, field1, field2, value1, value2, type, ",gearing,"*value, nper 
            FROM vega
            ",datestampqual,"
            field1='te1'
            AND field2!='job'
            AND domain IN ('pe','po','f1')
        "),channel=channel)
        mysqlQuery(psz("
            INSERT INTO ",nameve(channel2),".ga (domain, field1, field2, value1, value2, type, value, nper)
            SELECT domain, field1, field2, value1, value2, type, ",gearing^2,"*value, nper 
            FROM vega
            ",datestampqual,"
            field1='te1'
            AND field2!='job'
            AND domain IN ('va')
        "),channel=channel)
        mysqlQuery(psz("
            INSERT INTO ",nameve(channel2),".ga (domain, field1, field2, value1, value2, type, value, nper)
            SELECT domain, field1, field2, value1, value2, type, value, nper 
            FROM vega
            ",datestampqual,"
            field1='te1'
            AND field2!='job'
            AND domain IN ('ic')
        "),channel=channel)
    }
    mysqlQuery("ALTER TABLE ga ADD PRIMARY KEY (domain, field1, field2, value1, value2, type)")
    addidx("ga",c("field1","field2","type","domain"))
    chkga()
}
gacomponent <-
function(type=c("component","sign"))
{
    type <- match.arg(type)
    if(type=="component") {
        comp <- list(
                    mkt='market',
                    sys='systematic',
                    res='residual'
                    )
        qual <- ""
    } else {
        comp <- list(
                    long='long',
                    short='short'
                    )
        qual <-  ifelse(getsione("ppt",def=F),""," AND value1 NOT LIKE 'mkt%' ") 
    }
    droptab("tmp0")
    mysqlQuery("CREATE TABLE tmp0 AS SELECT * FROM ga")
    for(i in seq_along(comp)) {
        mysqlQuery(psz("
                UPDATE tmp0
                SET value1='",comp[i],"'
                WHERE field1='comp' AND value1 LIKE '%",names(comp)[i],"%' ",qual,"
                "))
    }
    gaga(mytab="tmp0",sum="sum")
}
gacomp <-
function(ime=getive("me"),ord="dalimedotycova") {
    res <- NULL
    for(i in seq_along(ime)) {res <- rbind(res,gatab(ime[i]))}
    len <- nchar(ord)/2
    oo <- vector("list",len)
    for(i in 1:(nchar(ord)/2)) {
        oo[[i]] <- res[,switch(substr(ord,2*i-1,2*i),
                        li="ili",
                        me="ime",
                        da="startdate",
                        do="domain",
                        ty="type",
                        co="value2",
                        va="value"
                        )]
    }
    res[do.call(what="order",oo),]
}
gacanic <-
function(fields) 
{
    stopifnot(identical(sort(colnames(fields)),sort(c("comp","pe","po2","re2"))))
    stopifnot(all(fields[,c("pe","po2","re2")] %in% dirfld("ga1")))
    q1 <- psz("
                SELECT DISTINCT date, domain, te1, te2, lag, lapo, '",fields[,"comp"],"' AS comp, 
                                SUM(",fields[,"pe"],") AS Npe,
                                SUM(",fields[,"po2"],") AS Dpo,
                                SUM(",fields[,"re2"],") AS Dre
                FROM ga1
                GROUP BY date, domain, te1, te2, lag, lapo, comp
                ")
    paste("CREATE TABLE ga2",paste(q1,collapse=" UNION "))
}
gacan <-
function(fields,domain) 
{
    stopifnot(all(fields %in% dirfld("ga1")))
    stopifnot(domain %in% domains())
    q1 <- psz("
                SELECT DISTINCT date, domain, te1, te2, lag, lapo, '",fields,"' AS comp, SUM(",fields,") AS value
                FROM ga1
                GROUP BY date, domain, te1, te2, lag, lapo, comp
                ")
    paste("CREATE TABLE ga2",paste(q1,collapse=" UNION "))
}
ga3query2 <-
function(
                domain,
                field1,
                whereclause
                )
{
    if(domain=="ic") {
        sumclause <- "SUM(Npe)/POW(SUM(Dpo)*SUM(Dre),.5)"
    } else {
        sumclause <- "SUM(value)"
    }
    query <- psz("
                INSERT INTO ga (domain, field1, field2, value1, value2, type, value, nper) 
                    SELECT domain, '",field1,"' AS field1, 'date' AS field2, ",field1," AS value1, date AS value2, 'SUM' as type, ",sumclause," AS value, 1 AS nper
                    FROM ga2
                    ",whereclause,"
                    GROUP BY date, ",field1
               )
    return(query)
}
ga3query1b <-
function(
                fun,
                field1,
                field2,
                whereclause,
                compound,
                domain
                )
{
    if(compound&(domain=='pe')) {
        myfun <- psz("IF (value<(-1+1e-4),-10,exp(",fun,"(log(1+value)))-1)") 
    } else {
        myfun <- psz(fun,"(value)")
    }
    psz("  #outer aggregation is timeseries, inner aggregation is cross-section
        INSERT INTO ga (domain, field1, field2, value1, value2, type, value, nper) 
            SELECT domain, field1, field2, value1, value2, '",fun,"' AS type, ",myfun," AS value, COUNT(value) AS nper
            FROM (
                SELECT domain, date, '",field1,"' AS field1, '",field2,"' AS field2, ",field1," AS value1, ",field2," AS value2, SUM(value) AS value
                FROM ga2
                ",whereclause,"
                GROUP BY date, ",field1,", ",field2,"
                ) AS x
            GROUP BY value1, value2")
}
ga3query1a <-
function(
                fun,
                field1,
                field2,
                whereclause
                )
{
    if(fun=="SUM") {
        tssummarise <- "SELECT domain, field1, field2, value1, value2, 'SUM' AS type, SUM(Npe)/POW(SUM(Dpo)*SUM(Dre),.5) AS value, COUNT(Npe) AS nper"
    } else {
        tssummarise <- psz("
                        SELECT domain, field1, field2, value1, value2, '",fun,"' AS type, ",fun,"(Npe/POW(Dpo*Dre,.5)) AS value, COUNT(Npe) AS nper
                        ")
    }
    psz("
        INSERT INTO ga (domain, field1, field2, value1, value2, type, value, nper)",
            tssummarise,"
            FROM (
                SELECT domain, date,  '",field1,"' AS field1, '",field2,"' AS field2, ",field1," AS value1, ",field2," AS value2, SUM(Npe) AS Npe, SUM(Dre) AS Dre, SUM(Dpo) AS Dpo
                FROM ga2
                ",whereclause,"
                GROUP BY date, ",field1,", ",field2,"
                ) AS x
            GROUP BY value1, value2")
}
ga3query1 <-
function(
                domain,
                fun,
                field1,
                field2,
                whereclause,
                compound
                )
{
    if(domain=="ic") {
        query <- ga3query1a(fun=fun,field1=field1,field2=field2,whereclause=whereclause)
    } else {
        query <- ga3query1b(fun=fun,field1=field1,field2=field2,whereclause=whereclause,compound=compound,domain=domain)
    }
    return(query)
}
ga3 <-
function(
            te1def="ALL",
            te2def="ALL",
            lagdef=0,
            lapodef=0,
            compdefpe=c("totx","tot","mktlong","syslong","reslong","mktshort","sysshort","resshort","syslong+sysshort"),
            compdefva=c("totx","tot","mktlong","syslong","reslong","mktshort","sysshort","resshort","syslong+sysshort"),
            compdefpo=c("net","gross","trade"),
            compdeff1=c("f1","f1long","f1short"),
            compdefic=c("tot","mktlong","syslong","reslong","mktshort","sysshort","resshort"),
            type=c("SUM","STD","AVG"),        #IR is done in derga
            compound=FALSE,
            qu=NULL
            ) 
{
    domain <- as.character(extract("ga2","domain","LIMIT 1"))
    compdefpe <- match.arg(compdefpe)
    compdefpo <- match.arg(compdefpo)
    compdefva <- match.arg(compdefva)
    compdeff1 <- match.arg(compdeff1)
    compdefic <- match.arg(compdefic)
    compdef <- switch(domain,pe=compdefpe,po=compdefpo,va=compdefva,f1=compdeff1,ic=compdefic)
    stopifnot(length(lagdef)==1)
    stopifnot(length(lapodef)==1)
    if(te1def!="ALL") { te1clause <- paste("te1 IN (",fldlst(te1def,"'","'"),")") } else {te1clause <- ""}
    if(te2def!="ALL") { te2clause <- paste("te2 IN (",fldlst(te2def,"'","'"),")") } else {te2clause <- ""}
    lagclause <-  psz("lag = '",lagdef,"'") 
    lapoclause <-  psz("lapo = '",lapodef,"'") 
    compclause <-  psz("comp = '",compdef,"'") 
    defaults <- c(te1=te1def,te2=te2def,lag=lagdef,lapo=lapodef,comp=compdef)
    clauses <- c(te1=te1clause,te2=te2clause,lag=lagclause,lapo=lapoclause,comp=compclause)
    if(0<length(qu)) {
        f1values <- setdiff(names(clauses),"te2")
        f2values <- "te2" #just adding non-te2 rows with cols=te2
    } else {
        f1values <- names(clauses)
        f2values <- names(clauses)
    }
    for(field1 in f1values) {
        for(field2 in f2values) {
            whereclause <- whereforpassive(clauses=clauses,activeset=c(field1,field2))
            for(fun in type) {
                query <- ga3query1(domain=domain,fun=fun,field1=field1,field2=field2,whereclause=whereclause,compound=compound)
                res <- mysqlQuery(query)
            }
        }
        if(length(qu)==0) {  #add timeseries
            whereclause <- whereforpassive(clauses=clauses,activeset=field1)
            query <- ga3query2(domain=domain,field1=field1,whereclause=whereclause)
            res <- mysqlQuery(query)
        }
        #domainclause <-  psz("domain = '",domain,"'") 
        #whereclause <- whereforpassive(clauses=c(domain=domainclause,clauses),activeset=field1)
    }
}
ga2derive <-
function() 
{
    domain <- as.character(extract("ga1","domain","LIMIT 1"))
    fields <- switch(domain,
                    pe=c("tot","totx","mktlong","syslong","reslong","mktshort","sysshort","resshort"),
                    va=c("tot","totx","mktlong","syslong","reslong","mktshort","sysshort","resshort"),
                    f1=c("f1","f1long","f1short"),
                    po=c("net","gross","trade"),
                    ic=as.matrix(data.frame(
                            comp=c("tot","mktlong","syslong","reslong","mktshort","sysshort","resshort"),
                            pe=c("tot","mktlong","syslong","reslong","mktshort","sysshort","resshort"),
                            re2=c("re2t","re2ml","re2sl","re2rl","re2ms","re2ss","re2rs"),
                            po2=c("po2t","po2l","po2l","po2l","po2s","po2s","po2s")
                            ))
                    )
    droptab("ga2")
    if(domain!="ic") {
        tt <- system.time(mysqlQuery(gacan(fields=fields,domain=domain)))[3]
    } else {
        tt <- system.time(mysqlQuery(gacanic(fields=fields)))[3]
    }
    if(getsione("verbose")) print(paste("queried ga2 <- ga1 in",floor(tt),"s"))
    mysqlQuery("ALTER TABLE ga2 MODIFY comp CHAR(8) NOT NULL")
    for(i in setdiff(dirfld("ga2"),c("domain","value","date","bui","Npe","Dpo","Dre"))) {
        tt <- system.time(addidx("ga2",i))[3]
        if(getsione("verbose"))  print(paste("indexed",domain,i,"canonical table in",floor(tt),"s"))
    }
}
ga1derive <-
function(
            domain=c("po","pe","va","f1","fa","xy","ic"),
            te1="industry_sector",
            te2="countryfull",
            qu=NULL #optional quantile fields
            ) 
{
    domain <- match.arg(domain)
    stopifnot(te1 %in% dirfld())
    stopifnot(te2 %in% dirfld())
    stopifnot(is.null(qu) || (length(qu)<=4 && all(qu%in% dirfld())))
    tab <- ifelse(domain%in%c("fa","xy"),"ma1","ga1") #routine also used by ma
    la <- switch(domain,fa='lace',xy='lacexy',va='lapo',f1='lapo',fa='lapo',xy='lapo',ic='lapo')
    nqu <- length(qu)
    if(0<nqu) { 
        quclause <- psz(", ",psz("pa.",qu,collapse=", "))
    } else {
        quclause <- NULL
    }
    droptab(tab)
    laclause <- switch(domain,
        pe=", lace as lag",             #lace maps to lag       !!!
        po=", 0 AS lag, 0 AS lapo ",
        f1=", lava AS lag, 0 AS lapo ", #lava also maps to lag  ! the reason both map to lag is that otherwise dependencies mess up the permutations in ga
        va=", lava AS lag, 0 AS lapo ",
        fa=", lace as lag",
        xy=", lacexy as lag",
        ic=", lace as lag"
        )
    mysqlQuery(psz("
                CREATE TABLE ",tab,"
                SELECT DISTINCT '",domain,"' AS domain, domain.*, pa.",te1," AS te1, pa.",te2," AS te2 ",laclause,quclause,"
                FROM ",domain,"vi as domain INNER JOIN pa
                ON domain.bui=pa.bui AND domain.date=pa.date "
                ))
    mysqlQuery(paste("ALTER TABLE ",tab," MODIFY domain CHAR(2) NOT NULL"))
    mysqlQuery(paste("ALTER TABLE ",tab," MODIFY te1 CHAR(60) NOT NULL"))
    mysqlQuery(paste("ALTER TABLE ",tab," MODIFY te2 CHAR(60) NOT NULL"))
    for(i in seq_along(qu)) {
        mysqlQuery(psz("UPDATE ",tab," SET ",qu[i]," ='missing' WHERE ",qu[i]," IS NULL"))    #this occurs when no lace>0 due to the lag
        mysqlQuery(psz("ALTER TABLE ",tab," MODIFY ",qu[i]," CHAR(8) NOT NULL"))
    }
    if("ls" %in% dirfld(tab) && tab=="ga1" && any(extract(tab,"DISTINCT ls")!=0) ) mysqlQuery("UPDATE ga1 SET ls=1 WHERE ls=0") #mysqlQuery("DELETE FROM ga1 WHERE ls=0")
    addidx(tab,c("date","domain","te1","te2","lag",la))
}
domains <-
function(doms=c("po","va","f1","pe","ic"))
{
    intersect(gsub(patt="vi",rep="",x=dirtab(psz(doms,'vi'))),doms)
}
decga <-
function() 
{
    structure(  c("domain", "field1", "field2", "value1", "value2", "type", "value", "nper", 
                "CHAR(6) ", "CHAR(18)", "CHAR(18)", "CHAR(60)", "CHAR(60)", "CHAR(6)", "DOUBLE", "SMALLINT", 
                "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NULL", "NOT NULL"), 
                .Dim = c(8L, 3L), 
                .Dimnames = list(NULL, c("xfield", "xtype", "xnull"))
            )
}
pasteup <-
function(fnam=c("a","b","b","c"),fnum=1:4,ratio=FALSE) 
{
    stopifnot(length(fnam)==length(fnum))
    expr1 <- NULL
    expr2 <- NULL
    ufnam <- unique(fnam)
    for (nam in ufnam) { 
        expr1 <- c(expr1,paste(paste(psz("re",fnum[fnam==nam]),collapse="+")))
        expr2 <- c(expr2,paste(paste(psz("va",fnum[fnam==nam]),collapse="+")))
    }
     expr <- paste(paste("(",expr1,")"),"/",paste("POW(",expr2,"+1.e-20,.5)"))
     ifelse(ratio,
        res <- paste(collapse=", ",psz(expr," AS ","no",ufnam)),
        res <- paste(collapse=", ",paste(expr1," AS ",ufnam))
        )
     res
}
fovi <-
function(domain=c("pe","po","va","f1","fa","xy","ic"),
                field="industry_sector",
                xtype=c("","IN","BETWEEN","="),
                xvalue=as.character(extract("pa",paste("DISTINCT",field))),
                dtype=c("","BETWEEN","=","IN"),
                dvalue=c("1900-01-01","2050-01-01"),
                type=c("TABLE","VIEW")
                ) 
{
    stopifnot(field %in% dirfld())    
    domain <- match.arg(domain)
    xtype <- match.arg(xtype)
    dtype <- match.arg(dtype)
    type <- match.arg(type)
    if(domain=="fa") {
        cust <- customaggregates(n=getsione("nfac"))
        expr1 <- pasteup(fnam=cust$fnam,fnum=cust$fnum,ratio=FALSE)
        expr2 <- pasteup(fnam=cust$fnam,fnum=cust$fnum,ratio=TRUE)
    }
    if(xtype=="=") stopifnot(length(xvalue)==1)
    if(xtype=="BETWEEN") stopifnot(length(xvalue)==2)
    if(dtype=="=") stopifnot(length(dvalue)==1)
    if(dtype=="BETWEEN") stopifnot(length(dvalue)==2)    
    createclause <- switch(domain,
            pe=psz("CREATE ",type," pevi AS 
                        SELECT  pe.date, 
                                pe.bui, 
                                pe.lace, 
                                pe.lapo, 
                                pe.pem * (ls+1)/2 AS mktlong, 
                                pe.pes * (ls+1)/2 AS syslong, 
                                pe.per * (ls+1)/2 AS reslong, 
                                pe.pem * (1-ls)/2 AS mktshort, 
                                pe.pes * (1-ls)/2 AS sysshort, 
                                pe.per * (1-ls)/2 AS resshort,
                                pe.pet AS tot, 
                                pe.pes+pe.per AS totx,
                                pe.ls 
                        FROM pe"
                        ),
            ic=psz("CREATE ",type," icvi AS 
                        SELECT  pe.date, 
                                pe.bui, 
                                pe.lace, 
                                pe.lapo, 
                                pe.pem * (ls+1)/2 AS mktlong,   #pe     long    mkt
                                pe.pes * (ls+1)/2 AS syslong,   #pe     long    sys
                                pe.per * (ls+1)/2 AS reslong,   #pe     long    res
                                pe.pem * (1-ls)/2 AS mktshort,  #pe     short   mkt
                                pe.pes * (1-ls)/2 AS sysshort,  #pe     short   sys
                                pe.per * (1-ls)/2 AS resshort,  #pe     short   res
                                pe.pet AS tot,                  #pe     total   total
                                pe.re2m * (ls+1)/2 AS re2ml,    #re2    long    mkt
                                pe.re2s * (ls+1)/2 AS re2sl,    #re2    long    sys
                                pe.re2r * (ls+1)/2 AS re2rl,    #re2    long    res
                                pe.re2m * (1-ls)/2 AS re2ms,    #re2    short   mkt
                                pe.re2s * (1-ls)/2 AS re2ss,    #re2    short   sys
                                pe.re2r * (1-ls)/2 AS re2rs,    #re2    short   res
                                pe.re2t AS re2t,                #re2    total   total
                                pe.po2 * (ls+1)/2 AS po2l,      #po2    long    -
                                pe.po2 * (1-ls)/2 AS po2s,      #po2    short   -
                                pe.po2 AS po2t,                 #po2    total   -
                                pe.ls
                        FROM pe"
                        ),
            po=psz("CREATE ",type," povi AS 
                        SELECT  po.date, 
                                po.bui, 
                                po.xo AS net, 
                                abs(po.xo) AS gross, 
                                abs(po.xt) AS trade, 
                                SIGN(po.xo) AS ls
                        FROM po"
                        ),
            va=psz("CREATE ",type," vavi AS 
                        SELECT  va.date, 
                                va.bui, 
                                va.lava, 
                                va.f1, 
                                va.vam * (ls+1)/2 AS mktlong, 
                                va.vas * (ls+1)/2 AS syslong, 
                                va.var * (ls+1)/2 AS reslong, 
                                va.vam * (1-ls)/2 AS mktshort, 
                                va.vas * (1-ls)/2 AS sysshort, 
                                va.var * (1-ls)/2 AS resshort, 
                                va.vat AS tot, 
                                va.vas+va.var AS totx, 
                                va.ls 
                        FROM va"
                        ),
            f1=psz("CREATE ",type," f1vi AS 
                        SELECT  va.date, 
                                va.bui, 
                                va.lava, 
                                va.f1 * (ls+1)/2 AS f1long, 
                                va.f1 * (1-ls)/2 AS f1short, 
                                va.f1,
                                va.ls 
                        FROM va"
                        ),
            fa=psz("CREATE ",type," favi AS 
                        SELECT  rece.date, 
                                rece.bui, 
                                rece.tare, 
                                rece.lace, 
                                rece.ret, 
                                rece.rer, 
                                rece.qua, 
                                rece.dev, 
                                ",expr1," , 
                                ",expr2,", 
                                rece.ret/POW(vat+1.e-20,.5) AS noret, 
                                rece.rer/POW(var+1.e-20,.5) AS norer 
                        FROM rece 
                        WHERE rece.date IN (SELECT da FROM ty WHERE ty.dapo IS NOT NULL )"
                        ),
            xy=psz("CREATE ",type," xyvi AS 
                        SELECT xy.*
                        FROM xy
                        WHERE xy.date IN(SELECT da FROM ty WHERE ty.dapo IS NOT NULL )"
                        )
            )
    if(nchar(xtype)==0) whereclause <- NULL else
    whereclause <- paste("WHERE",
        switch(xtype,
            "IN"=psz(" pa.",field," IN (",fldlst(xvalue,"'","'"),")"),
            "BETWEEN"=psz(" pa.",field," BETWEEN '",xvalue[1],"' AND '",xvalue[2],"'"),
            "="=psz(" pa.",field," = '",xvalue[1],"'")
            )
        )
    if(nchar(dtype)==0) andclause <- NULL else
    andclause <- switch(dtype,
            "IN"=psz(" pa.date IN (",fldlst(dvalue,"'","'"),")"),
            "BETWEEN"=psz(" pa.date BETWEEN '",dvalue[1],"' AND '",dvalue[2],"'"),
            "="=psz(" pa.date = '",dvalue[1],"'")
            )
    query <- paste(createclause,paste(paste(c(whereclause,andclause),collapse=" AND ")))
    vinam <- psz(domain,"vi")
    droptab(vinam)
    mysqlQuery(query)
    if(type=="TABLE" && ("ls" %in% dirfld(vinam))) mysqlQuery(paste("ALTER TABLE ",vinam," MODIFY ls TINYINT NOT NULL"))
    if(type=="TABLE") addidx(psz(domain,"vi"),fields=c("date","bui"))
}
customaggregates <-
function(n) 
{
    fnam=c("rem",rep("res",n-2),"rel")
    fnum=1:n
    stopifnot(length(fnam)==length(fnum))
    stopifnot(!any(fnam %in% c("ret","rer")) && grep(patt="^re",fnam)==1:n) #the custom names must start 're' & not be in reserved list (ret,rer)
    list(fnam=fnam,fnum=fnum)
}
keytyva <-
function() 
{   
    all(extract("va","DISTINCT date AS dapo, lava","WHERE lava IN (SELECT lag FROM lava) ORDER BY dapo, lava",res=data.frame)==
        extract("ty","DISTINCT dapo, lava","WHERE lava IN (SELECT lag FROM lava) AND dapo IS NOT NULL AND lava IS NOT NULL ORDER BY dapo, lava",res=data.frame))
}
keypova <-
function(tab="va") #all (dapo,po.bui) have corresponding va(dapo,po.bui) i.e. all positions have variance (for lava)
{   
    droptab("tmp1")     
    mysqlQuery("
        CREATE TABLE tmp1 AS 
            SELECT DISTINCT ty.*  
            FROM ty
            WHERE ty.lava IN (SELECT 
            lag FROM lava )
    ")
    addidx("tmp1","dapo")
    droptab("tmp")
    mysqlQuery("
        CREATE TABLE tmp AS 
            SELECT DISTINCT tmp1.da AS date, tmp1.dapo AS dapo, po.bui 
            FROM po INNER JOIN tmp1 
            ON po.date=tmp1.dapo 
            WHERE po.tapo=1 
    ")
    addidx("tmp",c("date","bui"))
    lava <- getla("va")
    x1 <- mysqlQuery(psz("
                    SELECT DISTINCT tmp.bui, tmp.date 
                    FROM tmp INNER JOIN va 
                    ON tmp.bui=va.bui AND tmp.dapo=va.date AND va.lava IN (",fldlst(lava),")
                    ORDER BY date, bui
                    "))
    x2 <- mysqlQuery("
                    SELECT DISTINCT tmp.bui, tmp.date 
                    FROM tmp 
                    ORDER BY date, bui
                    ")
    setdiff(paste(x2[,1],x2[,2]),paste(x1[,1],x1[,2])) #this is not empty!
    all.equal(x1,x2)
}
pepore <-
function() 
{
    droptab("tmp")
    mysqlQuery(psz("
            CREATE TABLE tmp AS
                SELECT DISTINCT ty.dapo, ty.da, po.bui
                FROM ty INNER JOIN po 
                ON ty.dapo = po.date
                WHERE lace=0
            "))
    addidx("tmp",c("dapo","bui"))
    droptab("tmp1")
    mysqlQuery(psz("
            CREATE TABLE tmp1 AS
                SELECT DISTINCT tmp.dapo, tmp.da, tmp.bui, pa.tapo
                FROM tmp INNER JOIN pa 
                ON tmp.dapo = pa.date AND tmp.bui=pa.bui
            "))
    addidx("tmp1",c("da","bui"))
    droptab("tmp2")
    mysqlQuery(psz("
            CREATE TABLE tmp2 AS
                SELECT DISTINCT tmp1.dapo, tmp1.da, tmp1.bui, tmp1.tapo, pa.tare
                FROM tmp1 INNER JOIN pa 
                ON tmp1.da = pa.date AND tmp1.bui=pa.bui
                WHERE pa.tare=1
            "))
    addidx("tmp2",c("da","bui"))
    petab <- extract("pe","DISTINCT date, bui, pet","WHERE lace=0 AND lapo=0")
    set1 <- apply(petab[,1:2],1,paste,collapse=" ")
    set2 <- apply(extract("tmp2","DISTINCT da, bui"),1,paste,collapse=" ")
    all(as.numeric(petab[which(!set1%in%set2),"pet",drop=FALSE])==0)  #pe which does not have tapo=tare=1
}
volpo <-
function(da=getda("no"),field=c("xc","xo","ti","xc-ti","xo-ti"),ce=getce(da),ttyp=c("gross","vol"))
{
    field <- match.arg(field)
    ttyp <- match.arg(ttyp)
    if(ttyp=="vol") {
        as.numeric(sqrt(apply(prdce(x=ce,po=getpo(da=da,field=field),scaletovol=FALSE,security=FALSE),2,sum)["vat"])) 
    } else {
        as.numeric(sum(abs(getpo(da=da,field=field))))
    }
}
tipofun <-
function(da,method,driver,poshorttyp,ttyp,jcon,pomaxmult,ngroups,invert,noshortco,dirallowed,dollar,region,poco,maxnet) 
{
    #print(psz("tipo ",da))
    target <- getpa(field="value",tab="co",ifield="date",jfield="control",qual=psz("WHERE apply='mid' AND date ='",da,"'"))[,'lever',drop=FALSE]
    if(invert && method=="neutral") { 
        ce <- getce(da)
        ten <- mysqlQuery(psz("
            SELECT bui, te, countryissue, minxo, maxxo
            FROM po
            WHERE date = '",da,"' AND pa IS NOT NULL AND tapo AND tapo1
            ORDER BY bui, date
            "))
        rownames(ten) <- ten[,"bui"]
        tab <- extract("po",c("date","bui","pa","ra","maxxo","minxo"),psz("WHERE date = '",da,"'"," AND pa IS NOT NULL AND tapo AND tapo1 ORDER BY bui, date"),result=data.frame)
        myargs <- list(
                            ce=ce,
                            fo=t(matrix(as.numeric(tab[,"ra"])-0.5,dimnames=list(tab[,"bui"],NULL))),   #the -0.5 centers the ranking & makes it reversible
                            categ=as.matrix(ten[tab[,"bui"],"te",drop=FALSE]),
                            categ2=as.matrix(ten[tab[,"bui"],"countryissue",drop=FALSE]),
                            jcon=jcon,
                            pomaxval=pomaxmult*2/nrow(tab),    #values of +/-2/n are just feasible
                            maxxo=as.matrix(ten[tab[,"bui"],"maxxo",drop=FALSE]),
                            minxo=as.matrix(ten[tab[,"bui"],"minxo",drop=FALSE]),
                            ngroups=ngroups,
                            region=region,
                            tgt=as.numeric(target),ttyp=ttyp,tol=1e-4,vcomp="T", #tol is adjusted inside tgt.solve.QP to achieve 1% accuracy in risk aversion
                            noshortco=noshortco,dirallowed=dirallowed,dollar=dollar,poco=poco,maxnet=maxnet
                            )
        tab[,"ti"] <- do.call(what="con.solve.QP",args=myargs)
        if(poco) {
            myargs$poco <- FALSE
            tab[,"tr"] <- do.call(what="con.solve.QP",args=myargs)
        } else {
            tab[,"tr"] <- tab[,"ti"]
        }
        sqlUpdate(channel=DBcon,
                dat=data.frame(tab[,c("date","bui","ti","tr")]),
                tablename="po",
                index=c("date","bui"))
    } else {
        dapoqual <- psz("WHERE tapo=1 AND tapo1=1 AND date ='",da,"'")
        lodir <- getpa(field="lodir",tab="po",class="matrix",qual=dapoqual) #this assumes that a security enters for more than one period, catch this condition at the end with ti=0
        pora <- getpa(field="ra",tab="po",class="matrix",qual=dapoqual) 
        if(method=="neutral") {
            poshort <- getpa(field="poshort",tab="po",class="matrix",qual=dapoqual) 
            a <- apply(lodir*pora,1,sum,na.rm=TRUE)/apply(lodir*poshort,1,sum,na.rm=TRUE)
            po <- pora-sweep(poshort,MARGIN=1,STATS=a,FUN="*")
            stopifnot(all.equal(as.numeric(apply(po*lodir,MARGIN=1,sum,na.rm=TRUE)),rep(0,nrow(lodir)))) #check neutrality
            if(((median(pora,na.rm=TRUE)==max(pora,na.rm=TRUE)) && poshorttyp=="evp")) stop("cannot hedge constant model with po.poshorttyp=evp")
        } else if(method=="asis") {
            po <- pora
        }
        stopifnot(identical(rownames(po),rownames(target)) && ncol(target)==1)
        b <- target/apply(abs(po),1,sum,na.rm=TRUE)
        w <- sweep(x=po, MARGIN=1, STATS=b, FUN="*")
        w[is.na(w)] <- 0 #where tapo=0 or tapo1=0 ; tradeout => ti=0
        sqlUpdate(channel=DBcon,
                dat=mattotab(w,field="ti"),
                tablename="po",
                index=c("date","bui"))
    }
}
raropofun <-
function(da,form)
{
    ten <- mysqlQuery(psz("
            SELECT te, count(*) AS n
            FROM po
            WHERE date = '",da,"' AND pa IS NOT NULL
            GROUP BY te
            "))
    levbu <- (as.numeric(ten[,2])-1)/sum(as.numeric(ten[,2])-1)
    for(i in seq_along(ten[,1])) {
        myte  <- ten[i,1]
        tab <- extract("po",c("date","bui","pa","ra"),psz("WHERE date = '",da,"'"," AND te = '",myte,"' AND pa IS NOT NULL"),result=data.frame)
        if(nrow(tab)>0) {
            if(form=="linear") {
                if(nrow(tab)>1) {
                    r <- rank(as.numeric(tab[,"pa"]))
                    tab[,"ra"] <- (r-1)/max(r-1)
                } else { 
                    tab[,"ra"] <- 0.5
                }
            } else if(form=="quadratic") {
               if(nrow(tab)>1) {
                    r <- rank(as.numeric(tab[,"pa"]))
                    rr <- (r-1)/max(r-1)    #0:1
                    tab[,"ra"] <- 2*((rr-0.5)*abs(rr-0.5))+0.5  #0:1
                } else { 
                    tab[,"ra"] <- 0.5
                }
            }  else if(form=="asis") {
               if(nrow(tab)>1) {
                    tab[,"ra"] <- tab[,"pa"]
                } else { 
                    tab[,"ra"] <- 0.
                }
            }
            sqlUpdate(channel=DBcon,
                    dat=data.frame(tab),
                    tablename="po",
                    index=c("date","bui"))
        }
    }

}
putwgtpo <-
function(
                wmax=-1,   #max position .05
                wmin=wmax,  #min position
                r2=2,      #weight rank above which no longs .95
                r1=-1,      #weight rank below which no shorts .25
                dirallowed=1,   #direction of allowed trade in noshortco
                epsilon=1.e-10,
                liquid=NA, #overrides other options, maxweight = liquid*turn/1e9 so for $1bn turnover maxxo=1
                ...
                )
{
    if(wmax==-1 && is.na(liquid)) return() #no constraints
    if(is.na(liquid)) {
        wgt <- getpa("weight","po")
        r <- t(apply(wgt,1,rank,na.last="keep"))-1
        mr <- apply(r,1,max,na.rm=T)
        ra <- sweep(r,STAT=mr,FUN="/",MAR=1)
        maxpo <- wmax*(r2-ra)/r2
        maxpo[is.na(maxpo)] <- 0
        maxpo[maxpo<0] <- +1.e-8
        minpo <- wmin*(r1-ra)/(1-r1)
        minpo[is.na(minpo)] <- 0
        minpo[0<minpo] <- -1.e-8
        tabmax <- mattotab(maxpo)
        tabmin <- mattotab(minpo)
        colnames(tabmax)[3] <- "maxxo"
        colnames(tabmin)[3] <- "minxo"
        sqlUpdate(channel=DBcon,
                dat=tabmax,
                tablename="po",
                index=c("date","bui"))    
        sqlUpdate(channel=DBcon,
                dat=tabmin,
                tablename="po",
                index=c("date","bui"))
    } else {
        stopifnot("turn"%in%dirfld())
        x <- getpa("turn")[as.Date(getda("po")),extract("po","distinct bui")]
        coredata(x)[is.na(coredata(x))] <- 1e6
        putpa(x=x*liquid/1e9,field="maxxo",tab="po")
        putpa(x=-x*liquid/1e9,field="minxo",tab="po")
    }
    #if(1<length(noshortco) || !is.na(noshortco)) mysqlQuery(psz("UPDATE po SET minxo=-1*",epsilon," WHERE countryissue IN (",fldlst(noshortco,"'","'"),")"))
    #if((1<length(noshortco) || !is.na(noshortco)) && dirallowed==0) mysqlQuery(psz("UPDATE po SET maxxo=",epsilon," WHERE countryissue IN (",fldlst(noshortco,"'","'"),")"))
}
puttepapo <-
function(
                tab="po",
                myte="countryfull",
                motab=c("pa","ce"),
                pa="mo",
                ...
                )
{
    motab <- match.arg(motab)
    stopifnot(myte %in% dirfld())
    copypa(ita=motab,ipa=pa,ota=tab,opa="pa")
    copypa(ita="pa",ipa=myte,ota=tab,opa="te")
    copypa(ita="pa",ipa="weight",ota=tab,opa="weight")
    copypa(ita="pa",ipa="countryissue",ota=tab,opa="countryissue")
}
puttatapo <-
function(evolve=TRUE) 
{
    mysqlQuery(psz("
        UPDATE po, pa
            SET po.tapo = pa.tapo,  po.evret = pa.retpa
            WHERE po.bui=pa.bui AND po.date=pa.date
        "))
    if(evolve) {
        mysqlQuery(psz("UPDATE po SET po.evret = 0 WHERE po.evret IS NULL")) #in particular for final date
    } else {
        mysqlQuery(psz("UPDATE po SET po.evret = 0"))
    }
    mysqlQuery(psz("
        UPDATE po, pa
            SET po.tapo1 = pa.tapo
            WHERE po.bui=pa.bui AND po.date=pa.minus1
        "))
    t2 <- extract("po","DISTINCT date","ORDER BY date DESC LIMIT 1")#max(getda("po"))
    mysqlQuery(psz(" 
        UPDATE po, pa
            SET po.tapo1 = pa.tapo
            WHERE po.bui=pa.bui AND pa.date='",t2,"' AND po.date='",t2,"'
        "))
    addidx("po","tapo")
    addidx("po","tapo1")
}
putlopo <-
function(
                lodirtyp=c("loadings1","betaevp","betamcp","betamvp")
                ) 
{
    lodirtyp <- match.arg(lodirtyp)
    if(lodirtyp=="loadings1") lodirtyp <- "loadings1*sdev"
    copypa(ita="ce",ipa=lodirtyp,ota="po",opa="lodir")
    #mysqlQuery("ALTER TABLE po MODIFY lodir DOUBLE NOT NULL") #null just in final period
}
putdasupo <-
function(tab="po") 
{
    mysqlQuery(psz("
        INSERT INTO ",tab," (date, bui)
            SELECT DISTINCT date AS date , bui 
            FROM pa
            WHERE tapo = '1' 
                AND date >= (SELECT MIN(dapo) FROM ty GROUP BY NULL)
                AND date <= (SELECT MAX(dapo) FROM ty GROUP BY NULL)
            UNION
            SELECT DISTINCT minus1 AS date , bui 
            FROM pa
            WHERE tapo = '1' 
                AND minus1 >= (SELECT MIN(dapo) FROM ty GROUP BY NULL)
                AND minus1 <= (SELECT MAX(dapo) FROM ty GROUP BY NULL)
            UNION
            SELECT DISTINCT plus1 AS date , bui 
            FROM pa
            WHERE tapo = '1' 
                AND plus1 >= (SELECT MIN(dapo) FROM ty GROUP BY NULL)
                AND pa.date <= (SELECT MAX(dapo) FROM ty GROUP BY NULL) #allows 1 future period
        "))
    addidx(tab=tab,fields=c("date"))
    addidx(tab=tab,fields=c("bui"))
}
putcepo <-
function(
                poshorttyp=c("evp","mcp","fmp1")
                )
{
    poshorttyp <- match.arg(poshorttyp)
    if(poshorttyp=="fmp1") poshorttyp <- "fmp1/sdev"
    mysqlQuery(psz("
        UPDATE po, ce
            SET po.poshort = ce.",poshorttyp," 
            WHERE po.bui=ce.bui AND po.date=ce.date
        "))
}
notnullpo <-
function(
                method=c("neutral","unit","asis")
                ) 
{
    method <- match.arg(method)
    if(method!="unit") {
        mysqlQuery(psz("ALTER TABLE po MODIFY te CHAR(60) NOT NULL"))
        mysqlQuery(psz("ALTER TABLE po MODIFY tapo TINYINT NOT NULL DEFAULT 1"))
        mysqlQuery(psz("ALTER TABLE po MODIFY tapo1 TINYINT NOT NULL DEFAULT 1"))
        mysqlQuery(psz("ALTER TABLE po MODIFY reason CHAR(60) NOT NULL"))
        mysqlQuery(psz("ALTER TABLE po MODIFY lever DOUBLE NOT NULL"))
        mysqlQuery(psz("ALTER TABLE po MODIFY neutral DOUBLE NOT NULL"))
        mysqlQuery(psz("ALTER TABLE po MODIFY alpha DOUBLE NOT NULL"))
        mysqlQuery(psz("ALTER TABLE po MODIFY countryissue CHAR(2) NOT NULL"))
    }
    mysqlQuery(psz("ALTER TABLE po MODIFY xt DOUBLE NOT NULL"))
    mysqlQuery(psz("ALTER TABLE po MODIFY xo DOUBLE NOT NULL"))
    mysqlQuery(psz("ALTER TABLE po MODIFY xc DOUBLE NOT NULL"))
}
getpo <-
function(da=getda("no"),
                field=c("xo","xc"),
                mode="numeric") 
{
    stopifnot(valda(da))
    tab <- extract(tab="po",field=paste(collapse=",",c("bui",field)),qual=psz("WHERE tapo=1 AND date = '",da,"'"))
    rownames(tab) <- tab[,"bui"]
    res <- tab[,field,drop=FALSE]
    mode(res) <- mode
    res
}
decpo <-
function() 
{
    structure(c(
                "date", "bui", "te", "lodir", "poshort", "pa", "tapo","tapo1", "polarity", "ra", "ro", "tr", "ti", "xt", "xo", "xc","reason", "lever", "neutral", "countryissue", "weight", "minxo","maxxo", "alpha", "evret", 
                "DATE", "CHAR(18)", "CHAR(60)", "DOUBLE","DOUBLE", "DOUBLE", "TINYINT", "TINYINT", "TINYINT", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "CHAR(60)","DOUBLE", "DOUBLE", "CHAR(2)", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", "DOUBLE", 
                "NOT NULL", "NOT NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"), 
            .Dim = c(25L, 3L), 
            .Dimnames = list(NULL, c("xfield", "xtype", "xnull")
            ))
}
compo <-
function(ipo=1:3,relever=1,channel=chve(),channel2=DBcon)
{
    stopifnot(all(ipo %in% getive("po",how="all")) && 1<=length(ipo))
    dst <- ixxtods(ixx=ipo,xx="po",channel=channel)
    intersectbui <- extract("vepo","DISTINCT bui",psz("WHERE datestamp='",dst[1],"'"),channel=channel)
    if(length(ipo)>1) {for(i in 2:length(ipo)) intersectbui <- intersect(intersectbui,extract("vepo","DISTINCT bui",psz("WHERE datestamp='",dst[i],"'"),channel=channel))}
    stopifnot(0<length(intersectbui))
    mysqlQuery("DELETE FROM po",channel=channel2)
    mysqlQuery(psz("       
        INSERT INTO ",nameve(channel2),".po (date, bui, xo, xc, xt, ti, tr, ra, ro, te, tapo, tapo1, reason, lever, neutral, countryissue, alpha, evret, lodir)
        SELECT MAX(date), MAX(bui), ",relever,"*SUM(xo), ",relever,"*SUM(xc), ",relever,"*SUM(xt), ",relever,"*SUM(ti), ",relever,"*SUM(tr), AVG(ra), AVG(ro), MAX(te), MAX(tapo), MAX(tapo1), MAX(reason), AVG(lever), AVG(neutral), MAX(countryissue), AVG(alpha), AVG(evret), AVG(lodir)
        FROM vepo
        WHERE datestamp IN (",fldlst(dst,"'","'"),")
        GROUP BY bui, date
        "),channel=channel)
    stopifnot(0<tabs("^po$",channel=channel2)[[1]][[1]][[4]])
}
tightperiod <-
function(da=getca(),n=3)
{
    stopifnot(n>0 && valda(da))
    i <- (match(da,getca()))%%n
    names(i) <- da
    i
}
tightflag <-
function(n=5,da=getda("po"),ioff=0)
{
    stopifnot(n>0)
    stopifnot(valda(da))
    i <- match(da,getca())+ioff
    as.numeric(i==n*floor(i/n))
}
thisperiodsu <-
function(da=thiswed("%Y-%m-%d"),n=3,addsu=NULL)#c(119,122))#addsu=c(117,120,123))
{
    stopifnot(n>0 && valda(da))
    i <- getive("su",how="all")
    sort(union(i[which(i%%n==tightperiod(da=da,n=n))],addsu))
}
mo2co <-
function(mo="mo")
{
    mysqlQuery(psz("
        UPDATE co, 
            (    
            SELECT DISTINCT x.date, x.std/z.avg AS scaling
            FROM 
                (
                    SELECT pa.date, SUM(ABS(pa.",mo,")) AS std
                    FROM pa
                    GROUP BY date
                ) AS x,
                (
                    SELECT AVG(y.std) AS avg
                    FROM
                        (
                        SELECT pa.date, SUM(ABS(pa.",mo,")) AS std
                        FROM pa
                        GROUP BY date
                        ) AS y
                ) AS z
            ) AS rep
        SET co.value=co.value*rep.scaling
        WHERE co.date=rep.date AND control='lever'
    "))
}
finalco <-
function(from=extract("co","max(date)"))
{
    pars <- as.numeric(extract("co","*",psz("where date='",from,"' ORDER BY control, apply"))[,4])
    list(levermid=pars[3],leverband=pars[2]-pars[3],neutralmid=pars[6],neutralband=pars[5]-pars[6])#,polarity=pars[8])
}
decco <-
function() 
{
    structure(c(
                "date", "control", "apply", "value", 
                "DATE","CHAR(18)","CHAR(18)", "DOUBLE", 
                "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL"), 
                .Dim = c(4L, 3L), 
                .Dimnames = list(NULL, c("xfield", "xtype", "xnull")
            ))
}
cofromsi <-
function(
                levermid=1.0,
                leverband=0.02,
                neutralmid=0.0,
                neutralband=0.02,
                alphamin=0.9,
                tightfactor=0,
                tightper=5,
                tightoffset=0
                )
{
    stopifnot(0<=tightfactor && tightfactor<=1 && tightper>0)
    tight <- tightflag(n=tightper,da=getda("po"),ioff=tightoffset)
    rbind( 
        cbind(
            date=getda("po"),
            control="lever",
            apply="min",
            value=levermid-leverband*(1-tight*tightfactor)
            ),
        cbind(
            date=getda("po"),
            control="lever",
            apply="mid",
            value=levermid
            ),
        cbind(
            date=getda("po"),
            control="lever",
            apply="max",
            value=levermid+leverband*(1-tight*tightfactor)
            ),
        cbind(
            date=getda("po"),
            control="neutral",
            apply="min",
            value=neutralmid-neutralband*(1-tight*tightfactor)
            ),
        cbind(
            date=getda("po"),
            control="neutral",
            apply="mid",
            value=neutralmid
            ),
        cbind(
            date=getda("po"),
            control="neutral",
            apply="max",
            value=neutralmid+neutralband*(1-tight*tightfactor)
            ),
        cbind(
            date=getda("po"),
            control="alpha",
            apply="min",
            value=alphamin + tight*tightfactor*(1-alphamin)
            ),
        cbind(
            date=getda("po"),
            control="polarity",
            apply="mid",
            value=1
            )
    )
}
prependxy <-
function()
{
    dace1 <- getda("ce")[1]
    teset <- gettexy(dace=dace1)
    #todo <- offda(x=min(extract("ty","dace","WHERE lace=0")),lags=getla("we"),method="apply")
    myte <- teset[1]
    for(myte in teset) {
        ro <- getro(dace=dace1,myte=myte)
        re <- getrete(dace=dace1,myte=myte,lacexy=getla("we"))
        stopifnot(all(rownames(re[[1]])==rownames(ro[[1]])))
        stopifnot(nrow(re[[1]])==nrow(ro[[1]]))
        buidate <- mattotab(re$ret)[,1:2]
        #mylacexy <- mattotab(matrix(NA,nrow(re$ret),ncol(re$ret),dimnames=list(1:nrow(re$ret),getla("we"))))[,2]
        mylacexy <- diffda(datum=dace1,da=colnames(re$ret))
        dfr <- data.frame(  
            date=buidate[,2],
            bui=buidate[,1],
            lacexy=mylacexy,
            ret=mattotab(re$ret)[,3],
            re1=mattotab(ro$x1%*%re$ret)[,3],
            re2=mattotab(ro$x2%*%re$ret)[,3],
            re3=mattotab(ro$x3%*%re$ret)[,3],
            yit=mattotab(re$yit)[,3],
            yi1=mattotab(ro$x1%*%re$yit)[,3],
            yi2=mattotab(ro$x2%*%re$yit)[,3],
            yi3=mattotab(ro$x3%*%re$yit)[,3],
            tare=mattotab(re$tare)[,3],
            tayi=mattotab(re$tayi)[,3]
            )
        colnames(dfr) <- c("date","bui","lacexy","ret","re1","re2","re3","yit","yi1","yi2","yi3","tare","tayi")
        sqlSave(channel=DBcon, 
            dat=dfr,
            tablename="xy",
            append=TRUE,
            rownames=FALSE,
            safer=TRUE
            )
    }
}
getro <-
function(dace=getda("ce")[1],myte="Financial") 
{
    stopifnot(valda(dace))
    stopifnot(valte(myte))
    lst <- list(x0=NULL,x1=NULL,x2=NULL)
    if(myte=="*") {qual <- ""} else {qual <-  psz("AND te IN (",fldlst(myte,"'","'"),")")}
    for(i in 1:3) {
        nam <- psz("x",i)
        lst[[nam]] <- tabtomat(extract("rovi",paste("bui,buix,",nam),psz("WHERE dace ='",dace,"'",qual)))
        mode(lst[[nam]]) <- "numeric"  
        lst[[nam]][is.na(lst[[nam]])] <- 0
    }
    lst
}
repa <-
function()
{
    addpa(c("rem","res","rer","relace")) 
    mysqlQuery("
    UPDATE pa, 
        (SELECT re.date, re.bui, re.lace, re.rem, re.res, re.rer
        FROM re INNER JOIN 
            (SELECT a.date, IF(maxlace<0,maxlace,0) AS maxlace
            FROM (
                SELECT DISTINCT date, MAX(lace) AS maxlace
                FROM re
                GROUP BY date
                ) AS a
            ) AS b
        WHERE b.date=re.date AND b.maxlace=re.lace
        )AS c
    SET pa.rem=c.rem, pa.res=c.res, pa.rer=c.rer, pa.relace=c.lace
    WHERE pa.bui=c.bui AND pa.date=c.date
    ")
}
refundly <-
function(dace)
{
    ce <- getce(dat=dace)
    mydates <- extract("red","date",psz("WHERE datew = '",dace,"'"))
    ret <- getpa('ret',
                tab='red',
                da=mydates,
                su=buice(ce))
    dfr1 <- mattotab(msrtce(ce=ce,x=ret))
    retv <- getpa('retv',
                tab='red',
                da=mydates,
                su=buice(ce))
    dfr2 <- mattotab(msrt(ce,retv))
    tare <- getpa('tare',
                tab='red',
                da=mydates,
                su=buice(ce))
    dfr3 <- mattotab(tare)
    dfr <- data.frame(cbind(dfr1,dfr2[,-c(1,2)],dfr3[,-c(1,2)]))
    stopifnot(ncol(dfr)==11)
    colnames(dfr) <- c("date","bui","rem","res","rer","ret","revm","revs","revr","revt","tare")
    sqlUpdate(channel=DBcon,
            dat=dfr,
            tablename="red",
            index=c("date","bui"))
}
refun <-
function(dace,mnem,from,rece,nn,k)
{
    ce <- getce(dat=dace)
    if(dace==getda("ce")[1]) {
            mydates <- offda(x=min(extract("ty","dace","WHERE lace=0")),lags=min(getla("we")):0,method="apply")   #initialise with history
            tanam1 <- "tall"    #for all
            tanam <- "tall"
    } else {
            mydates <- getty(key="dace",dates=dace,ret="da")
            tanam1 <- "tare"
            tanam <- psz("ta",mnem[nn]) #2010-04 changed from [j] to [nn], hardwired for nn=2?
    }
    pa <- getpa(from[1],
                da=mydates,
                su=buice(ce))
    tare <- getpa(tanam1,   
                da=mydates,
                su=buice(ce))
    if(any(is.na(pa))) coredata(pa)[which(is.na(pa))] <- coredata(msce(ce,natox(pa,0)))[which(is.na(pa))]
    rem <- mattotab(mktce(ce,pa),field=psz(mnem[1],"m"))
    res <- mattotab(sysce(ce,pa),field=psz(mnem[1],"s"))[,3,drop=FALSE]
    rer <- mattotab(resce(ce,pa),field=psz(mnem[1],"r"))[,3,drop=FALSE]
    lace <- match(rem[,"date"],getca())-match(dace,getca())
    tarem <- mattotab(tare,field="tare")[,3,drop=FALSE]
    if(rece) {
        sco <- coredata(scoce(ce,pa))
        fmp1 <- mattotab(sco,field="score",rclabel=c("date","fnum"))
        fmp1[,"fnum"] <- as.numeric(substr(fmp1[,"fnum"],4,5))
        gross <- t(sco) #these 4 lines a slightly clumsy way to rearrange gross fmp exposures into denormalised form...
        gross[] <- apply(abs(fmpce(ce)),2,sum)
        grosstab <- mattotab(t(gross))
        stopifnot(all(fmp1[,1]==grosstab[,1]) && all(fmp1[,2]==substr(grosstab[,2],4,nchar(grosstab[,2]))))#...check
        fmp <- cbind(fmp1,lace=match(fmp1[,"date"],getca())-match(dace,getca()),gross=grosstab[,3])

        qua <- mattotab(quace(ce,pa),field="qua")[,3,drop=FALSE]
        dev <- mattotab(devce(ce,pa),field="dev")[,3,drop=FALSE]
        lst <- face(ce,pa)
        vam <- genfrdce(ce) #matrix N x (2+k)
        ivam <- match(mattotab(tare)[,2],rownames(vam))
        rownames(vam) <- NULL #else get warnings for repeats
        lace <- match(rownames(lst[[1]]),getca())-match(dace,getca())
        dfr <- cbind(mattotab(lst[[1]])[,1:2],tarem,lace,dace,qua,dev)
        for(i in c("t","r",as.character(1:k))) { 
            dfr <- cbind(dfr, 
                        mattotab(lst[[psz("re",i)]],field=names(lst[psz("re",i)]))[,3,drop=FALSE],
                        vam[ivam,psz("va",i),drop=FALSE]
                        ) 
        }
        sqlSave(channel=DBcon,      
            dat=dfr,
            tablename="rece",
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
        sqlSave(channel=DBcon,      
            dat=fmp,
            tablename="fa1",
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
    }
    dfr <- cbind(tarem,lace,rem,res,rer,dace)
    if(nn>=2) {
        for(j in 2:nn) {
            pa <- getpa(from[j],
                    da=mydates,
                    su=buice(ce))
            taxx <- getpa(tanam,
                        da=mydates,
                        su=buice(ce))
            taxxm <- mattotab(taxx,field="tayi")[,3,drop=FALSE]
            if(any(is.na(pa))) coredata(pa)[which(is.na(pa))] <- coredata(msce(ce,natox(pa,0)))[which(is.na(pa))]
            tmp <- mattotab(mktce(ce,pa),field=psz(mnem[j],"m"))
            rem <- tmp[,3,drop=FALSE]
            res <- mattotab(sysce(ce,pa),field=psz(mnem[j],"s"))[,3,drop=FALSE]
            rer <- mattotab(resce(ce,pa),field=psz(mnem[j],"r"))[,3,drop=FALSE]
            stopifnot(all(tmp[,1:2]==dfr[,3:4]))
            dfr <- cbind(dfr,rem,res,rer,taxxm)
        }
    }
    sqlSave(channel=DBcon,      
        dat=dfr,
        tablename="re",
        append=TRUE,
        rownames=FALSE,
        safer=TRUE)
}
getre <-
function(bui=as.character(extract("re","DISTINCT bui")),
                da=getda(""),
                type="rem+res+rer AS ret",
                lace=0,
                tab="re") 
{
    stopifnot(all(bui %in% as.character(extract(tab,"DISTINCT bui"))))
    stopifnot(valda(da))
    stopifnot(all(lace %in% getla("ce")))
    tabtomat(mysqlQuery(psz("
        SELECT date, bui, ",type,"
        FROM ",tab,"
        WHERE bui IN (",fldlst(bui,"'","'"),")
        AND date IN  (",fldlst(da,"'","'"),")
        AND lace IN (",lace,")
        ")))
}
decre <-
function() 
{
    structure(c(
            "date", "bui",      "dace", "lace",     "tare",     "rem",      "res",      "rer",      "tayi",     "yim",      "yis",      "yir", 
            "DATE", "CHAR(18)", "DATE", "SMALLINT", "TINYINT",  "DOUBLE",   "DOUBLE",   "DOUBLE",   "TINYINT",  "DOUBLE",   "DOUBLE",   "DOUBLE", 
            "NOT NULL", "NOT NULL", "NOT NULL","NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL", "NOT NULL"), 
            .Dim = c(12L, 3L), 
            .Dimnames = list(NULL, c("xfield", "xtype", "xnull")
            ))
}
decfa <-
function() 
{
    structure(c(
            "type", "fnum", "var", 
            "CHAR(20)", "SMALLINT", "DOUBLE", 
            "NOT NULL", "NOT NULL", "NOT NULL"),
         .Dim = c(3L, 3L), 
         .Dimnames = list(NULL, c("xfield", "xtype", "xnull")
         ))
}
vixce <-
function(
                lawe=getla("we"),    #strictly negative, apply to both dvix and scores
                pa="prem"
                ) {
    dace <- as.Date(getda("ce"))
    vix <- getpa(fi="value",tab="aamacro",jfield="btk",qual="WHERE btk='vix'")[,"vix",drop=FALSE]
    res <- getpa()*NA
    dvix <- diff(vix)
    for(ida in seq_along(dace)) {
        print(ida)
        ce <- getce(as.character(dace[ida]))
        sco <- scoce(x=ce,ret=getpi(pa,as.character(dace[ida])))
        i <- as.Date(offda(dace[ida],la=lawe))
        ii<-i[!is.na(dvix[i,])]
        mylm <- summary(lm(dvix[ii,,drop=FALSE] ~ sco[ii,,drop=FALSE]),weights=1:length(ii))
        dd <- t(ldgce(ce)%*%mylm$coefficients[-1,1,drop=FALSE])
        ddz <- zoo(dd,dace[ida])
        res[match(dace[ida],index(res)),colnames(ddz)] <- ddz
    }
    addpa("vixce")
    putpa(res,"vixce")
}
valce <-
function(x) 
{
    result <- FALSE
    stopifnot(class(x)=="ce")
    stopifnot(identical(sort(names(x)),sort(c("loadings","fmp","hpl","method","full","uniqueness","sdev","qua","weight","call","mvp","mcp","evp","beta","poco"))))
    rn <- lapply(x,rownames)
    stopifnot(all(apply(cbind(rn$method,rn$full,rn$uniqueness,rn$sdev,rn$fmp,rn$mvp)==rn$loadings,1,all))) #rownames
    p <- ncol(x$loadings)
    stopifnot(identical(colnames(x$loadings),psz("loadings",1:p))) #colnames...
    stopifnot(identical(colnames(x$fmp),psz("fmp",1:p)))
    stopifnot(identical(colnames(x$hpl),psz("hpl",1:p)))
    stopifnot(identical(colnames(x$method),psz("method",1:p)))
    stopifnot(identical(colnames(x$full),"full"))
    stopifnot(identical(colnames(x$sdev),"sdev"))
    stopifnot(identical(colnames(x$uniqueness),"uniqueness"))
    stopifnot(!any(unlist(lapply(lapply(lapply(x[which(!(names(x)%in%c("mcp","beta")))],as.character),is.na),any))))   #no NA (possible in mcp)
    xmodes <- lapply(x,mode)
    stopifnot(xmodes$loadings=="numeric")
    stopifnot(xmodes$fmp=="numeric")
    stopifnot(xmodes$hpl=="numeric")
    stopifnot(xmodes$method=="character")
    stopifnot(xmodes$full=="logical")
    stopifnot(xmodes$uniqueness=="numeric")
    stopifnot(xmodes$sdev=="numeric")
    stopifnot(all.equal(as.numeric(t(x$fmp)%*%x$loadings),as.numeric(diag(ncol(x$loadings)))))
    stopifnot(all(as.numeric(t(x$hpl[,-1])%*%x$loadings[,-1])<1.e-10)) #zero exposure to other loadings
    #stopifnot(all.equal(abs(diag(t(x$hpl[fulce(x),-1,drop=FALSE])%*%abs(x$loadings[fulce(x),-1,drop=FALSE]))),rep(1,ncol(x$loadings)-1))) #only fails in DRB
    result <- TRUE
    result
}
putce <-
function(x,
                tabname="ce",
                dat=getda("no"))    
{
    stopifnot(class(x) %in% c("ce"))
    stopifnot(valda(dat) && length(dat)==1)
    if(dat %in% unique(extract(tabname,"date"))) stop(paste("ce already exists for date",dat))
    dfr <- dfrce(x,dat=dat)
    sqlSave(channel=DBcon,
        dat=dfr,
        tablename=tabname,
        append=TRUE,
        rownames=FALSE,
        safer=TRUE)
}
pomsv <-
function(
                jmin=1,
                jmax=ncol(getce()$loadings),
                type=c("net","gross"),
                pa="pomsv"
                )
{
    addpa(pa)
    type <- match.arg(type)
    for(da in getda("ce")) {
        print(da)
        ce <- getce(da)
        mi <- ldgce(ce)[,jmin:jmax,drop=FALSE]%*%t(fmpce(ce)[,jmin:jmax,drop=FALSE])
        if(type=="gross") mi <- abs(mi)
        bu <- apply(mi,1,sum)
        ijk <- data.frame(cbind(date=da,bui=names(bu),pa=bu))
        mode(ijk[,3]) <- "numeric"
        colnames(ijk)[3]<-pa
        sqlUpdate(channel=DBcon,
                    dat=ijk,
                    tablename="pa",
                    index=c("date","bui"))
    }
}
poce <-
function(x=getce()) 
{
    stopifnot(class(x)=="ce")
    bui <- buice(x)
    n <- length(bui)
    matrix(rep(1,n),n,1,dimnames=list(buice(x),NULL))
}
msrv <-
function()
{
    addpa(field=c("cem","ces","cer","cev"))
    mysqlQuery("UPDATE pa, ce SET pa.cem = POW(ce.loadings1,2)  WHERE pa.date = ce.date AND pa.bui = ce.bui")
    mysqlQuery("UPDATE pa, ce SET pa.cer = ce.uniqueness        WHERE pa.date = ce.date AND pa.bui = ce.bui")
    mysqlQuery("UPDATE pa, ce SET pa.ces = 1-pa.cem-pa.cer      WHERE pa.date = ce.date AND pa.bui = ce.bui")
    mysqlQuery("UPDATE pa, ce SET pa.cev = POW(ce.sdev,2)       WHERE pa.date = ce.date AND pa.bui = ce.bui")
}
idxce <-
function(create=c("CREATE","DROP"))
{
    create <- match.arg(create)
    addidx(tab="ce",field=c("date","bui"),create=create)
    addidx(tab="ce",field="date",create=create)
    addidx(tab="ce",field="bui",create=create)
}
getcedecomp <-
function(n=10,ca=customaggregates(getsione(("nfac")))) 
{
    dace <- getda("ce")
    if(n<2) n <- 2
    if(n>length(dace)) n <- length(dace)
    k <- getsione("nfac")
    i <- n-1
    ida <- floor(((0:i)/i)*(length(dace)-1)+1)
    myda <- dace[ida]
    for(dat in myda) {
        x <- getce(dat=dat)
        w <- fmpce(x)
        l <- apply(ldgce(x),2,mean)
        tab <- NULL
        for(i in 1:k) {
            tab <- rbind(tab,apply(prdce(x=x,po=w[,i,drop=F]),2,sum))
        }
        if(dat==myda[1]) {
            res <- tab/n
            avgldg <- (l/n)**2
        } else { 
            res <- res+tab/n
            avgldg <- avgldg+(l/n)**2
        }
    }
    stopifnot(length(ca[[1]])==nrow(res))
    lst <- as.list(unique(ca$fnam))
    names(lst) <- unique(ca$fnam)
    for(i in unique(ca$fnam)) {
        ii <- ca$fnam==i
        lst[[i]] <- weighted.mean(x=res[ii,"vat"],w=avgldg[ii])
    }
    lst
}
getce <-
function(dat=getda("no"),tab="ce",check=TRUE) 
{
    stopifnot(valda(dat) && length(dat)==1)
    cetab <- gettab(tabname=tab,
                qualifier=psz("WHERE date = ",fldlst(dat,"'","'")," ORDER BY bui, date"),
                return="dataframe",
                jrownames="bui")
    jbui <- grep("bui",colnames(cetab))
    jloadings <- grep("loadings",colnames(cetab))
    jfmp <- grep("fmp",colnames(cetab))
    jhpl <- grep("hpl",colnames(cetab))
    jmethod <- grep("method",colnames(cetab))
    jfull <- grep("full",colnames(cetab))
    juniqueness <- grep("uniqueness",colnames(cetab))
    jsdev <- grep("sdev",colnames(cetab))
    jqua <- grep("qua",colnames(cetab))
    jmvp <- match("mvp",colnames(cetab))
    jmcp <- match("mcp",colnames(cetab))
    jevp <- match("evp",colnames(cetab))
    jbeta <- grep("beta",colnames(cetab))
    jpoco <- grep("poco",colnames(cetab))
    bui <- cetab[,jbui,drop=TRUE]
    attributes(bui) <- NULL
    ifull <- which(cetab[,jfull]=="1")
    factors <- 1:ncol(cetab[,jloadings,drop=FALSE])
    ans <- list(
                loadings=as.matrix(cetab[,jloadings,drop=FALSE]), 
                fmp=as.matrix(cetab[,jfmp,drop=FALSE]),        
                hpl=as.matrix(cetab[,jhpl,drop=FALSE]),        
                method=as.matrix(cetab[,jmethod,drop=FALSE]),                   
                full=as.matrix(cetab[,jfull,drop=FALSE]),     
                uniqueness=as.matrix(cetab[,juniqueness,drop=FALSE]),
                sdev=as.matrix(cetab[,jsdev,drop=FALSE]),
                qua=as.matrix(cetab[,jqua,drop=FALSE]),
                evp=as.matrix(cetab[,jevp,drop=FALSE]),
                mcp=as.matrix(cetab[,jmcp,drop=FALSE]),
                mvp=as.matrix(cetab[,jmvp,drop=FALSE]),
                beta=as.matrix(cetab[,jbeta,drop=FALSE]),
                poco=as.matrix(cetab[,jpoco,drop=FALSE]),
                weight=NULL,
                call="restored"
                )
    #change mode for logical
    mode(ans$full) <- "numeric"
    mode(ans$full) <- "logical"
    #label
    dimnames(ans$loadings) <- list(bui,psz("loadings",factors))
    dimnames(ans$fmp) <- list(bui,psz("fmp",factors))
    dimnames(ans$hpl) <- list(bui,psz("hpl",factors))
    dimnames(ans$method) <- list(bui,psz("method",factors))
    dimnames(ans$full) <- list(bui,"full")
    dimnames(ans$uniqueness) <- list(bui,"uniqueness")
    dimnames(ans$sdev) <- list(bui,"sdev")
    dimnames(ans$evp) <- list(bui,"evp")
    dimnames(ans$mcp) <- list(bui,"mcp")
    dimnames(ans$mvp) <- list(bui,"mvp")
    dimnames(ans$beta) <- list(bui,c("evp","mcp","mvp"))
    dimnames(ans$poco) <- list(bui,"poco")
    class(ans) <- "ce"
    if(check) {stopifnot(valce(ans))}
    ans
}
dfrce <-
function(x,
                dat=getda("no")) 
{
    stopifnot(valda(dat) && length(dat)==1)
    stopifnot(class(x) %in% c("ce"))
    colnames(x$loadings) <- 1:ncol(x$loadings)  #fix colnames because data.frame prepends object name
    colnames(x$fmp) <- 1:ncol(x$fmp)
    colnames(x$hpl) <- 1:ncol(x$fmp)
    colnames(x$method) <- 1:ncol(x$method)
    res <- data.frame(
            date=dat,
            bui=as.matrix(rownames(x$loadings)),
            sdev=x$sdev,
            uniqueness=x$uniqueness,
            full=as.character(as.numeric(x$full)),
            loadings=x$loadings,
            fmp=x$fmp,
            hpl=x$hpl,
            method=metce(x),
            qua=x$qua,
            evp=x$evp,
            mcp=x$mcp,
            mvp=x$mvp,
            poco=x$poco,
            betaevp=x$beta[,"evp"],
            betamcp=x$beta[,"mcp"],
            betamvp=x$beta[,"mvp"]
            )
    names(res) <- gsub("\\.","",names(res))
    res
}
decce <-
function(k=getsione("nfac",def=20)) 
{
    dec <- rbind(
        c(  "date"              ,   "DATE"              ),
        c(  "bui"               ,   "CHAR(18)"          ),
        c(  "sdev"              ,   "DOUBLE"            ),
        c(  "uniqueness"        ,   "DOUBLE"            ),
        c(  "full"              ,   "BOOLEAN"           ),
        c(  "qua"               ,   "DOUBLE"            ),
        c(  "evp"               ,   "DOUBLE"            ),
        c(  "mcp"               ,   "DOUBLE"            ),
        c(  "mvp"               ,   "DOUBLE"            ),
        c(  "poco"              ,   "DOUBLE"            ),
        cbind(paste("beta",c("evp","mcp","mvp"),sep=""), "DOUBLE"),
        cbind(paste("loadings",1:k,sep=""), "DOUBLE"),
        cbind(paste("fmp",1:k,sep=""), "DOUBLE"),
        cbind(paste("hpl",1:k,sep=""), "DOUBLE"),
        cbind(paste("method",1:k,sep=""), "CHAR(1)")
        )
    dec <- cbind(dec,"NOT NULL")
    colnames(dec) <- c("xfield","xtype","xnull")
    dec[dec[,1]%in%c("mcp","betamcp","poco"),"xnull"] <- "NULL" #mcap may not be available
    dec
}
ceWrapper <-
function( 
                    dat, 
                    field, 
                    win, 
                    normalise, 
                    center, 
                    weight, 
                    nfac, 
                    lambda, 
                    mcap, 
                    bench, 
                    applyvix,
                    mixvix,
                    vixsmooth,
                    shrinkb,
                    ...) {
    pa <- getpi(date=dat,     
            field=field,       
            ta="tace",
            we=win)
    if(applyvix) {
        vix <- rollxts(x=getvix(mylag=-1),what="meantri",n=vixsmooth)
        vixinverse <- 1/(mixvix*vix + (1-mixvix)*rollapply(vix,6*52,mean,align="right",na.rm=T,fill=NA))
    }
    if(applyvix) { pa <- sweep(pa,MARGIN=1,FUN="*",STAT=vixinverse[index(pa),]/as.numeric(vixinverse[as.Date(dat),])) }
    if(normalise!="NONE") { pa <- zoonorm(pa,dimension=normalise) }
    fm <- fms2(pa,
                center=center,
                weight=weight,
                range.factors=c(nfac,nfac),
                lambda=lambda,
                shrinkb=shrinkb,
                ...)
    fm <- addbench(
            fm,
            mcap=mcap[match(as.Date(dat),index(mcap)),,drop=FALSE],
            bench=bench)
    putce(x=fm,
        tabname="ce",
        dat=dat)
}
addbench <-
function(
            ce,
            mcap=t(ce$loadings[,1,drop=FALSE]*NA), 
            bench=c("equal","minvar","mcap")
            )
{
    bench <- match.arg(bench)
    stopifnot(all(buice(ce)%in%colnames(mcap)))
    bui <- buice(ce)
    n <- length(bui)
    mcap <- coredata(mcap)[,bui,drop=TRUE] #nb no lagging is done to mcap, should already be lagged 1 period (ie opening mcap)
    if(mean(!is.na(mcap))<0.1) mcap <- mcap*NA      #insufficient data for cap-weighted
    if(mean(!is.na(mcap))>0) mcap[is.na(mcap)] <- min(mcap,na.rm=TRUE) #na assigned minimum weight
    mcap[mcap==0] <- min(mcap[mcap!=0],na.rm=TRUE) #0 assigned minimum weight
    vv <- vcvce(ce)$T
    stopifnot(all(colnames(vv)==colnames(mcap)))
    mvp <- solve(vv,rep(1,ncol(vv)))
    evp <- rep(1/ncol(vv),ncol(vv))
    mcp <- as.numeric(mcap/sum(mcap,na.rm=TRUE))
    mvp <- mvp/sum(mvp)
    evp <- evp/sum(abs(evp))
    ce$mvp <- matrix(mvp,n,1,dimn=list(bui,NULL))
    ce$evp <- matrix(evp,n,1,dimn=list(bui,NULL))
    ce$mcp <- matrix(mcp,n,1,dimn=list(bui,NULL))
    bpf <- switch(bench,
                equal=ce$evp,
                minvar=ce$mvp,
                mcap=ce$mcp
                )
    ce$beta <- cbind(
                vv%*%evp*1./as.numeric(((t(evp)%*%vv)%*%evp)),
                vv%*%mcp*1./as.numeric(((t(mcp)%*%vv)%*%mcp)),
                vv%*%mvp*1./as.numeric(((t(mvp)%*%vv)%*%mvp))
                )
    dimnames(ce$beta) <- list(bui,c("evp","mcp","mvp"))
    pol <- as.numeric(sign(t(ldgce(ce))%*%bpf))
    pol[which(pol==0)] <- 1
    ce$bench <- bench
    ce$fmp <- sweep(ce$fmp,STAT=pol,MAR=2,FUN="*")
    ce$hpl <- sweep(ce$hpl,STAT=pol,MAR=2,FUN="*")
    ce$loadings <- sweep(ce$loadings,STAT=pol,MAR=2,FUN="*")
    ce$poco <- matrix(0,n,1,dimn=list(bui,NULL))
    ce
}
valte <-
function(te) 
{
    te %in% extract("texy","DISTINCT te")
}
tetree <-
function(tree=c("bb","icb")) {
    tree <- match.arg(tree)
    list(bb=c("industry_sector","industry_group","industry_subgroup"),icb=c("icb_industry_name","icb_sector_name","icb_subsector_name"))
}
gettexy <-
function(dace=getda("ce")) 
{
    stopifnot(valda(dace) && all(dace %in% getda("ce")))
    sort(unique(extract("texy","te",psz("WHERE dace IN (",fldlst(dace,"'","'"),")"))))
}
getrete <-
function(dace=getda("cexy")[1],
                lacexy=getla("cexy")[1],
                myte="Financial") 
{
    stopifnot(mode(dace) %in% c("character")  && class(dace) %in% c("character") )
    stopifnot(valla(lacexy) )
    stopifnot(valte(myte))
    tab <- mysqlQuery(psz(
            "SELECT DISTINCT date, bui, rem+res+rer AS ret, tare AS tare, yim+yis+yir as yit, tayi AS tayi 
            FROM rete, lacexy
            WHERE rete.dace IN (",fldlst(pre="'",post="'",field=dace),") AND rete.te = '",myte,"' AND rete.lace IN (",fldlst(pre="'",post="'",field=lacexy),")
            ORDER by bui"
            ))
    list(ret=t(tabtomat(tab[,c("date","bui","ret")])),
        yit=t(tabtomat(tab[,c("date","bui","yit")])),
        tare=t(tabtomat(tab[,c("date","bui","tare")])),
        tayi=t(tabtomat(tab[,c("date","bui","tayi")]))
        )
}
agtepa <-
function(
            nmin=10,            #minimum multiplicity
            field="industry1",  #output panel
            tab="pa",           #must contain all sector levels - see agte1
            ifield="industry_subgroup",
            reda=NA,#exitsuda()     #if non-NA, re-prune on these dates
            tree=c("bb","icb")
            )
{
    tree <- match.arg(tree)
    if(ifield!=field && !(field%in%dirfld(tab)) && tab=="pa") addpa(field,use=ifield) #legacy behaviour
    agte1(nmin=nmin,field=field,tab=tab,tree=tree)
    mysqlQuery(paste(sep="","
        UPDATE ",tab,", tmp
        SET ",tab,".",field,"=tmp.kx3
        WHERE ",tab,".",ifield,"=tmp.k3
    "))
    if(!any(is.na(reda))) {
        dapo <- getda("po")
        if(any(reda%in%dapo)) {myda <- reda[reda%in%getda("po")] } else {myda <- dapo[1]}
        for(i in seq_along(myda)) {
            print(myda[i])
            agte1(nmin=nmin,field=field,tab=tab,qual=psz("WHERE date='",myda[i],"' AND tasu"),tree=tree) 
            mysqlQuery(paste(sep="","
                UPDATE ",tab,", tmp
                SET ",tab,".",field,"=tmp.kx3
                WHERE ",tab,".",ifield,"=tmp.k3 AND date>'",myda[i],"'
            "))
        }
    }
}
agte1 <-
function(
            nmin=10,            #minimum multiplicity
            field="industry1",   #output panel
            tab="pa",
            qual="",
            tree=c("bb","icb")
            )
{
    tree <- match.arg(tree)
    mylevels <- tetree()[[tree]]
    stopifnot(length(getsu())>nmin)
    stopifnot(all(c("bui",mylevels) %in% dirfld(tab)))
    droptab("tmp")
    mysqlQuery(paste(sep="","
    CREATE TABLE tmp AS
    SELECT DISTINCT 'Supersector' AS k0, k1, k2, k3, COUNT(*) AS mycount , k2 AS kx1, k2 AS kx2, k3 AS kx3
    FROM 
        (SELECT DISTINCT bui, ",mylevels[1]," AS k1, ",mylevels[2]," AS k2, ",mylevels[3]," AS k3
        FROM ",tab," ",qual,"
        GROUP BY bui) AS a
    GROUP BY k3
    "))
    agte(k1="k0",k2="k1",k="kx1",nm=nmin)
    agte(k1="kx1",k2="k2",k="kx2",nm=nmin)
    agte(k1="kx2",k2="k3",k="kx3",nm=nmin)
    addidx("tmp","k3")
    #stopifnot(all(as.numeric(extract("tmp","sum(mycount)","group by kx3"))>=nmin)) #this is not guaranteed, can be eg singleton at top level
}
agte <-
function(
            k1="k1",        #less granular category column
            k2="k2",        #more granular category column
            k="k",          #destination category column
            count="mycount",#field containing multiplicity
            nmin=2,         #minimum required per category
            tab="tmp")      #table name
{
    countstring <- paste(sep="","SUM(",count,")")
    stopifnot(tab%in%dirtab() && all(c(k1,k2,k)%in%dirfld(tab)))
    stopifnot(as.numeric(extract(tab,countstring))>nmin)
    mysqlQuery(paste(sep="","
        UPDATE ",tab,"
        SET ",k,"=",k2,"
    "))
    k1set <- unique(extract(tab,k1))
    for(k1v in k1set) {
        subtab <- paste(sep="","(SELECT * FROM ",tab," WHERE ",k1,"='",k1v,"') AS b")
        k2nc <- as.matrix(
            mysqlQuery(paste(sep=""," 
                SELECT ",k2,", nc
                FROM 
                    (SELECT ",k2,",",countstring," AS nc
                    FROM ",subtab,"
                    GROUP BY ",k2,") AS a
                ORDER BY nc ASC
            ")))
        if(any(as.numeric(k2nc[,2])<nmin)) {
            if(sum(as.numeric(k2nc[,2]))<nmin) stop()
            ii1 <- max(which(as.numeric(k2nc[,2])<nmin))    #1:ii1 are subsized
            ii2 <- min(which(cumsum(as.numeric(k2nc[,2]))>=nmin))   #1:ii2 are required to be promoted, if any are
            k1subset <- k2nc[1:max(ii1,ii2),1] #
            mysqlQuery(paste(sep="","   
                UPDATE ",tab,"
                SET ",k,"=",k1,"
                WHERE ",k," IN (",fldlst(k1subset,"'","'"),")
            "))
        }
    }
}
valta <-
function(x)
{
    valpa(x) && mode(x)=="character" && all(as.character(x) %in% c("0","1"))
}
testlags <-
function(resultfield="tare",    #results stored here
                testfield="prlo",        #field to test
                mylags=c(-1,0),             #lags to test
                logic="AND"                 #logic used to combine
                ) 
{
    stopifnot(testfield%in%dirfld() && resultfield%in%dirfld())
    stopifnot(all(mylags %in% txttola(dirfld())))   #the lags need to exist as panels 
    if(!"temp"%in%dirfld()) addpa("temp")   #assumes the panel is numeric
    for(i in mylags) {
        ilag <- latotxt(i)
        mysqlQuery("UPDATE pa SET pa.temp = NULL")
        mysqlQuery(psz("UPDATE pa AS pa, pa AS lag SET pa.temp = lag.",testfield," WHERE pa.",ilag,"=lag.date AND pa.bui=lag.bui"))
        mysqlQuery(psz("UPDATE pa SET pa.",resultfield," = pa.",resultfield," ",logic," NOT ISNULL(temp)"))
    }
}
tasuderive <-
function(ta="tasu")
{
    mysqlQuery(psz(" 
            UPDATE pa, su 
            SET pa.",ta," = TRUE
            WHERE pa.bui = su.bui
            AND pa.date =su.date
            "))
}
tapaany <-
function(field="prlo",prior=TRUE,ta="tare",...)
{
    stopifnot(field %in% dirfld())
    stopifnot(is.logical(prior))
    if(prior) mylags <- c(-1,0) else mylags <- 0
    testlags(resultfield=ta,     
                testfield=field,        
                mylags=mylags,            
                ...                
                )
}
taextend <-
function(tab="pa",mycol="tapo",k=1)
{
    stopifnot(all(as.numeric(coredata(getpa(mycol,tab)))%in%c(0,1)))
    stopifnot(all(0<k))
    dafinal <- as.character(extract(tab,"DISTINCT date",psz("WHERE ",mycol," ORDER BY date DESC LIMIT 1")))
    dfr <- extract(tab,psz("date, bui, ",mycol),psz("WHERE date='",dafinal,"'"),res=data.frame)
    for(i in 1:k){
        dfr[,"date"] <- offda(dafinal,i)
        sqlUpdate(channel=DBcon,
                    dat=dfr,
                    tablename=tab,
                    index=c("date","bui"))
    }
}
tacederive <-
function() 
{
    if(!"tace" %in% dirfld()) addfld(tabname="pa",tab=tabta("ce"))
    xl <- min( c(0 , min(getla("po"))-1*max(getla("ce")) ))
    xl <- min( c(xl, min(getla("wo"))) )           #[was -1] this is a misunderstanding of what lawo is: lawo applies to t and not TC ie lawo cannot imply a greater range for TC
    xr <- max( c(0 , max(getla("po"))-1*min(getla("ce")) ))
    xr <- max( c(xr, 1) )           #[was max(getla("wo")) but this causes tradeout problem] ditto, but lawo is being used to add a 'safety margin' which is required eg for tradeout if lace = {0}
    extendleft <- 1+abs(xl)
    extendright <- 1+abs(xr)
    ta <- getpa("tasu")
    tar <- zoo(focb.mat(coredata(rollmaxfix(x=ta,k=extendright,fill=NA,align="right"))),index(ta))    #remembers
    tal <- zoo(locf.mat(coredata(rollmaxfix(x=ta,k=extendleft,fill=NA,align="left"))),index(ta))      #anticipates
    mytace <- zbtc(tar=="1"|tal=="1")
    rownames(mytace) <- as.character(index(mytace))
    putpa(mytace,"tace")
    addidx("pa","tace")
}
tabta <-
function(ta="tace") 
{
    structure(
        c(ta, "tinyint(1)", "NOT NULL", "1"), 
        .Dim = c(1L, 4L), 
        .Dimnames = list(NULL, c("xfield", "xtype", "xnull", "xdefault"))
        )
}
sumta <-
function()
{
    print(paste("ca", getca()[1],getca()[length(getca())],length(getca())))
    name <- "pa"
    print(paste("da",name, getda(name)[1],getda(name)[length(getda(name))],length(getda(name))))
    print(getdu())
    print(paste("cu",nrow(gettab("cu")),"rows"))
    print(paste("su",length(getsu()),"rows"))
    print(tabs("pa")[[1]][[3]])
    print("current simulation date summary")
    z <- getpa("tasu")[getda("no"),]
    print(paste("ta 0/1   : ",sum(z=="0"),"/",sum(z=="1")))
}
rollmaxfix <-
function(x,k,...) {if(k==1) {return(x)} else {return(rollmax(x=x,k=k,...))}}
delta <-
function()
{
    delfld(tabname="pa",
            field=dirfld(tabname="pa",pattern="^ta"))
}
yearpa <-
function()
{
    x <- getpa()
    x[]  <- substr(as.character(index(x)),1,4)
    addpa("qqqyear")
    putpa(x,field="qqqyear")
}
weightsummary <-
function(gsf=c("MAX","MIN","SUM"),icu=aacu(),channel=chve(),channel2=DBcon)
{
    gsf <- match.arg(gsf)
    ds <- ixxtods(xx="cu",ixx=icu)
    x <- tabtomat(mysqlQuery(psz("
            SELECT DISTINCT pa.date, pa.bui, a.weight 
            FROM pa INNER JOIN 
                (SELECT bui, cucaprior.date, ",gsf,"(cu.weight) AS weight
                FROM ",nameve(channel),".vecuvi AS cu INNER JOIN cucaprior
                WHERE cu.date=cucaprior.idate AND datestamp='",ds,"'
                AND weight IS NOT NULL
                GROUP BY date, bui
                ) AS a
            ON pa.bui=a.bui AND pa.date=a.date 
            ")))
    mz(x)
}
weightpa <-
function(icu=aacu())
{
    addpa("weight")
    x <- weightsummary("MAX",icu=icu)
    xmin <- weightsummary("MIN",icu=icu)
    stopifnot(identical(x,xmin)) #check that weight is unique; should this fail, cu will need different handling (link to jo) OR pa.weight eliminate as only used in po
    putpa(mz(x),"weight")
    putpa(locf.local(getpa("weight")),"weight")
}
valpa <-
function(x) 
{
    res <- TRUE
    res <- res && is(x,"zoo") || is(x,"data.frame") || is(x,"matrix")
    res <- res && all(as.character(index(x))==getda("pa")) && identical(sort(colnames(x)),sort(getsu()))
    res
}
tecountpa <-
function(
                myte="icb_subsector_name",
                mypa="tecount",
                asof=getda("po")
                )
{
    delfld("pa",mypa)
    mysqlQuery(psz("ALTER TABLE pa ADD COLUMN ",mypa," CHAR(5)"))
    droptab("tmpte")
    mysqlQuery(psz("
        CREATE TABLE tmpte AS
        SELECT ",myte,", MAX(c1) AS c1
        FROM
        (SELECT ",myte,", date, count(*) AS c1
            FROM pa
            WHERE pa.date IN (",fldlst(asof,"'","'"),")
            GROUP BY ",myte,", date) AS a
        GROUP BY ",myte
    ))
    addidx("tmpte",myte)
    mysqlQuery(psz("
        UPDATE pa, tmpte
        SET pa.",mypa,"=LPAD(tmpte.c1,3,'0')
        WHERE pa.",myte,"=tmpte.",myte))
    #droptab("tmpte")
}
qupa <-
function(field="mcap",
                n=5,                #number of quantiles            
                qu="qu",            #output fieldname in pa
                mylag="minus1",     #value is assigned pa[mylag,]<-value[date,] so minus1 for a right shift (delay)
                ta="tall"           
                )
{
    pa1 <- getpa(field,qual=ifelse(is.null(ta),NULL,psz("WHERE ",ta)))
    i <- n<=apply(!is.na(pa1),1,sum)
    pa <- pa1[i,,drop=FALSE]
    if(n>0) {
        res <- zoo(t(apply(X=coredata(pa),MARGIN=1,FUN=quan,n=n)),index(pa))    
    } else {
        res <- zoo(t(apply(coredata(pa),1,rank)),index(pa))    
    }    
    delfld("pa",qu)
    addfld(tabname="pa",tab=cbind(xfield=qu,xtype="CHAR(8)",xnull="NULL"))
    mode(res) <- "character"
    coredata(res)[!is.na(coredata(res))] <- psz("q",coredata(res)[!is.na(res)])
    coredata(res)[is.na(coredata(res))] <- "NoQ"
    colnames(res) <- colnames(pa)
    putpa(res,field=qu,ifield=mylag)
}
putpa <-
function(x,                
                field,
                tab="pa",
                ifield=c("date","minus1","minus0","plus1")  #date in x is renamed to ifield, then matched on this field, so minus1 is a right shift (delay)
                )
{
    if(length(ifield)>1) ifield <- ifield[1]
    stopifnot(mode(x) %in% c("character","numeric")  && is(x,"zoo") && nrow(x)>0)
    stopifnot(mode(field) %in% c("character")  && class(field) %in% c("character") && length(field)==1)
    stopifnot(all(as.character(index(x)) %in% extract(tab,"date")) && all(colnames(x) %in% extract(tab,"DISTINCT bui")))
    if(!is.null(rownames(x))) stopifnot(all(rownames(x)==as.character(index(x)))) else rownames(x) <- as.character(index(x))
    sqlUpdate(channel=DBcon,
            dat=mattotab(x,field=field,fieldmode=mode(x),rclabel=c(ifield,"bui")),
            tablename=tab,
            index=c(ifield,"bui"))
}
prpa <-
function(z,sufield="name")
{
    stopifnot("name" %in% dirfld("su"))
    pr <- extract("su",c("bui",sufield))
    colnames(z) <- pr[match(colnames(z),pr[,"bui"]),sufield]
    z
}
numpa <-
function(nam="tmp",xtype="DOUBLE",tab="pa") 
{
    stopifnot(mode(nam)=="character" && class(nam)=="character")
    stopifnot(!any(nam %in% dirfld(tab)))
    for(i in seq_along(nam)) addfld(tabname=tab,tab=cbind(xfield=nam[i],xtype=xtype))
}
kpala <-
function(tab="pala") {
    fld <- dirfld(tab)
    string <- fld[max(grep(x=fld,patt="x"))]
    as.numeric(substr(string,2,nchar(string)))
}
imputepa <-
function(
                    ipa="prem",
                    iterations=2
                    ) {
    x <- getpa(ipa)
    dace <- as.Date(getda("ce"))
    for(i in seq_along(dace)) {
        ce <- getce(as.character(dace[i]))
        ida <- match(dace[i],index(x))
        jx <- which(colnames(x)%in%buice(ce))
        stopifnot(length(jx)>0)
        rhs <- x[ida,jx,drop=FALSE]
        if(any(is.na(rhs))) {
            jna <- which(is.na(rhs))
            rhs[,jna] <- 0
            for(j in 1:iterations) {
                rhs[,jna] <- coredata(msce(ce,rhs))[,jna]
            }
            x[ida,jx] <- rhs
        }
    }
    x
}
imputebestnoob <-
function(
                    best,
                    maxperiods=1/0  #maximum periods to apply 
                    )
{
    jj <- (apply(!is.na(coredata(best)),2,sum)>0)
    j <- names(jj)[jj]
    x <- best[,j]
    scr2 <- scr1 <- x
    scr1[!is.na(x)] <- 1    #non-NA=1, rest=NA
    scr2[!is.na(x)] <- 0    #non-NA=0, rest=1
    scr5 <- locf.local(scr1,maxperiods)
    scr2[is.na(x)] <- 1
    dbest <- dbestzna <- diff(log(locf.local(x)))*scr1[-1,]
    coredata(dbestzna)[is.na(dbestzna)] <- 0
    addpa("dbest")
    putpa(dbest,"dbest")
    x1 <- imputepa("dbest")[,j]
    x1[is.na(x1)] <- 0  #eg dates with no obsvns
    x2 <- locf.local(cumsum(x1*scr2)*scr1)  #only imputed changes, cumulated
    x3 <- diff(x2)
    yy <- as.numeric(dbest)[!is.na(dbest)]
    xx <- as.numeric(x3)[!is.na(dbest)]
    frac <- summary(lm(yy~xx))$coefficients[2,1]
    #put together in log domain
    imin <- as.numeric(lapply(apply(!is.na(coredata(x)),2,which),min)) #+1
    j1 <- seq_along(imin)   
    scr3 <- scr2*0
    scr3[cbind(imin,j1)] <- 1
    scr3 <- cumsum(scr3)
    res <- x
    res[] <- 0
    #1 - entirely imputed log change
    res <- res+x1*scr2*scr3*frac #+1
    #2 - correction to bring this to best
    res <- res+rbind(x[1,,drop=FALSE]*NA,(dbestzna-x3*frac)) #+1
    #3 - initial value, otherwise zero
    res[cbind(imin,j1)] <- log(coredata(x)[cbind(imin,j1)])
    scr4 <- scr3
    scr4[scr4==0] <- NA
    res[is.na(res)] <- 0
    rr <- exp(cumsum(res))*scr4*scr5
    rr
}
imputebest <-
function(
                    best,
                    maxperiods=1/0  #maximum periods to apply 
                    )
{
    jj <- (apply(!is.na(coredata(best)),2,sum)>0)
    j <- names(jj)[jj]
    x <- best[,j]
    scr2 <- scr1 <- x
    scr1[!is.na(x)] <- 1    #non-NA=1, rest=NA
    scr2[!is.na(x)] <- 0    #non-NA=0, rest=1
    scr5 <- locf.local(scr1,maxperiods)
    scr2[is.na(x)] <- 1
    dbest <- dbestzna <- diff(log(locf.local(x)))*scr1[-1,]
    coredata(dbestzna)[is.na(dbestzna)] <- 0
    addpa("dbest")
    putpa(dbest,"dbest")
    x1 <- imputepa("dbest")[,j]
    x1[is.na(x1)] <- 0  #eg dates with no obsvns
    x2 <- locf.local(cumsum(x1*scr2)*scr1)  #only imputed changes, cumulated
    x3 <- diff(x2)
    yy <- as.numeric(dbest)[!is.na(dbest)]
    xx <- as.numeric(x3)[!is.na(dbest)]
    frac <- summary(lm(yy~xx))$coefficients[2,1]
    #put together in log domain
    imin <- as.numeric(lapply(apply(!is.na(coredata(x)),2,which),min)) #+1
    j1 <- seq_along(imin)   
    scr3 <- scr2*0
    scr3[cbind(imin,j1)] <- 1
    scr3 <- cumsum(scr3)
    res <- x
    res[] <- 0
    #1 - entirely imputed log change
    res <- res+x1*scr2*scr3*frac #+1
    #2 - correction to bring this to best
    res <- res+rbind(x[1,,drop=FALSE]*NA,(dbestzna-x3*frac)) #+1
    #3 - initial value, otherwise zero
    res[cbind(imin,j1)] <- log(coredata(x)[cbind(imin,j1)])
    scr4 <- scr3
    scr4[scr4==0] <- NA
    res[is.na(res)] <- 0
    rr <- exp(cumsum(res))*scr4*scr5
    rr
}
getpi <-
function(
                field="prem",           #cannot be an expression due to the paste on the SELECT line
                date=getda("no"),       #dace must also exist
                ta="tace",              #estimation tradecondition
                we=as.numeric(getla("we")),       #lags in calendar periods
                minsam=length(we),
                tabname="pa",
                jfield="bui"
                )      #must have this many periods within ca, or error
{
    stopifnot(mode(date) %in% c("character")  && class(date) %in% c("character") && length(date)==1 && date %in% getda("pa"))
    stopifnot(mode(field) %in% c("character")  && class(field) %in% c("character") && length(field)==1)
    stopifnot(field%in%dirfld(tabname))
    stopifnot(mode(ta) %in% c("character")  && class(ta) %in% c("character") && length(ta)==1  && ta %in% dirfld(tabname=tabname))
    stopifnot(mode(we) %in% c("numeric")  && class(we) %in% c("numeric","integer") && length(we)>=1)
    daywe <- lacada(lags=we,x=date)
    stopifnot(length(daywe)>=minsam)
    dbx <- mysqlQuery(psz("
                SELECT ",fldlst(c("date",jfield,field),"pa."),"  #pa.date, pa.bui, pa.",field,"
                FROM ",tabname," AS pa, ",tabname," AS ta              
                WHERE DATEDIFF(pa.date, ta.date) BETWEEN ",min(daywe)," AND ",max(daywe),"  #converts periods to days
                AND pa.",jfield," = ta.",jfield,"
                AND ta.date = '",date,"'
                AND ta.",ta
                ))
    mz(tabtomat(dbx))
}
getpa <-
function(field="prlo",   #field can be an expression
                tab="pa",
                ifield="date",
                jfield="bui",
                da=NULL,
                su=NULL,
                qual=NULL,
                modeout=NULL,
                classout="zoo",
                check=TRUE
                ) 
{
    if(check) {stopifnot(mode(field) %in% c("character")  && class(field) %in% c("character") && length(field)==1)}
    padir <- dirfld(tab,check=check)
    if(!is.null(da)) qualda <- paste(ifield," IN (",fldlst(da,"'","'"),")") else qualda <- NULL
    if(!is.null(su)) qualsu <- paste(jfield," IN (",fldlst(su,"'","'"),")") else qualsu <- NULL
    if(!is.null(da) || !is.null(su)) allqual <- paste(qual, "WHERE",paste(c(qualda,qualsu),collapse=" AND ")) else allqual <- qual
    dbx1 <- extract(tabname=tab,
                fields=fldlst(c(ifield,jfield,field)),
                result=data.frame,
                qual=allqual)
    dbx <- dbx1[!is.na(dbx1[,ifield])&!is.na(dbx1[,jfield]),,drop=FALSE]
    if(length(grep("Unknown column",dbx[2,1]))>0) stop(paste("Unknown field '",field,"' requested in getpa",sep=""))
    res <- tabtomat(dbx)
    if(!is.null(modeout)) mode(res) <- modeout
    if(classout=="zoo") res <- mz(res) else res <- as(res,classout)
    res
}
getfr <-
function(field="prlo",lag=0,tacondition=FALSE,ta="tare",tab="pa") 
{
    stopifnot(field%in%dirfld(tab))
    stopifnot(latotxt(lag) %in% names(getcala()))
    tafilt <- if(tacondition) {
        paste("AND",ta,"=TRUE")
    } else {
        ""
    }
    getpa(tab=tab,field,qual=psz("WHERE ",tab,".date='",getcala()[latotxt(lag)],"' ",tafilt))
}
finalretpa <-
function(hedge=c("dollar","hedge","loc","vwap"))
{
    hedge <- match.arg(hedge)
    field <- switch(hedge,dollar="redoto",hedge="reloto",loc="reloto",vwap="redoto+(rrdotovw-rrdoto)")
    retpa <- getpa(field=field)
    putpa(x=retpa,field="retpa") #moved in from the else block 2011-10-28
    finaldate <- as.Date(max(getda("po")))
    priordate <- as.Date(offda(finaldate,-1))
    if(all(is.na(retpa[finaldate,]))) {
        finalprice <- getpa("prdol1")[finaldate,,drop=FALSE]  #last day's price ($ - no local)
        priorprice <- getpa("prdo")[priordate,,drop=FALSE] #last week's price
        ret <- coredata(finalprice)/coredata(priorprice)-1
        retpa <- zoo(ret,index(finalprice))
        putpa(x=retpa,field="retpa")
        mysqlQuery(psz("UPDATE pa SET prem=retpa WHERE date='",finaldate,"'")) #$ return and not premium
    }# else {
    #    putpa(x=retpa,field="retpa")
    #}
}
exprpa <-
function(field="test",
                    expr="prlo*volu"
                    ) 
{
    stopifnot(mode(field) %in% c("character")  && class(field) %in% c("character") && length(field)==1)
    stopifnot(mode(expr) %in% c("character")  && class(expr) %in% c("character") && length(expr)==1)
    tabname <- "pa"
    stopifnot(length(extract(tabname,field,"LIMIT 1"))==1)
    mysqlQuery(paste(
            "UPDATE",tabname,
            "SET",field,"=",expr
            ))
}
copypa <-
function(
                ita="ce",
                ipa="fmp1",
                ota="pa",
                opa=ipa,
                hasdate=TRUE,
                pre=TRUE,#if pre=FALSE, ipa is an expression that includes the table name
                verbose=FALSE,
                qual=""
                )
{
    stopifnot(length(ita)==1 && length(ota)==1)
    stopifnot(ita%in%dirtab())
    stopifnot(ota%in%dirtab())
    stopifnot(opa%in%dirfld(ota))
    stopifnot(length(ipa)==length(opa))
    itadot <- ifelse(pre,psz(ita,"."),"")
    myassign <- paste(paste(psz(ota,".",opa),psz(itadot,ipa),sep="="),collapse=",")
    myquery <- psz("UPDATE ",ita,",",ota," SET ",myassign," WHERE ",ita,".bui = ",ota,".bui",ifelse(hasdate,psz(" AND ",ita,".date = ",ota,".date"),"")," ",qual)
    if(verbose) print(myquery)
    mysqlQuery(myquery)
}
addpa <-
function(  field="prem",              #character
                    usedefinition=rep("rrdoto",length(field))
                    ) 
{
    stopifnot(mode(field) %in% c("character")  && class(field) %in% c("character"))
    stopifnot(mode(usedefinition) %in% c("character")  && class(field) %in% c("character"))
    field <- tolower(field)
    tabname <- "pa"
    fi <- gettab("fi") #,qual="WHERE xsourcetab='bbts'")
    stopifnot(mode(fi) %in% c("character","list")  && class(fi) %in% c("matrix","data.frame") && nrow(fi)>0 && all(usedefinition %in% fi[,"xfield"]))
    for(i in seq(along=field)){
        ii <- which(fi[,"xfield"]==usedefinition[i])[1] #use the first match
        thisdec <- fi[ii,,drop=FALSE]
        thisdec[,"xfield"] <- field[i]
        thisdec[,"xinclude"] <- "1"
        x <- addfld(tabname=tabname,tab=thisdec)
    }
}
valda <-
function(da,                #character,dates
                contiguous=FALSE) 
{
    test <- all(da%in%getca()) && 
    all(as.Date(da)==round(as.Date(da))) &&  
    class(da)%in%c("character","Date") &&     length(da)>0
    if(contiguous) test <- test && all(diff(match(da,getca()))==1)
    test
}
putdano <-
function(dano=as.character(getcala()["plus1"])) 
{
    stopifnot(valda(dano))
    mysqlQuery(psz("UPDATE dano SET date='",dano,"'"))
}
popda <-
function(
                ext=daex(),
                value=NULL      #Date or character - dates must not have fractional part
                ) 
{
    if(all(ext=="no")) stopifnot(length(value)==1)
    if(is.null(value)) {
        for(x in ext) {
            if(x=="pa") {
                dapa <- sort(
                        union(
                        union(
                        union(
                        union(
                        offda(x=getda("ce"),lags=getla("we"),method="apply"),   #required for ce estimation
                        offda(x=getda("po"),lags=sort(union(union(-600:1,getla("wo")),getla("we"))),method="apply")),  #required for li, po; also for ce estimation for pfcon nb 600 added to extend for scofo
                        getda("to")),                                               #required for re
                        getda("ce")),                                               #also want lag 0
                        getda("po"))                                                #also want lag 0
                        )
                print(psz("final panel date ",max(dapa)))
                stopifnot(valda(dapa,contiguous=TRUE))
                popda(ext="pa",value=dapa)
            }
            if(x=="no") {
                popda(ext="no",value=getda("ce")[1])
            }
            if(x=="fo") {
                popda(ext="fo",value=getda("po"))
            }
        }
    } else {
        stopifnot(class(value)%in%c("Date","character"))
        if(class(value)=="Date") stopifnot(value==round(value))
        for(x in ext) {
            newda(x)
            query <- psz(
                    "INSERT INTO da",x," (date)
                    VALUES ",
                    explicitvalues(sort(as.character(value)))
                    )
            mysqlQuery(query)
        }
    }
}
getda <-
function(ext="to",
                lags=0)
{
    stopifnot(length(ext)==1)
    stopifnot(is.numeric(lags) && length(lags)>=1)
    if(ext %in% c("pa","no","fo","mi")) { 
        da <- unique(as.character(unlist(mysqlQuery(psz("
                            SELECT date FROM da",ext," ORDER BY date ASC")))))
        } else if (ext=="ce") {
        da <- unique(as.character(as.matrix(mysqlQuery(" 
                            SELECT dace FROM ty ORDER BY dace ASC"))))
        } else if (ext=="po") {
        da <- unique(as.character(unlist(mysqlQuery("
                            SELECT dapo FROM ty WHERE dapo IS NOT NULL ORDER BY dapo ASC"))))
        } else if (ext=="to") { #atomic tables observation date range
        da <- unique(as.character(unlist(mysqlQuery("
                            SELECT da FROM ty WHERE dapo IS NOT NULL ORDER BY da ASC"))))
        } else if (ext=="cexy") { #nb (1) this returns only da and not dacexy (2) is no longer used
        da <- as.character(unlist(mysqlQuery("SELECT da FROM ty, lacexy WHERE ty.lace IN (SELECT lag FROM lacexy) ORDER by da"))) 
        } else if (ext=="su") {
        da <- extrca(t1=min(extract("su","date")),t2=max(extract("su","date")))
        }
    if(length(da>0)) {
        res <- sort(unique(offda(da,lags))) 
        stopifnot(valda(res,contiguous=TRUE))
    } else { 
        res <- NULL 
    }
    res
}
diffda <-
function(datum=getda("no"),da=getda("ce")) 
{ 
    ca <- getca()
    match(da,ca)-match(datum,ca)
}
daex <-
function() 
{ 
    c("pa","fo","no")
}
valty <-
function(x,jdatum="datum",jdate="date",jlag="lag") 
{
    stopifnot(mode(x[,jlag]) %in% c("character")  && class(x[,jlag]) %in% c("character") && length(x[,jlag])==length(x[,jdate]))
    i <- !apply(is.na(x),1,any)
    stopifnot(valda(x[i,jdate]))
    stopifnot(valda(x[i,jdatum]))
    all.equal(as.numeric(x[i,jlag]),match(x[i,jdate],getca())-match(x[i,jdatum],getca()))
}
validtp <-
function(startafter='1996-12-31') 
{
    d1 <- sort(unique(offda(x=rawt(),lags=getla("wo"),within=rawt(),meth="test")))
    d1[d1>startafter]
}
validtc <-
function() 
{
    unique(offda(x=rawt(),lags=getla("we"),within=rawt(),meth="test"))
}
rawt <-
function() 
{ 
    tsdb <- as.character(extract("fi","DISTINCT xsourcetab","WHERE xhasdate AND xinclude"))
    dates <- NULL
    for(idb in tsdb) {
        dates <- sort(union(dates,as.character(extract(idb,"distinct date","WHERE NOT ISNULL(prdol1) ORDER BY date"))))
    }
    dates
}
putty <-
function(ext="ce",...) 
{
    tab <- paste(sep="","tmp",ext)
    droptab(tab)
    newty(name=tab)     #NEW!!!
    sqlSave(channel=DBcon,
            dat=as.data.frame(perty(...)),
            tablename=tab,
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
}
perty <-
function(
            dateseries=rawt(),
            datumseries=validtc(),
            lagseries=getla("ce")) 
{
    stopifnot(valda(dateseries))
    stopifnot(valda(datumseries))
    stopifnot(valla(lagseries))
    ij <- as.matrix(expand.grid(lapply(list(date=dateseries,lag=lagseries),seq_along))) #generates all permutations of dateseries, lagseries
    idate <- match(dateseries[ij[,1]],getca())
    idatum <- idate-lagseries[ij[,2]]
    ivalid <- (idatum>0) & (idatum<length(getca()))
    perm <- cbind(   
        datum=getca()[idatum[ivalid]],
        date=getca()[idate[ivalid]], 
        lag=lagseries[ij[ivalid,2]]
        )
    perm[perm[,"datum"] %in% datumseries, ]
}
getty <-
function(key="dace",               
                dates=getda("no"),
                ret="da",
                ext="",
                qual=psz(" AND ",ret," IS NOT NULL")
                ) 
{
    sort(unique(as.character(as.matrix(
        mysqlQuery(psz(
        "SELECT ",ret," FROM ty",ext," WHERE ",key," IN (",fldlst(dates,"'","'"),") ",qual
        ))))))
}
filty <-
function(j="date",                                             #column whose multiplicity is tested
                imin=quantile(unclass(summary(as.factor(ty[,j]),maxsum=1e9)),.95), #required multiplicity in column j (reject this proportion if even distn)
                orderby=j,                                              #column to order by
                ext="ce",
                ...) 
{
    pertab <- paste(sep="","tmp",ext)
    ty <- gettab(pertab)  
    myty <- paste(sep="","ty",ext)
    newty(myty,...)
    droptab("tmp")
    mysqlQuery(paste(sep="","
        CREATE TABLE tmp AS
        SELECT ",j," FROM ",pertab,"
        GROUP BY ",j,"
        HAVING count(*) >= ",imin,"
        ORDER BY ",orderby
        ))
    addidx("tmp","date")
    mysqlQuery(paste(sep="","
        INSERT INTO ",myty,"
         (
         SELECT ",pertab,".* FROM ",pertab, " INNER JOIN tmp
         ON ",pertab,".",j," = tmp.",j,"
         ORDER BY ",pertab,".",orderby,", lag DESC
         )"
                 ))
    droptab(pertab)
}
editty <-
function(pa="mo",n=min(50,ncol(pa)/3))
{
    pa <- getpa(pa)
    istart <- min(which(apply(!is.na(coredata(pa)),1,sum)>n))
    stopifnot(0<istart && istart<nrow(pa))
    mysqlQuery(psz("UPDATE ty SET dapo=NULL, lapo=NULL, lava=NULL WHERE dapo < '",as.character(index(pa)[istart]),"'"))
}
valsu <-
function(su) 
{
    all(c("bui","date") %in% colnames(su))
    !any(duplicated(paste(su[,1],su[,2]))) 
}
suzoo <-
function()
{
    x <- tabtomat(cbind(gettab("su")[,c("date","bui")],1))
    mode(x) <- "numeric"
    zoo(x,as.Date(rownames(x)))
}
susummarystring <-
function() 
{
    if(!("cu"%in%dirtab()) || !("btk"%in%dirfld("cu"))) return(NULL)
    x <- as.matrix(gettab("cu"))
    exch <- paste(sort(unique(substr(x[,"btk"],(nchar(x[,"btk"])-1),(nchar(x[,"btk"]))))),collapse=",")
    psz(exch," ",substr(min(x[,1]),1,4),"-",substr(max(x[,1]),1,4))
}
sumsu <-
function(su=gettab("su")) { 
    stopifnot(identical(colnames(su),c("bui","date")))
    xs <- gettab("aaxs",qual=psz("WHERE bui IN (",fldlst(unique(su[,"bui"]),"'","'"),")"))
    sep <- ","
    t(data.frame(
        mindate=min(su[,"date"]),
        maxdate=max(su[,"date"]),
        issuecountry=paste(sort(unique(xs[,"countryissue"])),collapse=sep),
        bti=paste(sort(unique(xs[,"btimax"])),collapse=sep),
        industry=paste(sort(unique(xs[,"industry_sector"])),collapse=sep)
        ))
}
latestsu <-
function(
                isu=aasu(),
                onlylatest=TRUE, #the latest nper periods
                nper=2,
                channel=chve()
                )
{
    ds <- ixxtods(xx="su",ixx=isu,channel=channel)
    bui <- NULL
    for(i in seq_along(ds)) {
        query <- psz("
                    SELECT DISTINCT bui 
                    FROM vesu
                    WHERE datestamp IN (",fldlst(ds[i],"'","'"),")
                ")
        if(onlylatest) {
            maxdate <- as.character(as.matrix(mysqlQuery(psz("
                            SELECT DISTINCT date
                            FROM vesu
                            WHERE datestamp IN (",fldlst(ds[i],"'","'"),")
                            ORDER BY date DESC LIMIT ",nper
                        ),channel=channel)))
            query <- psz(query," AND date IN (",fldlst(maxdate,"'","'"),")")
        }
        bui <- union(bui,as.character(as.matrix(mysqlQuery(query,channel=channel))))
    }
    bui
}
getsu <-
function()
{
as.character(unlist(mysqlQuery(psz("SELECT DISTINCT bui FROM su ORDER BY bui ASC"))))
}
extendsu <-
function(
                tab="su",
                id="bui",
                otherfield=ifelse(tab=="su",NA,"tgt"),
                dats=as.character(extract("aats","DISTINCT date","ORDER BY date DESC LIMIT 1")),#date to extend to
                event=TRUE  #exclude event stocks from buievent
                )
{
    dasu <- as.character(extract(tab,"DISTINCT date","ORDER BY date DESC LIMIT 1"))
    ca <- getca()
    if(dasu<dats) {
        bui <- as.character(extract(tab,id,psz("WHERE date='",dasu,"'")))
        if(!is.na(otherfield)) {more <- extract(tab,otherfield,psz("WHERE date='",dasu,"'"))[]}
        dat <- ca[which(dasu<ca & ca<=dats)]
        for(i in seq_along(dat)) {
            if(is.na(otherfield)) {
                dfr <- data.frame(date=dat[i],bui=bui)
            } else {
                dfr <- data.frame(date=dat[i],x1=bui,x2=more)
                names(dfr) <- c("date",id,otherfield)
            }
            sqlSave(channel=DBcon,
                    dat=dfr,
                    tablename=tab,
                    append=TRUE,
                    rownames=FALSE,
                    safer=TRUE)
        }
    }
    if(event) { buievent() }
}
exitsuda <-
function()
{
    rev(as.character(as.matrix(mysqlQuery("
        SELECT lastd
        FROM
            (SELECT bui, MAX(date) AS lastd
            FROM su
            GROUP BY bui) AS a
        GROUP BY lastd
        ORDER BY lastd DESC
    ")))[-1])
}
decsu <-
function() 
{    
    structure(c("bui", "date", 
            "CHAR(18)", "DATE", 
            "NOT NULL", "NOT NULL",
            "1", "1"), 
            .Dim = c(2L, 4L), 
            .Dimnames = list(NULL, c("xfield","xtype", "xnull", "xkey")
            ))
}
valcu <-
function(cu) 
{
    TRUE
}
minmaxweight <-
function(big=c("big","bigish")) {
    big <- match.arg(big)
    switch(big,big=0.01,bigish=0.004)
}
currtab <-
function(
                mydir=aahome(),
                myfile="currtab.csv",
                ime=aame(),
                isu=aasu(ime=ime),
                bui=latestsu(isu),
                myfield="*"
                ) {
    btk <- mysqlQuery(psz("
            SELECT ",myfield," FROM aaxs
            WHERE bui IN (",flds(bui),")
            "))
    write.csv(btk,file=aafile(fnam=myfile,mydir=mydir))
}
cucapriorop <-
function()
{
    droptab("cucaprior")
    mysqlQuery(psz("
    CREATE TABLE cucaprior AS
        SELECT DISTINCT idate, date
        FROM ca INNER JOIN
            (
            SELECT DISTINCT date AS idate
            FROM aauniv #cu #changed 2010-11-11
            ) AS a
        WHERE ca.date<a.idate
            AND CAST(ADDDATE(a.idate,-8) AS date) < date
    ORDER BY idate
    "))
    addidx("cucaprior","date")
    addidx("cucaprior","idate")
}
cucaop <-
function(validfor=6) #months
{
    droptab("cuca")
    mysqlQuery(psz("
    CREATE TABLE cuca AS
        SELECT MAX(a.date) AS idate, MAX(ca.date) AS date
        FROM ca,
            (
            SELECT DISTINCT date, ADDDATE(CAST(ADDDATE(date,1) AS date) + INTERVAL ",validfor," MONTH,-1) AS nextdate, t1, t2
            FROM cu INNER JOIN du
            ON cu.bti=du.bti
            ) AS a
        WHERE a.date<ca.date AND ca.date<=a.nextdate #calendar date strictly greater than index rebalance date
            AND a.t1<=ca.date AND ca.date<=a.t2     #and inside du.t1 : du.t2
        GROUP BY a.date, ca.date
        ORDER BY date
    "))
}
cu1 <-
function(
            mytab=c("cu","cuxsts"),
            cols=c("countryissue","industry_sector","countryfull","big"),
            type=c("sum(weight)","avg(weight)","count(*)","sum(turn)/1e6","avg(turn)/1e6"),
            rows=c("date","countryissue","industry_sector","countryfull","big"),
            crit=c("big","bigish","big or bigish"),
            sumcol=c(TRUE,FALSE),
            sumrow=c(TRUE,FALSE),
            startdate=union(as.character(extract("cu","distinct date")),as.character(extract("cu","distinct date"))),
            enddate=union(as.character(extract("cu","distinct date")),as.character(extract("cu","distinct date")))
            ) {
    mytab <- match.arg(mytab)
    cols <- match.arg(cols)
    type <- match.arg(type)
    rows <- match.arg(rows)
    startdate <- match.arg(startdate)
    enddate <- match.arg(enddate)
    datefield <- ifelse(mytab=="cu","date","idate")
    crit <- psz(match.arg(crit)," AND ",datefield," >= (",flds(startdate),") AND ",datefield," <= (",flds(enddate),")")
    mat <- tabtomat(extract(mytab,psz("max(",rows,"), max(",cols,"),",type),psz("where ",crit," group by ",rows,",",cols)))
    mode(mat) <- "numeric"
    if(sumcol) {mat <- cbind(mat,apply(mat,1,sum,na.rm=TRUE))}
    if(sumrow) {mat <- rbind(mat,apply(mat,2,sum,na.rm=TRUE))}
    mat
}
aacu <-
function() {2}
valdu <-
function(x   #matrix or dataframe with named columns bti, t1, t2, include
                )
{
    stopifnot(is.matrix(x) || is.data.frame(x))
    stopifnot( c("bti","t1","t2","include") %in% colnames(x) )
    stopifnot(TFtext(x[,"include"]))
    valid <- TRUE
    for(i in 1:nrow(x)){
        tab <- mysqlQuery(psz("
                SELECT * 
                FROM aauniv 
                WHERE (aauniv.bti ='", x[i,"bti"],"') AND 
                (aauniv.date>='", x[i,"t1"],"') AND 
                (aauniv.date<='", x[i,"t2"], "')"
                ))
        valid <-  valid && (nrow(tab)>0)
    }
    valid
}
getdu <-
function()
{
    mysqlQuery("SELECT * FROM du WHERE include")
}
valla <-
function(la) 
{
    mode(la) %in% c("numeric")  && 
    class(la) %in% c("numeric","integer") && 
    all(la==sort(unique(la))) && 
    !any(is.na(la))
}
lawederive <-
function(
            la=-1*(getsione(par="wi",tab="la"):1),
            weight=seq(1, 3, length=(length(la)))
            ) 
{
    stopifnot(valla(la))
    stopifnot(all(is.numeric(weight)) && all(weight >= 0) && all(!is.na(weight)))
    stopifnot(length(weight)==length(la))
    weight <- weight/sum(weight)
    createTable(tabname="lawe",
        dec="lag SMALLINT NOT NULL, weight DOUBLE",
        engine="MYISAM"
        ) 
    sqlSave(channel=DBcon, 
                dat=data.frame(lag=la,weight=weight),
                tablename="lawe",
                append=TRUE,
                rownames=FALSE,
                safer=TRUE
                )    
}
lage <-
function(la,tabname="lapo") 
{
    stopifnot(valla(la))
    droptab(tabname)
    mysqlQuery(psz("
        CREATE TABLE `",tabname,"` (
            `lag` SMALLINT(6) NOT NULL
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    "))
    sqlSave(channel=DBcon, 
                dat=data.frame(lag=la),
                tablename=tabname,
                append=TRUE,
                rownames=FALSE,
                safer=TRUE
                )    
}
laex <-
function()
{
    c("ce","po","wo","we","va","cexy","ra","mi")
}
ladi <-
function(la=NULL) 
{
    if(is.null(la)) la <- -5:5
    stopifnot(valla(la) && 0 %in% la)
    createTable(tabname="ladi",
        dec="lag SMALLINT NOT NULL",
        engine="MYISAM"
        ) 
    sqlSave(channel=DBcon, 
                dat=data.frame(lag=la),
                tablename="ladi",
                append=TRUE,
                rownames=FALSE,
                safer=TRUE
                )    
}
getla <-
function(ext=laex()) 
{
    as.integer(gettab(psz("la",match.arg(ext)))[,"lag"])
}
getcala <-
function(da=getda("no"),qual="") 
{
    mysqlQuery(psz("SELECT * FROM cala WHERE minus0 = ",fldlst(da,"'","'"),qual))
}
caladerive <-
function(la=NULL)  
{
    if(is.null(la)) la <- -1:1
    stopifnot(valla(la))
    oca <- suppressWarnings(data.frame(cbind(1:length(getca()),lags(getca(),la))))
    colnames(oca)[1] <- "cai"
    droptab(c("tmp1","tmp2","cala"))
    dec <- data.frame(xfield=c(colnames(oca),"weekinyear","weekinmonth"),xtype=c("integer",rep("date",ncol(oca)-1),"integer","integer"))
    declare(tabname="cala",dec=dec,engine="MYISAM")
    sqlSave(channel=DBcon, 
                dat=oca,
                tablename="tmp1",
                append=TRUE,
                rownames=FALSE,
                safer=TRUE
                )
    mysqlQuery("
        CREATE TABLE tmp2 (
        SELECT y.date AS date, week(y.date) AS weekinyear, y.iw-x.iwmin+1 AS weekinmonth FROM
            (SELECT date, min(week(date)) AS iwmin, 100*year(date)+month(date) AS ym
            FROM ca
            GROUP BY 100*year(date)+month(date) ) AS x 
        INNER JOIN
            (SELECT date, week(date) AS iw, 100*year(date)+month(date) AS ym
            FROM ca) AS y
        ON x.ym = y.ym
        )
    ")

    mysqlQuery("
        INSERT INTO cala
            (SELECT tmp1.*, tmp2.weekinyear, tmp2.weekinmonth 
            FROM tmp1 INNER JOIN tmp2
            ON tmp1.minus0=tmp2.date)
        ")
    for(i in la) addidx("cala",latotxt(i))
    addidx("cala","cai")
}
valca <-
function(ca)
{
    all(as.Date(ca)==round(as.Date(ca))) &&  
    class(ca)%in%c("character","Date") &&     length(ca)>0
}
popca <-
function(start="1999-01-01",              #coercible to Date
                   end="2008-01-01",                #coercible to Date
                   by="day",
                   weekday=TRUE,                    
                   find=c("all","last","first"),    
                   period=c("week","month","year"),
                   partials=FALSE,
                   firstlast=FALSE,
                   select)
{
    dates <- seq(from=as.Date(start),by=by,to=as.Date(end))
    extractDates(dates=dates,weekday=weekday,find=find,period=period,partials=partials,firstlast=firstlast,select=select)
}
offda <-
function(x,                        #dateseries
                lags=0,                     #lagrange
                withinsequence=getca(),     #the dateseries within which result must lie
                method=c("apply","test"))   #either return lagged x values or x which could validly be lagged
{
    stopifnot(mode(x) %in% c("character","numeric")  && class(x) %in% c("character","Date"))
    stopifnot(mode(lags) %in% c("numeric","integer")  && class(lags) %in% c("numeric","integer") && length(lags)>=1)
    stopifnot(mode(withinsequence) %in% c("character","numeric")  && class(withinsequence) %in% c("character","Date") && length(withinsequence)>=1)
    stopifnot(all(as.Date(x) %in% as.Date(withinsequence)) )
    stopifnot(floor(lags)==lags)
    method <- match.arg(method)
    i <- match(as.Date(x),as.Date(withinsequence))
    ilag <- outer(i,lags,"+")
    if(method=="apply") {
        ii <- unique(as.integer(ilag))
        iii <- ii[ii %in% seq(along=withinsequence)] 
        ret <- sort(withinsequence[iii]) 
        } else {
        mat <- matrix(as.integer(ilag)%in%seq(along=withinsequence),length(i),length(lags))
        ivalid <- apply(mat,1,all)
        ret <- withinsequence[i[ivalid]] 
    }
    ret
}
lacada <-
function(lags=0,x=getda("no")) 
{
    stopifnot(mode(lags) %in% c("numeric")  && class(lags) %in% c("numeric","integer") && length(lags)>=1)
    stopifnot(mode(x) %in% c("character")  && class(x) %in% c("character") && length(x)==1)
    da <- offda(x=x,lags=sort(lags),within=getca())
    as.integer(as.Date(da)-as.Date(x))
}
getca <-
function()
{
    as.character(unlist(mysqlQuery("SELECT date FROM ca ORDER BY date ASC"))) #or getda("ca")
}
extrca <-
function(t1,             #coercible to date
                    t2,            #coercible to date
                    n) 
{
    stopifnot( (missing(t1)+missing(t2)+missing(n)) == 1)
    if(!missing(t1))stopifnot(vdate(t1))
    if(!missing(t2))stopifnot(vdate(t2))
    if(missing(n)) result <- (extract("ca","date",paste("WHERE (date>='",t1,"' AND date<='",t2,"')",sep="")))
    if(missing(t2))result <- (extract("ca","date",paste("WHERE (date>='",t1,"') ORDER BY date ASC LIMIT ",n,sep="")))
    if(missing(t1))result <- (rev(extract("ca","date",paste("WHERE (date<='",t2,"') ORDER BY date DESC LIMIT ",n,sep=""))))
    as.character(result)
}
tabfi <-
function(xsourcetab=c("aaxs"),field="*",...) 
{
    extract("fi",field=fldlst(field),qual=psz("WHERE xsourcetab IN (",fldlst(xsourcetab,"'","'"),") AND xinclude"))
}
getfi <-
function(xsourcetab=c("aaxs"))
{
    as.character(extract("fi",field="xfield",qual=psz("WHERE xsourcetab IN (",fldlst(xsourcetab,"'","'"),") AND xinclude")))
}
fixa <-
function(
            tab=c("aaxs","aats"),
            seq=1,
            sourcetab=tab,
            hasdate="0",
            type=ifelse(tab%in%c("aaxs"),"CHAR(32)","DOUBLE"),
            null="NULL",
            key="0",
            index="0",
            include="0",
            xtkr="bui"
            )
{
    tab <- match.arg(tab)
    field <- dirfld(tab)
    nfield <- length(field)
    x <- cbind(
            xsourcetab=rep(tab,nfield),
            xhasdate=rep(hasdate,nfield),
            xseq=rep(seq,nfield),
            xfield=field,
            xtype=rep(type,nfield),
            xnull=rep(null,nfield),
            xkey=rep(key,nfield),
            xindex=rep(index,nfield),
            xinclude=rep(include,nfield),
            xtkr=rep(xtkr,nfield)
            )
    rownames(x) <- names(field)
    data.frame(x)
}
edtfi <-
function(
                field=c("prlo","name","countryfull","industry_sector","industry_group","industry_subgroup","gics_industry_group_name","gics_industry_name","gics_sub_industry_name"),
                include=rep(1,length(field)),
                xsourcetab="aaxs",
                xtype=rep(ifelse(xsourcetab=="aaxs","CHAR(32)","DOUBLE"),length(field)),
                xhasdate=rep("0",length(field)),
                xtkr=rep("bui",length(field)),
                ...)
{
    if(length(field)==0) return()
    stopifnot(all(field %in% gettab("fi")[,"xfield"]))
    sqlUpdate(channel=DBcon,
                dat=data.frame(cbind(xfield=field,xsourcetab=xsourcetab,xinclude=include,xhasdate=xhasdate,xtkr=xtkr,xtype=xtype)),
                tablename="fi",
                index=c("xfield","xsourcetab"))
}
valli <-
function(
                x=extract("li","DISTINCT fun")
                ) 
{
    res <- TRUE
    for(i in seq_along(x)) res <- res && is.function(get(x[i]))
    for(i in seq_along(x)) res <- res && all(lapply(as.character(formals(get(x[i]))),nchar)==0)
    res
}
sourcecodein <-
function(
                    tab='li',
                    qual='',
                    loc=FALSE,
                    orderby=NULL    #this is an additional field for ordering code (not required if fun, line are key)
                    )
{
    tmp <- tempfile()
    li <- mysqlQuery(paste(sep=" ","SELECT code FROM ",tab,qual,"ORDER BY",fldlst(c(setdiff(orderby,c('fun','line')),'fun','line'))))
    write.table(li,tmp,quote=FALSE,row.names=FALSE,col.names=FALSE)
    source(tmp,local=loc)
    source(tmp,local=TRUE)
    fun <- ls(pattern="li[[:alpha:]]{2}[[:digit:]]{6}[[:alpha:]]")
    funarg <- lapply(lapply(lapply(fun,get),formals),names)
    names(funarg) <- fun
    arg <- unlist(funarg)[unlist(funarg)!='...']
    #names(arg) <- substr(names(arg),1,13)
    unlink(tmp)
     arg
}
runli <-
function(
                beforeclass=allxx(),        #class library to run
                whento=c("before","after"), #run before/after class is derived (this is flagged in a comment in the first line of the function using keyword AFTER)
                sourcetab="li"              #source code table
                ) 
{
    beforeclass <- match.arg(beforeclass)
    whento <- match.arg(whento)
    if(whento=='before') {
        li <- extract(sourcetab,"DISTINCT fun",psz("WHERE beforeclass = '",beforeclass,"' AND line=1 AND NOT code LIKE '%AFTER%'"))
    } else {
        li <- extract(sourcetab,"DISTINCT fun",psz("WHERE beforeclass = '",beforeclass,"' AND line=1 AND code LIKE '%AFTER%'"))
    }    
    for(i in seq_along(li)) {
        do.call(li[i],args=getsi(beforeclass))
    }
    return(TRUE)
}
lisummarystring <-
function()
{
    paste(sort(extract("li","DISTINCT fun")),collapse=",")
}
decli <-
function() 
{
    structure(
    c(  "code",     "fun",      "beforeclass",  "line", 
        "CHAR(255)","CHAR(60)", "CHAR(2)",      "SMALLINT", 
        "NULL",     "NULL",     "NULL",         "NULL"), 
    .Dim = c(4L, 3L), 
    .Dimnames = list(NULL, c("xfield", "xtype", "xnull"))
    )
}
ddlli <-
function() 
{
    droptab("li")
    mysqlQuery("
        CREATE TABLE `li` (
            `code` CHAR(255) NULL DEFAULT NULL,
            `line` SMALLINT(6) NULL DEFAULT NULL,
            `fun` CHAR(60) NULL DEFAULT NULL,
            `beforeclass` CHAR(2) NULL DEFAULT NULL,
            INDEX `line` (`line`),
            INDEX `fun` (`fun`),
            INDEX `beforeclass` (`beforeclass`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
zbtc <-
function(z)
{
    stopifnot(mode(z) %in% "logical" && class(z) %in% c("zoo"))
    zoo(matrix(as.character(as.numeric(z)),nrow=nrow(z),ncol=ncol(z),dimnames=dimnames(z)),order.by=as.Date(index(z)))    
}
vz <-
function(x) {is(x,"zoo") && is(index(x),"Date")}
vdate <-
function(x) 
{
    length(x)==1 && (is(x,"Date") || (is.character(x) && nchar(x)==10 && is(as.Date(x),"Date")) )
}
usesnow <-
function(n=10)   {return(FALSE)}
txttola <-
function(x=dirfld("cala")) 
{
    x <- union(x[grep(patt="^minus",x)],x[grep(patt="^plus",x)])
    x <- gsub(patt="minus",rep="-",x=x)
    x <- gsub(patt="plus",rep="",x=x)
    as.integer(x)
}
tip <-
function(tabname,qualifier="",n=10,returnclass="dataframe",channel=DBcon,...) 
{ 
    gettab(tabname=tabname,qualifier=paste(qualifier,"LIMIT",n),returnclass=returnclass,channel=channel,...)
}
texttonumdf <-
function(tab) 
{
    stopifnot(is.data.frame(tab))
    for(j in 1:ncol(tab)) {
        i <- !is.na(tab[,j])
        is.num <- !any(is.na(suppressWarnings(as(tab[i,j],"numeric"))))
        if(is.num) tab[,j] <- suppressWarnings(as(tab[,j],"numeric"))
    }
    tab
}
tabtomat <-
function(x) 
{
    stopifnot(is.matrix(x)|is.data.frame(x))
    stopifnot(ncol(x)==3)
    stopifnot(!any(duplicated(paste(x[,1],x[,2]))))
    da <- sort(unique(x[,1]))
    su <- sort(unique(x[,2]))
    res <- matrix(NA,nrow=length(da),ncol=length(su),dimnames=list(da,su))
    i <- match(x[,1],da)
    j <- match(x[,2],su)
    res[cbind(i,j)] <- x[,3]
    res
}
tabsset <-
function(engine="MyISAM",tabroot="",...) {
    tt1 <- tabs(tabroot=tabroot,...)
    x<-lapply(lapply(tt1,"[[",1),"[",1,1:2)
    xx <- NULL
    for(i in seq_along(x)) {xx <- rbind(xx,x[[i]])}
    i <- which(xx[,2]==engine)
    xx[i,1]
}
tabs <-
function(tabroot=c("fi","ca","la","du","cu","su","ty","da","pa","ta","ce","re","co","po","pe","va","vi","ga","te","ro","xy"),filt=FALSE,channel=DBcon)
{
    stopifnot(mode(tabroot) %in% c("character")  && class(tabroot) %in% c("character") && length(tabroot)>=1)
    tabname <- dirtab(psz("^",tabroot),channel=channel)
    tables <- tabname[tabname%in%mysqlQuery("SHOW TABLES",channel=channel)[,1]]
    l2 <- vector("list",length(tables))
    for(i in seq(along=tables))
        {
        x1 <- mysqlQuery(c1 <- psz("SHOW TABLE STATUS LIKE '",tables[i],"'"),channel=channel)[,c("Name","Engine","Row_format","Rows")]
        x2 <- mysqlQuery(c2 <- psz("SHOW INDEX FROM ",tables[i]),channel=channel)[,c("Table","Key_name","Seq_in_index","Column_name")]
        x3 <- mysqlQuery(c3 <- psz("SHOW COLUMNS FROM ",tables[i]),channel=channel)
        if(("MyISAM" %in% x1$Engine) || !filt) {
            l1 <- list(x1,x2,x3)
            names(l1) <- c(c1,c2,c3)
            l2[[i]] <- l1 } else
            l2 [[i]] <- NULL
    }
    l2[which(!unlist(lapply(l2,is.null)))]
}
sumtab <-
function(tab="pa") 
{
    alltabs <- dirtab(psz("^",tab))
    stopifnot(length(alltabs)>0)
    for(i in seq(along=alltabs)) {
        print(alltabs[i])
        print(summary(as.data.frame(gettab(alltabs[i],return="data"))))
    }
}
sqlnrow <-
function(tab=dirtab()[1],channel=DBcon) 
{
    stopifnot(length(tab)==1)
    as.numeric(tabs(tab,channel=channel)[[1]][[1]]["Rows"])
}
sqlnotempty <-
function(tab=dirtab()[1],channel=DBcon) 
{
    stopifnot(length(tab)==1)
    tab %in% dirtab(channel=channel) && as.numeric(tabs(psz("^",tab,"$"),channel=channel)[[1]][[1]]["Rows"])>0
}
snap <-
function(fn,mydir=aahome())
{
    fnam <- lapply(strsplit(dir(mydir),"\\."),"[",1)
    i <- max(0,suppressWarnings(max(as.numeric(fnam[which(!is.na(as.numeric(fnam)))]))))
    #i1 <- svalue(gedit(,container=gwindow("filename - no extension"))) # not modal - use confirmDialog or similar
    png(psz(mydir,i+1,'.png'))
    do.call(fn,list(h=list(action='file')))
    dev.off()
}
smalltab <-
function() 
{
    pref <- c("aaa","du","su","cu","li","co","ga","ma","aaindtree1")
    tabs <- dirtab()
    tabs[match(pref,tabs)[pref%in%tabs]][1]
}
sfLapplyWrap <-
function(X,FUN,...){sfLapply(x=X,fun=FUN,...)}
rollxts <-
function(x,what="max",n=5,...)
{
    rollapply(data=x,width=n,FUN=get(what),na.rm=TRUE,fill=NA,align="right",...)
}
retxts <-
function(x,...)
{
    diff(x)/lag(x)  #recall that for xts lag(x,k=1) moves older->newer ie feasible
}
regionru <-
function(x) {
    xa <- extract("aaindices","DISTINCT bti",psz("WHERE bti IN (",flds(x),") AND region IN ('Far East')"))
    xe <- extract("aaindices","DISTINCT bti",psz("WHERE bti IN (",flds(x),") AND region IN ('Europe','EMEA')"))
    xu <- extract("aaindices","DISTINCT bti",psz("WHERE bti IN (",flds(x),") AND region IN ('North America','Latin America')"))
    res <- cbind(x%in%xa,x%in%xe,x%in%xu)*1
    colnames(res) <- c("Asia","Europe","Americas")
    rownames(res) <- rownames(x)
    t(res)
}
quan <-
function(x,n=5) 
{
    as.numeric(cut(x, breaks = quantile(x, probs = seq(0, 1, 1/n),na.rm=TRUE), include.lowest = TRUE, labels = 1:n))
}
pt <-
function(msg) {print(paste(msg,date()))}
psz <-
function(...) {paste(sep="",...)}
pelapo <-
function(iga=925:928)
{
    tab <- mysqlQuery(psz("
            SELECT value2, ixx, value/nper
            FROM vegavi
            WHERE ixx IN (",fldlst(iga,"'","'"),")
                AND domain='pe' AND field1='comp' AND value1='totx' AND field2='lapo'
            ORDER BY value2 ASC
        "))
    mat1 <- tabtomat(tab)
    mat <- mat1[order(as.numeric(rownames(mat1))),]*10000
    mm <- 0.5
    par(mfrow=c(2,2),cex.lab=2,cex.axis=1.6,cex.main=2)
    yy <- c(-.002,.002)*10000
    barplot(mat[,2],ylim=yy,ylab='b.p.',main='a',col='orange')
    barplot(mat[,1],ylim=yy,main='b',col='orange')
    barplot(mat[,4],ylim=yy,,ylab='b.p.',xlab='weeks after trade',main='c',col='orange')
    barplot(-1*mat[,3],ylim=yy,xlab='weeks after trade',main='x',col='magenta')
    x <- apply(mat,1,sum)
    cc <- cumsum(x)
    par(mfrow=c(1,1),cex.lab=2,cex.axis=1.6,cex.main=2)
    plot(cbind(as.numeric(rownames(mat))+1,cc-cc[13]),ylim=c(0,250),xlab='weeks after trade',main='net = a+b+c-x',lwd=2,col='purple',ty='b',pch=16,ylab="cumulative b.p.")
}
ncpus <-
function(
                nsplit=3 #number of separate entire tasks
                )
{
    max(as.numeric(strsplit(shell("wmic cpu get NumberOfCores,NumberOfLogicalProcessors",intern=TRUE)," ")[[2]]),na.rm=T)
    #as.numeric(substr((shell("wmic cpu get NumberOfCores,NumberOfLogicalProcessors",intern=TRUE))[2],1,1)) #is responsible for the odd crash
}
natox <-
function(x,value=0) 
{
    stopifnot(is.zoo(x))
    xx <- coredata(x)
    xx[is.na(xx)] <- value
    zoo(xx,index(x))
}
naa <-
function() {
    #max(ncpus()-1,2) #previously.
    obj <- ls(env=globalenv())
    x1 <- grep("^aa.$",obj)
    ifelse(length(x1)>0,sum(sapply(sapply(obj[x1],get,env=globalenv(),simplify=FALSE),class)=="RODBC"),0)
}
mz <-
function(x) {
    stopifnot(is(as.Date(rownames(x)),"Date"))
    zoo(x,order.by=as.Date(rownames(x)))
}
mysqlQuerynoob <-
function(query,       #SQL text optionally terminated with ;
                    channel=DBcon, 
                    errors=TRUE, 
                    as.is=TRUE,
                    ..., 
                    rows_at_time = 1) 
{
    stopifnot(mode(query) %in% c("character")  && class(query) %in% c("character"))
    stopifnot((mode(channel) %in% c("numeric")  && class(channel) %in% c("RODBC")) || is.null(channel))
    stopifnot(mode(errors) %in% c("logical")  && class(errors) %in% c("logical"))
    stopifnot(mode(as.is) %in% c("logical")  && class(as.is) %in% c("logical"))
    stopifnot(mode(rows_at_time) %in% c("numeric")  && class(rows_at_time) %in% c("numeric"))
    if(!is.null(channel)) {
        res <- sqlQuery(channel=channel,query=query,errors=errors,as.is=as.is,...,rows_at_time=rows_at_time)
    } else {
        res <- sqldf(x=query,...)
    }
    #if(length(grep(pattern="ERROR",x=res[1]))>0) stop(paste(res,collapse=" : "))
    if(any(lapply(lapply(res,grep,pattern="ERROR"),length)>0)) {stop(paste(res,collapse=" : "))}
    res
}
mysqlQuery <-
function(query,       #SQL text optionally terminated with ;
                    channel=DBcon, 
                    errors=TRUE, 
                    as.is=TRUE,
                    ..., 
                    rows_at_time = 1) 
{
    stopifnot(mode(query) %in% c("character")  && class(query) %in% c("character"))
    stopifnot((mode(channel) %in% c("numeric")  && class(channel) %in% c("RODBC")) || is.null(channel))
    stopifnot(mode(errors) %in% c("logical")  && class(errors) %in% c("logical"))
    stopifnot(mode(as.is) %in% c("logical")  && class(as.is) %in% c("logical"))
    stopifnot(mode(rows_at_time) %in% c("numeric")  && class(rows_at_time) %in% c("numeric"))
    if(!is.null(channel)) {
        res <- sqlQuery(channel=channel,query=query,errors=errors,as.is=as.is,...,rows_at_time=rows_at_time)
    } else {
        res <- sqldf(x=query,...)
    }
    #if(length(grep(pattern="ERROR",x=res[1]))>0) stop(paste(res,collapse=" : "))
    if(any(lapply(lapply(res,grep,pattern="ERROR"),length)>0)) {stop(paste(res,collapse=" : "))}
    res
}
most.recent.local <-
function(x) 
{
    if (!is.logical(x)) stop("x must be logical")
    x.pos <- which(x)
    if (length(x.pos)==0 || x.pos[1] != 1) x.pos <- c(1, x.pos)
    rep(x.pos, c(diff(x.pos), length(x) - x.pos[length(x.pos)] + 1))
}
moldeclare <-
function(nr,mc)   
{
    mat <- matrix(NA,nr,mc)
    mode(mat) <- "list"
    mat
}
molassign <-
function(mol,i,j,x)   
{
    n <- nrow(mol)
    m <- ncol(mol)
    ij <- i+(j-1)*n
    mol[[ij]] <- x
    mol
}
meantri <-
function(x,minweight=(1/3),pow=1,...)
{
    if(length(x)==0) return()
    stopifnot(length(minweight)==1 && 0<=minweight && minweight<=1)
    wgt <- seq(from=minweight,to=1,length=length(x))
    weighted.mean(x=x**pow,w=wgt,...)
}
mattotab <-
function(x,field="field",fieldmode="numeric",rclabel=c("date","bui")) 
{
    stopifnot(!is.null(rownames(x)) && all(!duplicated(rownames(x))))
    stopifnot(!is.null(colnames(x)) && all(!duplicated(colnames(x))))
    #if(fieldmode!=mode(x)) print("changing mode in mattotab()")
    da <- rownames(x) 
    su <- colnames(x) 
    ij <- as.matrix(expand.grid(lapply(list(date=da,bui=su),seq)))
    res <- data.frame(cbind(   
            date=da[ij[,1]], 
            bui=su[ij[,2]],
            field=as.matrix(x)[as.matrix(ij)]
            ))
    colnames(res) <- c(rclabel,field)
    res[,3] <- as(res[,3],fieldmode)
    res
}
locwin <-
function(i=1,n=10,fac=1000,ncols=2,nrows=ceiling(n/ncols),origin=1) {
    j <- 0:(n-1)
    y <- j%%nrows
    x <- j%/%nrows
    yunit <- fac/(max(y)+1)
    xunit <- fac/(max(x)+1)
    cbind(x*xunit,y*yunit)+origin
}
locf.mat <-
function(
                    #class  NULL    freq    repeats 
            x,      #mat    yes     -       yes     
            maxperiods=Inf
            )
{
    if(nrow(x)<1) return(x)
    if(!any(is.na(x))) return(x)
    y <- x
    jna <- which(apply(is.na(x),2,any))
    for(j in jna)
        {
        i <- 1:nrow(y)-pmin((1:nrow(y))-most.recent.local(!is.na(y[,j])),maxperiods)
        y[,j] <- y[i,j]
        }
    return(y)
}
locf.localnoob <-
function(
            x,   
            maxperiods=Inf,
            ...
            )
{
    if(nrow(x)<1) return(x)
    if(!any(is.na(x))) return(x)
    y <- x
    jna <- which(apply(is.na(x),2,any))
    for(j in jna)
        {
        i <- 1:nrow(y)-pmin((1:nrow(y))-most.recent.local(!is.na(y[,j])),maxperiods)
        suppressWarnings(y[,j] <- y[i,j])   #the warning arises from one:many mapping
        stopifnot(!any(duplicated(index(y))))   #check that y has no repeats, ie warning was not relevant
        }
    return(y)
}
locf.local <-
function(
            x,   
            maxperiods=Inf,
            ...
            )
{
    if(nrow(x)<1) return(x)
    if(!any(is.na(x))) return(x)
    y <- x
    jna <- which(apply(is.na(x),2,any))
    for(j in jna)
        {
        i <- 1:nrow(y)-pmin((1:nrow(y))-most.recent.local(!is.na(y[,j])),maxperiods)
        suppressWarnings(y[,j] <- y[i,j])   #the warning arises from one:many mapping
        stopifnot(!any(duplicated(index(y))))   #check that y has no repeats, ie warning was not relevant
        }
    return(y)
}
latotxt <-
function(la) 
{
    mysign <- sign(la)
    latext <- as.character(abs(la))
    minus <- la<=0
    latext[minus] <- psz("minus",latext[minus])
    latext[!minus] <- psz("plus",latext[!minus])
    latext
}
lastvr <-
function(name="rino",qual=list(isu=NA,ime=NA)) {
    myqual <- psz("WHERE name=",flds(name))
    qual <- qual[!is.na(qual)]
    if(0<length(qual)) {
        for(i in seq_along(qual)) {
            myqual <- psz(myqual," AND ",names(qual)[i]," = ",qual[i])
        }
    }
    extract("aard","max(iseq)",myqual)
}
keyfields <-
function(tab=dirtab()[1]) 
{
    stopifnot(length(tab)==1)
    tabsumm <- tabs(tab)[[1]][[3]]
    tabsumm[tabsumm[,"Key"]!="","Field"]
}
juma <-
function(x,filt) {
    as.numeric(filter(x=x,filter=filt/sum(abs(filt)),met="con",side=1))
}
imgzoo <-
function(z,                 #zoo
                orderby=NULL,...)       #how to reorder columns
{
    stopifnot(is(z,"zoo"))
    if (is.null(orderby)) {
        if (any(is.na(z))) 
            orderby <- is.na(z)
        else if (mode(z) == "character") 
            orderby <- z == "0"
        else orderby <- z == 0
    }
    zz <- as.matrix(z)
    mode(zz) <- "numeric"
    jorder <- suppressWarnings(order(unlist(lapply(lapply(data.frame(!orderby),which),min)), #first value where orderby is not true
                    unlist(lapply(lapply(data.frame(orderby),which),min)),  #first value where orderby is true
                    as.numeric(unlist(lapply(lapply(data.frame(!orderby[(rev(seq(along=orderby[,1,drop=FALSE]))),,drop=FALSE]),which),min)))*-1 #first value from end where orderby is true
                    ))
    zzz <- t(zz[,jorder])[,nrow(zz):1]
    image(zzz,xlab="bui",ylab="date",axes=FALSE,...)
}
ggtable <-
function(
            x,
            digits=3,
            fun=round,
            widget=gtable,
            addrownames=FALSE,
            chosencol=1
            ) 
{
    stopifnot(identical(fun,round) || identical(fun,signif))
    stopifnot(digits>=0)
    xcopy <- x
    if(!is.data.frame(xcopy)) xcopy <- data.frame(xcopy)
    i <- lapply(xcopy,mode)=="numeric"
    xcopy[i] <- do.call(fun,list(x=xcopy[i],digits=digits))
    xcopy <- data.frame(lapply(xcopy,"class<-","character"))
    colnames(xcopy) <- colnames(x)
    if(addrownames) {
        xcopy <- cbind(rownames(x),xcopy)
        colnames(xcopy)[1] <- ""
    }
    do.call(widget,list(items=xcopy,chosencol=chosencol))
}
getWidget <-
function(x, cont=cont) {
     switch(class(x),
     "numeric" = gedit(x, coerce.with=as.numeric, cont=cont),
     "character" = gcombobox(x, active=1, cont=cont),
     "logical" = gcombobox(c(TRUE,FALSE), active = 1 + (x == FALSE), cont=cont),
     "name" = gedit("", cont=cont),
     "NULL" = gedit("NULL", cont=cont),
     "call" = getWidget(eval(x), cont=cont), # recurse
     gedit("", cont=cont) # default
     )
 }
getvr <-
function(iseq=as.numeric(mysqlQuery("SELECT last_insert_id()")),mydir=aarddir()) {
    load(file=psz(mydir,zeroprepend(iseq,5),".rdata"))
    #myname <- extract("aard","name",psz("WHERE iseq=",iseq))
    #assign(x=myname,value=x,env=globalenv())
    return(x)
}
getvix <-
function(da=sort(union(getda("ce"),getda("po"))),period="week",select=3,mylag=-1)
{
    #x<-tabtomat(extract("fa1","date, fnum, score","where lace=0 AND fnum=1"))
    #mode(x) <- "numeric"
    #f1 <- mz(x)
    vix <- extractxts(locf.local(getpa(fi="value",tab="aamacro",ifi="date",jfi="btk",qual="WHERE btk='vix'"),maxper=4),period=period,select=select)
    vix <- lag(vix,mylag)   #bug found 2010-09-15 this action was not done before
    stopifnot(all(as.Date(da)%in%index(vix)))
    stopifnot(all(class(vix)=="zoo") && !is(zoo,"xts"))
    stopifnot(mylag %in% c(-1,0)) #because otherwise unexpectedly returns a vector
    vix
}
gettab <-
function(tabname="ca",
                    qualifier="", 
                    returnclass=c("matrix","dataframe"),
                    jrownames=0,
                    channel=DBcon
                    )            
{
    returnclass <- match.arg(returnclass)
    stopifnot(mode(tabname) %in% c("character")  && class(tabname) %in% c("character") && length(tabname)==1)
    stopifnot(mode(qualifier) %in% c("character")  && class(qualifier) %in% c("character") && length(qualifier)==1)
    stopifnot(mode(returnclass) %in% c("character")  && class(returnclass) %in% c("character") && length(returnclass)==1)
    if(returnclass=="dataframe") conversion <- data.frame else conversion = as.matrix
    result <- extract(tabname=tabname,
            fields="*",
            qualifier=qualifier,
            result=conversion,
            channel=channel)
    if(jrownames!=0) rownames(result) <- as.character(result[,jrownames])
    result
}
getsuvr <-
function(
                name="rino",
                isu=251,
                iseq=lastvr(name),
                verbose=TRUE,
                strict=TRUE
                ) {
    x <- getvr(iseq)
    stopifnot(!is.list(x))  #object must be a panel, not a list of panels
    bui <- as.character(extract("vesuvi","DISTINCT bui",psz("WHERE ixx IN (",flds(isu),")")))
    buinotfound <- bui[!bui%in%colnames(x)]
    if(strict) {stopifnot(length(buinotfound)==0)}
    buifound <- setdiff(bui,buinotfound)
    if(verbose) {
        print(psz(length(buifound)," bui found; ",length(buinotfound)," bui not found : ",paste(buinotfound,collapse=",")))
        if(0<length(buinotfound)) {
            print(extract("aaxs","bui, btkmax, name, countryissue",psz("where bui in (",flds(buinotfound),")")))
        }
    }
    res <- x[,buifound]
    rownames(res) <- as.character(index(res))
    res
}
focb.mat <-
function(
                    #class  NULL    freq    repeats 
            x,      #mat    yes     -       yes     
            maxperiods=Inf
            )
{
    if(nrow(x)<1) return(x)
    if(!any(is.na(x))) return(x)
    y <- x[nrow(x):1,,drop=FALSE]
    locf.local(y,maxperiods=maxperiods)[nrow(x):1,,drop=FALSE]
}
flds <-
function(field=getfi(),       #character
                prefield=ifelse(mode(field)=="character","'",""),
                postfield=ifelse(mode(field)=="character","'",""),
                collapsefield=",")
{
    stopifnot(is.null(field) || mode(field) %in% c("character","numeric","logical"))
    stopifnot(mode(prefield) %in% c("character")  && length(prefield)==1)
    stopifnot(mode(postfield) %in% c("character")  && length(postfield)==1)
    if(is.null(field)){field <- "NULL"}
    field <- paste(paste(prefield,field,postfield,sep=""),sep="")
    result <- paste(field,collapse=collapsefield)
    result
}
fldlst <-
function(field=getfi(),       #character
                prefield="",
                postfield="",
                collapsefield=",")
{
    stopifnot(mode(field) %in% c("character","numeric")  && class(field) %in% c("character","numeric","integer"))
    stopifnot(mode(prefield) %in% c("character")  && class(prefield) %in% c("character") && length(prefield)==1)
    stopifnot(mode(postfield) %in% c("character")  && class(postfield) %in% c("character") && length(postfield)==1)
    field <- paste(paste(prefield,field,postfield,sep=""),sep="")
    result <- paste(field,collapse=collapsefield)
    result
}
extractxts <-
function(x,...)
{
    x[extractDates(index(x),...),,drop=FALSE]
}
extractDates <-
function(
                   dates,
                   weekday=FALSE,
                   find=c("all","last","first"),
                   period=c("week","month","year"),
                   partials=TRUE,
                   firstlast=FALSE,
                   select)
    {    
    find <- match.arg(find)
    period <- match.arg(period)
    myindex1 <- 1:length(dates)
    #1  optionally point only to weekdays
    if(weekday) {
        wday <- as.POSIXlt(dates)$wday
        myindex1 <- which( (0 < wday) & (wday < 6) )
    }
    if(period=="month") {
        theperiod <- 100*as.POSIXlt(dates[myindex1])$year+as.POSIXlt(dates[myindex1])$mon
        dayinperiod <- as.POSIXlt(dates[myindex1])$mday
    } else if(period=="year") {
        theperiod <- as.POSIXlt(dates[myindex1])$year
        dayinperiod <- as.POSIXlt(dates[myindex1])$yday
    } else if(period=="week") {
        theweek <- as.numeric(format(as.POSIXct(dates[myindex1]), "%U") )
        theyear <- as.numeric(format(dates[myindex1],"%Y"))
        incorrectPartialWeek <- theweek==0
        theyear[incorrectPartialWeek] <- theyear[incorrectPartialWeek]-1    #first partial week in January assigned to last year
        theweek[incorrectPartialWeek] <- as.numeric(format(ISOdate(theyear[incorrectPartialWeek]-1,12,31),"%U"))    #only incomplete Jan weeks are indexed 0 (see Jan 1995)
        theperiod <- 100*theyear+theweek
        dayinperiod <- as.POSIXlt(dates[myindex1])$wday
    }
    #2  if selecting based on 'find'
    if(find=="all"){
        myindex2 <- 1:length(myindex1)
    } else {
        myindex2 <- setdiff(which(diff(c(theperiod[1],theperiod))!=0),1)
        if(find=="last") {
            myindex2 <- myindex2-1
        }
        if(partials){
            if(find=="last") {
                myindex2 <- unique(c(myindex2,length(myindex1)))
            } else {
                myindex2 <- unique(c(1,myindex2))
            }
        }
    }
    #3 select based on 'select'
    if(missing(select)) {
        myindex3 <- 1:length(myindex2)
    } else {
        myindex3 <- which(dayinperiod[myindex2]%in%select)
    }
    myindex <- myindex1[myindex2][myindex3]
    if(firstlast) {
        myindex <- unique(c(1,myindex,myindex1[length(myindex1)]))
    }
    if(all(is.na(myindex))) myindex <- NULL
    return(dates[myindex])
    }
extract <-
function(tabname,   
                    fields,      
                    qualifier="",
                    result=as.matrix,
                    channel=DBcon)
{
    stopifnot(mode(tabname) %in% c("character")  && class(tabname) %in% c("character") && length(tabname)==1)
    stopifnot(mode(fields) %in% c("character")  && class(fields) %in% c("character"))
    stopifnot(mode(result) %in% c("function")) 
    query <- paste("SELECT",paste(fields,collapse=","),"FROM",tabname,qualifier)
    result(mysqlQuery(query,channel=channel))
}
explicitvalues <-
function(x) #maybe unneeded, only used once
{
    fldlst(x,"('","')")
}
existfld <-
function(tabname="pa",field="date")
{
    stopifnot(mode(tabname) %in% c("character")  && class(tabname) %in% c("character") && length(tabname)==1)
    stopifnot(mode(field) %in% c("character")  && class(field) %in% c("character"))
    field %in% mysqlQuery(psz("SHOW COLUMNS FROM ",tabname))[,"Field"]
}
ed <-
function(tab,qualifier="",...) 
{
    x <- edit(gettab(tab,return="dataframe",qual=qualifier,...))
}
droptab <-
function(drops=dirtab(),keepthosestartingwith="ve",channel=DBcon) 
{
    keep <- dirtab(psz("^",keepthosestartingwith),channel=channel)
    keep <- union(union(dirtab("aa",channel=channel),dirtab("bb",channel=channel)),keep)
    gonner <- setdiff(drops,keep)
    stopifnot(      #none of the keepalways tables in gonner 
        all(is.na(match(c("aabworld","aalocal","aauniv","aaxs","aats","aamacro","aaindtree","aaindtree1","aaindtree2","aagicstree","aagindtree1","aagindtree2","bbbb","bbts","bbbcse","bbbentity","bbbindex","bbbindtree","bbbindtree1","bbbindtree2","bbbsecurity","bbbtk_wide","bbco","bbconstituent","bbgicstree","bbin"),
        gonner)))
        )
    if(length(grep("bb",gonner))>0 ||length(grep("aa",gonner))>0) stop("failed: attempting delete of aa or bb tables")
    for(i in seq(along=gonner)) {
        mysqlQuery(paste("DROP TABLE if EXISTS",gonner[i]),channel=channel)
        mysqlQuery(paste("DROP VIEW if EXISTS",gonner[i]),channel=channel)
    }
}
delsnap <-
function() {
   shell(psz("del/q ",aasnapdir(),"*.*"))
   shell(psz("del/q ",aadir(),"*.*"))
}
delfld <-
function(tabname,               #character
                    field)                  #character
{
    stopifnot(mode(tabname) %in% c("character")  && class(tabname) %in% c("character") && length(tabname)==1)
    stopifnot(mode(field) %in% c("character")  && class(field) %in% c("character"))
    if(length(tabname)<1 || length(field)<1 || !tabname %in% dirtab() || !any(field %in% dirfld(tabname))) return()
    for(i in seq(along=field)){
        query <- paste(
                "ALTER TABLE", tabname,
                "DROP COLUMN ",field[i]
                )
        mysqlQuery(query)
    }
}
decvv <-
function(xx=setdiff(c(inpxx(),intxx(),outxx(),"me"),"li"))  {
    for(i in seq_along(xx)) {
        droptab(psz("vv",xx[i]))
        droptab(psz("vv",xx[i],"di"))
        mysqlQuery(psz("CREATE TABLE vv",xx[i]," AS SELECT * FROM ve",xx[i]," LIMIT 0"))
        mysqlQuery(psz("CREATE TABLE vv",xx[i],"di AS SELECT * FROM ve",xx[i],"di LIMIT 0"))
    }            
}
dectab <-
function(tab) #character table with columns: (* indicates mandatory) *xfield, *xtype, xnull, xkey, xdefault, xindex, xinclude
{
    stopifnot( all( c("xfield","xtype") %in% colnames(tab) ) )
    stopifnot(mode(tab) %in% c("character","list") && class(tab) %in% c("matrix","data.frame"))
    if("xinclude" %in% colnames(tab)) tab <- tab[which(tab[,"xinclude"]==1),,drop=FALSE]
    if("xnull" %in% colnames(tab)) nullcol <- tab[,"xnull"] else nullcol <- NULL
    defaultcol <- NULL
    if(nrow(tab)>0) {
        result <- paste(collapse=", ",
                            tab[,"xfield"],
                            tab[,"xtype"],
                            nullcol,
                            defaultcol
                            )
    } else {
        result <- NULL
    }
    if("xindex" %in% colnames(tab) && any(tab[,"xindex"]=="1"))
        result <- paste(sep=" , ",
                    result,
                    psz("INDEX USING BTREE (",fldlst(tab[which(tab[,"xindex"]=="1"),"xfield"]),")"))
    if("xkey" %in% colnames(tab) && any(tab[,"xkey"]=="1"))
        result <- paste(sep=" , ",
                    result,
                    psz("PRIMARY KEY (",fldlst(tab[which(tab[,"xkey"]=="1"),"xfield"]),")"))
    result
}
declare <-
function(tabname,      #character
                    dec,            #declaration table
                    channel=DBcon,
                    ...)
{
    stopifnot(mode(tabname) %in% c("character")  && class(tabname) %in% c("character"))
    stopifnot(nrow(dec)>0)
    createTable(tabname=tabname,
                dec=dectab(dec),
                channel=channel,
                ...)
}
createTable <-
function(tabname="tmp",
                    dec="foo CHAR(1), bar REAL",
                    engine="MYISAM",
                    table="TABLE",
                    channel=DBcon)
{
    stopifnot(mode(tabname) %in% c("character")  && class(tabname) %in% c("character") && length(tabname)==1)
    stopifnot(mode(dec) %in% c("character")  && class(dec) %in% c("character"))
    stopifnot(mode(engine) %in% c("character")  && class(engine) %in% c("character") && length(engine)==1)
    droptab(tabname,channel=channel)
    q1 <- paste("CREATE ",table,
                tabname,
                "(",
                dec,
                ") ENGINE = ",
                engine
                )
    stopifnot( mysqlQuery(q1,channel=channel) =="No Data")
}
coru <-
function(bui) {
    co <- extract("aaxs","bui, countryissue",psz("WHERE bui IN (",flds(bui),")"))
    rownames(co) <- co[,"bui"]
    co[match(bui,co[,"bui"]),"countryissue",drop=FALSE]
}
copytabs <-
function(way=c("forward","back"),tabs=c("ce","re","co","po","ty"),extension="")
{
    way <- match.arg(way)
    if(way=="forward") {
        droptab(psz(tabs,"copy"))
        for(i in seq_along(tabs)) mysqlQuery(psz("CREATE TABLE ",tabs[i],"copy",extension," AS SELECT * FROM ",tabs[i]))
    } else {
        for(i in seq_along(tabs)) {
            mysqlQuery(psz("DELETE FROM ",tabs[i]))
            mysqlQuery(psz("INSERT INTO ",tabs[i]," SELECT * FROM ",tabs[i],"copy",extension))    
        }
    }
}
belowhigh <-
function(x)
{
    stopifnot(is(x,"zoo") && mode(x)=="numeric")
    n <- nrow(x)
    xx <- zoo(rbind(coredata(x)*NA,coredata(x)))[-1,]
    xx[is.na(xx)] <- -1/0
    mx <- rollapply(xx,width=nrow(x),FUN=max,align="right")
    index(mx) <- index(x)
    as.zoo(x)/as.zoo(mx)
}
arcvv <-
function(
            ime,
            mydir=vvdir(),    #working directory for dump files
            docopyrow=TRUE,     #copy rows to vvxx (overwrite existing vvxx)
            dodump=TRUE,        #dump vvxx (overwrite existing dumps)
            dozip=TRUE,         #move vvxx (overwrite existing zip)
            dodelete=FALSE,      #delete rows from xx, including any orphaned inputs
            doinp=TRUE,         #operate on inputs
            doint=TRUE,         #operate on intermediates
            protectlastentry=TRUE #do not delete the final entry in any archive, to protect ixx from re-use
            ) {
    stopifnot(identical(namecon(),"bloomberg")) #must be run with DBcon pointing to archives
    stopifnot(is.numeric(ime) && all(ime %in% getive("me",how="all")))
    stopifnot(length(grep(x=mydir,patt=" "))==0)# && max(regexpr(patt=".?/$",text=mydir))<0) #no space, no final /
    stopifnot(!dodelete || (doinp & doint & dodump & dozip)) #to delete, require a full unload
    for(j in seq_along(ime)) {
        print(psz(j,"/",length(ime),"; ime=",ime[j]))
        ijo <- mexx(ime=ime[j],group="entire")
        if(!all(ijo%in%getijo("all"))) return(psz("jobs missing for ime=",ime[j]))
        droptab("vvjo")
        mysqlQuery(psz("CREATE TABLE vvjo AS SELECT * FROM jo WHERE ijo IN (",flds(ijo),") ORDER BY ijo"))
        xx <- union(outxx(),c("ga","ti"))
        if(doinp) {xx <- union(xx,inpxx())}
        if(doint) {xx <- union(xx,intxx())}
        xx <- setdiff(c(xx,"me"),"li")
        if(docopyrow) {
            decvv()
            for(i in seq_along(xx)) {
                print(psz("copy out ",xx[i]))
                if(xx[i]=="me") {
                    ixx <- ime
                } else {
                    ixx <- sort(unique(as.numeric(extract("vvjo",psz("i",xx[i])))))
                }
                mysqlQuery(psz("DROP TABLE IF EXISTS vv",xx[i]))
                mysqlQuery(psz("DROP TABLE IF EXISTS vv",xx[i],"di"))
                if(length(ixx)>0) {
                    ds <- ixxtods(xx=xx[i],i=ixx)
                    mysqlQuery(psz("CREATE TABLE vv",xx[i]," AS SELECT * FROM ve",xx[i]," WHERE datestamp IN (",flds(ds),")"))
                    mysqlQuery(psz("CREATE TABLE vv",xx[i],"di AS SELECT * FROM ve",xx[i],"di WHERE datestamp IN (",flds(ds),")"))
                } else {
                    mysqlQuery(psz("CREATE TABLE vv",xx[i]," AS SELECT * FROM ve",xx[i]," LIMIT 0"))
                    mysqlQuery(psz("CREATE TABLE vv",xx[i],"di AS SELECT * FROM ve",xx[i],"di LIMIT 0"))
                }
            }            
        }
        if(dodump) {
            fnam <- psz(mydir,ime[j],"-",thiswed())
            print("dump...")
            shell(psz("mysqldump -uroot -p7159 bloomberg ",allvv()," > ",fnam))
        }
        if(dozip) {
            print("zip...")
            fnam <- psz(mydir,ime[j],"-",thiswed())
            shell(psz("del ", gsub(fnam,patt="/",rep="\\\\"),".zip ")) #first delete zip
            shell(psz("zip -m -D -j -1 ",fnam,".zip ",fnam)) #move into zipfile (ie delete file)
        }
        if(dodelete) {
            print("delete jobs...")
            ijo1 <- mexx(ime=ime[j],group="allexcept")
            deljo(ijo=ijo1,deletefreeinputs=TRUE,protectlastentry=protectlastentry)
            #for(i in seq_along(c(intxx(),outxx()))) {  #this is not a good idea as v slow - just do it in cleanve
            #    mysqlQuery(psz("OPTIMIZE TABLE ve",xx[i])) #release rows
            #}
        }
        #droptab(strsplit(allvv(),split=" ")[[1]])
    }
}
alter <-
function(tabname,      #character
                    dec,            #declaration table
                    ...)
{
    stopifnot(mode(tabname) %in% c("character")  && class(tabname) %in% c("character") && tabname %in% dirtab())
    stopifnot(!any(dirfld(tabname) %in% dec[,"xfield"]))
    stopifnot(nrow(dec)>0)
    mysqlQuery(paste("ALTER TABLE ",tabname," ADD (",dectab(dec),")"))
    return()
}
allvv <-
function(vec=FALSE,xx=setdiff(c(inpxx(),intxx(),outxx(),"me"),"li")) {
    res <- psz(paste(psz("vv",c(xx,"jo")),collapse=" ")," ",paste(psz("vv",c(xx),"di"),collapse=" "))
    if(vec) {res <- strsplit(allvv(xx=xx),split=" ")[[1]]}
    res
}
addidx <-
function(tabname="pa",                  #character
                    fields=c("date","bui"),         #character
                    create=c("CREATE","DROP"),
                    channel=DBcon)
{
    create <- match.arg(create)
    stopifnot(mode(tabname) %in% c("character")  && class(tabname) %in% c("character") && length(tabname)==1)
    stopifnot(mode(fields) %in% "character" && class(fields) %in% "character")
    idxname <- paste(fields,collapse="_")
    hasindex <- idxname %in% mysqlQuery(paste("SHOW INDEX FROM",tabname),channel=channel)[,"Key_name"]
    if( (!hasindex & create=='CREATE')  ){
        mysqlQuery(paste("CREATE INDEX",idxname,"ON",tabname,"(", fldlst(fields), ")"),channel=channel)
    }
    if( (hasindex & create=='DROP') ){
        mysqlQuery(paste("DROP INDEX",idxname,"ON",tabname),channel=channel)
    }
}
addfld <-
function(tabname,         #character
                    tab,            #declaration table
                    ruthless=TRUE)
{
    stopifnot(mode(tabname) %in% c("character")  && class(tabname) %in% c("character") && length(tabname)==1)
    stopifnot(mode(tab) %in% c("character","list")  && class(tab) %in% c("matrix","data.frame"))
    stopifnot( all( c("xfield","xtype") %in% colnames(tab) ) )
    if(ruthless && any(xi <- existfld(tabname=tabname,field=tab[,"xfield"]))) {delfld(tabname=tabname,field=tab[xi,"xfield"])}
    dec <- dectab(tab)
    query <- paste(
                "ALTER TABLE", tabname,
                "ADD COLUMN (", dec, ")"
                )
    mysqlQuery(query)
}
accruexts <-
function (x, daysperyear = 365) 
{
    n <- nrow(x)
    xx <- coredata(x)
    xx[-1,] <- xx[-n, ] * diff(as.numeric(as.Date(index(x))))/daysperyear
    xx[1,] <- NA
    xts(focb.mat(xx),index(x))
}
Aarollxts <-
function(...) {rollxts(...)}
Aameantri <-
function(...) {meantri(...)}
ghdita2 <-
function(h,...) 
{
    tabname <- svalue(gnb$edit$w1)
    if(!(length(grep(x=tabname,patt="^bb"))==0) || !(length(grep(x=tabname,patt="^ve"))==0) ) stop("protected table")
    tab <- gnb$edit$rhs$rawtable[] #may need to be transposed 
    droptab("tmp")
    sqlSave(channel=DBcon, 
                dat=tab, 
                tablename = "tmp", #tabname, 
                )
    rucuupdate()
}
ditaupdate <-
function(...,clear=FALSE) 
{
    on.exit({enablegu(enable=TRUE)})   
    enablegu(enable=FALSE)
    delete(obj=gnb$edit$perm,widget=gnb$edit$temp)
    gnb$edit$temp <<- ggroup(use.scroll=TRUE,horiz=FALSE)
    if(clear) {
        add(gnb$edit$perm,gnb$edit$temp,expand=TRUE)
    } else {
        add(gnb$edit$perm,dita3(gnb$edit$temp),expand=TRUE)
    }
}
ghruqunew <-
function(h,...) 
{
    derqu()
    ruquupdate()
}
rene1 <-
function(...) 
{
    add(gnb$report$panel$graphics$perm,gnb$report$panel$graphics$temp,expand=TRUE)
    add(gnb$report$panel$perm,rene3(gnb$report$panel$temp),expand=TRUE)   #perm <- temp <- content
}
shreagplot <-
function(
  tab=data.frame(do.call(what="getga",args=reagarglst())),
  is.horizontal=TRUE,
  is.cumulative=FALSE,
  is.timeseries=FALSE,
  is.line=is.timeseries,
  abcissa=reagarglst()$rows,
  ordinate=reagarglst()$domain,
  sort=FALSE,
  legend.flag=FALSE,
  label.flag=FALSE,
  grey.flag=FALSE,
  stack.flag=FALSE,
  ppt=FALSE,   
  ...) 
{
  jorder <- seq(1:(ncol(tab)-1))
  x <- names(tab[,-1,drop=FALSE])[jorder]
  if(is.timeseries && valda(x)) {
    x <- as.Date(x)
  } else if(is.line) {
    x <- as.numeric(x)
  }
  y <- t(tab[,-1,drop=FALSE][,jorder,drop=FALSE])
  dimnames(y) <- list(colnames(tab)[-1][jorder],tab[,1])
  mode(y) <- "numeric"
  if(is.cumulative) { y[] <- apply(y,2,cumsum) } 
  if(is.line) {
    if(abcissa=="date") {abcissa <- ""}
    zobj <- zoo(x=y,order.by=x)
    arg <- list(
      x=zobj,
      ylab=ordinate,
      xlab=abcissa,
      plot.type='single',
      lwd=1
    )
    if(!'grey'%in%svalue(gnb$report$aggregate$text$plotopt)) {
      if(ppt) {
        arg[[length(arg)+1]] <- mycol <- c("purple")#POWERPOINT1#
      } else {
        arg[[length(arg)+1]] <- mycol <- rainbow(ncol(zobj),alpha=0.9) #NORMAL#grey((1:ncol(zobj))/ncol(zobj)*.7)#STET#
      }
      names(arg)[length(arg)] <- 'col'
    } else {
      if(ppt) {
        arg[[length(arg)+1]] <- mycol <- c("orange","pink","purple")#POWERPOINT2# 
      } else {
        arg[[length(arg)+1]] <- mycol <- grey((1:ncol(zobj))/ncol(zobj)*.7)#NORMAL#arg[[length(arg)+1]] <- mycol <- STET# 
      }
      names(arg)[length(arg)] <- 'col'
    }
    if(ppt) {
      arg[[length(arg)+1]] <- 2   #POWERPOINT3#
      names(arg)[length(arg)] <- 'lwd'    #POWERPOINT#
    }
    do.call(what="plot",args=arg)
    if(label.flag) {
      curves <- vector("list",ncol(zobj))
      for(i in 1:ncol(zobj)) {
        curves[[i]] <- list(x=as.Date(index(zobj)),y=as.numeric(zobj[,i]))
      }
      labcurve(
        curves=curves,
        labels=colnames(zobj),
        col.=mycol
      )
    }
    if(legend.flag) {
      legend(
        x='topleft',
        legend=colnames(zobj),
        col=mycol,
        lty=1,
        bty='n')
    }
  } else {
    if(abcissa=='date') { yarg <- t(y) } else {yarg <- t(y)} 
    arg <- list(
      height=yarg,
      names.arg=x,
      horiz=is.horizontal,
      #density=30,    #POWERPOINT4#
      border=NA,
      las=1,
      legend=legend.flag,
      ylab=ifelse(isTRUE(is.horizontal),abcissa,ordinate),
      xlab=ifelse(isTRUE(abcissa=="date"),"",ifelse(isTRUE(is.horizontal),ordinate,abcissa))
    )
    if(ppt) { arg <- c(arg,list(density=30)) }
    if(!grey.flag) {
      if(ppt) {
        if( !is.null(y) && length(y)>0 && ncol(y)==1) {nicecol <- "orange"} else {nicecol <- rainbow(ncol(y),alpha=.9,start=0)}#POWERPOINT5##rainbow(ncol(y),alpha=getsione("baralpha")*.7,start=.6)}
      } else {
        if( !is.null(y) && length(y)>0 && ncol(y)==1) {nicecol <- rainbow(ncol(y),alpha=.5,start=.6)} else {nicecol <- rainbow(ncol(y),alpha=.5,start=0)} #NORMAL#
      }
      arg[[length(arg)+1]] <- nicecol
      names(arg)[length(arg)] <- 'col'
    }
    if(stack.flag) {
      arg[[length(arg)+1]] <- TRUE
      names(arg)[length(arg)] <- 'beside'
    }
    if(is.horizontal) {
      arg[[length(arg)+1]] <- 0
      names(arg)[length(arg)] <- 'hadj'
    }
    do.call(what="barplot",args=arg)
  }
  grid(col='darkgray')
}
csvga <-
function(mydir="Z:/-Amberalpha/")
{
    write.csv(x=do.call(what="getga",args=reagarglst()),file=psz(mydir,"ga.csv"))
}
ghlili1 <-
function(h,...) 
{
    x <- strsplit(svalue(gnb$lib$gli),"\n")[[1]]
    declare("li",decli())
    sqlSave(channel=DBcon,      
            dat=data.frame(code=x),
            tablename="li",
            append=TRUE,
            rownames=FALSE,
            safer=TRUE)
    mysqlQuery("ALTER TABLE li ADD COLUMN (line SMALLINT PRIMARY KEY AUTO_INCREMENT)")
}
unse1 <-
function(...) 
{
    unse2(cont=gnb$universe$security$perm)                                        #perm <- toolbar
    add(gnb$universe$security$graphics$perm,gnb$universe$security$graphics$temp,expand=TRUE)
    add(gnb$universe$security$perm,unse3(gnb$universe$security$temp),expand=TRUE) #perm <- temp <- content
}
uncota1 <-
function(...) {
    uncota2(cont=gnb$universe$constituent$table$perm)                         #perm <- toolbar
    add(gnb$universe$constituent$table$perm,uncota3(gnb$universe$constituent$table$temp),expand=TRUE) #perm <- temp <- content

}
genaaindices <-
function(directory=Aaroot) 
{
    mysqlQuery("DROP TABLE IF EXISTS aaindices")
    mysqlQuery("CREATE TABLE aaindices (bti CHAR(8), name CHAR(30), country CHAR(20), region CHAR(20), development CHAR(20), i SMALLINT PRIMARY KEY AUTO_INCREMENT) ENGINE=MyISAM")
    mysqlQuery(psz(" LOAD DATA LOCAL INFILE '",directory,"indicesv4.csv' #table saved as csv with no text enclosures and 1 header line, dates ISO format
                INTO TABLE aaindices
                FIELDS
                TERMINATED BY ','
                "))
    mysqlQuery("ALTER TABLE aaindices ENGINE=MyISAM")
}
bbsfm <-
function() 
{
    c("US","JP","GB","HK","FR","DE","CH","SE","IT","ES","NL","FI","NO","DK","BE","AT","PT","IE","AU","KR","SG","CA")
}
chksolveqp <-
function()
{
    ce <- getce()
    n <- length(ce$full)
    constr <- poslim(
                n=n,
                lwr=-1e6,
                upr=1e6
                )
    sol1 <- tgt.solve.QP(
                ce=ce,
                dvec=matrix(rep(1,n),dimnames=list(buice(ce),NULL)),
                constr=constr,
                tgt=.1,
                ttyp="vol",
                tol=.1
                )
    soluncon <- solve(sol1$root*vcvce(ce)$T,rep(1,n))
    res <- all.equal(sol1$solution$unconstrainted.solution,soluncon) #solution matches unconstrained
    #constraints are met
    meanpos <- mean(abs(sol1$solution$solution))
    constr <- poslim(
                n=n,
                lwr=-meanpos,
                upr=meanpos
                )
    sol2 <- tgt.solve.QP(
                ce=ce,
                 dvec=matrix(rep(1,n),dimnames=list(buice(ce),NULL)),
                constr=constr,
                tgt=.1,
                ttyp="vol",
                tol=.1
                )
    res <- res & all(t(constr$Am)%*%sol2$solution$solution<=meanpos*1.00001)
    res <- res & all(t(constr$Am)%*%sol2$solution$solution>=-meanpos*1.00001)
    constr <- poslim(
                n=n,
                lambda=cbind((1:n)/2,rnorm(n)),
                categ=floor((1:n)/5)
                )
    sol3a <- tgt.solve.QP(
                ce=ce,
                 dvec=matrix(rep(1,n),dimnames=list(buice(ce),NULL)),
                constr=constr,
                tgt=.1,
                ttyp="vol",
                tol=.1
                )
    res <- res & all(t(constr$Am)%*%sol3a$solution$solution<=1.e-8)
    res <- res & all(t(constr$Am)%*%sol3a$solution$solution>=-1.e-8)
    constr <- poslim(
                n=n,
                lambda=cbind(rep(1,n))
                ) 
    sol3 <- tgt.solve.QP(
                ce=ce,
                dvec=matrix(rnorm(n),dimnames=list(buice(ce),NULL)),
                constr=constr,
                tgt=.1,
                ttyp="g",
                tol=.1
                )
    res <- res & all(t(constr$Am)%*%sol3$solution$solution<=meanpos*1.00001)
    res <- res & all(t(constr$Am)%*%sol3$solution$solution>=-meanpos*1.00001)
    res <- res & abs(sum(abs(sol3$solution$solution))-.1)<1.e-6
    sol4 <- tgt.solve.QP(
                ce=ce,
                dvec=matrix(rnorm(n),dimnames=list(buice(ce),NULL)),
                constr=constr,
                tgt=.01,
                ttyp="vol",
                tol=.1
                )
    ss <- sol4$solution$solution
    res <- res & all(t(constr$Am)%*%ss<=meanpos*1.00001)
    res <- res & all(t(constr$Am)%*%ss>=-meanpos*1.00001)
    res <- res & abs(sqrt(t(ss)%*%vcvce(ce)$T%*%ss)-.01)<1.e-6
    res[1,1]
}
chkfopo <-
function() {
    dace <- getda("ce")
    res <- TRUE
    prem <- getpa("prem")
    comp <- "s" #other components will not verify, due to differences in vcv
    for(method in c("svd","pca")) {
        x1 <- fopo(x=prem,method=method,comp=comp,expo=0)
        xinv <- fopo(x=prem,method=method,comp=comp,expo=-1)
        res <- res&chk(da=dace[1],x=x1,xinv=xinv,comp=comp)
        res <- res&chk(da=dace[length(dace)],x=x1,xinv=xinv,comp=comp)
    }
    test1a <- fopo(x=prem,method="svd",comp="t",expo=1) 
    test2a <- fopo(x=test1a,method="svd",comp="t",expo=-1)
    res <- res&max(abs(prem-test2a),na.rm=TRUE)<1e-10
    test1a <- fopo(x=prem,method="pca",comp="t",expo=1) 
    test2a <- fopo(x=test1a,method="pca",comp="t",expo=-1)
    res <- res&max(abs(prem-test2a),na.rm=TRUE)<1e-10
    test1a <- fopo(x=prem,method="pca",comp="m",expo=0) 
    res <- res&max(abs(prem-test1a),na.rm=TRUE)>1e-10
    res
}
chksdl <-
function(myseed=1234,la=-11,bb=1,b4=0,freq=1)
{
    set.seed(myseed)
    yx <- zoo(matrix(rnorm(400),200,2),as.Date(getca()[101:300]))
    #0   unweighted regression coefficients match lm
    r01 <- sdl(
        yx=yx,
        la=0,
        w=seq(from=1,to=1,length=nrow(yx)),
        b1=1,   #tail=0
        b2=0,   #head=0
        b3=1,   #curv=0
        b4=0,   #triangular
        bb=0,   #overall bayes
        napex=0  #apex of triangle
        )
    r00 <- summary(lm(yx[,1] ~ yx[,2]))
    stopifnot(all.equal(as.numeric(r00$coefficients[,1]),as.numeric(r01$coef)))
    stopifnot(all.equal(as.numeric(r00$r.squared),as.numeric(r01$r.squared)))
    stopifnot(all.equal(cor(r01$fit)[1,2]^2,r01$r.squared))
    #1   weighted regression coefficients match lm
    wt <- seq(from=1,to=nrow(yx),length=nrow(yx))
    r11 <- sdl(
        yx=yx,
        la=0,
        w=wt,
        b1=1,   #tail=0
        b2=0,   #head=0
        b3=1,   #curv=0
        b4=0,   #triangular
        bb=0,   #overall bayes
        napex=0  #apex of triangle
        )
    r10 <- summary(lm(yx[,1] ~ yx[,2],weight=wt))
    stopifnot(all.equal(as.numeric(r10$coefficients[,1]),as.numeric(r11$coef)))
    stopifnot(all.equal(as.numeric(r10$r.squared),as.numeric(r11$r.squared)))
    #4   R'R achieves desired smoothing
    #curvature reduced
    r2a <- sdl(
        yx=yx,
        la=la:-1,
        bb=0
        )
    r2b <- sdl(
        yx=yx,
        la=la:-1,
        bb=1
        )
    r2c <- sdl(
        yx=yx,
        la=la:-1,
        bb=10
        )
    r1curv <- sum(abs(r2a$coef[-1]%*%t(sdlcurv(-la))))
    r2curv <- sum(abs(r2b$coef[-1]%*%t(sdlcurv(-la))))
    r3curv <- sum(abs(r2c$coef[-1]%*%t(sdlcurv(-la))))
    stopifnot(is.na(rev(coredata(r2a$fit[,1]))[1]) && all(!is.na(rev(coredata(r2a$fit[,1]))[-1])))
    stopifnot(cor(r2b$coef[-1],r2a$coef[-1])>0)
    stopifnot(cor(r2c$coef[-1],r2b$coef[-1])>0)
    stopifnot(r1curv>r2curv)
    stopifnot(r2curv>r3curv && r3curv>0)
    #tail/head reduced
    r3a <- sdl(
        yx=yx,
        la=la:-1,
        b1=0,
        b2=0,
        bb=1
        )
    r3b <- sdl(
        yx=yx,
        la=la:-1,
        b1=1,
        b2=0,
        bb=1
        )
    r3c <- sdl(
        yx=yx,
        la=la:-1,
        b1=0,
        b2=1,
        bb=1
        )
    stopifnot(abs(r3b$coef[2])<abs(r3a$coef[2]))
    stopifnot(abs(r3c$coef[-la+1])<abs(r3a$coef[-la+1]))
    #rsquared reduced
    stopifnot(r2a$r.squared>r2b$r.squared)
    #plot fot visual check
    barplot(r3b$coef)
    #check final date of fit is final date of yx+taumo
    stopifnot(max(index(r3c$fit))==offda(max(index(yx)),1))
    #fuzzy check on correctness
    x <- folagpad(zoo(t(t(rnorm(200))),as.Date(getca()[101:300])),-30:0,pad=F)
    co<-sin(4*pi*freq*(1:31)/31)
    y <- zoo(x%*%co,index(x))+10*rnorm(nrow(x))
    yx <- cbind(y,x[,latotxt(0)])[-(1:30),]
    barplot(rbind(co,sdl(yx,la=-30:0,b1=0,bb=bb,b4=b4)$coef[-1]),bes=T,col=2:3)
    TRUE
}
chklags <-
function()
{
    res <- TRUE
    x <- rnorm(10)
    x1 <- lags(x,0)
    res <- res&all(x1==x && length(x1)==length(x))
    x1 <- lags(x,-1:1)
    res <- res&all(x1[,latotxt(0)]==x && length(x1[,latotxt(0)])==length(x))
    x1 <- lags(x,-11:0)
    res <- res&all(x1[,latotxt(0)]==x && length(x1[,latotxt(0)])==length(x))
    x1 <- lags(x,0:11)
    res <- res&all(x1[,latotxt(0)]==x && length(x1[,latotxt(0)])==length(x))
    x1 <- lags(x,1:3,pad=FALSE)
    res <- res&all(x1[1:9,latotxt(1)]==x[2:10]) && length(x1[,latotxt(1)])==length(x)
    x1 <- lags(x,1:3,pad=TRUE)
    res <- res&all(x1[3:12,latotxt(1)]==x[1:10]) && length(x1[,latotxt(1)])==length(x)+3
    x1 <- lags(x,-3:-1,pad=FALSE)
    res <- res&all(x1[2:10,latotxt(-1)]==x[1:9]) && length(x1[,latotxt(-1)])==length(x)
    res <- res&all(x1[4:10,latotxt(-3)]==x[1:7]) && length(x1[,latotxt(-1)])==length(x)
    x1 <- lags(x,-3:-1,pad=TRUE)
    res <- res&all(x1[2:11,latotxt(-1)]==x[1:10]) && length(x1[,latotxt(-1)])==length(x)+3
    res <- res&all(x1[4:13,latotxt(-3)]==x[1:10]) && length(x1[,latotxt(-1)])==length(x)+3
    x1 <- lags(x,-5:5,pad=TRUE)
    res <- res&all(x1[6:15,latotxt(0)]==x && length(x1[,latotxt(0)])==20)
    res
}
chkfolagpad <-
function()
{
    x <- zoo(matrix(rnorm(10),10,1),as.Date(getca()[101:110]))
    xx <- folagpad(x,-2:2,pad=TRUE)
    res <- TRUE
    res <- res && nrow(xx)==14 && ncol(xx)==5
    res <- res && all(coredata(xx[3:12,latotxt(0)])==coredata(x))
    res <- res && all(coredata(xx[1:10,latotxt(2)])==coredata(x))
    res <- res && all(coredata(xx[5:14,latotxt(-2)])==coredata(x))
    xx <- folagpad(x,-2:2,pad=FALSE)
    res <- res && nrow(xx)==10 && ncol(xx)==5
    res <- res && all(coredata(xx[1:10,latotxt(0)])==coredata(x))
    res <- res && all(coredata(xx[1:8,latotxt(2)])==coredata(x[3:10]))
    res <- res && all(coredata(xx[3:10,latotxt(-2)])==coredata(x[1:8]))
    xx <- folagpad(x,-4:-2,pad=TRUE)
    res <- res && nrow(xx)==14 && ncol(xx)==3
    res <- res && all(coredata(xx[3:12,3])==coredata(x))
    res <- res && all(coredata(xx[5:14,1])==coredata(x))
    xx <- folagpad(x,2:4,pad=FALSE)
    res <- res && nrow(xx)==10 && ncol(xx)==3
    res <- res && all(coredata(xx[1:8,1])==coredata(x[3:10,1]))
    res <- res && all(coredata(xx[1:6,3])==coredata(x[5:10]))
    res
}
newred <-
function()
{
mysqlQuery("DROP TABLE IF EXISTS aared")
mysqlQuery("
    CREATE TABLE aared (
      date    date DEFAULT NULL,
      dateminus1  date DEFAULT NULL,
      year    smallint DEFAULT NULL,
      month   tinyint DEFAULT NULL,
      datew   date DEFAULT NULL,
      bui     char(18) DEFAULT NULL,

      prdo    double DEFAULT NULL COMMENT '$ close',
      prdov   double DEFAULT NULL COMMENT '$ vwap',
      cash    double DEFAULT NULL,
      ret     double DEFAULT NULL,
      revt    double DEFAULT NULL COMMENT '$ premium vwap',

      prlo    double DEFAULT NULL COMMENT ' close',
      prlov   double DEFAULT NULL COMMENT ' vwap',
      cashlo  double DEFAULT NULL,
      retlo   double DEFAULT NULL,
      revtlo  double DEFAULT NULL COMMENT ' premium vwap',

      turn    double DEFAULT NULL COMMENT '$ median daily turnover 5 days',
      xo      double DEFAULT NULL,
      xt      double DEFAULT 0,
      xc      double DEFAULT NULL,
      pe      double DEFAULT NULL,
      pev     double DEFAULT NULL
    ) ENGINE = MyISAM
")
}
newaasnap <-
function(really=FALSE)
{
    return() #this for safety!
    if(!really) return()
    if("aasnap"%in%dirtab()) {
    mysqlQuery("DELETE FROM aasnap")
    } else {
    mysqlQuery("
        CREATE TABLE aasnap (
          downdate  date NOT NULL,
          date      date NOT NULL,
          bui       char(18) NOT NULL,
          prdonoco  double,
          target    double,
          PRIMARY KEY (downdate, date, bui)
        ) ENGINE = MyISAM;
    ")
    }
}
chkaatsdaily <-
function(bui=aabuidone()[1])
{
    droptab("tmp")
    mysqlQuery(psz("
        CREATE TABLE tmp AS
            SELECT datew, bui, EXP(SUM(LOG(1+a.ret)))-1 AS rd
            FROM 
                (
                SELECT * FROM aared
                WHERE bui = '",bui,"'
                ) AS a
            GROUP BY datew, bui
        "))
    addidx("tmp",c("datew","bui"))
    res <- mysqlQuery("
            SELECT tmp.*, aats.redoto
            FROM aats INNER JOIN tmp
            ON aats.bui=tmp.bui AND aats.date=tmp.datew
        ")
    all.equal(res[-1,3],res[-1,4])
}
newnupms <-
function() {
    droptab("nupms")
    mysqlQuery(
        "CREATE TABLE `nupms` (
        `btkpms` CHAR(18) NULL DEFAULT NULL,
        `qty` DOUBLE NULL DEFAULT NULL,
        `nav` DOUBLE NULL DEFAULT NULL,
        `buipms` CHAR(18) NULL DEFAULT NULL,
        `name` CHAR(80) NULL DEFAULT NULL
        )
        ENGINE=MyISAM
        ")
}
newnuba <-
function() {
    droptab("nuba")
    mysqlQuery("
        CREATE TABLE `nuba` (
        `btkba` CHAR(18) NULL DEFAULT NULL,
        `buiba` CHAR(18) NULL DEFAULT NULL,
        `nuba` DOUBLE NULL DEFAULT NULL
    )
    ENGINE=MyISAM
    ROW_FORMAT=DEFAULT
    ")
    addidx("nuba","buiba")
}
chkxtnu <-
function(
            myoffset=0,
            ime=80,
            inuprev=20 #for consistency check
            ) {
    mydate <- thiswed(myoffset=myoffset)
    fnams <- sort(dir(tradedir(mydate)))
    fnum <- max(grep(x=fnams,patt="^gh............csv$")) #latest
    ipotrade <- apply(0<pometrics(type='trade',ime=ime,da=thiswed(myformat="%Y-%m-%d",myoffset=+1)),2,any)
    chknam <- c(
            "1  dated this week",
            "2  cashnu() reasonable",
            "3  trade file exists",
            "4  pometrics OK",
            "5  NA price/ticker absent in tradefile",
            "6  number of trades ~ universe size",
            "7  first trade workout",
            "8  last trade workout",
            "9  weight correlation vs last period"
            )
    checks <- rep(FALSE,length(chknam))
    names(checks) <- chknam
    danu <- as.character(extract("nu","distinct date"))
    checks[1] <- identical(danu,thiswed(myformat="%Y-%m-%d",myoffset=myoffset))
    checks[2] <- as.numeric(cashnu(mydate=thiswed(myoffset=myoffset))[,"SUM((nut)*prdovw)/MAX(nav)"])<0.02
    checks[3] <- 0<length(fnum)
    pp <- pometrics(da=mydate,ime=ime)
    checks[4] <- all(.5<pp & pp<1)
    trades <- read.csv(psz(tradedir(mydate),fnams[fnum]),skip=2)
    checks[5] <- !any(is.na(trades[,c("LastPrice","bloomberg")])) && !any(trades[,c("LastPrice","bloomberg")]=="NA")
    ntrades <- nrow(trades)
    dssu <- ixxtods(ixx=as.numeric(names(ipotrade)[ipotrade]),xx="su")
    nbui <- as.numeric(extract("vesu","date, count(*)",psz("where datestamp IN (",flds(dssu),") group by date order by date desc limit 1"))[,2])
    checks[6] <- abs(nbui-ntrades)/nbui<0.05
    currenttrade <- trades[1,,drop=FALSE]
    thisnu <- extract("nu","date,btkpms,current, nu,nut",psz("where btkpms='",currenttrade[,"bloomberg"],"' "))
    prevnu <- extract("venuvi","date,btkpms,current, nu,nut",psz("where ixx = ",inuprev," AND btkpms='",currenttrade[,"bloomberg"],"' order by date desc limit 2"))[1,,drop=FALSE]
    checks[7] <- prevnu[,"nu"]==thisnu[,"current"] & thisnu[,"nut"]==currenttrade[,"Qty"]
    currenttrade <- trades[nrow(trades),,drop=FALSE]
    thisnu <- extract("nu","date,btkpms,current, nu,nut",psz("where btkpms='",currenttrade[,"bloomberg"],"' "))
    prevnu <- extract("venuvi","date,btkpms,current, nu,nut",psz("where ixx = ",inuprev," AND btkpms='",currenttrade[,"bloomberg"],"' order by date desc limit 2"))[1,,drop=FALSE]
    checks[8] <- prevnu[,"nu"]==thisnu[,"current"] & thisnu[,"nut"]==currenttrade[,"Qty"]
    nuti <- mysqlQuery(psz("
                SELECT nu.ti, venuvi.ti
                FROM nu, venuvi
                WHERE nu.bui=venuvi.bui AND venuvi.ixx = ",inuprev
                ))
    checks[9] <- 0.9<cor(nuti,use="c")[1,2]
    msgs <- c("OK","CHECKS FAILED")
    chkmodal(checks=checks,msgs=msgs)
}
chkpre <-
function(ime=92) {
    chknam <- c(
            "1  account files exist",
            "2  no .Rdata",
            "3  00src()",
            "4  no browser() debug in libraries",
            "5  aaupdate()",
            "6  thiswed()",
            "7  aadir()",
            "8  aame()",
            "9  disk space",
            "10 queue empty",
            "11 dergu()"
            )
    checks <- rep(FALSE,length(chknam))
    names(checks) <- chknam
    checks[[1]] <- length(filenu(ime=ime))==2
    fnams <- dir(aagetwd(),all.files=TRUE,ignore.case=TRUE,pattern=".r")
    checks[[2]] <- ".rprofile"%in%fnams && !".RData"%in%fnams
    source(psz(Aaroot,"00src.R"))
    checks[[3]] <- TRUE
    fnam <- psz(Aaroot,"Aa.R")
    shell(psz("del ",fnam))
    dump(ls(name=globalenv()),file=fnam)
    checks[[4]] <- length(grep(patt="browser()",x=scan(fnam,what="character"),fixed=TRUE))==2 #this instance + 1 above!
    dlength <- as.integer(difftime(aaupdate(),as.Date(Sys.time())))
    checks[[5]] <- (-700<dlength)&&(dlength<-350)
    iday <- as.integer(difftime(as.Date(thiswed(myfo="%Y-%m-%d")),as.Date(Sys.time())))
    checks[[6]] <- (-8<iday)&&(iday<0)
    checks[[7]] <- length(list.files(aadir(),recursive=TRUE))==0
    checks[[8]] <- aame()==ime
    checks[[9]] <- 20<diskfree("d:")/1e9
    newcon()
    derqu()
    checks[[10]] <- sqlnrow("qu")==0
    checks[[11]] <- dergu(visible=FALSE)
    msgs <- c("READY - reboot, login, leave idle and unlocked, no Bloomberg app, screen off","NOT READY - fix issues")
    chkmodal(checks=checks,msgs=msgs)
}
chkmodal <-
function(
                checks, #named logical
                msgs
                ) {
    gwin <- gframe(horiz=FALSE,expand=TRUE,cont=gwindow())
    enabled(gwin) <- FALSE
    add(gwin,gcheckboxgroup(items=names(checks),checked=checks))
    statusmsg <- ifelse(all(checks),msgs[1],msgs[2])
    add(gwin,gtext(statusmsg))
}
chkbat <-
function(ime=92) {
    chknam <- c(
            "1  reinvested filecount",
            "2  price-only filecount",
            "3  weekly table aats has current rows",
            "4  daily table aared has current rows",
            "5  queue derived qu is empty",
            "6  portfolio po has current trades",
            "7  runtimes melo reasonable",
            "8  snapshot aasnap loaded"
            )
    checks <- rep(FALSE,length(chknam))
    names(checks) <- chknam
    nbui <- length(latestsu())
    checks[[1]] <- nbui<=length(grep(patt="EQ.*rdata",x=dir(aadir(),include.dirs=FALSE)))
    checks[[2]] <- nbui==length(grep(patt="EQ.*rdata",x=dir(aasnapdir(),include.dirs=FALSE)))
    extract("aats","distinct bui",psz("WHERE date=",flds(thiswed(myfo="%Y-%m-%d"))))
    n0 <- as.numeric(extract("aats","count(*)",psz("WHERE date=",flds(thiswed(myfo="%Y-%m-%d")))))
    n1 <- as.numeric(extract("aats","count(*)",psz("WHERE date=",flds(thiswed(myfo="%Y-%m-%d",myoff=-1)))))
    checks[[3]] <-  ((nbui-20)<=n0) & (abs(n0-n1)<5)
    n0 <- as.numeric(extract("aared","count(*)",psz("WHERE date=",flds(thiswed(myfo="%Y-%m-%d")))))
    n1 <- as.numeric(extract("aared","count(*)",psz("WHERE date=",flds(thiswed(myfo="%Y-%m-%d",myoff=-1)))))
    checks[[4]] <-  ((nbui-110)<=n0) & (abs(n0-n1)<15)
    derqu()
    checks[[5]] <- sqlnrow("qu")==0
    trades <- pometrics(ty='tr',da=thiswed(myfor="%Y-%m-%d",myoff=1))
    checks[[6]] <- 2<sum(0<trades)
    hours <- rev(melo(ime=ime)[,"elapsed"])[1]
    checks[[7]] <- (2<hours) & (hours<20)
    n0 <- as.numeric(extract("aasnap","downdate, count(*)",psz("where downdate=",flds(thiswed(myfo="%Y-%m-%d",myoff=0))))[1,2])
    n1 <- as.numeric(extract("aasnap","downdate, count(*)",psz("where downdate=",flds(thiswed(myfo="%Y-%m-%d",myoff=-1))))[1,2])
    checks[[8]] <- (1.5e5<n0) & (1.5e5<n0) & abs(n0-n1)/n1<.1
    msgs <- c("OK","CHECKS FAILED")
    chkmodal(checks=checks,msgs=msgs)
}
chkme <-
function() 
{
    res <- TRUE
    me <- gettab("me")[,c("ijo","isu","ili","isi")]
    mode(me) <- "numeric"
    res <- res & nrow(me)>0
    res <- res & all(me[,"ijo"]%in%getijo("all"))
    res <- res & all(me[,"isu"]%in%getive("su",how="all"))
    res <- res & all(me[,"isi"]%in%getive("si",how="all"))
    res <- res & all(me[,"ili"]%in%c(getive("li",how="all"),0))
    res
}
newpoco <-
function() 
{
    mysqlQuery("
            CREATE TABLE `poco` (
              `date`  date NOT NULL,
              `bui`   char(18) NOT NULL,
              `cov`   double NOT NULL
            ) ENGINE = MyISAM
            ")
    addidx(tab="poco",fields="date")
    addidx(tab="poco",fields="bui")
    addidx(tab="poco",fields=c("date","bui"))
}
newru <-
function(tab="ru") 
{
    droptab(tab)
    declare(tabname=tab,
            dec=decru(),
            engine="MyISAM")
    mysqlQuery("ALTER TABLE ru ADD PRIMARY KEY (date,indu,type)")
}
chkrece <-
function() 
{
    res <- TRUE
    k <- getsione("nfac")
    refields <- psz("re",1:k)
    vafields <- psz("va",1:k)
    res <- res && identical(sort(dirfld("rece")),sort(c("date","bui","dace","lace","tare","rer","ret","vat","var",refields,vafields)))
    key1 <- extract(fields="DISTINCT date, bui",
                    tab="rece",
                    qual="ORDER BY date, bui")
    key2 <- extract(fields="DISTINCT date, bui",
                    tab="re",
                    qual="ORDER BY date, bui")
    res <- res && identical(key1,key2) #add factors 2-k to compare with re.res
    m1 <- tabtomat(extract(field=c("date","bui",paste(refields[-1],collapse="+")),
        tab="rece",
        qual="WHERE lace=0"))
    m2 <- tabtomat(extract(field=c("date","bui","res"),
        tab="re",
        qual="WHERE lace=0"))
    mode(m1) <- "numeric"
    mode(m2) <- "numeric"
    res <- res&all.equal(m1,m2) #fails!
    res
}
chkrepe <-
function(dano=getda("no"))
{
    tst <- TRUE
    ce <- getce()
    fococe(inp="rem",out="rem")   #slow
    rem <- getpa("rem")
    res <- getpa("res")
    rer <- getpa("rer")
    ret <- rem+res+rer
    remm <- getpa("remm")    
    rems <- getpa("rems")    
    remr <- getpa("remr")    
    remt <- getpa("remt")   
    tst <- tst & all.equal(remm,remt)
    tst <- tst & all.equal(rems,rems*0)
    tst <- tst & all.equal(remr,remr*0)
    hh <- remm*NA
    hh[!is.na(remm)] <- rnorm(sum(!is.na(remm)))
    addpa("hh")
    putpa(hh,"hh")
    pococe0("hh","hh")
    pococe0("hhm","hhm")
    hhm <- getpa("hhm")
    hhs <- getpa("hhs")
    hhr <- getpa("hhr")
    hhmm <- getpa("hhmm")
    hhms <- getpa("hhms")
    hhmr <- getpa("hhmr")
    hhmt <- getpa("hhmt")
    tst <- tst & all.equal(hhmm+hhms+hhmr,hhmt)
    tst <- tst & all.equal(hhmm,hhmt)
    tst <- tst & all.equal(hhms,hhms*0)
    tst <- tst & all.equal(hhmr,hhmr*0)
    tst <- tst & all.equal(apply(hhm*rem+hhs*res,1,sum,na.rm=TRUE),apply((hhm+hhs)*(rem+res),1,sum,na.rm=TRUE))
    tst <- tst & all.equal(apply(hhs*rem,1,sum,na.rm=TRUE),apply(hhm*0,1,sum,na.rm=TRUE))
    tst <- tst & all.equal(apply(hhr*rem,1,sum,na.rm=TRUE),apply(hhm*0,1,sum,na.rm=TRUE))
    tst <- tst & all.equal(apply(hhm*res,1,sum,na.rm=TRUE),apply(hhm*0,1,sum,na.rm=TRUE))
    tst <- tst & all.equal(apply(hhr*res,1,sum,na.rm=TRUE),apply(hhm*0,1,sum,na.rm=TRUE))
    tst
}
chkneutce <-
function(
                out="mone"
                )
{
    dace <- getda("po")
    da <- dace[c(2,length(dace))]
    test <- getpa(out)
    res<-TRUE
    for(i in seq_along(da)) {
        po <- t(test[as.Date(da[i]),,drop=FALSE])
        ce <- getce(da[i])
        vam <- prdce(x=ce,po=po[!is.na(po),,drop=FALSE])[,"vam"]
        res <- res&sum(abs(vam))<1.e-10
    }
    res
}
chkinvce <-
function()
{
    ce <- getce()
    vv <- vcvce(ce)$T
    all.equal(as.numeric(solve(vv)),as.numeric(invce(ce)))
}
chkhce <-
function(ce=getce())
{
    h <- hce(ce)
    vv <- vcvce(ce)
    all.equal(as.numeric(t(h)%*%(vv$S+vv$M)%*%h),as.numeric(diag(ncol(h))))
}
chkfotopoqpce <-
function()
{
    fotopoce()
    fotopoqpce()
    po <- getpa("po")
    poqp <- getpa("poqp")
    res <- all.equal(coredata(po)[!is.na(po)],coredata(poqp)[!is.na(poqp)],tol=1.e-6)
    x <- apply(abs(po),1,sum,na.rm=TRUE)
    res <- res & all(abs(x)<1.e-6 | abs(x-1)<1.e-6)
    fotopoqpce(mzero=TRUE)
    delfld("pa","poqpm")
    pococe0(inp="poqp",out="poqp")
    poqpm <- getpa("poqpm")
    res <- res & all(coredata(poqpm)[!is.na(poqpm)]<1.e-10)
    fotopoqpce(szero=TRUE)
    delfld("pa","poqps")
    pococe0(inp="poqp",out="poqp")
    poqps <- getpa("poqps")
    res <- res & all(coredata(poqps)[!is.na(poqps)]<1.e-10)
    poqp <- getpa("poqp")
    res <- res & !all(abs(coredata(poqp)[!is.na(poqp)])<=4e-2)
    delfld("pa","poqp")
    fotopoqpce(pomaxval=0.04)
    poqp <- getpa("poqp")
    res <- res & all(abs(coredata(poqp)[!is.na(poqp)])<=4e-2)
    res
}
chkfopofo <-
function()
{
    res <- TRUE
    fotopoce(inp='prem',out='po1',unit=FALSE)
    potofoce(inp='po1',out='po1fo1')
    prem <- getpa('prem')
    prem1t <- getpa('po1fo1t')
    prem1m <- getpa('po1fo1m')
    prem1s <- getpa('po1fo1s')
    prem1r <- getpa('po1fo1r')
    tst <- prem-prem1t
    res <- res & all(coredata(tst)[!is.na(tst)]<1.e-10)
    fococe(inp='prem',out='fo1')
    fo1m <- getpa("fo1m")
    fo1s <- getpa("fo1s")
    fo1r <- getpa("fo1r")
    fo1t <- getpa("fo1t")
    tst <- prem1t-fo1t
    res <- res & all(coredata(tst)[!is.na(tst)]<1.e-10)
    tst <- prem1m-fo1m
    res <- res & all(coredata(tst)[!is.na(tst)]<1.e-10)
    tst <- prem1s-fo1s
    res <- res & all(coredata(tst)[!is.na(tst)]<1.e-10)
    tst <- prem1r-fo1r
    res <- res & all(coredata(tst)[!is.na(tst)]<1.e-10)
}
chkeq14 <-
function(ce=getce())
{
    res <- TRUE
    x <- ldgce(ce)
    h <- hce(ce)
    hpa <- (1:nrow(x))/nrow(x)
    hcf <- h%*%t(x)%*%hpa
    vv <- vcvce(ce)$T
    a <- vv%*%hpa
    deltainv <- diag(as.numeric(1./spvce(ce)**2))
    k <- ncol(x)
    n <- nrow(x)
    hsp <- deltainv%*%(diag(n)-x%*%t(h))%*%a
    a29lhs <- t(x)%*%invce(ce)%*%x%*%t(h)
    a29rhs <- t(x)%*%invce(ce)
    res <- res&all.equal(a29lhs,a29rhs)
    a30lhs <- a29rhs
    colnames(a30lhs) <- NULL
    a30rhs <- solve(diag(k)+t(x)%*%deltainv%*%x)%*%t(x)%*%deltainv
    res <- res&all.equal(a30lhs,a30rhs)
    a31lhs <- vv%*%h
    a31rhs <- x%*%(diag(k)+solve(t(x)%*%deltainv%*%x))
    res <- res&all.equal(a31lhs,a31rhs)
    a31alhs <- vv%*%hcf
    a31arhs <- x%*%t(h)%*%a
    res <- res&all.equal(a31alhs,a31arhs)
    a31blhs <- vv%*%hsp
    a31brhs <- (a-x%*%t(h)%*%a)
    res <- res&all.equal(a31blhs,a31brhs)
    a32lhs <- vv%*%h%*%t(x)%*%solve(vv)
    a32rhs <- x%*%t(h)
    res <- res&all.equal(a32lhs,a32rhs)
    res
}
chkimputepa <-
function(n=20,print=TRUE,iter=2) {
    test <- prem <- getpa("prem")
    putdano(getda("ce")[1])
    ce <- getce()
    i <- fulce(ce)
    stopifnot(length(i)>n)
    #bui <- i[1:n]
    bui <- sample(i,n)
    test[,bui] <- NA
    addpa("test")
    putpa(x=test,field="test")
    test1 <- imputepa(ipa="test",iter=iter)
    i <- which(!apply(is.na(test1[,bui]),1,any))
    cc <- cor(cbind(prem[i,bui],test1[i,bui]))
    diag(cc) <- NA
    cc1 <- mean(cc[1:n,(1:n)+n],na.rm=TRUE)
    cc2 <- mean(cc[cbind(1:n,(1:n)+n)])
    y <- as.numeric(prem[i,bui])
    x <- as.numeric(test1[i,bui])
    lms <- summary(lm(y ~ x))
    if(print) print(lms)
    b <- lms$coefficients
    cc2/cc1>1 &&    #correlation with own systematic is greater than correlation with all systematic
    b[2,1]>0.5 &&   #regression of data on systematic has coeff between 0.5...
    b[2,1]<1.5 &&   #and 1.5
    b[2,3]>20       #and the t-stat is highly significant
}
chkchar <-
function(x,illegals=c(",","=","'")) {
    for(i in seq_along(illegals)) if(0<length(grep(patt=psz("\\",illegals[i]),x=x))) return(FALSE)
    TRUE
}
chkbtiregions <-
function() { 
    mybti <- as.character(extract("cu","DISTINCT bti"))
    all(mybti%in%unlist(btiregions())) && all(!duplicated(unlist(btiregions())))
}
chkautosu <-
function(big=minmaxweight(),isustart=1)
{
    droptab("tmp")
    mysqlQuery(psz("
        CREATE TABLE tmp 
        AS SELECT bui, MIN(date) as bigdate
        FROM aauniv
        WHERE weight>",big,"
        GROUP BY bui
        "))
    addidx("tmp","bui")
    ds <- ixxtods(xx="su",ixx=isustart)
    droptab("tmp1")
    mysqlQuery(psz("
        CREATE TABLE tmp1 AS
        SELECT a.bui, minsudate, bigdate
        FROM tmp INNER JOIN
            (
            SELECT bui, MIN(date) AS minsudate 
            FROM vesu 
            WHERE vesu.datestamp>='",ds,"'
            GROUP BY bui
            ) AS a
        ON tmp.bui=a.bui
        GROUP BY bui
        "))
    tab <- gettab("tmp1")
    nrow(tab)>0 && all(tab[,"minsudate"]>tab[,"bigdate"])
}
newcuxsts <-
function(tree=c("bb","icb"),screenedon="best")
{
    tree <- match.arg(tree)
    droptab("cuxsts")
    mysqlQuery("
        CREATE TABLE cuxsts AS 
            SELECT max(cu.date) AS date, max(cu.btk) AS btk, max(cu.weight) AS weight, max(cu.bui) AS bui, max(cu.countryfull) AS countryfull, max(cu.countryissue) AS countryissue, max(cu.bti) AS bti, max(cu.btklocal) AS btklocal, max(cu.weightlocal) AS weightlocal, max(cu.big) AS big, max(cu.bigish) AS bigish
            FROM cu 
            WHERE cu.big OR cu.bigish
            GROUP BY date, bui #eliminate repeats on this key
            ")
    delbui <- setdiff(extract("cu","DISTINCT bui"),extract("aaxs","DISTINCT bui"))
    mysqlQuery(psz("DELETE FROM cuxsts WHERE bui IN (",fldlst(delbui,"'","'"),")"))
    mysqlQuery("ALTER TABLE cuxsts CHANGE date idate char(10)")
    mysqlQuery("ALTER TABLE cuxsts ADD date char(10)")
    mysqlQuery("ALTER TABLE cuxsts ADD bopr double")
    mysqlQuery("ALTER TABLE cuxsts ADD industry_sector char(32)")
    mysqlQuery("ALTER TABLE cuxsts ADD industry_group char(32)")
    mysqlQuery("ALTER TABLE cuxsts ADD industry_subgroup char(32)")
    mysqlQuery("ALTER TABLE cuxsts ADD industry_group_num smallint")
    mysqlQuery("ALTER TABLE cuxsts ADD industry_subgroup_num smallint")
    mysqlQuery("ALTER TABLE cuxsts ADD industry_group_herf double")
    mysqlQuery("ALTER TABLE cuxsts ADD industry_subgroup_herf double")
    mysqlQuery("ALTER TABLE cuxsts ADD belowhigh double")
    mysqlQuery("ALTER TABLE cuxsts ADD momentum double")
    mysqlQuery("ALTER TABLE cuxsts ADD metric double")
    mysqlQuery("ALTER TABLE cuxsts ADD category char(32)")  #a working column used in cuxstsme() GSF for calculating av
    mysqlQuery("ALTER TABLE cuxsts ADD av double")
    mysqlQuery("ALTER TABLE cuxsts ADD ter1 double")
    mysqlQuery("ALTER TABLE cuxsts ADD ter2 double")
    mysqlQuery("ALTER TABLE cuxsts ADD qu tinyint")
    mysqlQuery("ALTER TABLE cuxsts ADD te char(100)")   #a category name, often composed derived from qu + comment, or can just be sector
    mysqlQuery("ALTER TABLE cuxsts ADD validbest smallint")
    mysqlQuery("ALTER TABLE cuxsts ADD turn double")
    mysqlQuery("ALTER TABLE cuxsts ADD bestidate char(10)") #last idate for which tests in cuxstsscr fail, one per te
    mysqlQuery("ALTER TABLE cuxsts ADD PRIMARY KEY (idate, bui)")
    addidx("cuxsts")
    mysqlQuery("UPDATE cuxsts, cucaprior SET cuxsts.date=cucaprior.date WHERE cuxsts.idate=cucaprior.idate")
    addidx("cuxsts",c("date","bui"))
    cuxstsvalidbest(screenedon=screenedon)
    if(tree=="bb") {
        mysqlQuery("
            UPDATE cuxsts, aaxs
            SET cuxsts.industry_sector = aaxs.industry_sector, cuxsts.industry_group = aaxs.industry_group, cuxsts.industry_subgroup = aaxs.industry_subgroup
            WHERE cuxsts.bui=aaxs.bui
            ")
    } else {
        mysqlQuery("
            UPDATE cuxsts, aaxs
            SET cuxsts.industry_sector = aaxs.icb_industry_name, cuxsts.industry_group = aaxs.icb_sector_name, cuxsts.industry_subgroup = aaxs.icb_subsector_name
            WHERE cuxsts.bui=aaxs.bui
            ")
    }
    mysqlQuery("UPDATE cuxsts SET industry_sector='unassigned' WHERE industry_sector IS NULL")
    mysqlQuery("ALTER TABLE cuxsts CHANGE industry_sector industry_sector CHAR(32) NOT NULL")
    #mysqlQuery("
    #    UPDATE cuxsts, aats
    #    SET cuxsts.bopr = aats.bopr, cuxsts.belowhigh = aats.belowhigh, cuxsts.momentum=aats.momentum
    #    WHERE cuxsts.bui=aats.bui AND cuxsts.date=aats.date
    #    ")
    for(ind in c("industry_group","industry_subgroup")) {#herfindahl bits
        mysqlQuery(psz("
            UPDATE cuxsts,
                (
                SELECT cuxsts.",ind,", cuxsts.date, POW(a.totweight,2)/SUM(POW(cuxsts.weight,2)) AS herfnum, count(*) AS num
                FROM cuxsts, 
                    (
                    SELECT ",ind,", date, SUM(weight) AS totweight
                    FROM cuxsts
                    GROUP BY ",ind,", date
                    ) AS a
                WHERE cuxsts.",ind,"=a.",ind,"
                    AND cuxsts.date=a.date
                GROUP BY cuxsts.",ind,", cuxsts.date
                ) AS herf
            SET cuxsts.",ind,"_herf=herf.herfnum, cuxsts.",ind,"_num=herf.num
            WHERE cuxsts.date=herf.date AND cuxsts.",ind,"=herf.",ind,"
        "))
    }
}
chkcuxstsdel <-
function(minperte=30) {
    mysqlQuery(psz("
            SELECT te, MAX(idate) AS idate
            FROM
                (SELECT te, idate, COUNT(*) AS count
                FROM cuxsts
                GROUP BY te, idate) AS a
            WHERE count<30
            GROUP BY te
        "))
}
newvr <-
function() {
    mysqlQuery("
        CREATE TABLE `aard` (
            `iseq` INT NULL DEFAULT NULL AUTO_INCREMENT,
            `name` CHAR(100) NULL DEFAULT NULL,
            `myclass` CHAR(20) NULL DEFAULT NULL,
            `ime` INT(10) NULL DEFAULT NULL,
            `ijo` INT(10) NULL DEFAULT NULL,
            `isu` INT(10) NULL DEFAULT NULL,
            `datestamp` CHAR(30) NULL DEFAULT NULL,
            `enddate` DATE NULL DEFAULT NULL,
            `comp` CHAR(2) NULL DEFAULT NULL,
            `freq` CHAR(2) NULL DEFAULT NULL,
            `mycomment` CHAR(100) NULL DEFAULT NULL,
            `note1` CHAR(20) NULL DEFAULT NULL,
            `note2` CHAR(20) NULL DEFAULT NULL,
            PRIMARY KEY (`iseq`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM
        ROW_FORMAT=DEFAULT
        AUTO_INCREMENT=1
    ")
}
newjure <-
function() {
    droptab("jure")
    mysqlQuery("
        CREATE TABLE `jure` (
            `industry` CHAR(60) NOT NULL,
            `mystart` DATE  NULL,
            `myend` DATE  NULL,
            `bcoef` DOUBLE NULL,
            `btstat` DOUBLE  NULL,
            `R2` DOUBLE  NULL,
            `Fstat` DOUBLE  NULL,
            `df` SMALLINT  NULL#,
            #PRIMARY KEY (`industry`, `mystart`, `myend`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM
        ROW_FORMAT=DEFAULT
    ")
}
newju <-
function() {
    droptab("ju")
    mysqlQuery("
        CREATE TABLE `ju` (
            `date` DATE NOT NULL,
            `bui` CHAR(18) NOT NULL,
            `totret` DOUBLE NOT NULL,
            `bl1` DOUBLE  NULL, # best
            `bretl1` DOUBLE  NULL, # best
            `bret5l1` DOUBLE  NULL, # best
            `bret15l1` DOUBLE  NULL, # best
            `voll1` DOUBLE  NULL, # volume
            `breakl1` DOUBLE  NULL, # breakaway (divergent change in price,tgt)
            `fresh` DOUBLE  NULL, # days
            `industry` CHAR(60) NULL,
            PRIMARY KEY (`date`, `bui`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM
        ROW_FORMAT=DEFAULT
    ")
}
aasource <-
function(classes=allxx())
{
    for(i in seq(along=classes))
        {
        print(classes[i])
        print(system.time(eval(call(psz("der",classes[i])))))
         }
}
foce <-
function(x=foret(...),...)
{
    x$ce <- getce()
    ceret <- x$ceret
    i <- is.na(ceret)
    coredata(ceret)[i] <- coredata(msce(x=x$ce,ret=natox(ceret,0)))[i] #bootstrap: for the first p periods where NA may be present
    if(x$dores) { x$res <- resce(x=x$ce,ret=ceret) }
    d1 <- offda(x=min(index(x$ret)),lags=x$tauma-1+x$taumo)   #first available x date, after lagging for model alignment (first estimation y date)
    d2 <- max(index(x$ret))                             #last available x date without lagging (last estimation y date)
    x$estdate <- as.Date(extrca(d1,d2))
    x$fitdate <- as.Date(extrca(d1,offda(d2,x$taumo)))
    x$estweight <- seq(from=1,to=3,length=length(x$estdate))   #for estimation
    x$score <- scoce(x=x$ce,ret=ceret,type=x$type)[,x$jfactor,drop=FALSE]
    if(x$demean) {
        scoremean <- apply(x$score[x$estdate,,drop=FALSE],MARGIN=2,FUN=weighted.mean,w=x$estweight,na.rm=TRUE)
        x$score <- sweep(x$score,STATS=scoremean,MARGIN=2,FUN="-")
    }
    if(x$retpos=="ret") {
        x$ldg <- ldgce(x=x$ce,type=x$type)[,x$jfactor,drop=FALSE]
    } else if(x$retpos=="pos") {
        x$ldg <- fmpce(x=x$ce,type=x$type)[,x$jfactor,drop=FALSE]
    }
    if(x$norm) {
        x$score <- zoonorm(x$score,dimension="ts")
        if(x$dores) {x$res <- zoonorm(x$res,dimension="ts")}
    }
    x
}
releasedir <-
function() {psz(aahome(),"\\RSQL\\")}
bbudir <-
function(mydate=thiswed(),ver=aame(),type="xo") {aadir(mydate,subdir=,psz("bbu",ver,type,"\\"))}
newaauniv <-
function()
{
    mysqlQuery("DROP TABLE IF EXISTS aauniv")
    mysqlQuery("
        CREATE TABLE `aauniv` (
            `date` CHAR(10) NOT NULL,
            `btk` CHAR(17) NOT NULL,
            `weight` DOUBLE NULL DEFAULT NULL,
            `bui` CHAR(18) NULL DEFAULT NULL,
            `countryfull` CHAR(100) NULL DEFAULT NULL,
            `countryissue` CHAR(6) NULL DEFAULT NULL,
            `bti` CHAR(17) NOT NULL DEFAULT 'noLocal',
            `btklocal` CHAR(17) NULL DEFAULT NULL,
            `weightlocal` DOUBLE NULL DEFAULT NULL,
            PRIMARY KEY (`date`, `btk`, `bti`),
            INDEX `bui` (`bui`),
            INDEX `countryissue` (`countryissue`),
            INDEX `bti` (`bti`),
            INDEX `weight` (`weight`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM
        ROW_FORMAT=DEFAULT
    ")
}
delrdata <-
function(bui) {
    stopifnot(any(bui%in%union(aabuidone(what="aats"),aabuidone(what="aared"))))
    for(i in seq_along(bui)) {
        suppressWarnings(shell(paste("del ",psz(psz(aadir(),bui[i],".rdata"),collapse=", "))))
        suppressWarnings(shell(paste("del ",psz(psz(aasnapdir(),bui[i],".rdata"),collapse=", "))))
    }
}
aafinish <-
function(t1="1996-01-01",safe=FALSE)
{
    mysqlQuery("DROP TABLE IF EXISTS aatslong")
    mysqlQuery("CREATE TABLE aatslong AS SELECT * FROM aats")
    addidx("aatslong","date")
    addidx("aatslong","bui")
    addidx("aatslong",c("date","bui"))
    if(!safe) mysqlQuery(psz("DELETE FROM aats WHERE date<'",t1,"'"))
    addidx("aats",c("date","bui"))
    addidx("aats","date")
    addidx("aats","bui")
}
splitchk <-
function() {
    return(TRUE) #because bui can go missing, eg in the re-download done 2011-11-22
    stopifnot(all(c("aats","aared","qu")%in%sort(dirtab())))
    meds <- ixxtods(ixx=as.numeric(extract("qu","DISTINCT isu")),xx="su")
    mebui <- as.character(extract("vesu","DISTINCT bui",psz("WHERE datestamp IN (",flds(meds),")"),channel=chve()))
    aatsbui  <- extract("aats","DISTINCT bui")
    aaredbui  <- extract("aats","DISTINCT bui")
    res <- all(mebui%in%aatsbui)
    res <- res & all(mebui%in%aaredbui)
    res
}
delmefast <-
function(
            ime=getive("me"),
            si=TRUE,        #flag deletion of pars
            ruthless=FALSE,  #flag deletion of all intermediates
            inter=list(ce=ruthless,re=ruthless,co=ruthless,po=ruthless), #flag deletion of selected intermediates
            out=list(lo=ruthless,ga=ruthless,ti=ruthless) #flag deletion of selected outputs
            ) {
    resve(xx="me",ixx=ime)
    if(si) {
        ixx <- as.numeric(extract("me","DISTINCT isi"))
        if(any(ixx%in%getive(xx="si",how="all"))) { delve(xx='si',ixx=ixx) }
    }
    for(i in seq_along(inter)) {
        if(inter[[i]]) {
            ixx <- unique(mexx(ime=ime,xx=names(inter)[i]))
            if(any(ixx%in%getive(xx=names(inter)[i],how="all")))  { delve(xx=names(inter)[i],ixx=ixx) }
        }
    }
    for(i in seq_along(out)) {
        if(out[[i]]) {
            ixx <- unique(mexx(ime=ime,xx=names(out)[i]))
            if(any(ixx%in%getive(xx=names(out)[i],how="all")))  { delve(xx=names(out)[i],ixx=ixx) }
        }
    }
    deljo(ijo=intersect(as.numeric(extract("me","DISTINCT ijo")),getijo("all")),depend=FALSE)
}
delsi <-
function(mytab="ca",parameter="end",from=c("si","vesi","vvsi"))
{
    from <- match.arg(from)
    stopifnot(is.character(mytab) && length(mytab)==1)
    stopifnot(is.character(parameter) && length(parameter)==1)
    from <- match.arg(from)
    myquery <- psz("DELETE FROM ",from," WHERE mytable=",flds(mytab)," AND parameter=",flds(parameter))
    mysqlQuery(myquery)
    myquery
}
pometrics <-
function(da=thiswed("%Y-%m-%d",0),ime=aame(),type=c("alpha","neutral","lever","trade","nettrade"),su=c("isu","comment")) {
    type <- match.arg(type)
    su <- match.arg(su)
    resve(xx="me",ixx=ime)
    ds <- ixxtods(ixx=mexx(xx="po",ime=ime,group="neutral"),xx="po")
    metrics <- mysqlQuery(psz("
                SELECT ",ifelse(su=="comment","vesudi.comment","isu"),", a.*
                FROM vesudi, 
                    (SELECT isu, ili, alpha, lever, neutral, trade, nettrade
                    FROM(
                        SELECT datestamp, date, max(alpha) as alpha, max(lever) as lever, max(neutral) as neutral, sum(abs(xt)) as trade, sum(xt) as nettrade
                        FROM vepo
                        WHERE datestamp IN (",flds(ds),") AND date=",flds(da)," group by datestamp
                        ) AS a, vepodi, me
                    WHERE a.datestamp=vepodi.datestamp AND vepodi.ixx=me.ipo AND me.method='neutral') AS a
                WHERE vesudi.ixx=a.isu
                "))
    mat <- tabtomat(metrics[,c("ili",su,type)])
    mat
}
cometrics <-
function(da=thiswed("%Y-%m-%d",0),ime=aame(),type=c("alpha","lever","neutral"),su=c("isu","comment")) {
    type <- match.arg(type)
    su <- match.arg(su)
    resve(xx="me",ixx=ime)
    ds <- ixxtods(xx="co",ixx=jolu(xx="co",ijo=mexx(xx="jo",ime=ime,group="neutral")))
    metrics <- mysqlQuery(psz("
                SELECT ",ifelse(su=="comment","vesudi.comment","isu"),", a.*
                FROM vesudi, 
                    (SELECT jo.isu, jo.ili, value
                    FROM(
                        SELECT datestamp, date, max(value) AS value
                        FROM veco
                        WHERE datestamp IN (",flds(ds),") AND date=",flds(da)," AND control=",flds(type)," AND apply='min' group by datestamp
                        ) AS a, vecodi, me, jo
                    WHERE a.datestamp=vecodi.datestamp AND vecodi.ixx=jo.ico AND me.method='neutral' AND me.ijo=jo.ijo) AS a
                WHERE vesudi.ixx=a.isu
                "))
    mat <- tabtomat(metrics[,c("ili",su,"value")])
    mat
}
decru <-
function() 
{
    dec <- rbind(
        c(  "date"      ,   "DATE",     "NOT NULL"              ),
        c(  "indu"      ,   "CHAR(60)", "NOT NULL"          ),
        c(  "const"     ,   "DOUBLE",   " NULL"            ),
        c(  "value"     ,   "DOUBLE",   " NULL"            ),
        c(  "rsq"       ,   "DOUBLE",   " NULL"            ),
        c(  "nobs"      ,   "DOUBLE",   "NOT NULL"            ),
        c(  "type"      ,   "CHAR(11)", "NOT NULL"            )        
        )
    #dec <- cbind(dec,c("NOT NULL")
    colnames(dec) <- c("xfield","xtype","xnull")
    dec
}
potofoce <-
function(
                inp="prem",
                out="foco",
                verbose=TRUE
                ) {
    stopifnot(length(inp)==1 && inp%in%dirfld())
    stopifnot(length(out)==1)
    dace <- getda("ce")
    po <- getpa(inp)
    res <- list(m=po*NA,s=po*NA,r=po*NA,t=po*NA)
    for(i in seq_along(dace)) {
        if(verbose) print(dace[i])
        ce <- getce(dace[i])
        ida <- match(dace[i],rownames(po))
        bui <- colnames(po)[which(colnames(po)%in%buice(ce) & !is.na(po[ida,,drop=FALSE]))]
        x <- ldgce(ce)[bui,,drop=FALSE]
        vv <- vcvce(ce,po=bui)$T
        h <- hce(ce,bui)
        stopifnot(all(colnames(vv)==colnames(po[ida,bui])))
        a <- vv%*%t(po[ida,bui])
        res[["t"]][ida,bui] <- a
        res[["m"]][ida,bui] <- x[,1,drop=FALSE]%*%t(h[,1,drop=FALSE])%*%a
        res[["s"]][ida,bui] <- x[,-1,drop=FALSE]%*%t(h[,-1,drop=FALSE])%*%a
        res[["r"]][ida,bui] <- (diag(length(bui))- x%*%t(h))%*%a
    }
    addpa(psz(out,names(res)))
    for(j in seq_along(names(res))) putpa(res[[j]],field=psz(out,names(res)[j]))
}
poneutce <-
function(
                inp="prem",
                out="mone"
                )
{
    delfld(tab="pa",field="temp")
    addpa(field="temp")
    exprpa(field="temp",expr=inp)
    mysqlQuery(psz("UPDATE pa SET temp=NULL WHERE tasu=0 AND date IN (",fldlst(getda("ce")[-1],"'","'"),")"))
    pococe0(inp="temp",out="temp",unitgross=FALSE)
    addpa(field=out)
    exprpa(field=out,expr="tempt-tempm")
    delfld("pa",c("temp","tempm","temps","tempr","tempt"))
}
invce <-
function(ce,bui=buice(ce))
{
    k <- ncol(ce$loadings)
    n <- length(bui)
    delta <- spvce(ce)[bui,,drop=FALSE]**2
    x <- ldgce(ce)[bui,,drop=FALSE]
    x1 <- sweep(x,MAR=1,FUN="/",STAT=delta)
    x2 <- solve(t(x)%*%x1+diag(k))  #k x k
    x3 <- diag(n) - x%*%x2%*%t(x1)  #n x n
    x4 <- sweep(x3,MAR=1,FUN="/",STAT=delta)
    x4
}
hce <-
function(ce,bui=buice(ce))
{
    stopifnot(all(bui%in%buice(ce)) && length(bui)>ncol(ce$loadings))
    x <- ldgce(ce)[bui,,drop=FALSE]
    delta <- spvce(ce)[bui,,drop=FALSE]**2
    x1 <- sweep(x,MAR=1,FUN="/",STAT=delta)
    x2 <- sweep(x,MAR=1,FUN="/",STAT=delta**.5)
    t(solve(crossprod(x2))%*%t(x1))
}
fotopoqpce <-
function(
                inp="prem",     #forecast fo
                out="poqp",   #position
                ttyp=c("gross","vol"), #target type
                tgt=1,          #target value
                jcon=1,    #factors to target
                pomaxval=NA,
                ngroups=NA,
                verbose=TRUE
                ) {
    stopifnot(length(inp)==1 && inp%in%dirfld())
    stopifnot(length(out)==1)
    ttyp <- match.arg(ttyp)
    k <- getsione("nfac")
    dace <- getda("ce")
    fo <- getpa(inp)
    res <- fo*NA
    for(i in seq_along(dace)) {
        if(verbose) print(dace[i])
        ce <- getce(dace[i])
        ida <- match(dace[i],rownames(fo))
        bui <- colnames(fo)[which(colnames(fo)%in%buice(ce) & !is.na(fo[ida,,drop=FALSE]))]
        sol <- con.solve.QP(ce=ce,fo=fo[ida,bui],jcon=jcon,pomaxval=pomaxval,ngroups=ngroups,tgt=tgt,ttyp=ttyp,tol=.1,vcomp="T",dirallowed=-1,noshortco=NA)  
        res[ida,bui] <- sol
    }
    addpa(out)
    putpa(res,field=out)
}
fotopoce <-
function(
                inp="prem",
                out="po",
                unitgross=TRUE,
                verbose=TRUE
                ) {
    stopifnot(length(inp)==1 && inp%in%dirfld())
    stopifnot(length(out)==1)
    dace <- getda("ce")
    fo <- getpa(inp)
    res <- fo*NA
    for(i in seq_along(dace)) {
        if(verbose) print(dace[i])
        ce <- getce(dace[i])
        ida <- match(dace[i],rownames(fo))
        bui <- colnames(fo)[which(colnames(fo)%in%buice(ce) & !is.na(fo[ida,,drop=FALSE]))]
        vvi <- invce(ce,bui)
        stopifnot(all(colnames(vvi)==colnames(fo[ida,bui])))
        sol <- vvi%*%t(fo[ida,bui])
        res[ida,bui] <- sol
        if(unitgross) res[ida,bui] <- res[ida,bui]/sum(abs(res[ida,bui]))
    }
    addpa(out)
    putpa(res,field=out)
}
fococe <-
function(
                inp="prem", #forecast fo
                out="foco",  #forecast components msrt
                verbose=TRUE,
                tauc=c("-1","0") #-1 = in-sample
                ) {
    mylag <- -1*as.numeric(match.arg(tauc))
    stopifnot(length(inp)==1 && inp%in%dirfld())
    stopifnot(length(out)==1)
    offdace <- intersect(offda(getda("ce"),-1*mylag),getda("ce"))
    fo <- getpa(inp)
    res <- list(m=fo*NA,s=fo*NA,r=fo*NA,t=fo*NA)
    for(i in seq_along(offdace)) {
        if(verbose) print(offdace[i])
        ce <- getce(offda(offdace[i],mylag))
        ida <- match(offdace[i],rownames(fo))
        bui <- colnames(fo)[which(colnames(fo)%in%buice(ce) & !is.na(fo[ida,,drop=FALSE]))]
        x <- ldgce(x=ce)[bui,,drop=FALSE]
        h <- hce(ce=ce,bui=bui)
        res[["t"]][ida,bui] <- fo[ida,bui]
        res[["m"]][ida,bui] <- x[,1,drop=FALSE]%*%t(h[,1,drop=FALSE])%*%t(fo[ida,bui])
        res[["s"]][ida,bui] <- x[,-1,drop=FALSE]%*%t(h[,-1,drop=FALSE])%*%t(fo[ida,bui])
        res[["r"]][ida,bui] <- (diag(length(bui))-x%*%t(h))%*%t(fo[ida,bui])
    }
    stopifnot(max(abs(res[["t"]]-(res[["m"]]+res[["s"]]+res[["r"]])),na.rm=TRUE)<1.e-10)
    addpa(psz(out,names(res)))
    for(j in seq_along(names(res))) putpa(res[[j]],field=psz(out,names(res)[j]))
}
eqce <-
function(ce1,ce2) 
{
    stopifnot(class(ce1)=="ce")
    stopifnot(class(ce2)=="ce")
    res <- TRUE
    for(i in c("loadings","fmp","uniqueness","sdev","method","full")) {
    res <- res && all.equal(ce1[[i]],ce2[[i]]) }
    res
}
copyce <-
function(ext="global",restore=FALSE)
{
    copynam <- psz("ce",ext)
    if(restore) {
        stopifnot(copynam%in%dirtab())
        droptab("ce")
        mysqlQuery(psz("CREATE TABLE ce AS SELECT * FROM ",copynam))
        idxce()
    } else {
        stopifnot(!(copynam%in%dirtab()))
        mysqlQuery(psz("CREATE TABLE ",copynam," AS SELECT * FROM ce"))
    }
}
test.rollavpa <-
function()
{
    x <- zoo(c(rep(0,49),50,-49,48,-47),1:53)
    xx <- rollapply(data=x, 
                    width=50, 
                    FUN=meantri,
                    minweight=1/50,
                    ascending = TRUE, 
                    fill=NA, 
                    align = "right"
                    )
    abs(xx[length(xx)])<1.e-10 && abs(xx[length(xx)-2])<1.e-10
    all.equal(as.numeric(xx[length(xx)-1]-(48*50-49*49+50*48)/(51*50/2)) , 0)
}
rollavpa <-
function(
                    ipa="prem",
                    nper=50,
                    type=c("mean","ir"),
                    minweight=1/3
                    )
{
    stopifnot(0<nper)
    x <- getpa(ipa)
    type <- match.arg(type)
    myargs <- list(data=x, 
                width=nper, 
                FUN=meantri,
                minweight=minweight,
                ascending = TRUE, 
                fill=NA, 
                align = "right"
                )
        #do.call(what="rollapply",args=myargs) #removed spurious line 2012-01-03
    if(type=="mean") {
        do.call(what="rollapply",args=myargs)
    } else {
        do.call(what="rollapply",args=myargs)/
                (do.call(what="rollapply",args=c(myargs,pow=2))**.5)
    }
}
newsu <-
function() 
{
    droptab("su")
    mysqlQuery("
        CREATE TABLE `su` (
            `bui` CHAR(18) NOT NULL,
            `date` DATE NOT NULL,
            INDEX `bui` (`bui`),
            INDEX `date` (`date`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM;
    ")
}
deccu <-
function() 
{
    structure(
        c("date", "btk", "weight", "bui", "countryfull", "countryissue","bti", "btklocal", "weightlocal", "big", "bigish", 
        "CHAR(10)","CHAR(17)", "DOUBLE", "CHAR(18)", "CHAR(100)", "CHAR(6)", "CHAR(17)","CHAR(17)", "DOUBLE", "INT(1)", "INT(1)", 
        "NOT NULL", "NOT NULL", "NULL", "NULL", "NULL", "NULL", "NOT NULL", "NULL", "NULL", "NULL", "NOT NULL"), 
        .Dim = c(11L, 3L), 
        .Dimnames = list(NULL, c("xfield", "xtype", "xnull"))
    )
}
cuxstsvalidbest <-
function(nper=26*5,screenedon="best")
{
    bui <- as.character(extract("cuxsts","DISTINCT bui"))
    best <- getpa(screenedon,"aats",qual=psz("WHERE bui IN (",flds(bui),")"))[,bui] #added qual 2012-10
    turn <- getpa("turn","aats",qual=psz("WHERE bui IN (",flds(bui),")"))[,bui]
    b1 <- zoo(!is.na(best),index(best))
    mode(b1) <- "numeric"
    b2 <- rollxts(b1,"sum",n=nper)
    dd <- as.Date(extract("cuxsts","DISTINCT date"))
    putpa(b2[dd,],"validbest","cuxsts")
    putpa(turn[dd,],"turn","cuxsts")
}
cuxstssu <-
function(minstocks=30) {
    myte <- sort(as.character(extract("cuxsts","DISTINCT te")))
    for(i in seq_along(myte)) {
        nte <- as.numeric(extract("cuxsts","COUNT(*)",psz("WHERE te='",myte[i],"' GROUP BY date")))
        if(minstocks<=min(nte)) {
            droptab("cu")
            mysqlQuery(psz("
                CREATE TABLE cu AS 
                    SELECT copycu.*
                    FROM cuxsts INNER JOIN copycu 
                    ON cuxsts.bui=copycu.bui AND cuxsts.idate=copycu.date 
                    WHERE cuxsts.te='",myte[i],"'"))
            stopifnot(dersu(minstocks=minstocks))
            arcve("su",myco=psz(myte[i]," ",susize()))
        }
    }
}
cuxstsscr <-
function(
                minvalidbest=2, #delete from cuxsts when this number of best not found in last 6m
                minturn=1e6,    #delete when median turn < minturn
                minperte=30     #then delete te for all dates up to the last where n(te)<minperte
                ) {
    mysqlQuery(psz("
        DELETE FROM cuxsts
        WHERE validbest<",minvalidbest
        ))
    mysqlQuery(psz("
        DELETE FROM cuxsts
        WHERE turn IS NULL OR turn<",minturn    #null condition is legacy, should now be valid unless no prior px
        ))
    cuxstsdel(minperte=minperte)
    mysqlQuery(psz("UPDATE  cuxsts SET te=CONCAT('screened',' ',te)"))
}
cuxstsqu <-
function(mycomment="") {
    br1 <- t(apply(getpa("metric/av","cuxsts"),1,quantile,pro=c(1/3,2/3),na.rm=TRUE))
    breaks <- data.frame(rownames(br1),br1)
    colnames(breaks) <- c("date","ter1","ter2")
    sqlUpdate(channel=DBcon,
            dat=data.frame(breaks),
            tablename="cuxsts",
            index=c("date"))
    stopifnot(all(extract("cuxsts","DISTINCT ter1")<1.5))
    stopifnot(all(extract("cuxsts","DISTINCT ter2")>.3))
    mysqlQuery("UPDATE  cuxsts SET qu=1 WHERE metric/av <= ter1")
    mysqlQuery("UPDATE  cuxsts SET qu=2 WHERE metric/av <= ter2 AND metric/av > ter1")
    mysqlQuery("UPDATE  cuxsts SET qu=3 WHERE metric/av >= ter2")
    mysqlQuery(psz("UPDATE  cuxsts SET te=CONCAT('",mycomment," ','q',qu)"))
    }
cuxstsme <-
function(me="momentum",category="industry_sector",mycomment="",minstocks=30) {
    mysqlQuery(psz("
        UPDATE cuxsts
        SET metric=",me,""
    ))
    mysqlQuery(psz("
        UPDATE cuxsts
        SET category=",category,""
    ))
    mysqlQuery("
        UPDATE cuxsts,  
            (
            SELECT date, category, AVG(metric) AS av
            FROM cuxsts
            GROUP BY date, category
            ) AS a
        SET cuxsts.av = a.av
        WHERE cuxsts.category=a.category AND cuxsts.date=a.date
    ")
}
cuxstsdel <-
function(minperte=30) {
    mysqlQuery(psz("
        UPDATE cuxsts, 
            (SELECT te, MAX(idate) AS idate
            FROM
                (SELECT te, idate, COUNT(*) AS count
                FROM cuxsts
                GROUP BY te, idate) AS a
            WHERE count<30
            GROUP BY te) AS b
        SET cuxsts.bestidate=b.idate
        WHERE cuxsts.te=b.te
        "))
    mysqlQuery(psz("
        DELETE FROM cuxsts
        WHERE idate<=bestidate
        "))
}
vvfnam <-
function(
                mydir=vvdir(),
                ime=aame()
                ) {
    fnam <- dir(mydir)
    patt1 <- ".zip"
    patt2 <- psz("^",as.character(ime))
    i <- max(intersect(grep(x=fnam,patt=patt1),grep(x=fnam,patt=patt2)))
    gsub(patt=".zip",rep="",x=fnam[i])
}
resvv <-
function(
            ime=aame(),
            mydir=vvdir(),
            fnam=vvfnam(mydir=mydir,ime=ime),
            overwrite=FALSE     #flags to overwrite existing entries for all xx in vv
            ) {
    stopifnot(length(ime)==1)
    dd <- mydir
    znam <- psz(fnam,".zip")
    stopifnot(znam %in% dir(dd))
    deltab <- ifelse(overwrite,"ve","vv") #if overwrite, delete entries in ve
    print("unzip vv dump files...")
    shell(psz("unzip -o -j ",dd,znam," -d ",dd)) #overwrite, junk directories, directory=dd
    xx <- setdiff(c(inpxx(),intxx(),outxx(),"me"),"li")
    droptab(dirtab("vv"))   #delete vv
    print("restore vv tables from dump...")
    shell(psz("mysql -uroot -p7159 bloomberg < ",dd,fnam))
    if(!(all(c("vvme","vvmedi")%in%dirtab()))) {xx <- setdiff(xx,"me")} #older vv before ime=76 have no me
    stopifnot(all(allvv(vec=TRUE,xx=xx) %in% dirtab()))
    print(psz("delete ",deltab," entries not required..."))
    for(i in seq_along(xx)) {    #delete entries in deltab which correspond to existing entries in ve
        print(xx[i])
        ixx1 <- extract(psz("vv",xx[i],"di"),"DISTINCT ixx")
        ixx2 <- extract(psz("ve",xx[i],"di"),"DISTINCT ixx")
        ixx0 <- intersect(ixx1,ixx2)
        print(ixx0)
        if(length(ixx0)>0) {
            ds <- extract(psz("vv",xx[i],"di"),"DISTINCT datestamp",psz("WHERE ixx IN (",flds(ixx0),")"))
            mysqlQuery(psz("DELETE FROM ",deltab,xx[i],"di WHERE datestamp IN (",flds(ds),")"))
            mysqlQuery(psz("DELETE FROM ",deltab,xx[i]," WHERE datestamp IN (",flds(ds),")"))
        }
    }
    print("delete vvsi rows not required...")
    delsi(mytab="ca",parameter="end",from="vvsi") #un-needed parameter, use default
    print("copy vv rows back to ve...")
    for(i in seq_along(xx)) {    #copy back
        print(xx[i])
        vflds1 <- paste(dirfld(psz("vv",xx[i],"di")),collapse=",")
        q1 <- psz("INSERT INTO ve",xx[i],"di (",vflds1,")SELECT ",vflds1," FROM vv",xx[i],"di")
        mysqlQuery(q1)
        vflds2 <- paste(dirfld(psz("vv",xx[i])),collapse=",")
        q2 <- psz("INSERT INTO ve",xx[i]," (",vflds2,")SELECT ",vflds2," FROM vv",xx[i],"")
        mysqlQuery(q2)
    }
    vflds1 <- paste(dirfld("jo"),collapse=",")
    q1 <- psz("INSERT INTO jo (",vflds1,")SELECT ",vflds1," FROM vvjo WHERE ijo NOT IN (SELECT DISTINCT ijo FROM jo)")
    mysqlQuery(q1)
    #mysqlQuery("INSERT INTO jo SELECT * FROM vvjo WHERE ijo NOT IN (SELECT DISTINCT ijo FROM jo)")
    stopifnot(chkjo())
    vv <- dirtab("vv")
    #droptab(vv)
}
arcvvjo <-
function(
            ijo=13483:13489,
            dumpnam=psz("ijo-",min(ijo),"-",max(ijo)),
            mydir=vvdir(),    #working directory for dump files
            docopyrow=TRUE,     #copy rows to vvxx (overwrite existing vvxx)
            dodump=TRUE,        #dump vvxx (overwrite existing dumps)
            dozip=TRUE,         #move vvxx (overwrite existing zip)
            dodelete=FALSE,      #delete rows from xx, including any orphaned inputs
            doinp=TRUE,         #operate on inputs
            doint=TRUE,         #operate on intermediates
            protectlastentry=TRUE #do not delete the final entry in any archive, to protect ixx from re-use
            ) {
    stopifnot(identical(namecon(),"bloomberg")) #must be run with DBcon pointing to archives
    stopifnot(!dodelete || (doinp & doint & dodump & dozip)) #to delete, require a full unload
        if(!all(ijo%in%getijo("all"))) return(psz("jobs missing "))
        droptab("vvjo")
        mysqlQuery(psz("CREATE TABLE vvjo AS SELECT * FROM jo WHERE ijo IN (",flds(ijo),") ORDER BY ijo"))
        xx <- union(outxx(),c("ga","ti"))
        if(doinp) {xx <- union(xx,inpxx())}
        if(doint) {xx <- union(xx,intxx())}
        xx <- setdiff(xx,"li")
        if(docopyrow) {
            decvv()
            for(i in seq_along(xx)) {
                print(psz("copy out ",xx[i]))
                if(xx[i]=="me") {
                    ixx <- ime
                } else {
                    ixx <- sort(unique(as.numeric(extract("vvjo",psz("i",xx[i])))))
                }
                mysqlQuery(psz("DROP TABLE IF EXISTS vv",xx[i]))
                mysqlQuery(psz("DROP TABLE IF EXISTS vv",xx[i],"di"))
                if(length(ixx)>0) {
                    ds <- ixxtods(xx=xx[i],i=ixx)
                    mysqlQuery(psz("CREATE TABLE vv",xx[i]," AS SELECT * FROM ve",xx[i]," WHERE datestamp IN (",flds(ds),")"))
                    mysqlQuery(psz("CREATE TABLE vv",xx[i],"di AS SELECT * FROM ve",xx[i],"di WHERE datestamp IN (",flds(ds),")"))
                } else {
                    mysqlQuery(psz("CREATE TABLE vv",xx[i]," AS SELECT * FROM ve",xx[i]," LIMIT 0"))
                    mysqlQuery(psz("CREATE TABLE vv",xx[i],"di AS SELECT * FROM ve",xx[i],"di LIMIT 0"))
                }
            }            
        }
        if(dodump) {
            fnam <- psz(mydir,dumpnam,"-",thiswed())
            print("dump...")
            shell(psz("mysqldump -uroot -p7159 bloomberg ",allvv()," > ",fnam))
        }
        if(dozip) {
            print("zip...")
            fnam <- psz(mydir,dumpnam,"-",thiswed())
            shell(psz("del ", gsub(fnam,patt="/",rep="\\\\"),".zip ")) #first delete zip
            shell(psz("zip -m -D -j -1 ",fnam,".zip ",fnam)) #move into zipfile (ie delete file)
        }
        if(dodelete) {
            print("delete jobs...")
            ijo1 <- mexx(ime=ime[j],group="allexcept")
            deljo(ijo=ijo1,deletefreeinputs=TRUE,protectlastentry=protectlastentry)
            #for(i in seq_along(c(intxx(),outxx()))) {  #this is not a good idea as v slow - just do it in cleanve
            #    mysqlQuery(psz("OPTIMIZE TABLE ve",xx[i])) #release rows
            #}
        }
        #droptab(strsplit(allvv(),split=" ")[[1]])
    
}
aadb <-
function() {
  onames <- names(odbcDataSources())
  onames[grep("^aa.$",onames)]
}
.onLoad <-
function(libname,pkgname) {
  options(warn=1)
  options("stringsAsFactors"=FALSE)
  require(RODBC)
  bloomberg <<- DBcon <<- odbcConnect("bloomberg")
  aa <- aadb()
  for(i in seq_along(aa)) {
    assign(aa[i],value=odbcConnect(aa[i]),env=globalenv())
  }
}
